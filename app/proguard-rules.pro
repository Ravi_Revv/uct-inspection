# Add project specific ProGuard rules here.
# You can control the set of applied configuration files using the
# proguardFiles setting in build.gradle.
#
# For more details, see
#   http://developer.android.com/guide/developing/tools/proguard.html

# If your project uses WebView with JS, uncomment the following
# and specify the fully qualified class name to the JavaScript interface
# class:
#-keepclassmembers class fqcn.of.javascript.interface.for.webview {
#   public *;
#}

# Uncomment this to preserve the line number information for
# debugging stack traces.
#-keepattributes SourceFile,LineNumberTable

# If you keep the line number information, uncomment this to
# hide the original source file name.
#-renamesourcefileattribute SourceFile

# Add project specific ProGuard rules here.
# You can control the set of applied configuration files using the
# proguardFiles setting in build.gradle.
#
# For more details, see
#   http://developer.android.com/guide/developing/tools/proguard.html

# If your project uses WebView with JS, uncomment the following
# and specify the fully qualified class name to the JavaScript interface
# class:
#-keepclassmembers class fqcn.of.javascript.interface.for.webview {
#   public *;
#}

# Uncomment this to preserve the line number information for
# debugging stack traces.
#-keepattributes SourceFile,LineNumberTable

# If you keep the line number information, uncomment this to
# hide the original source file name.
#-renamesourcefileattribute SourceFile

-optimizationpasses 5
-dontusemixedcaseclassnames
-dontskipnonpubliclibraryclasses
-dontskipnonpubliclibraryclassmembers
-dontpreverify
-verbose
#-dump class_files.txt
-printseeds seeds.txt
-printusage unused.txt
-printmapping mapping.txt
-optimizations !code/simplification/arithmetic,!field/*,!class/merging/*
-keepattributes Signature
-keepattributes SourceFile,LineNumberTable
-keepattributes Exceptions, Signature, InnerClasses
-keepattributes *Annotation*

-keep public class * extends android.app.Application
-keep public class * extends android.app.Service
-keep public class * extends android.content.BroadcastReceiver
-keep public class * extends android.content.ContentProvider
-keep public class * extends android.app.Application
-keep public class * extends com.uct.inspection.core.base.BaseActivity
-keep public class * extends com.uct.inspection.core.base.BaseCoreActivity
-keep public class * extends androidx.appcompat.app.AppCompatActivity
-keep public class * extends com.uct.inspection.core.base.BaseFragment
-keep public class * extends com.uct.inspection.core.base.BaseCoreFragment
-keep public class * extends androidx.fragment.app.Fragment

-keep class com.uct.inspection.MyApplication


-keep class com.uct.inspection.aws.** { *; }
-keep class com.uct.inspection.camera.** { *; }
-keep class com.uct.inspection.db.** { *; }
-keep class com.uct.inspection.core.** { *; }
-keep class com.uct.inspection.interfaces.** { *; }
-keep class com.uct.inspection.preference.** { *; }
-keep class com.uct.inspection.retrofit.** { *; }
-keep class com.uct.inspection.model.** { *; }
-keep class com.uct.inspection.view.** { *; }
-keep class com.uct.inspection.viewmodel.** { *; }

-keep class * implements android.os.Parcelable {
  public static final android.os.Parcelable$Creator *;
}
# Gson specific classes
#-keep class sun.misc.Unsafe { *; }
-keep class com.google.gson.stream.** { *; }

-keep class com.squareup.okhttp3.** {
*;
}
-keep class okhttp3.** { *; }
-keep interface okhttp3.** { *; }
-dontwarn okhttp3.**
-dontwarn okio.**

#end of event bus
#for google
-keep public class com.google.android.gms.* { public *; }
-dontwarn com.google.android.gms.**


-dontwarn com.google.firebase.**
#for carousel
#-keep class com.google.firebase.** { *; }


-keepattributes JavascriptInterface
-keepattributes *Annotation*


#-keep class com.squareup.picasso.** { *; }
#-keep interface com.squareup.picasso.** { *; }
-dontwarn com.squareup.okhttp.**
-dontwarn kotlinx.coroutines.flow.**
-dontwarn com.google.android.libraries.places.**



## Android architecture components: Lifecycle
# LifecycleObserver's empty constructor is considered to be unused by proguard
-keepclassmembers class * implements android.arch.lifecycle.LifecycleObserver {
    <init>(...);
}
# ViewModel's empty constructor is considered to be unused by proguard
-keepclassmembers class * extends android.arch.lifecycle.ViewModel {
    <init>(...);
}
# keep Lifecycle State and Event enums values
-keepclassmembers class android.arch.lifecycle.Lifecycle$State { *; }
-keepclassmembers class android.arch.lifecycle.Lifecycle$Event { *; }
# keep methods annotated with @OnLifecycleEvent even if they seem to be unused
# (Mostly for LiveData.LifecycleBoundObserver.onStateChange(), but who knows)
-keepclassmembers class * {
    @android.arch.lifecycle.OnLifecycleEvent *;
}

-keepclassmembers class * implements android.arch.lifecycle.LifecycleObserver {
    <init>(...);
}

-keep class * implements android.arch.lifecycle.LifecycleObserver {
    <init>(...);
}
-keepclassmembers class android.arch.** { *; }
-keep class android.arch.** { *; }
-dontwarn android.arch.**
-keep class * implements android.arch.lifecycle.GeneratedAdapter {<init>(...);}


-keep class * extends androidx.lifecycle.ViewModel {
    <init>();
}
-keep class * extends androidx.lifecycle.AndroidViewModel {
    <init>(android.app.Application);
}

-keep class com.amazonaws.** { *; }
-keepnames class com.amazonaws.** { *; }
-dontwarn java.lang.instrument.ClassFileTransformer
