package com.uct.inspection.view.activity

import android.content.Intent
import android.view.View
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.uct.inspection.R
import com.uct.inspection.core.base.BaseActivity
import com.uct.inspection.model.inspectionlist.InspectionListData
import com.uct.inspection.utils.CommonData
import com.uct.inspection.utils.CommonUtils
import com.uct.inspection.utils.enums.InspectionCardType
import com.uct.inspection.view.adapter.InspectionFieldsAdapter
import kotlinx.android.synthetic.main.activity_inspection_detail.*


class InspectionDetailActivity : BaseActivity() {

    private var mInspectionListData: InspectionListData? = null
    private var mInspectionFieldsAdapter: InspectionFieldsAdapter? = null
    override fun getIntentValues() {
        super.getIntentValues()
        mInspectionListData = intent.getParcelableExtra("inspectionData");
    }

    override fun setLayoutResource(): Int {
        return R.layout.activity_inspection_detail
    }

    override fun setValues() {
//        mInspectionListData=CommonData.mInspectionData
        setDataView()
        if (mInspectionListData?.insertTS!! > 0) {
            mTxtStartInspection.setText("Resume inspection")
        } else {
            mTxtStartInspection.setText("Start inspection")
            /*if (mInspectionListData?.isEditLead!!) {
                mTxtStartInspection.setText("Update & confirm")
            } else {
                mTxtStartInspection.setText("Start inspection")
            }*/
        }
        if (mInspectionListData?.isEditLead!!) {
            mTxtEdit.visibility = View.VISIBLE
        } else {
            mTxtEdit.visibility = View.GONE
        }


    }


    private fun setDataView() {
        mTxtInspectionTime.setText(mInspectionListData?.inspectionTimeDisplay)
        mTxtSellerName.setText(mInspectionListData?.sellerName)
        mTxtSellerNnumber.setText(mInspectionListData?.contactNumber.toString())
        mTxtSellerAddress.setText(mInspectionListData?.sellerAddress)
        mTxtAssignBDE.setText(mInspectionListData?.assignedBDE)
        mTxtLeadId.setText(mInspectionListData?.leadID)
        mTxtMake.setText(mInspectionListData?.carMake)
        mTxtModel.setText(
            String.format(
                "%s %s",
                mInspectionListData?.carModel,
                mInspectionListData?.carVariant
            )
        )
        mTxtFuelType.setText(mInspectionListData?.carFuel)
        mTxtTransmission.setText(mInspectionListData?.carTransmission)
        mTxtDriven.setText(mInspectionListData?.kmDrivenDisplay)
        mTxtRto.setText(mInspectionListData?.rto)
        mTxtRegisteredOn.setText(mInspectionListData?.registrationTimeDisplay)
        mTxtRegNumber.setText(mInspectionListData?.carRegistrationNumber)
        mTxtInsuranceType.setText(mInspectionListData?.insuranceType)
        mTxtValidTill.setText(mInspectionListData?.insuranceValidityDisplay)

    }

    override fun setToolbarId(): View? {
        return null
    }


    override fun registerClickListener(): Array<View>? {
        return arrayOf(mImgBack, mLytCall, mLytNavigation, mImgCallBDE, mTxtStartInspection,mTxtEdit)
    }

    override fun onClick(v: View?) {
        super.onClick(v)
        when (v?.id) {
            R.id.mImgBack -> {
                finish()
            }

            R.id.mLytCall -> {
                CommonUtils.openCallDialer(mInspectionListData?.contactNumber.toString(), mContext)
            }

            R.id.mLytNavigation -> {
                CommonUtils.openNavigationMap(
                    mInspectionListData?.sellerLat!!,
                    mInspectionListData?.sellerLng!!,
                    mContext
                )
            }

            R.id.mImgCallBDE -> {
                CommonUtils.openCallDialer(mInspectionListData?.contactNumber.toString(), mContext)
            }

            R.id.mTxtStartInspection -> {
                /*if (mTxtStartInspection.text.toString().trim() == "Update & confirm") {
                    CommonData.mInspectionData = mInspectionListData
                    val intent = Intent(mContext, EditLeadInfoActivity::class.java)
                    intent.putExtra("inspectionData", mInspectionListData)
                    startActivity(intent)
                } else {
                    val intent = Intent(mContext, InspectionChecklistActivity::class.java)
                    intent.putExtra("mInspectionId", mInspectionListData?.id)
                    intent.putExtra("mInsertTS", mInspectionListData?.insertTS)
                    intent.putExtra(
                        "mCarModel",
                        mInspectionListData?.carMake + " " + mInspectionListData?.carModel
                    )
                    intent.putExtra("mRegNo", mInspectionListData?.carRegistrationNumber)
                    intent.putExtra("mInspectionType", mInspectionListData?.inspectionType)
                    intent.putExtra(
                        "mInspectionCardType",
                        InspectionCardType.INSPECTION.inspectionCardType
                    )
                    startActivity(intent)
                }*/

                val intent = Intent(mContext, InspectionChecklistActivity::class.java)
                intent.putExtra("mInspectionId", mInspectionListData?.id)
                intent.putExtra("mInsertTS", mInspectionListData?.insertTS)
                intent.putExtra(
                    "mCarModel",
                    mInspectionListData?.carMake + " " + mInspectionListData?.carModel
                )
                intent.putExtra("mRegNo", mInspectionListData?.carRegistrationNumber)
                intent.putExtra("mInspectionType", mInspectionListData?.inspectionType)
                intent.putExtra(
                    "mInspectionCardType",
                    InspectionCardType.INSPECTION.inspectionCardType
                )
                startActivity(intent)
            }

            R.id.mTxtEdit -> {
//                CommonData.mInspectionData = mInspectionListData
                val intent = Intent(mContext, EditLeadInfoActivity::class.java)
                intent.putExtra("inspectionData", mInspectionListData)
                startActivity(intent)
            }
        }

    }

}
