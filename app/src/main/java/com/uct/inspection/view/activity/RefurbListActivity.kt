package com.uct.inspection.view.activity

import android.app.Activity
import android.content.Intent
import android.provider.MediaStore
import android.util.Log
import android.view.View
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.amazonaws.services.s3.AmazonS3Client
import com.google.gson.Gson
import com.uct.inspection.R
import com.uct.inspection.aws.AwsDirectoryTask
import com.uct.inspection.aws.AwsLoginTask
import com.uct.inspection.aws.AwsSingleFileUploadTask
import com.uct.inspection.camera.CameraActivity
import com.uct.inspection.core.base.BaseActivity
import com.uct.inspection.interfaces.IAwsCallback
import com.uct.inspection.interfaces.IRefurbDocumentClickCallback
import com.uct.inspection.interfaces.IUploadDocumentCallback
import com.uct.inspection.model.inspectionlist.InspectionListData
import com.uct.inspection.model.refurb.JobListData
import com.uct.inspection.model.refurb.RefurbDocument
import com.uct.inspection.model.refurb.RefurbList
import com.uct.inspection.utils.*
import com.uct.inspection.utils.enums.InspectionCardType
import com.uct.inspection.view.adapter.JobListAdapter
import com.uct.inspection.view.adapter.RefurbDocumentAdapter
import com.uct.inspection.view.fragment.UploadDocumentBottomFragment
import com.uct.inspection.viewmodel.RefurbJobViewModel
import kotlinx.android.synthetic.main.activity_refurb_list.*
import java.io.File


class RefurbListActivity : BaseActivity(), IUploadDocumentCallback, IAwsCallback {


    private var mJobListAdapter: JobListAdapter? = null
    private var mRefurbDocumentAdapter: RefurbDocumentAdapter? = null
    private var mSelectedPosition: Int = 0
    private var mSelectedFile: String? = null
    private var mInspectionListData: InspectionListData? = null
    private var mRefurbJobViewModelViewModel: RefurbJobViewModel? = null
    private var mRefurbList: RefurbList? = RefurbList()
    private var jobList: ArrayList<JobListData>? = null
    private var refurbDocument: ArrayList<RefurbDocument>? = null

    override fun getIntentValues() {
        super.getIntentValues()
        mInspectionListData = intent.getParcelableExtra("inspectionData");
    }

    override fun setLayoutResource(): Int {
        return R.layout.activity_refurb_list
    }

    override fun setValues() {
        /*val docStr = CommonUtils.loadJSONFromAsset(mContext, "job_list.json")
        val refurb =
            Gson().fromJson<RefurbList>(
                docStr,
                RefurbList::class.java
            ) as RefurbList*/



        CommonData.amazonS3Client = null
        try {
            val inspectionId = mInspectionListData?.id
            val createDir = File(Constants.DIRECTORY, "UCT-Inspection/Images/$inspectionId")

            if (!createDir.exists()) {
                createDir.mkdirs()

                val noMediaFile = File(createDir, ".nomedia")
                if (!noMediaFile.exists()) {
                    noMediaFile.createNewFile()
                }
            }

        } catch (e: Exception) {
            e.printStackTrace()
        }
        prepareRefurbList()
        prepareDocumentList()
        setUpObserver()


    }

    private fun setDataView() {
        mTxtCarModel.setText(mRefurbList?.refurbListData!![0].carModel)
        mTxtCarRegNo.setText(mRefurbList?.refurbListData!![0].regNo)
        mTxtJobCount.setText(mRefurbList?.refurbListData!![0].jobsCount)
        this.jobList?.clear()
        this.jobList?.addAll(mRefurbList?.refurbListData!![0].jobListData)
        this.refurbDocument?.clear()
        this.refurbDocument?.addAll(mRefurbList?.refurbListData!![0].documentList)
        this.mJobListAdapter?.notifyDataSetChanged()
        this.mRefurbDocumentAdapter?.notifyDataSetChanged()
        /*if (refurbListData?.jobCard != null && refurbListData?.jobCard.size > 0) {
            mBtnSubmit.setText("Submit")
            mLytJobCard.visibility = View.VISIBLE
            if (refurbListData?.jobCard[0].contains("https")) {
                Picasso.get().load(refurbListData?.jobCard[0])
                    .fit().centerCrop().placeholder(R.drawable.ic_car_img_placeholder)
                    .error(R.drawable.ic_car_img_placeholder).into(mImgJobCard)
            } else {
                Picasso.get()
                    .load(File(refurbListData?.jobCard[0]))
                    .fit().centerCrop().placeholder(R.drawable.ic_car_img_placeholder)
                    .error(R.drawable.ic_car_img_placeholder).into(mImgJobCard)
            }
        } else {
            mBtnSubmit.setText("Upload job card")
            mLytJobCard.visibility = View.GONE
        }*/


    }


    private fun setUpObserver() {
        mRefurbJobViewModelViewModel = RefurbJobViewModel(mContext)
        mRefurbJobViewModelViewModel?.refurbListData?.observe(
            this,
            androidx.lifecycle.Observer {
                this.mRefurbList = it
                setDataView()
            })


        mRefurbJobViewModelViewModel?.getRefurbJobList(mInspectionListData?.id!!)


        mRefurbJobViewModelViewModel?.isRefurbDocumentSaveLiveData?.observe(
            this,
            androidx.lifecycle.Observer {
                if (it) {
                    CommonDialog.With(mActivity)
                        .showDataValidationFailureDialog("Thank You",
                            "You have successfully submitted document data",
                            object : CommonDialog.IDataValidationFailureDialogClickCallback {

                                override fun onOkClick() {
                                    startInspectionListActivity()
                                }

                            })
                }
            })
    }

    private fun startInspectionListActivity() {
        val bdIntent = Intent(mContext, InspectionListActivity::class.java)
        bdIntent.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
        startActivity(bdIntent)
    }

    private fun prepareRefurbList() {
        jobList = ArrayList()
        mJobListAdapter =
            JobListAdapter(mContext, jobList)
        mRcJobs.setHasFixedSize(true)
        mRcJobs.layoutManager =
            LinearLayoutManager(mContext, LinearLayoutManager.VERTICAL, false)
        mRcJobs.itemAnimator = DefaultItemAnimator()
        mRcJobs.adapter = mJobListAdapter
//        ViewCompat.setNestedScrollingEnabled(mRcDocument, false)
    }

    private fun prepareDocumentList() {
        refurbDocument = ArrayList()
        mRefurbDocumentAdapter = RefurbDocumentAdapter(
            mContext,
            refurbDocument
        )

        mRefurbDocumentAdapter?.setRefurbDocumentClickCallback(object :
            IRefurbDocumentClickCallback {
            override fun onRefurbDocumentAddImageClick(position: Int) {
                mSelectedPosition = position
                showUploadDocumentBottomFragment(
                    position,
                    mRefurbList?.refurbListData!![0].documentList.get(position).documentName

                )
            }

            override fun onRefurbDocumentDeleteImageClick(mParentPosition: Int, position: Int) {
                mRefurbList?.refurbListData!![0].documentList.get(mParentPosition)
                    .documentList.removeAt(position)
                mRefurbDocumentAdapter?.notifyItemChanged(mParentPosition)

            }

            override fun onRefurbDocumentImageClick(mParentPosition: Int, position: Int) {
                val intent = Intent(mContext, ImageZoomHelperActivity::class.java)
                intent.putExtra(
                    "imagePath",
                    mRefurbList?.refurbListData!![0].documentList.get(mParentPosition).documentList.get(
                        position
                    )
                )
                intent.putExtra(
                    "mHeaderPosition",
                    position
                )
                startActivity(intent)

            }
        })
        mRcRefurbDocument.setHasFixedSize(true)
        mRcRefurbDocument.layoutManager =
            LinearLayoutManager(mActivity, LinearLayoutManager.VERTICAL, false)
        mRcRefurbDocument.itemAnimator = DefaultItemAnimator() as RecyclerView.ItemAnimator?
        mRcRefurbDocument.adapter = mRefurbDocumentAdapter
        //        ViewCompat.setNestedScrollingEnabled(recyclerView, false);
    }

    override fun setToolbarId(): View? {
        return null
    }


    override fun registerClickListener(): Array<View>? {
        return arrayOf(mImgViewBack, mBtnSubmit, mTxtEditJob)
    }

    override fun onClick(v: View?) {
        super.onClick(v)
        when (v?.id) {
            R.id.mImgViewBack -> {
                finish()
            }

            R.id.mBtnSubmit -> {
                val documentListValidation = ChecklistUtil.isValidRefurbDocumentList(refurbDocument)
                if (documentListValidation.isDataValid) {
                    Log.d("mRefurbDocumentListJson", Gson().toJson(refurbDocument))
                    mRefurbJobViewModelViewModel?.saveRefurbJobList(mInspectionListData?.id!!,refurbDocument)
                } else {
                    CommonDialog.With(mActivity)
                        .showDataValidationFailureDialog("",
                            "Please upload card for " + documentListValidation.data.documentName + ".",
                            object : CommonDialog.IDataValidationFailureDialogClickCallback {

                                override fun onOkClick() {

                                }

                            })
                }

            }
            R.id.mTxtEditJob -> {
                startInspectionChecklistActivity()
            }
        }

    }

    private fun startInspectionChecklistActivity() {
        val intent = Intent(mContext, InspectionChecklistActivity::class.java)
        intent.putExtra("mInspectionId", mRefurbList?.refurbListData!![0].inspectionID)
        intent.putExtra("mInsertTS", mRefurbList?.refurbListData!![0].insertTS)
        intent.putExtra(
            "mCarModel",
            mRefurbList?.refurbListData!![0].carModel
        )
        intent.putExtra("mRegNo", mRefurbList?.refurbListData!![0].regNo)
        intent.putExtra("mInspectionType", mRefurbList?.refurbListData!![0].inspectionType)
        intent.putExtra("mInspectionCardType", InspectionCardType.REFURBISHMENT.inspectionCardType)
        startActivityForResult(intent, Constants.INTENT_REFURB_EDIT_JOB)
    }

    private fun showUploadDocumentBottomFragment(position: Int, documentName: String) {
        val uploadDocumentBottomFragment =
            UploadDocumentBottomFragment.newInstance(this, position, documentName)
        uploadDocumentBottomFragment.show(supportFragmentManager, "")
    }

    override fun onCameraClick(position: Int) {
        val intent = Intent(mContext, CameraActivity::class.java)
        intent.putExtra(
            "mInspectionId",
            mInspectionListData?.id
        )
        intent.putExtra(
            "mHeaderPosition",
            position
        )
        intent.putExtra(
            "mFileName",
            String.format(
                "%s%s%s%s%s",
                mInspectionListData?.id,
                "_",
                CommonUtils.removeSpace(mRefurbList?.refurbListData!![0].documentList[position].documentName),
                "_",
                System.currentTimeMillis()
            )
        )

        startActivityForResult(intent, Constants.INTENT_CAMERA_REQUEST_CODE)
    }

    override fun onImageGalleryClick(position: Int) {
        val pickPhoto = Intent(
            Intent.ACTION_PICK,
            android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI
        )
        startActivityForResult(pickPhoto, Constants.INTENT_GALLERY_REQUEST_CODE)
    }

    override fun onCloseClick(position: Int) {
//        mDocumentList?.get(position)?.isSelected = false

    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == Constants.INTENT_CAMERA_REQUEST_CODE) {
            if (resultCode == Activity.RESULT_OK && data != null && data.hasExtra("mHeaderPosition") && data.hasExtra(
                    "mImagePath"
                )
            ) {
                val position = data.getIntExtra("mHeaderPosition", 0)
                val image = data.getStringExtra("mImagePath")
//                mInspectionChecklistData.checklistHeader[position].images.add(image)
//                mChecklistHeaderAdapter?.notifyItemChanged(position)
//                mIChecklistPageClickCallback?.onInsertDataInDb(mPosition)
                this.mSelectedFile = image
                startAwsLoginTask()
            } else {
//                mDocumentList?.get(mSelectedPosition)?.isSelected = false
//                mUploadDocumentAdapter?.notifyItemChanged(mSelectedPosition)
            }

        } else if (requestCode == Constants.INTENT_GALLERY_REQUEST_CODE) {
            if (resultCode == Activity.RESULT_OK && data != null) {
                val selectedImage = data.data
                val filePathColumn = arrayOf(MediaStore.Images.Media.DATA)
                if (selectedImage != null) {
                    val cursor = contentResolver.query(
                        selectedImage,
                        filePathColumn, null, null, null
                    )
                    if (cursor != null) {
                        cursor.moveToFirst()

                        val columnIndex = cursor.getColumnIndex(filePathColumn[0])
                        val picturePath = cursor.getString(columnIndex)
//                        imageView.setImageBitmap(BitmapFactory.decodeFile(picturePath))
//                        Toast.makeText(mContext, picturePath, Toast.LENGTH_LONG).show()
                        this.mSelectedFile = picturePath
                        startAwsLoginTask()
                        cursor.close()
                    }
                }

            } else {
//                mDocumentList?.get(mSelectedPosition)?.isSelected = false
//                mUploadDocumentAdapter?.notifyItemChanged(mSelectedPosition)
            }
        } else if (requestCode == Constants.INTENT_REFURB_EDIT_JOB) {
//            if (resultCode == Activity.RESULT_OK) {
                mRefurbJobViewModelViewModel?.getRefurbJobList(mInspectionListData?.id!!)
//            }
        }
    }


    private fun startAwsLoginTask() {
        if (CommonData.amazonS3Client == null)
            AwsLoginTask(mContext, this).execute()
        else
            onAwsDirectoryCreationSuccess(CommonData.amazonS3Client)
    }

    override fun onAwsLoginSuccess(amazonS3Client: AmazonS3Client?) {
        CommonData.amazonS3Client = amazonS3Client
        AwsDirectoryTask(
            mContext,
            this,
            mInspectionListData?.id!!,
            amazonS3Client!!
        ).execute()
    }

    override fun onAwsDirectoryCreationSuccess(amazonS3Client: AmazonS3Client?) {
        AwsSingleFileUploadTask(
            mContext,
            this,
            amazonS3Client!!,
            mSelectedFile!!,
            mInspectionListData?.id!!
        ).execute()
    }

    override fun onAwsFileUploadSuccess(uploadedFilePath: String?) {
//        mDocumentList?.get(mSelectedPosition)?.documentPath = uploadedFilePath
//        mUploadDocumentAdapter?.notifyItemChanged(mSelectedPosition)
        mRefurbList?.refurbListData!![0].documentList[mSelectedPosition].documentList.add(
            uploadedFilePath
        )
        mRefurbDocumentAdapter?.notifyItemChanged(mSelectedPosition)


    }

    override fun onAwsFileUploadSuccess(isUploaded: Boolean) {
    }
}

