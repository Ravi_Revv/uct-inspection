package com.uct.inspection.view.adapter;

import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.uct.inspection.R;
import com.uct.inspection.interfaces.IChecklistClickCallback;
import com.uct.inspection.model.checklist.ChecklistIssue;
import com.uct.inspection.model.checklist.ChecklistSubIssue;

import java.util.ArrayList;

public class ChecklistSubIssueDataAdapter extends RecyclerView.Adapter<ChecklistSubIssueDataAdapter.ViewHolder> {


    private int mParentPosition = -1;
    private IChecklistClickCallback mIChecklistClickCallback;

    public ChecklistSubIssueDataAdapter(ArrayList<ChecklistSubIssue> mCheckSubIssues) {
        this.mCheckSubIssues = mCheckSubIssues;
    }

    public void setIChecklistClickCallback(IChecklistClickCallback mIChecklistClickCallback) {
        this.mIChecklistClickCallback = mIChecklistClickCallback;
    }

    private ArrayList<ChecklistSubIssue> mCheckSubIssues;


    public void setParentPosition(int mParentPosition) {
        this.mParentPosition = mParentPosition;
    }


    @NonNull
    @Override
    public ChecklistSubIssueDataAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup mParent, int viewType) {

        return new ViewHolder(LayoutInflater.from(mParent.getContext()).inflate(R.layout.item_checklist_sub_issue, mParent, false));
    }


    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.mCbChecklistIssue.setText(mCheckSubIssues.get(holder.getAdapterPosition()).getJobName());
        holder.mCbChecklistIssue.setChecked(mCheckSubIssues.get(holder.getAdapterPosition()).getSelected());
        holder.mEdtCost.setText(String.valueOf(mCheckSubIssues.get(holder.getAdapterPosition()).getCost()));
        if (holder.mCbChecklistIssue.isChecked() && mCheckSubIssues.get(holder.getAdapterPosition()).getMinCost() != 0 && mCheckSubIssues.get(holder.getAdapterPosition()).getMaxCost() != 0) {
            holder.mLytCost.setVisibility(View.VISIBLE);
        } else {
            holder.mLytCost.setVisibility(View.GONE);
        }



        /*if (holder.getAdapterPosition() == 0 || holder.getAdapterPosition() == 3) {
            holder.mCbChecklistIssue.setChecked(true);
            holder.mLytCost.setVisibility(View.VISIBLE);
//            holder.mTxtChecklistIssue.setSelected(true);
        } else {
            holder.mCbChecklistIssue.setChecked(false);
            holder.mLytCost.setVisibility(View.GONE);
//            holder.mTxtChecklistIssue.setSelected(false);
        }*/

    }


    @Override
    public int getItemCount() {
        return this.mCheckSubIssues.size();
    }


    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        LinearLayout mLytCost;
        CheckBox mCbChecklistIssue;
        EditText mEdtCost;
        TextView mTxtCostDec, mTxtCostInc;

        public ViewHolder(View v) {
            super(v);
            mLytCost = v.findViewById(R.id.mLytCost);
            mCbChecklistIssue = v.findViewById(R.id.mCbChecklistIssue);
            mEdtCost = v.findViewById(R.id.mEdtCost);
            mTxtCostInc = v.findViewById(R.id.mTxtCostInc);
            mTxtCostDec = v.findViewById(R.id.mTxtCostDec);
            mTxtCostDec.setOnClickListener(this);
            mTxtCostInc.setOnClickListener(this);

            mCbChecklistIssue.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
//                    Log.d("mCbChecklistIssue", "click");
//                    mCheckSubIssues.get(getAdapterPosition()).setSelected(isChecked);
                    if (mCheckSubIssues.get(getAdapterPosition()).getSubIssueType() == 0 && !mCheckSubIssues.get(getAdapterPosition()).getSelected()) {
                        for (ChecklistSubIssue checklistSubIssue : mCheckSubIssues) {
                            checklistSubIssue.setSelected(false);
                        }
                        mCheckSubIssues.get(getAdapterPosition()).setSelected(true);

                    } else if (mCheckSubIssues.get(getAdapterPosition()).getSubIssueType() == 0 && mCheckSubIssues.get(getAdapterPosition()).getSelected()) {
                        for (ChecklistSubIssue checklistSubIssue : mCheckSubIssues) {
                            checklistSubIssue.setSelected(false);
                        }
                        mCheckSubIssues.get(getAdapterPosition()).setSelected(false);
                    } else {
                        for (ChecklistSubIssue checklistSubIssue : mCheckSubIssues) {
                            if (checklistSubIssue.getSubIssueType() == 0 && checklistSubIssue.getSelected())
                                checklistSubIssue.setSelected(false);
                        }
                        mCheckSubIssues.get(getAdapterPosition()).setSelected(!mCheckSubIssues.get(getAdapterPosition()).getSelected());
                    }
                    notifyDataSetChanged();
                    if (mIChecklistClickCallback != null)
                        mIChecklistClickCallback.onChecklistSubIssueClick(mParentPosition);
                }
            });


            /*mCbChecklistIssue.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
//                    mCheckSubIssues.get(getAdapterPosition()).setSelected(isChecked);
                    if (mCheckSubIssues.get(getAdapterPosition()).getSubIssueType() == 0 && !mCheckSubIssues.get(getAdapterPosition()).getSelected()) {
                        for (ChecklistSubIssue checklistSubIssue : mCheckSubIssues) {
                            checklistSubIssue.setSelected(false);
                        }
                        mCheckSubIssues.get(getAdapterPosition()).setSelected(true);

                    } else if (mCheckSubIssues.get(getAdapterPosition()).getSubIssueType() == 0 && mCheckSubIssues.get(getAdapterPosition()).getSelected()) {
                        for (ChecklistSubIssue checklistSubIssue : mCheckSubIssues) {
                            checklistSubIssue.setSelected(false);
                        }
                        mCheckSubIssues.get(getAdapterPosition()).setSelected(false);
                    } else {
                        for (ChecklistSubIssue checklistSubIssue : mCheckSubIssues) {
                            if (checklistSubIssue.getSubIssueType() == 0 && checklistSubIssue.getSelected())
                                checklistSubIssue.setSelected(false);
                        }
                        mCheckSubIssues.get(getAdapterPosition()).setSelected(!mCheckSubIssues.get(getAdapterPosition()).getSelected());
                    }

                    if (mIChecklistClickCallback != null)
                        mIChecklistClickCallback.onChecklistSubIssueClick(mParentPosition);
//                    notifyDataSetChanged();


//                    mCheckSubIssues.get(getAdapterPosition()).setSelected(isChecked);
                    *//*if (isChecked) {
                        mLytCost.setVisibility(View.VISIBLE);
                    } else {
                        mLytCost.setVisibility(View.GONE);
                    }*//*
                }
            });*/

            mEdtCost.addTextChangedListener(new TextWatcher() {

                @Override
                public void afterTextChanged(Editable s) {
                }

                @Override
                public void beforeTextChanged(CharSequence s, int start,
                                              int count, int after) {
                }

                @Override
                public void onTextChanged(CharSequence s, int start,
                                          int before, int count) {
//                    if (mCheckSubIssues.get(getAdapterPosition()).getCost())
                    if (mEdtCost.getText() != null && !mEdtCost.getText().toString().trim().isEmpty() && mCheckSubIssues.get(getAdapterPosition()).getCost() != Integer.parseInt(mEdtCost.getText().toString().trim())) {
                        mCheckSubIssues.get(getAdapterPosition()).setCost(Integer.parseInt(mEdtCost.getText().toString().trim()));
                        Log.d("onTextChanged", "cost");
                        if (mIChecklistClickCallback != null)
                            mIChecklistClickCallback.onChecklistCostChange(mParentPosition);
                    } else if (mEdtCost.getText() != null && mEdtCost.getText().toString().trim().isEmpty()) {
                        mCheckSubIssues.get(getAdapterPosition()).setCost(0);
                        Log.d("onTextChanged", "comment");
                        if (mIChecklistClickCallback != null)
                            mIChecklistClickCallback.onChecklistCostChange(mParentPosition);
                    }

                }
            });
        }

        @Override
        public void onClick(View v) {
            switch (v.getId()) {
                case R.id.mTxtCostDec:
                    if (mCheckSubIssues.get(getAdapterPosition()).getCost() > mCheckSubIssues.get(getAdapterPosition()).getMinCost()) {
                        mCheckSubIssues.get(getAdapterPosition()).setCost(mCheckSubIssues.get(getAdapterPosition()).getCost() - 1);
                    }
                    notifyItemChanged(getAdapterPosition());
                    break;

                case R.id.mTxtCostInc:
                    if (mCheckSubIssues.get(getAdapterPosition()).getCost() < mCheckSubIssues.get(getAdapterPosition()).getMaxCost()) {
                        mCheckSubIssues.get(getAdapterPosition()).setCost(mCheckSubIssues.get(getAdapterPosition()).getCost() + 1);
                    }
                    notifyItemChanged(getAdapterPosition());
                    break;
            }

        }
    }

   /* public interface ICarFiltersDataClick {
        void onCarFiltersDataClick(int parentPosition, int childPosition);
    }*/
} 