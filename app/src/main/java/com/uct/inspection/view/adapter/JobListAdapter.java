package com.uct.inspection.view.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.uct.inspection.R;
import com.uct.inspection.model.refurb.JobListData;

import java.util.ArrayList;

public class JobListAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private Context mContext = null;
    private ArrayList<JobListData> mJobListData = null;

    public JobListAdapter(Context mContext, ArrayList<JobListData> jobListData) {
        this.mContext = mContext;
        this.mJobListData = jobListData;

    }


    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup mParent, int viewType) {
        return new JobListAdapterViewHolder(LayoutInflater.from(mParent.getContext()).inflate(R.layout.item_job_list, mParent, false));

    }


    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        final JobListAdapterViewHolder viewHolder = (JobListAdapterViewHolder) holder;
        viewHolder.mTxtJobName.setText(mJobListData.get(holder.getAdapterPosition()).getSubPartName());
        viewHolder.mTxtSubIssues.setText(mJobListData.get(holder.getAdapterPosition()).getJobs());
        viewHolder.mTxtIssues.setText(mJobListData.get(holder.getAdapterPosition()).getIssues());



    }


    @Override
    public int getItemCount() {
        return this.mJobListData.size();
    }


    public class JobListAdapterViewHolder extends RecyclerView.ViewHolder {

        TextView mTxtJobName, mTxtIssues, mTxtSubIssues;

        public JobListAdapterViewHolder(View v) {
            super(v);
            mTxtJobName = v.findViewById(R.id.mTxtJobName);
            mTxtIssues = v.findViewById(R.id.mTxtIssues);
            mTxtSubIssues = v.findViewById(R.id.mTxtSubIssues);
        }


    }


}