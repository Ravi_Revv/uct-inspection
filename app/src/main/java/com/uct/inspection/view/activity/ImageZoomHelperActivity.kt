package com.uct.inspection.view.activity

import android.view.View
import com.squareup.picasso.Picasso
import com.uct.inspection.R
import com.uct.inspection.core.base.BaseActivity
import kotlinx.android.synthetic.main.activity_image_zoom_helper.*
import java.io.File

class ImageZoomHelperActivity : BaseActivity() {
    private var mImagePath: String? = null

    override fun getIntentValues() {
        super.getIntentValues()
        mImagePath = intent.getStringExtra("imagePath")
    }

    override fun setLayoutResource(): Int {
        return R.layout.activity_image_zoom_helper
    }

    override fun setValues() {
        Picasso.get().load(mImagePath).fit().centerCrop()
            .placeholder(R.drawable.ic_car_img_placeholder).error(R.drawable.ic_car_img_placeholder)
            .into(mImgZoom)

        if (mImagePath!!.contains("https")) {
            Picasso.get().load(mImagePath).fit().centerCrop()
                .placeholder(R.drawable.ic_car_img_placeholder)
                .error(R.drawable.ic_car_img_placeholder)
                .into(mImgZoom)
        } else {
            Picasso.get().load(File(mImagePath!!)).fit().centerCrop()
                .placeholder(R.drawable.ic_car_img_placeholder)
                .error(R.drawable.ic_car_img_placeholder)
                .into(mImgZoom)
        }
    }

    override fun setToolbarId(): View? {
        return null
    }


    override fun registerClickListener(): Array<View>? {
        return null
    }

    override fun onClick(v: View?) {
        super.onClick(v)
        /*when (v?.id) {
            R.id.mTxt -> {
//                LoadingDialog().showLoadingDialog(mContext, "")
                Toast.makeText(mContext, "clicked me", Toast.LENGTH_LONG).show()
//                AwsLoginTask(mContext).execute()
                startActivity(Intent(mContext, CameraActivity::class.java))
            }
        }*/

    }
}
