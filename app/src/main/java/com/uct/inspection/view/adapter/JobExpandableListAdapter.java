package com.uct.inspection.view.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.lifecycle.LifecycleOwner;

import com.uct.inspection.R;
import com.uct.inspection.model.checklist.InspectionChecklist;
import com.uct.inspection.model.inspectionlist.InspectionList;
import com.uct.inspection.model.inspectionlist.InspectionListData;
import com.uct.inspection.utils.enums.InspectionCardType;

import java.util.ArrayList;
import java.util.HashMap;

public class JobExpandableListAdapter extends BaseExpandableListAdapter {

    private ArrayList<InspectionList> mInspectionList;
    private HashMap<Integer, InspectionChecklist> mInspectionCheckListMap;
    private IJobListClickCallback mIJobListClickCallback;
    private Context mContext;
    private LifecycleOwner owner;


    public JobExpandableListAdapter(Context mContext, LifecycleOwner owner, ArrayList<InspectionList> mInspectionList, HashMap<Integer, InspectionChecklist> mInspectionCheckListMap, IJobListClickCallback mIJobListClickCallback) {
        this.mInspectionList = mInspectionList;
        this.mIJobListClickCallback = mIJobListClickCallback;
        this.mContext = mContext;
        this.owner = owner;
        this.mInspectionCheckListMap = mInspectionCheckListMap;
    }

    @Override
    public int getGroupCount() {
        return mInspectionList.size();
    }

    @Override
    public int getChildrenCount(int groupPosition) {
        return (mInspectionList.get(groupPosition).getData() != null) ? mInspectionList.get(groupPosition).getData().size() : 0;
    }

    @Override
    public Object getGroup(int groupPosition) {
        return mInspectionList.get(groupPosition);

    }

    @Override
    public Object getChild(int groupPosition, int childPosition) {
        return mInspectionList.get(groupPosition).getData().get(childPosition);
    }

    @Override
    public long getGroupId(int groupPosition) {
        return groupPosition;
    }

    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return childPosition;
    }

    @Override
    public boolean hasStableIds() {
        return false;
    }

    @Override
    public View getGroupView(int groupPosition, boolean isExpanded, View convertView, ViewGroup parent) {
        if (convertView == null) {
            LayoutInflater infalInflater = (LayoutInflater) parent.getContext()
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = infalInflater.inflate(R.layout.item_job_list_parent, null);
        }

        ImageView mImgIndicator = convertView.findViewById(R.id.mImgIndicator);
        TextView mTxtInspectionTitle = convertView.findViewById(R.id.mTxtInspectionTitle);
        TextView mTxtInspectionCount = convertView.findViewById(R.id.mTxtInspectionCount);
        mTxtInspectionTitle.setText(String.format("%s %s", mInspectionList.get(groupPosition).getName(), "-"));
        mTxtInspectionCount.setText(mInspectionList.get(groupPosition).getCount());
        if (isExpanded)
            mImgIndicator.setImageResource(R.drawable.ic_upindicator_green);
        else mImgIndicator.setImageResource(R.drawable.ic_downindicator_green);


        return convertView;
    }

    @Override
    public View getChildView(final int groupPosition, final int childPosition, boolean isLastChild, View convertView, ViewGroup parent) {
        final InspectionListData inspectionData = mInspectionList.get(groupPosition).getData().get(childPosition);

        if (convertView == null) {
            LayoutInflater infalInflater = (LayoutInflater) parent.getContext()
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = infalInflater.inflate(R.layout.item_job_list_child, null);
        }
        LinearLayout mLytJobChild = convertView.findViewById(R.id.mLytJobChild);
        TextView mTxtInspectionTime = convertView.findViewById(R.id.mTxtInspectionTime);
        TextView mTxtInspectionType = convertView.findViewById(R.id.mTxtInspectionType);
        TextView mTxtCarModel = convertView.findViewById(R.id.mTxtCarModel);
        TextView mTxtInspectionId = convertView.findViewById(R.id.mTxtInspectionId);
        TextView mTxtInspectionAddress = convertView.findViewById(R.id.mTxtInspectionAddress);
        final TextView mTxtSyncOnline = convertView.findViewById(R.id.mTxtSyncOnline);

        mTxtInspectionTime.setText(inspectionData.getInspectionTimeDisplay());
        mTxtInspectionType.setText(inspectionData.getInspectionType());
        mTxtCarModel.setText(String.format("%s %s %s", inspectionData.getCarMake(), inspectionData.getCarModel(), inspectionData.getCarVariant()));
        mTxtInspectionId.setText(inspectionData.getInspectionIDDisplay());
        if (inspectionData.getCardType().equals(InspectionCardType.POST_REFURBISHMENT.getInspectionCardType())) {
            mTxtInspectionAddress.setText(inspectionData.getWorkshopAddress());
        } else {
            mTxtInspectionAddress.setText(inspectionData.getSellerAddress());
        }


        if (inspectionData.getCardType().equals(InspectionCardType.REFURBISHMENT.getInspectionCardType())) {
            mTxtSyncOnline.setText("Complete");
            if (inspectionData.getInsertTS() > 0) {
                mTxtSyncOnline.setVisibility(View.VISIBLE);
            } else {
                mTxtSyncOnline.setVisibility(View.GONE);
            }

        } else {
            mTxtSyncOnline.setText("Sync Online");
            if (mInspectionCheckListMap.containsKey(inspectionData.getId())) {
                if (mInspectionCheckListMap.get(inspectionData.getId()) != null)
                    if (mInspectionCheckListMap.get(inspectionData.getId()) != null && mInspectionCheckListMap.get(inspectionData.getId()).getInsertTS() > inspectionData.getInsertTS()) {
                        mTxtSyncOnline.setVisibility(View.VISIBLE);
                    } else {
                        mTxtSyncOnline.setVisibility(View.GONE);
                    }
            } else {
                mTxtSyncOnline.setVisibility(View.GONE);
            }
        }


        mLytJobChild.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mIJobListClickCallback != null)
                    mIJobListClickCallback.onJobListClickCallback(inspectionData);
            }
        });

        mTxtSyncOnline.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mIJobListClickCallback != null)
                    mIJobListClickCallback.onSyncOnlineClickCallback(inspectionData);
            }
        });
        return convertView;
    }

    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return false;
    }

    public interface IJobListClickCallback {
        //        void onBookingClickCallback(BookingsData bookingsData);
        void onJobListClickCallback(InspectionListData inspectionData);

        void onSyncOnlineClickCallback(InspectionListData inspectionData);


    }
}


