package com.uct.inspection.view.fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import com.uct.inspection.R
import com.uct.inspection.interfaces.IUploadDocumentCallback
import kotlinx.android.synthetic.main.fragment_bottom_upload_document.view.*

class UploadDocumentBottomFragment private constructor(
    val mIUploadDocumentCallback: IUploadDocumentCallback,
    val position: Int,
    val mDocumentName: String
) :
    BottomSheetDialogFragment() {

    companion object {
        fun newInstance(
            uploadDocumentCallback: IUploadDocumentCallback,
            position: Int,
            documentName: String
        ): UploadDocumentBottomFragment {
            return UploadDocumentBottomFragment(uploadDocumentCallback, position, documentName)
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        isCancelable = false
    }

    private lateinit var mView: View

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        mView = inflater.inflate(R.layout.fragment_bottom_upload_document, null)

        mView.mTxtTitle.setText(String.format("%s %s", "Upload", mDocumentName))
        mView.mTxtCamera.setOnClickListener {
            dismiss()
            mIUploadDocumentCallback.onCameraClick(position)
        }

        mView.mTxtGallery.setOnClickListener {
            dismiss()
            mIUploadDocumentCallback.onImageGalleryClick(position)
        }

        mView.mImgClose.setOnClickListener {
            dismiss()
            mIUploadDocumentCallback.onCloseClick(position)
        }


        return mView
    }


}