package com.uct.inspection.view.activity

import android.Manifest
import android.content.Intent
import android.text.Editable
import android.text.TextWatcher
import android.view.View
import com.uct.inspection.R
import com.uct.inspection.core.base.BaseActivity
import com.uct.inspection.utils.CommonData
import com.uct.inspection.utils.CommonUtils
import com.uct.inspection.utils.PermissionUtils
import com.uct.inspection.viewmodel.LoginViewModel
import kotlinx.android.synthetic.main.activity_login.*

class LoginActivity : BaseActivity() {
    private var mLoginViewModel: LoginViewModel? = null

    override fun setLayoutResource(): Int {
        return R.layout.activity_login
    }

    override fun setValues() {
        checkPermissions()
//        mEdtEmail.setText("akshay.singh@revv.co.in")
//        mEdtEmail.setText("akshay.singh+1@revv.co.in")
//        mEdtPassword.setText("akshay")
        setUpObserver()
        managePasswordEditText()
        manageEmailEditText()

        if (CommonData.getInspectorData(mContext) != null && CommonData.getAuthToken(mContext) != null) {
            startActivity(Intent(mContext, InspectionListActivity::class.java))
            finish()
        }
    }

    private fun checkPermissions() {
        mPermissionUtils = PermissionUtils(this@LoginActivity)
        mPermissions.add(Manifest.permission.INTERNET)
        mPermissions.add(Manifest.permission.WRITE_EXTERNAL_STORAGE)
        mPermissions.add(Manifest.permission.READ_EXTERNAL_STORAGE)
        mPermissions.add(Manifest.permission.CAMERA)
        mPermissions.add(Manifest.permission.RECORD_AUDIO)
//        mPermissions.add(Manifest.permission.ACCESS_FINE_LOCATION)
        mPermissions.add(Manifest.permission.CALL_PHONE)

//        val htmlString = "<html><body><b>Camera</b> permissions for taking pics.<br> <b>Location permissions</b> for getting exact location of DE location at hub and DE at customer's location.</br> <b>Read-write permissions</b> for creating folders and saving image in it."
        val htmlString =
            "<html><body><b>Camera</b> permissions for taking pics.<br> <b>Location permissions</b> for getting exact location of DE location at hub and DE at customer's location.</br> <b>Read-write permissions</b> for creating folders and saving image in it."

        mPermissionUtils?.check_permission(mPermissions, htmlString, 1)
    }


    private fun setUpObserver() {
        mLoginViewModel = LoginViewModel(mContext)
        mLoginViewModel?.inspectorData?.observe(this, androidx.lifecycle.Observer {
            CommonData.saveInspectorData(
                mContext,
                it
            )
            startActivity(Intent(mContext, InspectionListActivity::class.java))
            finish()
        })


    }

    override fun setToolbarId(): View? {
        return null
    }


    override fun registerClickListener(): Array<View>? {
        return arrayOf(mBtnLogin)
    }

    override fun onClick(v: View?) {
        super.onClick(v)
        when (v?.id) {
            R.id.mBtnLogin -> {
                mLoginViewModel?.login(
                    mEdtEmail.text.toString().trim(),
                    mEdtPassword.text.toString().trim()
                )
//                startActivity(Intent(mContext, InspectionListActivity::class.java))
            }
        }

    }


    private fun manageEmailEditText() {
        mEdtEmail.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {}

            override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {
                checkFieldForPasswordBtn()
            }

            override fun afterTextChanged(s: Editable) {

            }
        })


        mEdtEmail.setOnFocusChangeListener(View.OnFocusChangeListener { v, hasFocus ->
            if (!hasFocus) {
                checkFieldForPasswordBtn()
            }
        })
    }

    private fun managePasswordEditText() {
        mEdtPassword.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {}

            override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {
                checkFieldForPasswordBtn()
            }

            override fun afterTextChanged(s: Editable) {

            }
        })


        mEdtPassword.setOnFocusChangeListener(View.OnFocusChangeListener { v, hasFocus ->
            if (!hasFocus) {
                checkFieldForPasswordBtn()
            }
        })
    }


    private fun checkFieldForPasswordBtn() {
        if (!CommonUtils.isValidEmail(mEdtEmail.text.toString())) {
            mBtnLogin.isEnabled = false
        } else if (mEdtPassword.text.toString().isEmpty()) {
            mBtnLogin.isEnabled = false
        } else mBtnLogin.isEnabled = true
    }


    override fun onDestroy() {
        super.onDestroy()
        mLoginViewModel?.reset()
    }
}
