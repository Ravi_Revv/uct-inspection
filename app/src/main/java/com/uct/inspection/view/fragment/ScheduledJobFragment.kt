package com.uct.inspection.view.fragment

import android.content.Intent
import android.util.Log
import android.view.View
import android.widget.ExpandableListView
import com.uct.inspection.R
import com.uct.inspection.core.base.BaseFragment
import com.uct.inspection.model.inspectionlist.InspectionList
import com.uct.inspection.model.inspectionlist.InspectionListData
import com.uct.inspection.utils.CommonData
import com.uct.inspection.utils.LoadingBox
import com.uct.inspection.utils.enums.InspectionCardType
import com.uct.inspection.view.activity.*
import com.uct.inspection.view.adapter.JobExpandableListAdapter
import kotlinx.android.synthetic.main.fragment_scheduled_job.*

class ScheduledJobFragment(private var mInspectionList: ArrayList<InspectionList>?) :
    BaseFragment() {
    private var mSchJobExpandableListAdapter: JobExpandableListAdapter? = null

    companion object {
        fun getInstance(
            mInspectionList: ArrayList<InspectionList>?
        ): ScheduledJobFragment {
            val scheduledJobFragment =
                ScheduledJobFragment(
                    mInspectionList
                )
            return scheduledJobFragment
        }

    }


    override fun onClick(v: View?) {

    }

    override fun setLayoutResource(): Int {
        return R.layout.fragment_scheduled_job
    }

    override fun setValues() {
        setUpScheduledJobList()
    }

    override fun registerClickListener(): Array<View>? {
        return null
    }


    private fun setUpScheduledJobList() {
        mSchJobExpandableListAdapter =
            JobExpandableListAdapter(mContext,
                this,
                mInspectionList,
                (mActivity as InspectionListActivity).getAllInspectionList(),
                object : JobExpandableListAdapter.IJobListClickCallback {
                    override fun onJobListClickCallback(inspectionData: InspectionListData) {
//                        inspectionData.isShowPickupChecklist = true
                        when {
                            inspectionData.cardType == InspectionCardType.PICKUP_CHECKLIST.inspectionCardType -> {
                                val intent =
                                    Intent(mContext, InspectionUploadDocumentActivity::class.java)
                                intent.putExtra("inspectionData", inspectionData)
                                startActivity(intent)
                            }
                            inspectionData.cardType == InspectionCardType.INSPECTION.inspectionCardType -> {
                                CommonData.mInspectionData = inspectionData
                                val intent = Intent(mContext, InspectionDetailActivity::class.java)
                                intent.putExtra("inspectionData", inspectionData)
                                startActivity(intent)
                            }
                            inspectionData.cardType == InspectionCardType.REFURBISHMENT.inspectionCardType -> {
                                val intent = Intent(mContext, RefurbListActivity::class.java)
                                intent.putExtra("inspectionData", inspectionData)
                                startActivity(intent)

                            }

                            inspectionData.cardType == InspectionCardType.POST_REFURBISHMENT.inspectionCardType -> {
                                val intent =
                                    Intent(mContext, InspectionDetailE4Activity::class.java)
                                intent.putExtra("inspectionData", inspectionData)
                                startActivity(intent)


                            }
                        }

                    }

                    override fun onSyncOnlineClickCallback(inspectionData: InspectionListData) {
//                        Log.d("onJobListClickCallback", inspectionId.toString())
                        if (inspectionData.cardType == InspectionCardType.REFURBISHMENT.inspectionCardType) {
                            (mActivity as InspectionListActivity).updateRefurbStatus(
                                inspectionData,
                                "COMPLETED"
                            )
                        } else {
                            LoadingBox.showLoadingDialog(mActivity, "")
                            val map = (mActivity as InspectionListActivity).getAllInspectionList()
                            if (map != null && map.containsKey(inspectionData.id)) {
                                (mActivity as InspectionListActivity).syncOnlineData(
                                    map.get(
                                        inspectionData.id
                                    )!!
                                )
                            }
                        }

                    }

                })

        mSchJobExpandableList.setAdapter(mSchJobExpandableListAdapter)

        mSchJobExpandableList.expandGroup(0)
        mSchJobExpandableList.setOnGroupExpandListener(object :
            ExpandableListView.OnGroupExpandListener {

            // TODO Colapse Here Using this... in android
            internal var previousGroup = 0
            internal var flag = false

            override fun onGroupExpand(groupPosition: Int) {
                if (groupPosition != previousGroup) {
                    mSchJobExpandableList.collapseGroup(previousGroup)
                }
                previousGroup = groupPosition

                flag = true

            }
        })
    }


}
