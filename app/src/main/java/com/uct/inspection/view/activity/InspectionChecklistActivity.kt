package com.uct.inspection.view.activity

import android.app.Activity
import android.content.Intent
import android.util.Log
import android.view.View
import androidx.viewpager.widget.ViewPager
import com.amazonaws.services.s3.AmazonS3Client
import com.google.gson.Gson
import com.uct.inspection.R
import com.uct.inspection.core.base.BaseActivity
import com.uct.inspection.db.repository.InspectionChecklistRepository
import com.uct.inspection.interfaces.IAwsCallback
import com.uct.inspection.model.checklist.InspectionChecklist
import com.uct.inspection.utils.ChecklistUtil
import com.uct.inspection.utils.CommonDialog
import com.uct.inspection.utils.Constants
import com.uct.inspection.utils.InternetConnectionStatus
import com.uct.inspection.utils.enums.InspectionCardType
import com.uct.inspection.view.adapter.JobListPagerAdapter
import com.uct.inspection.view.fragment.InspectionChecklistFragment
import com.uct.inspection.viewmodel.InspectionCheckListViewModel
import kotlinx.android.synthetic.main.activity_inspection_checklist.*
import java.io.File


class InspectionChecklistActivity : BaseActivity(),
    InspectionChecklistFragment.IChecklistPageClickCallback {
    private var mInspectionCheckListViewModel: InspectionCheckListViewModel? = null
    private var mInspectionId: Int? = 0
    private var mInsertTS: Long? = 0
    private var mCarModel: String? = ""
    private var mRegNo: String? = ""
    private var mInspectionCardType: String? = ""
    private var mInspectionType: String? = ""
    private var isCallDb: Boolean = false
    private var mJobListPagerAdapter: JobListPagerAdapter? = null
    private var mInspectionChecklist: InspectionChecklist? = null

    public fun getInspectionId(): Int {
        return this.mInspectionId!!
    }

    public fun getInspectionCheckListViewModel(): InspectionCheckListViewModel? {
        return this.mInspectionCheckListViewModel!!
    }

    override fun getIntentValues() {
        super.getIntentValues()
        mInspectionId = intent.getIntExtra("mInspectionId", 0)
        mInsertTS = intent.getLongExtra("mInsertTS", 0)
        mCarModel = intent.getStringExtra("mCarModel")
        mRegNo = intent.getStringExtra("mRegNo")
        mInspectionType = intent.getStringExtra("mInspectionType")
        mInspectionCardType = intent.getStringExtra("mInspectionCardType")
    }

    override fun setLayoutResource(): Int {
        return R.layout.activity_inspection_checklist
    }

    override fun setValues() {
//        Log.d("checklistJson", CommonUtils.loadJSONFromAsset(mContext))
        /*val gson = Gson()
        val dummyResponse =
            gson.fromJson(CommonUtils.loadJSONFromAsset(mContext), InspectionChecklist::class.java)
        dummyResponse.inspectionId = 1
        setUpTabLayout(dummyResponse)
        this.mInspectionChecklist = dummyResponse
        mInspectionCheckListViewModel = InspectionCheckListViewModel(mContext)*/
        try {
            val createDir = File(Constants.DIRECTORY, "UCT-Inspection/Images/$mInspectionId")

            if (!createDir.exists()) {
                createDir.mkdirs()

                val noMediaFile = File(createDir, ".nomedia")
                if (!noMediaFile.exists()) {
                    noMediaFile.createNewFile()
                }
            }

        } catch (e: Exception) {
            e.printStackTrace()
        }

        setUpObserver()
        setUpObserverDb()
    }

    private fun setUpObserver() {
        mInspectionCheckListViewModel = InspectionCheckListViewModel(mContext)
        mInspectionCheckListViewModel?.inspectionChecklistData?.observe(
            this,
            androidx.lifecycle.Observer {
                mInspectionChecklist = it
                mInspectionChecklist?.inspectionId = mInspectionId!!
                setUpTabLayout(it)
            })

        mInspectionCheckListViewModel?.isFinalSubmit?.observe(
            this,
            androidx.lifecycle.Observer {
                if (it) {
                    CommonDialog.With(mActivity)
                        .showDataValidationFailureDialog("Thank You",
                            "You have successfully submitted all data.",
                            object : CommonDialog.IDataValidationFailureDialogClickCallback {

                                override fun onOkClick() {
                                    if (mInspectionCardType==InspectionCardType.REFURBISHMENT.inspectionCardType){
                                        setResultForRefurbJobList()
                                    }else{
                                        startVerdictActivity()
                                    }

                                }

                            })
                }
            })

        mInspectionCheckListViewModel?.isUnAssuredSubmit?.observe(
            this,
            androidx.lifecycle.Observer {
                if (it) {
                    startVerdictActivity()
                }
            })

    }

    public fun saveChecklistForUnAssured(position: Int) {
        mInspectionCheckListViewModel?.insertInspectionChecklistData(mInspectionChecklist, position)
        mInspectionCheckListViewModel?.saveInspectionChecklistFromUnAssured(
            mInspectionId!!,
            mInspectionChecklist,
            true
        )
    }


    public fun startVerdictActivity() {
        val intent = Intent(mContext, VerdictInspectionActivity::class.java)
        intent.putExtra("mInspectionId", mInspectionId)
        intent.putExtra("mCarModel", mCarModel)
        intent.putExtra("mRegNo", mRegNo)
        startActivity(intent)
    }

    public fun setResultForRefurbJobList() {
        val intent = Intent()
//        intent.putExtra("mImagePath", path)
//        intent.putExtra("mHeaderPosition", mHeaderPosition)
        setResult(RESULT_OK, intent)
        finish()
    }

    public fun startUploadDocumentActivity() {
        val intent = Intent(mContext, InspectionUploadDocumentActivity::class.java)
        intent.putExtra("mInspectionId", mInspectionId)
        intent.putExtra("mCarModel", mCarModel)
        intent.putExtra("mRegNo", mRegNo)
        startActivity(intent)
    }

    private fun setUpObserverDb() {
        mInspectionCheckListViewModel?.getInspectionChecklistDataFromDb(mInspectionId!!)?.observe(
            this,
            androidx.lifecycle.Observer {
                if (!isCallDb) {
                    isCallDb = true
                    if (it != null) {
                        if (it.insertTS > mInsertTS!!) {
                            this.mInspectionChecklist = it
//                        Toast.makeText(mContext,"setUpObserverDb",Toast.LENGTH_SHORT).show()
                            setUpTabLayout(it)
                        } else {
                            InspectionChecklistRepository.deleteInspectionChecklistById(
                                mContext,
                                mInspectionId!!
                            )
                            mInspectionCheckListViewModel?.getInspectionChecklist(mInspectionId!!)
                        }
                    } else {
                        mInspectionCheckListViewModel?.getInspectionChecklist(mInspectionId!!)
                    }
                }

            })

    }

    override fun setToolbarId(): View? {
        return null
    }


    override fun registerClickListener(): Array<View>? {
        return arrayOf(mImgBack)
    }

    override fun onClick(v: View?) {
        super.onClick(v)
        when (v?.id) {
            R.id.mImgBack -> {
                finish()
            }

        }

    }

    private fun setUpTabLayout(inspectionChecklist: InspectionChecklist) {
        mTxtTitle.setText(String.format("%s %s %s", "Inspections", "-", mInspectionType))
        mJobListPagerAdapter = JobListPagerAdapter(supportFragmentManager)
        for (i in inspectionChecklist.inspectionChecklistData.indices) {
            mJobListPagerAdapter?.addFragment(
                InspectionChecklistFragment.display(
                    this,
                    inspectionChecklist.inspectionChecklistData!![i],
                    i
                ), inspectionChecklist.inspectionChecklistData!![i].headerName
            )
        }
        mPagerPart.adapter = mJobListPagerAdapter
        mTabPart.setupWithViewPager(mPagerPart)
        mPagerPart.addOnPageChangeListener(object : ViewPager.OnPageChangeListener {
            override fun onPageScrollStateChanged(state: Int) {
//                Log.d("mPagerPart", "onPageScrollStateChanged :- " + state.toString())
            }

            override fun onPageScrolled(
                position: Int,
                positionOffset: Float,
                positionOffsetPixels: Int
            ) {
//                Log.d("mPagerPart", "onPageScrolled :- " + position.toString())
            }

            override fun onPageSelected(position: Int) {
                Log.d("mPagerPart", "onPageSelected :- " + position.toString())
                if (position > 0) {
                    val chkData =
                        ChecklistUtil.validateChecklistData(inspectionChecklist, position)
                    if (!chkData.isDataValid) {
                        CommonDialog.With(mActivity)
                            .showDataValidationFailureDialog("",
                                "Data validation failed.\nPlease check again before page change.",
                                object : CommonDialog.IDataValidationFailureDialogClickCallback {

                                    override fun onOkClick() {
                                        mPagerPart.setCurrentItem(chkData.nonValidTabPosition, true)
                                        val fragment =
                                            mJobListPagerAdapter?.getItem(mPagerPart.getCurrentItem()) as InspectionChecklistFragment
                                        fragment.redirectToIncompleteItem(chkData)
                                    }

                                })
                    } else {
                        mInspectionCheckListViewModel?.insertInspectionChecklistData(
                            mInspectionChecklist,
                            position - 1
                        )

                        if (InternetConnectionStatus.with(mActivity).isOnline()) {
                            mInspectionCheckListViewModel?.saveInspectionChecklist(
                                mInspectionId!!,
                                mInspectionChecklist,
                                false
                            )
                        }
                    }
                }
            }
        })
    }

    override fun onNext(position: Int) {
//        Log.d("checklistJson", Gson().toJson(mInspectionChecklist))
        mInspectionCheckListViewModel?.insertInspectionChecklistData(mInspectionChecklist, position)
//        Toast.makeText(mContext, "data inserted successfully", Toast.LENGTH_SHORT).show()
        if (mPagerPart.currentItem == mInspectionChecklist?.inspectionChecklistData?.size!! - 1) {
            Log.d("mPagerPart", "onPageSelected :- " + "last")
            if (InternetConnectionStatus.with(mActivity).isOnline()) {
                mInspectionCheckListViewModel?.saveInspectionChecklist(
                    mInspectionId!!,
                    mInspectionChecklist,
                    true
                )
            } else {
                CommonDialog.With(mActivity)
                    .showDataValidationFailureDialog("",
                        "Check your internet connection and try again.",
                        object : CommonDialog.IDataValidationFailureDialogClickCallback {

                            override fun onOkClick() {

                            }

                        })
            }

        } else {
            Log.d("mPagerPart", "setCurrentItem")
            mPagerPart.setCurrentItem(mPagerPart.currentItem + 1, true)
        }

    }


    override fun onInsertDataInDb(position: Int) {
        mInspectionCheckListViewModel?.insertInspectionChecklistData(mInspectionChecklist, position)
    }


    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == Activity.RESULT_OK && requestCode == Constants.INTENT_CAMERA_REQUEST_CODE) {
            val fragment =
                mJobListPagerAdapter?.getItem(mPagerPart.getCurrentItem()) as InspectionChecklistFragment
            fragment.onActivityResult(requestCode, resultCode, data)
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        mInspectionCheckListViewModel?.reset()
    }
}
