package com.uct.inspection.view.activity

import android.content.Intent
import android.view.View
import android.widget.Toast
import com.amazonaws.services.s3.AmazonS3Client
import com.uct.inspection.R
import com.uct.inspection.aws.AwsDirectoryTask
import com.uct.inspection.aws.AwsLoginTask
import com.uct.inspection.aws.AwsUploadTask
import com.uct.inspection.core.base.BaseActivity
import com.uct.inspection.db.repository.InspectionChecklistRepository
import com.uct.inspection.interfaces.IAwsCallback
import com.uct.inspection.model.checklist.InspectionChecklist
import com.uct.inspection.model.verdict.InspectionVerdictData
import com.uct.inspection.utils.CommonDialog
import com.uct.inspection.utils.Constants
import com.uct.inspection.viewmodel.InspectionCheckListViewModel
import com.uct.inspection.viewmodel.InspectionVerdictViewModel
import kotlinx.android.synthetic.main.activity_verdict_inspection.*

class VerdictInspectionActivity : BaseActivity(), IAwsCallback {
    override fun onAwsFileUploadSuccess(uploadedFilePath: String?) {

    }


    private var mInspectionVerdictViewModel: InspectionVerdictViewModel? = null
    private var mInspectionCheckListViewModel: InspectionCheckListViewModel? = null
    private var mInspectionId: Int? = 0
    private var mCarModel: String? = ""
    private var mRegNo: String? = ""
    private var mVerdict: String? = null
    private var isCallDb: Boolean = false
    private var mInspectionChecklist: InspectionChecklist? = null

    override fun getIntentValues() {
        super.getIntentValues()
        mInspectionId = intent.getIntExtra("mInspectionId", 0)
//        mInsertTS = intent.getLongExtra("mInsertTS", 0)
        mCarModel = intent.getStringExtra("mCarModel")
        mRegNo = intent.getStringExtra("mRegNo")
    }

    override fun setLayoutResource(): Int {
        return R.layout.activity_verdict_inspection
    }

    override fun setValues() {
        setUpObserver()
        setUpObserverDb()
    }

    private fun setUpObserver() {
        mInspectionVerdictViewModel = InspectionVerdictViewModel(mContext)
        mInspectionVerdictViewModel?.inspectionVerdictData?.observe(
            this,
            androidx.lifecycle.Observer {
                setUpUi(it.inspectionVerdictData)

            })

        mInspectionVerdictViewModel?.isInspectionSaveLiveData?.observe(
            this,
            androidx.lifecycle.Observer {
                if (it) {
                    CommonDialog.With(mActivity)
                        .showDataValidationFailureDialog("Thank You",
                            "You have successfully submitted verdict data.",
                            object : CommonDialog.IDataValidationFailureDialogClickCallback {

                                override fun onOkClick() {
//                                    startInspectionListActivity()
                                    startAwsLoginTask()
                                }

                            })
                } else {
                    CommonDialog.With(mActivity)
                        .showDataValidationFailureDialog("",
                            "Something went wrong",
                            object : CommonDialog.IDataValidationFailureDialogClickCallback {

                                override fun onOkClick() {

                                }

                            })
                }
            })


        mInspectionVerdictViewModel?.getInspectionVerdict(mInspectionId!!)


        mInspectionCheckListViewModel = InspectionCheckListViewModel(mContext)
        mInspectionCheckListViewModel?.isFinalFileSubmit?.observe(
            this,
            androidx.lifecycle.Observer {
                if (it) {
                    CommonDialog.With(mActivity)
                        .showDataValidationFailureDialog("Thank You",
                            "You have successfully submitted all Files.",
                            object : CommonDialog.IDataValidationFailureDialogClickCallback {

                                override fun onOkClick() {
                                    startInspectionListActivity()
                                }

                            })
                } else {
                    CommonDialog.With(mActivity)
                        .showDataValidationFailureDialog("",
                            "Something went wrong in uploading files",
                            object : CommonDialog.IDataValidationFailureDialogClickCallback {

                                override fun onOkClick() {

                                }

                            })
                }
            })
    }


    private fun setUpObserverDb() {
        InspectionChecklistRepository.getInspectionChecklistData(mContext, mInspectionId!!)
            ?.observe(
                this,
                androidx.lifecycle.Observer {
                    if (!isCallDb) {
                        isCallDb = true
                        if (it != null) {
                            mInspectionChecklist = it
                        } else {
//                            mInspectionCheckListViewModel?.getInspectionChecklist(mInspectionId!!)
                        }
                    }

                })
    }


    private fun startInspectionListActivity() {
        val bdIntent = Intent(mContext, InspectionListActivity::class.java)
        bdIntent.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
        startActivity(bdIntent)
    }


    private fun setUpUi(inspectionVerdictData: InspectionVerdictData) {
        mLytVerdict.visibility = View.VISIBLE
        mTxtInspectionStatus.setText(inspectionVerdictData.title)
        if (inspectionVerdictData.warning) {
            mTxtInspectionStatus.setTextColor(mContext.resources.getColor(R.color.colorVerdictAlert))
            mTxtInspectionStatus.setCompoundDrawablesWithIntrinsicBounds(
                0,
                R.drawable.ic_red_alert,
                0,
                0
            )
        } else {
            mTxtInspectionStatus.setTextColor(mContext.resources.getColor(R.color.colorTxtDark))
            mTxtInspectionStatus.setCompoundDrawablesWithIntrinsicBounds(
                0,
                R.drawable.ic_green_tick,
                0,
                0
            )

        }

        mTxtCarModel.setText(mCarModel)
        mTxtCarRegNo.setText(mRegNo)
        mTxtLeadId.setText(inspectionVerdictData.leadID.toString())
        mInspectionType.setText(inspectionVerdictData.inspectionType)
        mTxtIssueFound.setText(inspectionVerdictData.issuesCount.toString())
        mTxtJobFound.setText(inspectionVerdictData.jobsCount.toString())
        mTxtUnAssured.setText(inspectionVerdictData.unAssuredPartCount.toString())
        mTxtRefurbCost.setText(inspectionVerdictData.refurbishedCost.toString())
        mTxtIncompletePart.setText(inspectionVerdictData.partsNotInspectedCount.toString())
        /*mTxtRefurbCost.setText(
            String.format(
                "%s %s",
                mContext.getString(R.string.rupee_symbole),
                inspectionVerdictData.refurbishedCost.toString()
            )
        )*/

    }

    override fun setToolbarId(): View? {
        return null
    }


    override fun registerClickListener(): Array<View>? {
        return arrayOf(mImgBack, mTxtSubmit, mTxtPass, mTxtFail, mTxtEdit)
    }

    override fun onClick(v: View?) {
        super.onClick(v)
        when (v?.id) {
            R.id.mImgBack, R.id.mTxtEdit -> {
                finish()
            }

            R.id.mTxtPass -> {
                if (mTxtPass.isSelected) {
                    mTxtPass.isSelected = false
                    mTxtPass.setTextColor(mContext.resources.getColor(R.color.colorPrimary))
                } else {
                    mVerdict = Constants.VERDICT_PASS
                    mTxtFail.isSelected = false
                    mTxtFail.setTextColor(mContext.resources.getColor(R.color.colorPrimary))
                    mTxtPass.isSelected = true
                    mTxtPass.setTextColor(mContext.resources.getColor(R.color.colorWhite))
                }

            }

            R.id.mTxtFail -> {
                if (mTxtFail.isSelected) {
                    mTxtFail.isSelected = false
                    mTxtFail.setTextColor(mContext.resources.getColor(R.color.colorPrimary))
                } else {
                    mVerdict = Constants.VERDICT_FAIL
                    mTxtPass.isSelected = false
                    mTxtPass.setTextColor(mContext.resources.getColor(R.color.colorPrimary))
                    mTxtFail.isSelected = true
                    mTxtFail.setTextColor(mContext.resources.getColor(R.color.colorWhite))
                }
            }

            R.id.mTxtSubmit -> {
                if (mVerdict == null) {
                    Toast.makeText(mContext, "Please select atleast one verdict", Toast.LENGTH_LONG)
                        .show()
                } else {
                    mInspectionVerdictViewModel?.saveInspectionVerdict(
                        mInspectionId!!,
                        mVerdict,
                        mEdtComment.text.toString().trim()
                    )
                }

            }


        }

    }

    private fun startAwsLoginTask() {
        AwsLoginTask(mContext, this).execute()
    }

    override fun onAwsLoginSuccess(amazonS3Client: AmazonS3Client?) {
        AwsDirectoryTask(
            mContext,
            this,
            mInspectionId!!,
            amazonS3Client!!
        ).execute()
    }

    override fun onAwsDirectoryCreationSuccess(amazonS3Client: AmazonS3Client?) {
        AwsUploadTask(
            mContext,
            this,
            amazonS3Client!!,
            mInspectionChecklist!!
        ).execute()
    }

    override fun onAwsFileUploadSuccess(isUploaded: Boolean) {
        if (isUploaded) {
            InspectionChecklistRepository.insertAllInspectionChecklistData(
                mContext,
                mInspectionChecklist!!
            )

            mInspectionCheckListViewModel?.saveInspectionChecklistImage(
                mInspectionId!!,
                mInspectionChecklist,
                true
            )
        } else {
            CommonDialog.With(mActivity)
                .showDataValidationFailureDialog("",
                    "Something went wrong in uploading files",
                    object : CommonDialog.IDataValidationFailureDialogClickCallback {

                        override fun onOkClick() {

                        }

                    })

        }
    }

    override fun onDestroy() {
        super.onDestroy()
        mInspectionVerdictViewModel?.reset()
        mInspectionVerdictViewModel?.reset()
    }
}
