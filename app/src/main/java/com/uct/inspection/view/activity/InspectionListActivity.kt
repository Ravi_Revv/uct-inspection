package com.uct.inspection.view.activity

import android.view.View
import android.widget.Toast
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import com.amazonaws.services.s3.AmazonS3Client
import com.google.gson.Gson
import com.uct.inspection.R
import com.uct.inspection.aws.AwsDirectoryTask
import com.uct.inspection.aws.AwsLoginTask
import com.uct.inspection.aws.AwsUploadTask
import com.uct.inspection.core.base.BaseActivity
import com.uct.inspection.db.repository.InspectionChecklistRepository
import com.uct.inspection.interfaces.IAwsCallback
import com.uct.inspection.model.checklist.InspectionChecklist
import com.uct.inspection.model.inspectionlist.Inspection
import com.uct.inspection.model.inspectionlist.InspectionData
import com.uct.inspection.model.inspectionlist.InspectionListData
import com.uct.inspection.utils.CommonDialog
import com.uct.inspection.utils.CommonUtils
import com.uct.inspection.view.adapter.JobListPagerAdapter
import com.uct.inspection.view.fragment.PastJobFragment
import com.uct.inspection.view.fragment.ScheduledJobFragment
import com.uct.inspection.viewmodel.InspectionCheckListViewModel
import com.uct.inspection.viewmodel.InspectionListViewModel
import com.uct.inspection.viewmodel.RefurbJobViewModel
import kotlinx.android.synthetic.main.activity_inspection_list.*

class InspectionListActivity : BaseActivity(), IAwsCallback, SwipeRefreshLayout.OnRefreshListener {
    override fun onRefresh() {
        isSwipe = true
        mInspectionListViewModel?.getInspectionList(isSwipe)
    }

    override fun onAwsFileUploadSuccess(uploadedFilePath: String?) {

    }

    private var mInspectionListViewModel: InspectionListViewModel? = null
    private var mInspectionCheckListViewModel: InspectionCheckListViewModel? = null
    private var mRefurbJobViewModel: RefurbJobViewModel? = null
    private var localChecklistData: HashMap<Int, InspectionChecklist>? = null
    private var mInspectionChecklistUploaded: InspectionChecklist? = null
    private var isSwipe = false

    public fun getAllInspectionList(): HashMap<Int, InspectionChecklist> {
        return this.localChecklistData!!
    }

    override fun setLayoutResource(): Int {
        return R.layout.activity_inspection_list
    }

    override fun setValues() {
        setUpObserver()
        setUpObserverDb()
        mSwipe.setOnRefreshListener(this)

    }

    private fun setUpObserver() {

        /*val docStr = CommonUtils.loadJSONFromAsset(mContext, "inspection_list_e4.json")
       val inspection =
           Gson().fromJson<Inspection>(
               docStr,
               Inspection::class.java
           ) as Inspection
        setUpTabLayout(ArrayList(inspection.data))*/

        mInspectionListViewModel = InspectionListViewModel(mContext)
        mRefurbJobViewModel = RefurbJobViewModel(mContext)
        mInspectionListViewModel?.inspectionData?.observe(this, androidx.lifecycle.Observer {
            changeSwipeRefreshingStatus()
            isSwipe = false
            setUpTabLayout(ArrayList(it.data))
        })


        mRefurbJobViewModel?.isRefurbStatusUpdateLiveData?.observe(
            this,
            androidx.lifecycle.Observer {
                if (it) {
                    CommonDialog.With(mActivity)
                        .showDataValidationFailureDialog("Thank You",
                            "You have successfully update refurb status.",
                            object : CommonDialog.IDataValidationFailureDialogClickCallback {

                                override fun onOkClick() {
                                    mInspectionListViewModel?.getInspectionList(isSwipe)
                                }

                            })
                } else {
                    CommonDialog.With(mActivity)
                        .showDataValidationFailureDialog("",
                            "Something went wrong update refurb status.",
                            object : CommonDialog.IDataValidationFailureDialogClickCallback {

                                override fun onOkClick() {

                                }

                            })
                }
            })

        mInspectionListViewModel?.getInspectionList(isSwipe)
    }

    private fun setUpObserverDb() {
        localChecklistData = HashMap()
        mInspectionCheckListViewModel = InspectionCheckListViewModel(mContext!!)
        mInspectionCheckListViewModel?.getAllInspectionChecklistDataFromDb()?.observe(
            this,
            androidx.lifecycle.Observer {
                localChecklistData?.clear()
                for (checklist in it) {
                    localChecklistData?.put(checklist.inspectionId, checklist)
                }

//                mInspectionListViewModel?.getInspectionList(isSwipe)

            })

        mInspectionCheckListViewModel?.isFinalSubmit?.observe(this, androidx.lifecycle.Observer {
            if (it) {
                startAwsLoginTask()
            }

        })


        mInspectionCheckListViewModel?.isFinalFileSubmit?.observe(
            this,
            androidx.lifecycle.Observer {
                if (it) {
                    CommonDialog.With(mActivity)
                        .showDataValidationFailureDialog("Thank You",
                            "You have successfully submitted all Files.",
                            object : CommonDialog.IDataValidationFailureDialogClickCallback {

                                override fun onOkClick() {
                                    mInspectionListViewModel?.getInspectionList(isSwipe)
                                }

                            })
                } else {
                    CommonDialog.With(mActivity)
                        .showDataValidationFailureDialog("",
                            "Something went wrong in uploading files",
                            object : CommonDialog.IDataValidationFailureDialogClickCallback {

                                override fun onOkClick() {

                                }

                            })
                }
            })
    }


    private fun changeSwipeRefreshingStatus(isRefresh: Boolean = false) {
        mSwipe?.isRefreshing = isRefresh
    }

    override fun setToolbarId(): View? {
        return null
    }


    override fun registerClickListener(): Array<View>? {
        return arrayOf()
    }

    override fun onClick(v: View?) {
        super.onClick(v)
        when (v?.id) {

        }

    }

    public fun syncOnlineData(inspectionChecklist: InspectionChecklist) {
        this.mInspectionChecklistUploaded = inspectionChecklist
        mInspectionCheckListViewModel?.saveInspectionChecklist(
            inspectionChecklist.inspectionId,
            inspectionChecklist,
            true
        )
    }

    public fun updateRefurbStatus(inspectionData: InspectionListData, status: String) {
        mRefurbJobViewModel?.updateRefurbStatus(
            inspectionData.id,
            status
        )
    }


    private fun startAwsLoginTask() {
        AwsLoginTask(mContext, this).execute()
    }

    override fun onAwsLoginSuccess(amazonS3Client: AmazonS3Client?) {
        if (mInspectionChecklistUploaded != null) {
            AwsDirectoryTask(
                mContext,
                this,
                mInspectionChecklistUploaded?.inspectionId!!,
                amazonS3Client!!
            ).execute()
        }

    }

    override fun onAwsDirectoryCreationSuccess(amazonS3Client: AmazonS3Client?) {
        AwsUploadTask(
            mContext,
            this,
            amazonS3Client!!,
            mInspectionChecklistUploaded!!
        ).execute()
    }

    override fun onAwsFileUploadSuccess(isUploaded: Boolean) {
        if (isUploaded) {
            mInspectionCheckListViewModel?.saveInspectionChecklistImage(
                mInspectionChecklistUploaded?.inspectionId!!,
                mInspectionChecklistUploaded,
                true
            )

        } else {
            CommonDialog.With(mActivity)
                .showDataValidationFailureDialog("",
                    "Something went wrong in uploading files",
                    object : CommonDialog.IDataValidationFailureDialogClickCallback {

                        override fun onOkClick() {

                        }

                    })

        }
    }


    private fun setUpTabLayout(inspectionDataList: ArrayList<InspectionData>) {
        val adapter = JobListPagerAdapter(supportFragmentManager)
        adapter.addFragment(
            ScheduledJobFragment.getInstance(ArrayList(inspectionDataList[0].inspectionsList)),
            inspectionDataList[0].name
        )
        adapter.addFragment(
            PastJobFragment.getInstance(ArrayList(inspectionDataList[1].inspectionsList)),
            inspectionDataList[1].name
        )
        mPagerJob.adapter = adapter
        mTabJob.setupWithViewPager(mPagerJob)
    }


    override fun onDestroy() {
        super.onDestroy()
        mInspectionListViewModel?.reset()
        mInspectionCheckListViewModel?.reset()
    }
}
