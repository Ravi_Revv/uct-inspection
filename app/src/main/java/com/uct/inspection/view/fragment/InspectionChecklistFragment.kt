package com.uct.inspection.view.fragment

import android.app.Activity
import android.content.Intent
import android.view.View
import androidx.core.view.ViewCompat
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.uct.inspection.R
import com.uct.inspection.camera.CameraActivity
import com.uct.inspection.core.base.BaseFragment
import com.uct.inspection.interfaces.IChecklistClickCallback
import com.uct.inspection.model.checklist.ChecklistValidation
import com.uct.inspection.model.checklist.InspectionChecklistData
import com.uct.inspection.utils.ChecklistUtil
import com.uct.inspection.utils.CommonDialog
import com.uct.inspection.utils.CommonUtils
import com.uct.inspection.utils.Constants
import com.uct.inspection.view.activity.ImageZoomHelperActivity
import com.uct.inspection.view.activity.InspectionChecklistActivity
import com.uct.inspection.view.adapter.ChecklistHeaderAdapter
import kotlinx.android.synthetic.main.fragment_inspection_checklist.*


class InspectionChecklistFragment(
    private var mIChecklistPageClickCallback: IChecklistPageClickCallback?,
    private var mInspectionChecklistData: InspectionChecklistData,
    private var mPosition: Int
) : BaseFragment(), IChecklistClickCallback {
    private var mChecklistHeaderAdapter: ChecklistHeaderAdapter? = null

    companion object {
        val TAG = "InspectionChecklistFragment"
        fun display(
            iChecklistPageClickCallback: IChecklistPageClickCallback,
            inspectionChecklistData: InspectionChecklistData,
            position: Int
        ): InspectionChecklistFragment {
            val inspectionChecklistFragment =
                InspectionChecklistFragment(
                    iChecklistPageClickCallback,
                    inspectionChecklistData,
                    position
                )

            return inspectionChecklistFragment
        }

    }

    override fun setLayoutResource(): Int {
        return R.layout.fragment_inspection_checklist
    }

    override fun setValues() {
        setUpChecklistHeaderList()
    }

    override fun registerClickListener(): Array<View>? {
        return arrayOf(mBtnNext)
    }

    private fun setUpChecklistHeaderList() {
        mChecklistHeaderAdapter =
            ChecklistHeaderAdapter(mContext, ArrayList(mInspectionChecklistData.checklistHeader))
        mChecklistHeaderAdapter?.setIChecklistClickCallback(this)
        mRcChecklistHeader.setHasFixedSize(true)
        mRcChecklistHeader.layoutManager =
            LinearLayoutManager(mActivity, LinearLayoutManager.VERTICAL, false)
        mRcChecklistHeader.itemAnimator = DefaultItemAnimator() as RecyclerView.ItemAnimator?
        mRcChecklistHeader.adapter = mChecklistHeaderAdapter
        ViewCompat.setNestedScrollingEnabled(mRcChecklistHeader, false)
    }

    override fun onClick(v: View?) {
        when (v?.id) {
            R.id.mBtnNext -> {
                val chkData = ChecklistUtil.validateChecklistData(mInspectionChecklistData)
//                val chkData = validateChecklistData(mInspectionChecklistData)
                if (mIChecklistPageClickCallback != null && chkData.isDataValid) {
                    mIChecklistPageClickCallback?.onNext(mPosition)
                } else {
                    redirectToIncompleteItem(chkData)
                }
            }
        }

    }


    public fun redirectToIncompleteItem(chkData: ChecklistValidation) {
        mRcChecklistHeader.getLayoutManager()
            ?.scrollToPosition(chkData.nonValidPosition)

        mChecklistHeaderAdapter?.setError(true, chkData.nonValidPosition)
    }

    override fun onUnAssuredChecklistIssueClick() {
        CommonDialog.With(mActivity)
            .showUnAssuredDialog(object : CommonDialog.IUnassuredClickCallback {
                override fun onGoVerdict() {
                    (mActivity as InspectionChecklistActivity).saveChecklistForUnAssured(mPosition)
//                    (mActivity as InspectionChecklistActivity).startVerdictActivity()
                }

                override fun onContinueInspection() {
//                    Toast.makeText(mContext, "Continue Inspection", Toast.LENGTH_SHORT).show()
                }

            })
    }

    override fun onChecklistIssueClick(parentPosition: Int) {
        mChecklistHeaderAdapter?.notifyItemChanged(parentPosition)
        mIChecklistPageClickCallback?.onInsertDataInDb(mPosition)
    }

    override fun onChecklistSubIssueClick(parentPosition: Int) {
        mIChecklistPageClickCallback?.onInsertDataInDb(mPosition)
    }

    override fun onChecklistAddImageClick(position: Int) {
        val intent = Intent(mContext, CameraActivity::class.java)
        intent.putExtra(
            "mInspectionId",
            (mActivity as InspectionChecklistActivity).getInspectionId()
        )
        intent.putExtra(
            "mHeaderPosition",
            position
        )
        intent.putExtra(
            "mFileName",
            String.format(
                "%s%s%s%s%s%s%s",
                (mActivity as InspectionChecklistActivity).getInspectionId(),
                "_",
                CommonUtils.removeSpace(mInspectionChecklistData.headerID),
                "_",
                CommonUtils.removeSpace(mInspectionChecklistData.checklistHeader[position].subPartID),
                "_",
                System.currentTimeMillis()
            )
        )

        mActivity?.startActivityForResult(intent, Constants.INTENT_CAMERA_REQUEST_CODE)
    }

    override fun onChecklistDeleteImageClick(mParentPosition: Int, position: Int) {
        mInspectionChecklistData.checklistHeader[mParentPosition].images.removeAt(position)
        mChecklistHeaderAdapter?.notifyItemChanged(mParentPosition)
        mIChecklistPageClickCallback?.onInsertDataInDb(mPosition)

    }


    override fun onChecklistCostChange(mParentPosition: Int) {
        mIChecklistPageClickCallback?.onInsertDataInDb(mPosition)
    }

    override fun onChecklistCommentChange(mParentPosition: Int) {
        mIChecklistPageClickCallback?.onInsertDataInDb(mPosition)
    }

    override fun onChecklistImageClick(mParentPosition: Int, position: Int) {
        val intent = Intent(mContext, ImageZoomHelperActivity::class.java)
        intent.putExtra(
            "imagePath",
            mInspectionChecklistData.checklistHeader[mParentPosition].images.get(position)
        )
        intent.putExtra(
            "mHeaderPosition",
            position
        )
        startActivity(intent)


    }


    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == Activity.RESULT_OK && requestCode == Constants.INTENT_CAMERA_REQUEST_CODE) {
            if (data != null && data.hasExtra("mHeaderPosition") && data.hasExtra("mImagePath")) {
                val position = data.getIntExtra("mHeaderPosition", 0)
                val image = data.getStringExtra("mImagePath")
                mInspectionChecklistData.checklistHeader[position].images.add(image)
                mChecklistHeaderAdapter?.notifyItemChanged(position)
                mIChecklistPageClickCallback?.onInsertDataInDb(mPosition)
            }

        }

    }

    interface IChecklistPageClickCallback {
        fun onNext(position: Int)
        fun onInsertDataInDb(position: Int)
    }
}