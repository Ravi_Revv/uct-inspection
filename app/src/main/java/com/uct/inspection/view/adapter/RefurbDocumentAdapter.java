package com.uct.inspection.view.adapter;

import android.content.Context;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.uct.inspection.R;
import com.uct.inspection.interfaces.IChecklistClickCallback;
import com.uct.inspection.interfaces.IRefurbDocumentClickCallback;
import com.uct.inspection.model.checklist.ChecklistIssue;
import com.uct.inspection.model.checklist.ChecklistSubIssue;
import com.uct.inspection.model.refurb.RefurbDocument;
import com.uct.inspection.utils.ClickGuard;

import java.util.ArrayList;

public class RefurbDocumentAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private Context mContext = null;
    private ArrayList<RefurbDocument> mRefurbDocument;
    private IRefurbDocumentClickCallback mIRefurbDocumentClickCallback;
    private int mErrorPosition = -1;
    private boolean isError;

    public void setRefurbDocumentClickCallback(IRefurbDocumentClickCallback mIRefurbDocumentClickCallback) {
        this.mIRefurbDocumentClickCallback = mIRefurbDocumentClickCallback;
    }

    public RefurbDocumentAdapter(Context mContext,ArrayList<RefurbDocument> refurbDocument) {
        this.mContext = mContext;
        this.mRefurbDocument = refurbDocument;
    }


    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup mParent, int viewType) {
        return new RefurbDocumentViewHolder(LayoutInflater.from(mParent.getContext()).inflate(R.layout.item_refurb_document, mParent, false));

    }


    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        final RefurbDocumentViewHolder viewHolder = (RefurbDocumentViewHolder) holder;

        if (isError && position == mErrorPosition) {
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    isError = false;
                    mErrorPosition = -1;
                    viewHolder.mLytDocumentListItem.setBackgroundColor(mContext.getResources().getColor(R.color.colorTransparent));

                }
            }, 1000);

            viewHolder.mLytDocumentListItem.setBackgroundColor(mContext.getResources().getColor(R.color.colorError));
        }


        viewHolder.mTxtDocumentName.setText(mRefurbDocument.get(viewHolder.getAdapterPosition()).getDocumentName());
        prepareCheckListImage(viewHolder.mRcImage, viewHolder.getAdapterPosition(), new ArrayList<String>(mRefurbDocument.get(viewHolder.getAdapterPosition()).getDocumentList()));
       /* if (mChecklistHeader.get(holder.getAdapterPosition()).isFileRequired())
            viewHolder.mBtnAddImage.setVisibility(View.VISIBLE);
        else viewHolder.mBtnAddImage.setVisibility(View.GONE);*/



    }


    @Override
    public int getItemCount() {
        return mRefurbDocument.size();

    }

    public void setError(boolean isError, int position) {
        this.isError = isError;
        this.mErrorPosition = position;
        notifyDataSetChanged();
    }

    public class RefurbDocumentViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        TextView mTxtDocumentName;
        RecyclerView mRcImage;
        Button mBtnAddImage;
        LinearLayout mLytDocumentListItem;

        public RefurbDocumentViewHolder(View v) {
            super(v);
            mTxtDocumentName = v.findViewById(R.id.mTxtDocumentName);
            mRcImage = v.findViewById(R.id.mRcImage);
            mBtnAddImage = v.findViewById(R.id.mBtnAddImage);
            mLytDocumentListItem = v.findViewById(R.id.mLytDocumentListItem);
            mBtnAddImage.setOnClickListener(this);
            ClickGuard.guard(mBtnAddImage);

        }


        @Override
        public void onClick(View v) {
            switch (v.getId()) {
                case R.id.mBtnAddImage:
                    if (mIRefurbDocumentClickCallback != null)
                        mIRefurbDocumentClickCallback.onRefurbDocumentAddImageClick(getAdapterPosition());
                    break;
                default:
                    break;
            }
        }
    }


    private void prepareCheckListImage(RecyclerView recyclerView, int mParentPosition, ArrayList<String> imageList) {
        ChecklistImageAdapter adapter = new ChecklistImageAdapter(mContext, imageList);
        adapter.setParentPosition(mParentPosition);
        adapter.setRefurbDocumentClickCallback(mIRefurbDocumentClickCallback);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new GridLayoutManager(mContext, 4));
        /*recyclerView.addItemDecoration(new ItemDecoratorImageGridView(
                mContext.getResources().getDimensionPixelSize(R.dimen._10sdp),
                4));*/
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(adapter);
//        ViewCompat.setNestedScrollingEnabled(recyclerView, false);
    }

}