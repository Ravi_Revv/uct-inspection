package com.uct.inspection.view.activity

import android.content.Intent
import android.view.View
import com.uct.inspection.R
import com.uct.inspection.core.base.BaseActivity
import com.uct.inspection.model.inspectionlist.InspectionListData
import com.uct.inspection.utils.CommonUtils
import com.uct.inspection.utils.enums.InspectionCardType
import kotlinx.android.synthetic.main.activity_inspection_detail_e4.*


class InspectionDetailE4Activity : BaseActivity() {

    private var mInspectionListData: InspectionListData? = null
    override fun getIntentValues() {
        super.getIntentValues()
        mInspectionListData = intent.getParcelableExtra("inspectionData");
    }

    override fun setLayoutResource(): Int {
        return R.layout.activity_inspection_detail_e4
    }

    override fun setValues() {
        setDataView()
        /*if (mInspectionListData?.insertTS!! > 0) {
            mTxtStartInspection.setText("Resume inspection")
        } else {
            mTxtStartInspection.setText("Start inspection")
        }*/


    }


    private fun setDataView() {
        mTxtWorkshopName.setText(mInspectionListData?.workshopName)
        mTxtWorkshopAddress.setText(mInspectionListData?.workshopAddress)
        mTxtCarId.setText(mInspectionListData?.carId.toString())
        mTxtNoOfJobs.setText(mInspectionListData?.noOfJobs)
        mTxtE3Inspector.setText(mInspectionListData?.e3Inspector)
        if (mInspectionListData?.workshops?.size!! > 1) {
            mTxtWorkshopNameLabel.setText(
                String.format(
                    "%s %s%s",
                    "Workshop Name",
                    "+", mInspectionListData?.workshops?.size!! - 1
                )
            )
        } else {
            mTxtWorkshopNameLabel.setText("Workshop Name")
        }


        if (mInspectionListData?.refurbSpocs?.size!! > 1) {
            mTxtRefurbSpocLabel.setText(
                String.format(
                    "%s %s%s",
                    "Refurb Spoc",
                    "+", mInspectionListData?.refurbSpocs?.size!! - 1
                )
            )
        } else {
            mTxtRefurbSpocLabel.setText("Refurb Spoc")
        }
        mTxtRefurbSpoc.setText(mInspectionListData?.refurbSpoc)
        mTxtInspectionTime.setText(mInspectionListData?.inspectionTimeDisplay)
        mTxtSellerNnumber.setText(mInspectionListData?.contactNumber.toString())
        mTxtMake.setText(mInspectionListData?.carMake)
        mTxtModel.setText(
            String.format(
                "%s %s",
                mInspectionListData?.carModel,
                mInspectionListData?.carVariant
            )
        )
        mTxtFuelType.setText(mInspectionListData?.carFuel)
        mTxtTransmission.setText(mInspectionListData?.carTransmission)
        mTxtDriven.setText(mInspectionListData?.kmDrivenDisplay)

        mTxtRegisteredOn.setText(mInspectionListData?.registrationTimeDisplay)
        mTxtRegNumber.setText(mInspectionListData?.carRegistrationNumber)
    }

    override fun setToolbarId(): View? {
        return null
    }


    override fun registerClickListener(): Array<View>? {
        return arrayOf(mImgBack, mLytCall, mLytNavigation, mTxtStartInspection)
    }

    override fun onClick(v: View?) {
        super.onClick(v)
        when (v?.id) {
            R.id.mImgBack -> {
                finish()
            }

            R.id.mLytCall -> {
                CommonUtils.openCallDialer(mInspectionListData?.contactNumber.toString(), mContext)
            }

            R.id.mLytNavigation -> {
                CommonUtils.openNavigationMap(
                    mInspectionListData?.sellerLat!!,
                    mInspectionListData?.sellerLng!!,
                    mContext
                )
            }

            R.id.mTxtStartInspection -> {
                val intent = Intent(mContext, InspectionE4ChecklistActivity::class.java)
                intent.putExtra("mInspectionId", mInspectionListData?.id)
                intent.putExtra("mInsertTS", mInspectionListData?.insertTS)
                intent.putExtra(
                    "mCarModel",
                    mInspectionListData?.carMake + " " + mInspectionListData?.carModel
                )
                intent.putExtra("mRegNo", mInspectionListData?.carRegistrationNumber)
                intent.putExtra("mInspectionType", mInspectionListData?.inspectionType)
                intent.putExtra(
                    "mInspectionCardType",
                    InspectionCardType.POST_REFURBISHMENT.inspectionCardType
                )
                startActivity(intent)
            }

        }

    }

}
