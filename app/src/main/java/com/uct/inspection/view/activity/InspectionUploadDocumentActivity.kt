package com.uct.inspection.view.activity

import android.app.Activity
import android.content.Intent
import android.provider.MediaStore
import android.util.Log
import android.view.View
import android.widget.Toast
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.LinearLayoutManager
import com.amazonaws.services.s3.AmazonS3Client
import com.google.gson.Gson
import com.uct.inspection.R
import com.uct.inspection.aws.AwsDirectoryTask
import com.uct.inspection.aws.AwsLoginTask
import com.uct.inspection.aws.AwsSingleFileUploadTask
import com.uct.inspection.aws.AwsUploadTask
import com.uct.inspection.camera.CameraActivity
import com.uct.inspection.core.base.BaseActivity
import com.uct.inspection.db.repository.InspectionChecklistRepository
import com.uct.inspection.interfaces.IAwsCallback
import com.uct.inspection.interfaces.IUploadDocumentCallback
import com.uct.inspection.model.document.InspectionDocumentData
import com.uct.inspection.model.inspectionlist.InspectionListData
import com.uct.inspection.utils.*
import com.uct.inspection.view.adapter.UploadDocumentAdapter
import com.uct.inspection.view.fragment.UploadDocumentBottomFragment
import com.uct.inspection.viewmodel.InspectionDocumentViewModel
import kotlinx.android.synthetic.main.activity_inspection_checklist.mImgBack
import kotlinx.android.synthetic.main.activity_inspection_upload_documents.*
import kotlinx.android.synthetic.main.activity_inspection_upload_documents.mTxtTitle
import java.io.File


class InspectionUploadDocumentActivity : BaseActivity(), IUploadDocumentCallback, IAwsCallback {


    private var mUploadDocumentAdapter: UploadDocumentAdapter? = null
    private var mSelectedPosition: Int = 0
    private var mSelectedFile: String? = null
    private var mInspectionListData: InspectionListData? = null
    private var mDocumentList: ArrayList<InspectionDocumentData>? = null
    private var mInspectionDocumentViewModel: InspectionDocumentViewModel? = null

    override fun getIntentValues() {
        super.getIntentValues()
        mInspectionListData = intent.getParcelableExtra("inspectionData");
    }

    override fun setLayoutResource(): Int {
        return R.layout.activity_inspection_upload_documents
    }

    override fun setValues() {
        /*val docStr = CommonUtils.loadJSONFromAsset(mContext, "document_list.json")
        val Document =
            Gson().fromJson<Inspection>(
                docStr,
                InspectionDocument::class.java
            ) as InspectionDocument*/
        CommonData.amazonS3Client = null
        try {
            val inspectionId = mInspectionListData?.id
            val createDir = File(Constants.DIRECTORY, "UCT-Inspection/Images/$inspectionId")

            if (!createDir.exists()) {
                createDir.mkdirs()

                val noMediaFile = File(createDir, ".nomedia")
                if (!noMediaFile.exists()) {
                    noMediaFile.createNewFile()
                }
            }

        } catch (e: Exception) {
            e.printStackTrace()
        }
        prepareDocumentList()
        setUpObserver()

    }


    private fun setUpObserver() {
        mInspectionDocumentViewModel = InspectionDocumentViewModel(mContext)
        mInspectionDocumentViewModel?.inspectionDocumentData?.observe(
            this,
            androidx.lifecycle.Observer {
                this.mDocumentList?.addAll(it.documentData)
                mUploadDocumentAdapter?.notifyDataSetChanged()

            })


        mInspectionDocumentViewModel?.getPickupChecklist(mInspectionListData?.id!!)


        mInspectionDocumentViewModel?.isInspectionDocumentSaveLiveData?.observe(
            this,
            androidx.lifecycle.Observer {
                if (it) {
                    CommonDialog.With(mActivity)
                        .showDataValidationFailureDialog("Thank You",
                            "You have successfully submitted pickup checklist.",
                            object : CommonDialog.IDataValidationFailureDialogClickCallback {

                                override fun onOkClick() {
                                    startInspectionListActivity()
                                }

                            })
                }
            })
    }

    private fun startInspectionListActivity() {
        val bdIntent = Intent(mContext, InspectionListActivity::class.java)
        bdIntent.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
        startActivity(bdIntent)
    }

    private fun prepareDocumentList() {
        this.mDocumentList = ArrayList()
//        this.mDocumentList?.addAll(documentList)
        mTxtTitle.setText(
            String.format(
                "%s %s %s",
                "Inspections",
                "-",
                mInspectionListData?.inspectionType
            )
        )
        mUploadDocumentAdapter = UploadDocumentAdapter(
            mContext, mDocumentList,
            object : UploadDocumentAdapter.IDocumentItemClickCallback {
                override fun onDocumentClick(position: Int) {
                    if (!(mDocumentList?.get(position)?.isSelected)!!) {
                        mDocumentList?.get(position)?.isSelected = true
                        mUploadDocumentAdapter?.notifyItemChanged(position)
                        mSelectedPosition = position
                        showUploadDocumentBottomFragment(
                            position,
                            mDocumentList?.get(position)?.documentName!!
                        )
                    } else {
                        mDocumentList?.get(position)?.documentPath = null
                        mDocumentList?.get(position)?.isSelected = false
                        mUploadDocumentAdapter?.notifyItemChanged(position)
                    }

                }

                override fun onDocumentDeleteClick(position: Int) {
                    mDocumentList?.get(position)?.documentPath = null
                    mDocumentList?.get(position)?.isSelected = false
                    mUploadDocumentAdapter?.notifyItemChanged(position)
                }

                override fun onDocumentImageClick(position: Int) {
                    val intent = Intent(mContext, ImageZoomHelperActivity::class.java)
                    intent.putExtra(
                        "imagePath",
                        mDocumentList!![position].documentPath
                    )
                    intent.putExtra(
                        "mHeaderPosition",
                        position
                    )
                    startActivity(intent)
                }
            })
        mRcDocument.setHasFixedSize(true)
        mRcDocument.layoutManager =
            LinearLayoutManager(mContext, LinearLayoutManager.VERTICAL, false)
        mRcDocument.itemAnimator = DefaultItemAnimator()
        mRcDocument.adapter = mUploadDocumentAdapter
//        ViewCompat.setNestedScrollingEnabled(mRcDocument, false)
    }

    private fun showUploadDocumentBottomFragment(position: Int, documentName: String) {
        val uploadDocumentBottomFragment =
            UploadDocumentBottomFragment.newInstance(this, position, documentName)
        uploadDocumentBottomFragment.show(supportFragmentManager, "")
    }

    override fun setToolbarId(): View? {
        return null
    }


    override fun registerClickListener(): Array<View>? {
        return arrayOf(mImgBack, mBtnSubmit)
    }

    override fun onClick(v: View?) {
        super.onClick(v)
        when (v?.id) {
            R.id.mImgBack -> {
                finish()
            }

            R.id.mBtnSubmit -> {
//                showUploadDocumentBottomFragment(1)
//                startActivity(Intent(mActivity, InspectionPickupActivity::class.java))

                val documentListValidation = ChecklistUtil.isValidDocumentList(mDocumentList)
                if (documentListValidation.isDataValid) {
                    Log.d("mDocumentListJson", Gson().toJson(mDocumentList))
                    mInspectionDocumentViewModel?.savePickupChecklist(
                        mInspectionListData?.id!!,
                        mDocumentList
                    )
                } else {
                    CommonDialog.With(mActivity)
                        .showDataValidationFailureDialog("",
                            "Please upload picture for " + documentListValidation.data.documentName + ".",
                            object : CommonDialog.IDataValidationFailureDialogClickCallback {

                                override fun onOkClick() {
                                    mRcDocument.getLayoutManager()
                                        ?.scrollToPosition(documentListValidation.nonValidPosition)

                                    mUploadDocumentAdapter?.setError(
                                        true,
                                        documentListValidation.nonValidPosition
                                    )
                                }

                            })
                }


            }
        }

    }

    override fun onCameraClick(position: Int) {
        val intent = Intent(mContext, CameraActivity::class.java)
        intent.putExtra(
            "mInspectionId",
            mInspectionListData?.id
        )
        intent.putExtra(
            "mHeaderPosition",
            position
        )
        intent.putExtra(
            "mFileName",
            String.format(
                "%s%s%s%s%s",
                mInspectionListData?.id,
                "_",
                CommonUtils.removeSpace(mDocumentList!![position].documentName),
                "_",
                System.currentTimeMillis()
            )
        )

        startActivityForResult(intent, Constants.INTENT_CAMERA_REQUEST_CODE)
    }

    override fun onImageGalleryClick(position: Int) {
        val pickPhoto = Intent(
            Intent.ACTION_PICK,
            android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI
        )
        startActivityForResult(pickPhoto, Constants.INTENT_GALLERY_REQUEST_CODE)
    }

    override fun onCloseClick(position: Int) {
        mDocumentList?.get(position)?.isSelected = false
        mUploadDocumentAdapter?.notifyItemChanged(position)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == Constants.INTENT_CAMERA_REQUEST_CODE) {
            if (resultCode == Activity.RESULT_OK && data != null && data.hasExtra("mHeaderPosition") && data.hasExtra(
                    "mImagePath"
                )
            ) {
                val position = data.getIntExtra("mHeaderPosition", 0)
                val image = data.getStringExtra("mImagePath")
//                mInspectionChecklistData.checklistHeader[position].images.add(image)
//                mChecklistHeaderAdapter?.notifyItemChanged(position)
//                mIChecklistPageClickCallback?.onInsertDataInDb(mPosition)
                this.mSelectedFile = image
                startAwsLoginTask()
            } else {
                mDocumentList?.get(mSelectedPosition)?.isSelected = false
                mUploadDocumentAdapter?.notifyItemChanged(mSelectedPosition)
            }

        } else if (requestCode == Constants.INTENT_GALLERY_REQUEST_CODE) {
            if (resultCode == Activity.RESULT_OK && data != null) {
                val selectedImage = data.data
                val filePathColumn = arrayOf(MediaStore.Images.Media.DATA)
                if (selectedImage != null) {
                    val cursor = contentResolver.query(
                        selectedImage,
                        filePathColumn, null, null, null
                    )
                    if (cursor != null) {
                        cursor.moveToFirst()

                        val columnIndex = cursor.getColumnIndex(filePathColumn[0])
                        val picturePath = cursor.getString(columnIndex)
//                        imageView.setImageBitmap(BitmapFactory.decodeFile(picturePath))
//                        Toast.makeText(mContext, picturePath, Toast.LENGTH_LONG).show()
                        this.mSelectedFile = picturePath
                        startAwsLoginTask()
                        cursor.close()
                    }
                }

            } else {
                mDocumentList?.get(mSelectedPosition)?.isSelected = false
                mUploadDocumentAdapter?.notifyItemChanged(mSelectedPosition)
            }
        }
    }


    private fun startAwsLoginTask() {
        if (CommonData.amazonS3Client == null)
            AwsLoginTask(mContext, this).execute()
        else
            onAwsDirectoryCreationSuccess(CommonData.amazonS3Client)
    }

    override fun onAwsLoginSuccess(amazonS3Client: AmazonS3Client?) {
        CommonData.amazonS3Client = amazonS3Client
        AwsDirectoryTask(
            mContext,
            this,
            mInspectionListData?.id!!,
            amazonS3Client!!
        ).execute()
    }

    override fun onAwsDirectoryCreationSuccess(amazonS3Client: AmazonS3Client?) {
        AwsSingleFileUploadTask(
            mContext,
            this,
            amazonS3Client!!,
            mSelectedFile!!,
            mInspectionListData?.id!!
        ).execute()
    }

    override fun onAwsFileUploadSuccess(uploadedFilePath: String?) {
        mDocumentList?.get(mSelectedPosition)?.documentPath = uploadedFilePath
        mUploadDocumentAdapter?.notifyItemChanged(mSelectedPosition)

    }

    override fun onAwsFileUploadSuccess(isUploaded: Boolean) {
    }
}

