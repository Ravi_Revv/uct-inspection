package com.uct.inspection.view.activity

import android.app.Activity
import android.content.Intent
import android.util.Log
import android.view.View
import androidx.viewpager.widget.ViewPager
import com.amazonaws.services.s3.AmazonS3Client
import com.google.gson.Gson
import com.uct.inspection.R
import com.uct.inspection.aws.AwsDirectoryTask
import com.uct.inspection.aws.AwsLoginTask
import com.uct.inspection.aws.AwsUploadTask
import com.uct.inspection.core.base.BaseActivity
import com.uct.inspection.db.repository.InspectionChecklistRepository
import com.uct.inspection.db.repository.InspectionE4ChecklistRepository
import com.uct.inspection.interfaces.IAwsCallback
import com.uct.inspection.model.checklist.InspectionChecklist
import com.uct.inspection.model.checklistE4.InspectionE4Checklist
import com.uct.inspection.utils.*
import com.uct.inspection.utils.enums.InspectionCardType
import com.uct.inspection.view.adapter.JobListPagerAdapter
import com.uct.inspection.view.fragment.InspectionChecklistFragment
import com.uct.inspection.view.fragment.InspectionE4ChecklistFragment
import com.uct.inspection.viewmodel.InspectionCheckListE4ViewModel
import com.uct.inspection.viewmodel.InspectionCheckListViewModel
import com.uct.inspection.viewmodel.InspectionVerdictViewModel
import kotlinx.android.synthetic.main.activity_inspection_e4_checklist.*
import java.io.File


class InspectionE4ChecklistActivity : BaseActivity(),
    InspectionE4ChecklistFragment.IChecklistPageClickCallback, IAwsCallback {
    private var mInspectionCheckListViewModel: InspectionCheckListViewModel? = null
    private var mInspectionVerdictViewModel: InspectionVerdictViewModel? = null
    private var mInspectionId: Int? = 0
    private var mInsertTS: Long? = 0
    private var mCarModel: String? = ""
    private var mRegNo: String? = ""
    private var mInspectionCardType: String? = ""
    private var mInspectionType: String? = ""
    private var isCallDb: Boolean = false
    private var mJobListPagerAdapter: JobListPagerAdapter? = null
    private var mInspectionE4Checklist: InspectionChecklist? = null

    public fun getInspectionId(): Int {
        return this.mInspectionId!!
    }

    public fun getInspectionCheckListViewModel(): InspectionCheckListViewModel? {
        return this.mInspectionCheckListViewModel!!
    }

    override fun getIntentValues() {
        super.getIntentValues()
        mInspectionId = intent.getIntExtra("mInspectionId", 0)
        mInsertTS = intent.getLongExtra("mInsertTS", 0)
        mCarModel = intent.getStringExtra("mCarModel")
        mRegNo = intent.getStringExtra("mRegNo")
        mInspectionType = intent.getStringExtra("mInspectionType")
        mInspectionCardType = intent.getStringExtra("mInspectionCardType")
    }

    override fun setLayoutResource(): Int {
        return R.layout.activity_inspection_e4_checklist
    }

    override fun setValues() {
        /*val docStr = CommonUtils.loadJSONFromAsset(mContext, "inspection_checklist_e4.json")
        val inspectionE4 =
            Gson().fromJson<InspectionChecklist>(
                docStr,
                InspectionChecklist::class.java
            ) as InspectionChecklist


        setUpTabLayout(inspectionE4)
        this.mInspectionE4Checklist = inspectionE4
        mInspectionE4Checklist?.inspectionId = mInspectionId!!
        mInspectionE4CheckListViewModel = InspectionCheckListViewModel(mContext)*/

        try {
            val createDir = File(Constants.DIRECTORY, "UCT-Inspection/Images/$mInspectionId")

            if (!createDir.exists()) {
                createDir.mkdirs()

                val noMediaFile = File(createDir, ".nomedia")
                if (!noMediaFile.exists()) {
                    noMediaFile.createNewFile()
                }
            }

        } catch (e: Exception) {
            e.printStackTrace()
        }

        setUpObserver()
        setUpObserverDb()
    }

    private fun setUpObserver() {
        mInspectionVerdictViewModel = InspectionVerdictViewModel(mContext)
        mInspectionCheckListViewModel = InspectionCheckListViewModel(mContext)
        mInspectionCheckListViewModel?.inspectionChecklistData?.observe(
            this,
            androidx.lifecycle.Observer {
                mInspectionE4Checklist = it
                mInspectionE4Checklist?.inspectionId = mInspectionId!!
                setUpTabLayout(it)
            })

        mInspectionCheckListViewModel?.isFinalSubmit?.observe(
            this,
            androidx.lifecycle.Observer {
                if (it) {
                        postVerdictData()
                }
            })


        mInspectionVerdictViewModel?.isInspectionSaveLiveData?.observe(
            this,
            androidx.lifecycle.Observer {
                if (it) {
                    startAwsLoginTask()

                } else {
                    CommonDialog.With(mActivity)
                        .showDataValidationFailureDialog("",
                            "Something went wrong",
                            object : CommonDialog.IDataValidationFailureDialogClickCallback {

                                override fun onOkClick() {

                                }

                            })
                }
            })


        mInspectionCheckListViewModel?.isFinalFileSubmit?.observe(
            this,
            androidx.lifecycle.Observer {
                if (it) {
                    CommonDialog.With(mActivity)
                        .showDataValidationFailureDialog("Thank You",
                            "You have successfully submitted all Files.",
                            object : CommonDialog.IDataValidationFailureDialogClickCallback {

                                override fun onOkClick() {
                                    startInspectionListActivity()
                                }

                            })
                } else {
                    CommonDialog.With(mActivity)
                        .showDataValidationFailureDialog("",
                            "Something went wrong in uploading files",
                            object : CommonDialog.IDataValidationFailureDialogClickCallback {

                                override fun onOkClick() {

                                }

                            })
                }
            })
    }

    private fun startInspectionListActivity() {
        val bdIntent = Intent(mContext, InspectionListActivity::class.java)
        bdIntent.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
        startActivity(bdIntent)
    }

    private fun postVerdictData() {
        mInspectionVerdictViewModel?.saveInspectionVerdict(
            mInspectionId!!,
            Constants.VERDICT_PASS,
            ""
        )
    }

    private fun startAwsLoginTask() {
        AwsLoginTask(mContext, this).execute()
    }


    override fun onAwsLoginSuccess(amazonS3Client: AmazonS3Client?) {
        AwsDirectoryTask(
            mContext,
            this,
            mInspectionId!!,
            amazonS3Client!!
        ).execute()
    }

    override fun onAwsDirectoryCreationSuccess(amazonS3Client: AmazonS3Client?) {
        AwsUploadTask(
            mContext,
            this,
            amazonS3Client!!,
            mInspectionE4Checklist!!
        ).execute()
    }

    override fun onAwsFileUploadSuccess(isUploaded: Boolean) {
        if (isUploaded) {
            InspectionChecklistRepository.insertAllInspectionChecklistData(
                mContext,
                mInspectionE4Checklist!!
            )

            mInspectionCheckListViewModel?.saveInspectionChecklistImage(
                mInspectionId!!,
                mInspectionE4Checklist,
                true
            )
        } else {
            CommonDialog.With(mActivity)
                .showDataValidationFailureDialog("",
                    "Something went wrong in uploading files",
                    object : CommonDialog.IDataValidationFailureDialogClickCallback {

                        override fun onOkClick() {

                        }

                    })

        }
    }

    override fun onAwsFileUploadSuccess(uploadedFilePath: String?) {

    }

    public fun startVerdictActivity() {
        val intent = Intent(mContext, VerdictInspectionActivity::class.java)
        intent.putExtra("mInspectionId", mInspectionId)
        intent.putExtra("mCarModel", mCarModel)
        intent.putExtra("mRegNo", mRegNo)
        startActivity(intent)
    }

    public fun setResultForRefurbJobList() {
        val intent = Intent()
//        intent.putExtra("mImagePath", path)
//        intent.putExtra("mHeaderPosition", mHeaderPosition)
        setResult(RESULT_OK, intent)
        finish()
    }


    private fun setUpObserverDb() {
        mInspectionCheckListViewModel?.getInspectionChecklistDataFromDb(mInspectionId!!)?.observe(
            this,
            androidx.lifecycle.Observer {
                if (!isCallDb) {
                    isCallDb = true
                    if (it != null) {
                        if (it.insertTS > mInsertTS!!) {
                            this.mInspectionE4Checklist = it
//                        Toast.makeText(mContext,"setUpObserverDb",Toast.LENGTH_SHORT).show()
                            setUpTabLayout(it)
                        } else {
                            InspectionE4ChecklistRepository.deleteInspectionChecklistById(
                                mContext,
                                mInspectionId!!
                            )
                            mInspectionCheckListViewModel?.getInspectionChecklist(mInspectionId!!)
                        }
                    } else {
                        mInspectionCheckListViewModel?.getInspectionChecklist(mInspectionId!!)
                    }
                }

            })

    }

    override fun setToolbarId(): View? {
        return null
    }


    override fun registerClickListener(): Array<View>? {
        return arrayOf(mImgBack)
    }

    override fun onClick(v: View?) {
        super.onClick(v)
        when (v?.id) {
            R.id.mImgBack -> {
                finish()
            }

        }

    }

    private fun setUpTabLayout(inspectionChecklist: InspectionChecklist) {
        mTxtTitle.setText(String.format("%s %s %s", "Inspections", "-", mInspectionType))
        mJobListPagerAdapter = JobListPagerAdapter(supportFragmentManager)
        for (i in inspectionChecklist.inspectionChecklistData.indices) {
            mJobListPagerAdapter?.addFragment(
                InspectionE4ChecklistFragment.display(
                    this,
                    inspectionChecklist.inspectionChecklistData[i],
                    i
                ), inspectionChecklist.inspectionChecklistData[i].headerName
            )
        }
        mPagerPartE4.adapter = mJobListPagerAdapter
        mTabPartE4.setupWithViewPager(mPagerPartE4)
        mPagerPartE4.addOnPageChangeListener(object : ViewPager.OnPageChangeListener {
            override fun onPageScrollStateChanged(state: Int) {
//                Log.d("mPagerPart", "onPageScrollStateChanged :- " + state.toString())
            }

            override fun onPageScrolled(
                position: Int,
                positionOffset: Float,
                positionOffsetPixels: Int
            ) {
//                Log.d("mPagerPart", "onPageScrolled :- " + position.toString())
            }

            override fun onPageSelected(position: Int) {
                Log.d("mPagerPart", "onPageSelected :- " + position.toString())
                if (position > 0) {
                    val chkData =
                        ChecklistUtil.validateChecklistDataStatus(inspectionChecklist, position)
                    if (!chkData.isDataValid) {
                        CommonDialog.With(mActivity)
                            .showDataValidationFailureDialog("",
                                "Data validation failed.\nPlease check again before page change.",
                                object : CommonDialog.IDataValidationFailureDialogClickCallback {

                                    override fun onOkClick() {
                                        mPagerPartE4.setCurrentItem(
                                            chkData.nonValidTabPosition,
                                            true
                                        )
                                        val fragment =
                                            mJobListPagerAdapter?.getItem(mPagerPartE4.getCurrentItem()) as InspectionE4ChecklistFragment
                                        fragment.redirectToIncompleteItem(chkData)
                                    }

                                })
                    } else {
                        mInspectionCheckListViewModel?.insertInspectionChecklistData(
                            mInspectionE4Checklist,
                            position - 1
                        )

                        if (InternetConnectionStatus.with(mActivity).isOnline()) {
                            mInspectionCheckListViewModel?.saveInspectionChecklist(
                                mInspectionId!!,
                                mInspectionE4Checklist,
                                false
                            )
                        }
                    }
                }
            }
        })
    }

    override fun onNext(position: Int) {
//        Log.d("checklistJson", Gson().toJson(mInspectionE4Checklist))
        mInspectionCheckListViewModel?.insertInspectionChecklistData(
            mInspectionE4Checklist,
            position
        )
//        Toast.makeText(mContext, "data inserted successfully", Toast.LENGTH_SHORT).show()
        if (mPagerPartE4.currentItem == mInspectionE4Checklist?.getInspectionChecklistData()?.size!! - 1) {
            Log.d("mPagerPart", "onPageSelected :- " + "last")
            if (InternetConnectionStatus.with(mActivity).isOnline()) {
                mInspectionCheckListViewModel?.saveInspectionChecklist(
                    mInspectionId!!,
                    mInspectionE4Checklist,
                    true
                )
            } else {
                CommonDialog.With(mActivity)
                    .showDataValidationFailureDialog("",
                        "Check your internet connection and try again.",
                        object : CommonDialog.IDataValidationFailureDialogClickCallback {

                            override fun onOkClick() {

                            }

                        })
            }

        } else {
            Log.d("mPagerPart", "setCurrentItem")
            mPagerPartE4.setCurrentItem(mPagerPartE4.currentItem + 1, true)
        }

    }


    override fun onInsertDataInDb(position: Int) {
        mInspectionCheckListViewModel?.insertInspectionChecklistData(
            mInspectionE4Checklist,
            position
        )
    }


    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == Activity.RESULT_OK && requestCode == Constants.INTENT_CAMERA_REQUEST_CODE) {
            val fragment =
                mJobListPagerAdapter?.getItem(mPagerPartE4.getCurrentItem()) as InspectionE4ChecklistFragment
            fragment.onActivityResult(requestCode, resultCode, data)
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        mInspectionCheckListViewModel?.reset()
    }
}
