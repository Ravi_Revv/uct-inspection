package com.uct.inspection.view.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.uct.inspection.R;
import com.uct.inspection.interfaces.IChecklistClickCallback;
import com.uct.inspection.model.checklist.ChecklistIssue;
import com.uct.inspection.model.checklist.ChecklistSubIssue;

import java.util.ArrayList;
import java.util.List;

public class ChecklistIssueDataAdapter extends RecyclerView.Adapter<ChecklistIssueDataAdapter.ViewHolder> {


    private int mParentPosition = -1;
    private ArrayList<ChecklistIssue> mChecklistIssue;
    private String allowedSelection;
    private boolean isUnAssured = false;
    private IChecklistClickCallback mIChecklistClickCallback;

    public void setDefaultSubIssues(List<ChecklistSubIssue> mDefaultSubIssues) {
        this.mDefaultSubIssues = mDefaultSubIssues;
    }

    List<ChecklistSubIssue> mDefaultSubIssues = null;

    public void setUnAssured(boolean unAssured) {
        this.isUnAssured = unAssured;
    }


    public void setIChecklistClickCallback(IChecklistClickCallback mIChecklistClickCallback) {
        this.mIChecklistClickCallback = mIChecklistClickCallback;
    }

    public void setChecklistIssueClick(IChecklistIssueClick mChecklistIssueClick) {
        this.mChecklistIssueClick = mChecklistIssueClick;
    }

    private IChecklistIssueClick mChecklistIssueClick;

    public ChecklistIssueDataAdapter(ArrayList<ChecklistIssue> mChecklistIssue) {
        this.mChecklistIssue = mChecklistIssue;
    }

    public void setAllowedSelection(String allowedSelection) {
        this.allowedSelection = allowedSelection;
    }


    public void setParentPosition(int mParentPosition) {
        this.mParentPosition = mParentPosition;
    }


    @NonNull
    @Override
    public ChecklistIssueDataAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup mParent, int viewType) {

        return new ViewHolder(LayoutInflater.from(mParent.getContext()).inflate(R.layout.item_checklist_issue, mParent, false));
    }


    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.mTxtChecklistIssue.setText(mChecklistIssue.get(holder.getAdapterPosition()).getIssueName());
        holder.mLytChecklistIssue.setSelected(mChecklistIssue.get(holder.getAdapterPosition()).getSelected());
        /*if () {
            holder.mLytChecklistIssue.setSelected(true);
        } else {
            holder.mLytChecklistIssue.setSelected(false);
        }*/

    }


    @Override
    public int getItemCount() {
        return this.mChecklistIssue.size();
    }


    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        LinearLayout mLytChecklistIssue;
        TextView mTxtChecklistIssue;

        public ViewHolder(View v) {
            super(v);
            mLytChecklistIssue = v.findViewById(R.id.mLytChecklistIssue);
            mTxtChecklistIssue = v.findViewById(R.id.mTxtChecklistIssue);
            mLytChecklistIssue.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            switch (v.getId()) {
                case R.id.mLytChecklistIssue:
                    if (allowedSelection.equals("SINGLE")) {
                        if (mChecklistIssue.get(getAdapterPosition()).getSelected()) {
                            for (ChecklistIssue checklistIssue : mChecklistIssue) {
                                checklistIssue.setSelected(false);
                            }
                            mChecklistIssue.get(getAdapterPosition()).setSelected(false);
                            if (mChecklistIssue.get(getAdapterPosition()).getIssueName().equalsIgnoreCase("ok") && mDefaultSubIssues != null && mDefaultSubIssues.size() > 0)
                                mDefaultSubIssues.get(0).setSelected(false);
                        } else {
                            for (ChecklistIssue checklistIssue : mChecklistIssue) {
                                checklistIssue.setSelected(false);
                            }
                            mChecklistIssue.get(getAdapterPosition()).setSelected(true);
                            if (mChecklistIssue.get(getAdapterPosition()).getIssueName().equalsIgnoreCase("ok") && mDefaultSubIssues != null && mDefaultSubIssues.size() > 0)
                                mDefaultSubIssues.get(0).setSelected(true);

                        }

                    } else {
                        if (mChecklistIssue.get(getAdapterPosition()).getIssueType() == 0 && !mChecklistIssue.get(getAdapterPosition()).getSelected()) {
                            for (ChecklistIssue checklistIssue : mChecklistIssue) {
                                checklistIssue.setSelected(false);
                            }

                            mChecklistIssue.get(getAdapterPosition()).setSelected(true);
                            if (mChecklistIssue.get(getAdapterPosition()).getIssueName().equalsIgnoreCase("ok") && mDefaultSubIssues != null && mDefaultSubIssues.size() > 0)
                                mDefaultSubIssues.get(0).setSelected(true);

                        } else if (mChecklistIssue.get(getAdapterPosition()).getIssueType() == 0 && mChecklistIssue.get(getAdapterPosition()).getSelected()) {
                            for (ChecklistIssue checklistIssue : mChecklistIssue) {
                                checklistIssue.setSelected(false);
                            }
                            mChecklistIssue.get(getAdapterPosition()).setSelected(false);
                            if (mChecklistIssue.get(getAdapterPosition()).getIssueName().equalsIgnoreCase("ok") && mDefaultSubIssues != null && mDefaultSubIssues.size() > 0)
                                mDefaultSubIssues.get(0).setSelected(false);
                        } else {
                            for (ChecklistIssue checklistIssue : mChecklistIssue) {
                                if (checklistIssue.getIssueType() == 0 && checklistIssue.getSelected())
                                    checklistIssue.setSelected(false);
                            }
                            mChecklistIssue.get(getAdapterPosition()).setSelected(!mChecklistIssue.get(getAdapterPosition()).getSelected());
                            if (mDefaultSubIssues != null && mDefaultSubIssues.size() > 0)
                                mDefaultSubIssues.get(0).setSelected(false);
                        }
                    }


                    if (mIChecklistClickCallback != null) {
//                        Log.d("SubIssueList", "ChecklistIssueDataAdapter :- "+String.valueOf(mChecklistIssue.get(getAdapterPosition()).getChecklistSubIssue().size()));
//                        mChecklistIssueClick.onChecklistIssueClick(new ArrayList<ChecklistSubIssue>(mChecklistIssue.get(getAdapterPosition()).getChecklistSubIssue()), getAdapterPosition(), mChecklistIssue.get(getAdapterPosition()).getSelected());
                        mIChecklistClickCallback.onChecklistIssueClick(mParentPosition);
                    }

                    if (mIChecklistClickCallback != null && isUnAssured && mChecklistIssue.get(getAdapterPosition()).getSelected() && mChecklistIssue.get(getAdapterPosition()).getIssueType() == 1) {
//                        Log.d("SubIssueList", "ChecklistIssueDataAdapter :- "+String.valueOf(mChecklistIssue.get(getAdapterPosition()).getChecklistSubIssue().size()));
//                        mChecklistIssueClick.onChecklistIssueClick(new ArrayList<ChecklistSubIssue>(mChecklistIssue.get(getAdapterPosition()).getChecklistSubIssue()), getAdapterPosition(), mChecklistIssue.get(getAdapterPosition()).getSelected());
                        mIChecklistClickCallback.onUnAssuredChecklistIssueClick();
                    }

//                    notifyItemChanged(getAdapterPosition());
                    break;
            }

        }
    }

    public interface IChecklistIssueClick {
        //        void onChecklistIssueClick(ArrayList<ChecklistSubIssue> checklistSubIssues, int position, boolean isSelected);
        void onChecklistIssueClick();
    }
} 