package com.uct.inspection.view.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.uct.inspection.R;
import com.uct.inspection.interfaces.IRefurbDocumentClickCallback;
import com.uct.inspection.model.inspectionlist.InspectionFieldData;
import com.uct.inspection.utils.CommonUtils;

import java.util.ArrayList;

public class EditInfoAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private Context mContext = null;
    private ArrayList<InspectionFieldData> mInspectionFieldData;
    private IRefurbDocumentClickCallback mIRefurbDocumentClickCallback;


    public EditInfoAdapter(Context mContext, ArrayList<InspectionFieldData> inspectionFieldData) {
        this.mContext = mContext;
        this.mInspectionFieldData = inspectionFieldData;
    }


    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup mParent, int viewType) {
        return new EditInfoViewHolder(LayoutInflater.from(mParent.getContext()).inflate(R.layout.item_edit_info, mParent, false));

    }


    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        final EditInfoViewHolder viewHolder = (EditInfoViewHolder) holder;
        viewHolder.mEdtInfo.setHint(mInspectionFieldData.get(viewHolder.getAdapterPosition()).getName());


    }


    @Override
    public int getItemCount() {
        return mInspectionFieldData.size();

    }


    public class EditInfoViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        EditText mEdtInfo;
        LinearLayout mLytEdtInfo;

        public EditInfoViewHolder(View v) {
            super(v);
            mEdtInfo = v.findViewById(R.id.mEdtInfo);
            mLytEdtInfo = v.findViewById(R.id.mLytEdtInfo);
//            mBtnAddImage.setOnClickListener(this);
//            ClickGuard.guard(mBtnAddImage);

        }


        @Override
        public void onClick(View v) {
            switch (v.getId()) {
                case R.id.mBtnAddImage:
                    if (mIRefurbDocumentClickCallback != null)
                        mIRefurbDocumentClickCallback.onRefurbDocumentAddImageClick(getAdapterPosition());
                    break;
                default:
                    break;
            }
        }
    }


}