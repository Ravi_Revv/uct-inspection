package com.uct.inspection.view.adapter;

import android.content.Context;
import android.os.Handler;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.flexbox.FlexDirection;
import com.google.android.flexbox.FlexboxLayoutManager;
import com.google.android.flexbox.JustifyContent;
import com.uct.inspection.R;
import com.uct.inspection.interfaces.IChecklistClickCallback;
import com.uct.inspection.model.checklist.ChecklistHeader;
import com.uct.inspection.model.checklist.ChecklistIssue;
import com.uct.inspection.model.checklist.ChecklistSubIssue;
import com.uct.inspection.utils.ClickGuard;
import com.uct.inspection.utils.ItemDecoratorImageGridView;

import java.util.ArrayList;
import java.util.List;

public class ChecklistHeaderAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private Context mContext = null;
    private ArrayList<ChecklistHeader> mChecklistHeader;
    private IChecklistClickCallback mIChecklistClickCallback;
    private int mErrorPosition = -1;
    private boolean isError;

    public void setIChecklistClickCallback(IChecklistClickCallback mIChecklistClickCallback) {
        this.mIChecklistClickCallback = mIChecklistClickCallback;
    }

    public ChecklistHeaderAdapter(Context mContext, ArrayList<ChecklistHeader> checklistHeader) {
        this.mContext = mContext;
        this.mChecklistHeader = checklistHeader;
    }


    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup mParent, int viewType) {
        return new IssueViewHolder(LayoutInflater.from(mParent.getContext()).inflate(R.layout.item_checklist_header, mParent, false));

    }


    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        final IssueViewHolder viewHolder = (IssueViewHolder) holder;

        if (isError && position == mErrorPosition) {
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    isError = false;
                    mErrorPosition = -1;
                    viewHolder.mLytChecklistItem.setBackgroundColor(mContext.getResources().getColor(R.color.colorTransparent));

                }
            }, 1000);

            viewHolder.mLytChecklistItem.setBackgroundColor(mContext.getResources().getColor(R.color.colorError));
        }


        viewHolder.mTxtChecklistHeader.setText(mChecklistHeader.get(viewHolder.getAdapterPosition()).getSubPartName());
        prepareIssueList(viewHolder.mRcIssue, viewHolder.getAdapterPosition(), new ArrayList<ChecklistIssue>(mChecklistHeader.get(viewHolder.getAdapterPosition()).getChecklistIssue()));
        prepareCheckListImage(viewHolder.mRcImage, viewHolder.getAdapterPosition(), new ArrayList<String>(mChecklistHeader.get(viewHolder.getAdapterPosition()).getImages()));
        prepareSubIssueList(viewHolder.mRcSubIssue, viewHolder.getAdapterPosition(), new ArrayList<ChecklistSubIssue>(mChecklistHeader.get(viewHolder.getAdapterPosition()).getAllSubIssues()));
        viewHolder.mEdtComment.setText(mChecklistHeader.get(holder.getAdapterPosition()).getComments());
        if (mChecklistHeader.get(holder.getAdapterPosition()).isFileRequired())
            viewHolder.mBtnAddImage.setVisibility(View.VISIBLE);
        else viewHolder.mBtnAddImage.setVisibility(View.GONE);

        if (mChecklistHeader.get(holder.getAdapterPosition()).getLastNFIStatus() != null && !mChecklistHeader.get(holder.getAdapterPosition()).getLastNFIStatus().isEmpty()) {
            viewHolder.mTxtLastNFIStatus.setVisibility(View.VISIBLE);
            viewHolder.mTxtLastNFIStatus.setText(mChecklistHeader.get(holder.getAdapterPosition()).getLastNFIStatus());
        } else {
            viewHolder.mTxtLastNFIStatus.setVisibility(View.GONE);
        }

    }


    @Override
    public int getItemCount() {
        return mChecklistHeader.size();

    }

    public void setError(boolean isError, int position) {
        this.isError = isError;
        this.mErrorPosition = position;
        notifyDataSetChanged();
    }

    public class IssueViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        TextView mTxtChecklistHeader, mTxtLastNFIStatus;
        RecyclerView mRcIssue, mRcSubIssue, mRcImage;
        Button mBtnAddImage;
        EditText mEdtComment;
        LinearLayout mLytChecklistItem;

        public IssueViewHolder(View v) {
            super(v);
            mTxtChecklistHeader = v.findViewById(R.id.mTxtChecklistHeader);
            mRcIssue = v.findViewById(R.id.mRcIssue);
            mRcSubIssue = v.findViewById(R.id.mRcSubIssue);
            mRcImage = v.findViewById(R.id.mRcImage);
            mBtnAddImage = v.findViewById(R.id.mBtnAddImage);
            mEdtComment = v.findViewById(R.id.mEdtComment);
            mLytChecklistItem = v.findViewById(R.id.mLytChecklistItem);
            mTxtLastNFIStatus = v.findViewById(R.id.mTxtLastNFIStatus);
            mBtnAddImage.setOnClickListener(this);
            ClickGuard.guard(mBtnAddImage);


            mEdtComment.addTextChangedListener(new TextWatcher() {

                @Override
                public void afterTextChanged(Editable s) {
                }

                @Override
                public void beforeTextChanged(CharSequence s, int start,
                                              int count, int after) {
                }

                @Override
                public void onTextChanged(CharSequence s, int start,
                                          int before, int count) {

                    if (mEdtComment.getText() != null && !mEdtComment.getText().toString().trim().isEmpty() && !mChecklistHeader.get(getAdapterPosition()).getComments().equalsIgnoreCase(mEdtComment.getText().toString().trim())) {
                        mChecklistHeader.get(getAdapterPosition()).setComments(mEdtComment.getText().toString().trim());
                        Log.d("onTextChanged", "comment");
                        if (mIChecklistClickCallback != null)
                            mIChecklistClickCallback.onChecklistCommentChange(getAdapterPosition());
                    } else if (mEdtComment.getText() != null && mEdtComment.getText().toString().trim().isEmpty() && !mChecklistHeader.get(getAdapterPosition()).getComments().equalsIgnoreCase(mEdtComment.getText().toString().trim())) {
                        mChecklistHeader.get(getAdapterPosition()).setComments("");
                        Log.d("onTextChanged", "comment");
                        if (mIChecklistClickCallback != null)
                            mIChecklistClickCallback.onChecklistCommentChange(getAdapterPosition());
                    }

                    /*if (mEdtCost.getText().toString().length() > 0 && Integer.parseInt(mEdtCost.getText().toString()) >= mCheckSubIssues.get(getAdapterPosition()).getMinCost() && Integer.parseInt(mEdtCost.getText().toString()) <= mCheckSubIssues.get(getAdapterPosition()).getMaxCost()) {
                        mCheckSubIssues.get(getAdapterPosition()).setCost(Integer.parseInt(mEdtCost.getText().toString()));
                    } else {
                        String str = mEdtCost.getText().toString();
                        if (str.length() != 0) {
                            str = str.substring(0, str.length() - 1);
                            mEdtCost.setText(str);
                        }
                    }*/
                }
            });

        }


        @Override
        public void onClick(View v) {
            switch (v.getId()) {
                case R.id.mBtnAddImage:
                    if (mIChecklistClickCallback != null)
                        mIChecklistClickCallback.onChecklistAddImageClick(getAdapterPosition());
                    break;
                default:
                    break;
            }
        }
    }

    private void prepareIssueList(RecyclerView recyclerView, final int mParentPosition, final ArrayList<ChecklistIssue> mChecklistIssue) {
        final ChecklistIssueDataAdapter adapter = new ChecklistIssueDataAdapter(mChecklistIssue);
        adapter.setAllowedSelection(mChecklistHeader.get(mParentPosition).getAllowedSelection());
        adapter.setUnAssured(mChecklistHeader.get(mParentPosition).isUnAssured());
        adapter.setDefaultSubIssues(mChecklistHeader.get(mParentPosition).getDefaultSubIssues());
        /*adapter.setChecklistIssueClick(new ChecklistIssueDataAdapter.IChecklistIssueClick() {
         *//*@Override
            public void onChecklistIssueClick(ArrayList<ChecklistSubIssue> checklistSubIssues, int position, boolean isSelected) {
                Log.d("SubIssueList", "onChecklistIssueClick :- " + String.valueOf(checklistSubIssues.size()));

//                addRemoveSubIssue(mParentPosition, position, isSelected, checklistSubIssues);
                notifyItemChanged(mParentPosition);
            }*//*
            @Override
            public void onChecklistIssueClick() {
                notifyItemChanged(mParentPosition);
            }
        });*/
        adapter.setIChecklistClickCallback(mIChecklistClickCallback);
        adapter.setParentPosition(mParentPosition);
        // Create the FlexboxLayoutMananger, only flexbox library version 0.3.0 or higher support.
        FlexboxLayoutManager flexboxLayoutManager = new FlexboxLayoutManager(mContext);
        // Set flex direction.
        flexboxLayoutManager.setFlexDirection(FlexDirection.ROW);
        // Set JustifyContent.
        flexboxLayoutManager.setJustifyContent(JustifyContent.FLEX_START);
        recyclerView.setLayoutManager(flexboxLayoutManager);
        recyclerView.setHasFixedSize(true);
        recyclerView.setAdapter(adapter);

//        ViewCompat.setNestedScrollingEnabled(recyclerView, false);
    }

    private void prepareSubIssueList(RecyclerView recyclerView, int mParentPosition, ArrayList<ChecklistSubIssue> mCheckSubIssues) {
        ChecklistSubIssueDataAdapter adapter = new ChecklistSubIssueDataAdapter(mCheckSubIssues);
        adapter.setParentPosition(mParentPosition);
        adapter.setIChecklistClickCallback(mIChecklistClickCallback);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(mContext, LinearLayoutManager.VERTICAL, false));
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(adapter);
//        ViewCompat.setNestedScrollingEnabled(recyclerView, false);
    }

    private void prepareCheckListImage(RecyclerView recyclerView, int mParentPosition, ArrayList<String> imageList) {
        ChecklistImageAdapter adapter = new ChecklistImageAdapter(mContext, imageList);
        adapter.setParentPosition(mParentPosition);
        adapter.setIChecklistClickCallback(mIChecklistClickCallback);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new GridLayoutManager(mContext, 4));
        /*recyclerView.addItemDecoration(new ItemDecoratorImageGridView(
                mContext.getResources().getDimensionPixelSize(R.dimen._10sdp),
                4));*/
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(adapter);
//        ViewCompat.setNestedScrollingEnabled(recyclerView, false);
    }

}