package com.uct.inspection.view.adapter;

import android.content.Context;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.squareup.picasso.Picasso;
import com.uct.inspection.R;
import com.uct.inspection.model.document.InspectionDocumentData;
import com.uct.inspection.utils.ClickGuard;

import java.io.File;
import java.util.ArrayList;

public class UploadDocumentAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private Context mContext = null;
    private ArrayList<InspectionDocumentData> mDocumentList = null;
    private IDocumentItemClickCallback mDocumentItemClickCallback;
    private int mErrorPosition = -1;
    private boolean isError;


    public UploadDocumentAdapter(Context mContext, ArrayList<InspectionDocumentData> documentList, IDocumentItemClickCallback mDocumentItemClickCallback) {
        this.mContext = mContext;
        this.mDocumentList = documentList;
        this.mDocumentItemClickCallback = mDocumentItemClickCallback;
    }


    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup mParent, int viewType) {
        return new UploadDocumentAdapterViewHolder(LayoutInflater.from(mParent.getContext()).inflate(R.layout.item_upload_document, mParent, false));

    }


    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        final UploadDocumentAdapterViewHolder viewHolder = (UploadDocumentAdapterViewHolder) holder;
        //"https
//        if (mImageList.get(holder.getAdapterPosition()).contains("https")) {
//            Picasso.get().load(mImageList.get(holder.getAdapterPosition())).fit().centerCrop().
//                    placeholder(R.drawable.ic_car_img_placeholder).error(R.drawable.ic_car_img_placeholder).into(viewHolder.mImgChecklist);
//        } else {
//            Picasso.get().load(new File(mImageList.get(holder.getAdapterPosition()))).fit().centerCrop().
//                    placeholder(R.drawable.ic_car_img_placeholder).error(R.drawable.ic_car_img_placeholder).into(viewHolder.mImgChecklist);
//        }

        if (isError && position == mErrorPosition) {
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    isError = false;
                    mErrorPosition = -1;
                    viewHolder.mLytDocumentItem.setBackgroundColor(mContext.getResources().getColor(R.color.colorTransparent));

                }
            }, 1000);

            viewHolder.mLytDocumentItem.setBackgroundColor(mContext.getResources().getColor(R.color.colorError));
        }


        if (mDocumentList.get(holder.getAdapterPosition()).getDocumentPath() != null && !mDocumentList.get(holder.getAdapterPosition()).getDocumentPath().isEmpty()) {
            mDocumentList.get(holder.getAdapterPosition()).setSelected(true);
            viewHolder.mFrameImage.setVisibility(View.VISIBLE);
            if (mDocumentList.get(holder.getAdapterPosition()).getDocumentPath().contains("https")) {
                Picasso.get().load(mDocumentList.get(holder.getAdapterPosition()).getDocumentPath()).fit().centerCrop().
                        placeholder(R.drawable.ic_car_img_placeholder).error(R.drawable.ic_car_img_placeholder).into(viewHolder.mImgDocument);
            } else {
                Picasso.get().load(new File(mDocumentList.get(holder.getAdapterPosition()).getDocumentPath())).fit().centerCrop().
                        placeholder(R.drawable.ic_car_img_placeholder).error(R.drawable.ic_car_img_placeholder).into(viewHolder.mImgDocument);
            }
        } else {
//            mDocumentList.get(holder.getAdapterPosition()).setSelected(false);
            viewHolder.mFrameImage.setVisibility(View.INVISIBLE);
        }
        viewHolder.mCbDocumentName.setText(mDocumentList.get(holder.getAdapterPosition()).getDocumentName());
        viewHolder.mCbDocumentName.setChecked(mDocumentList.get(holder.getAdapterPosition()).isSelected());


    }


    @Override
    public int getItemCount() {
        return this.mDocumentList.size();
    }

    public void setError(boolean isError, int position) {
        this.isError = isError;
        this.mErrorPosition = position;
        notifyDataSetChanged();
    }

    public class UploadDocumentAdapterViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        CheckBox mCbDocumentName;
        ImageView mImgDocument, mImgDelete;
        FrameLayout mFrameImage;
        LinearLayout mLytDocumentItem;

        public UploadDocumentAdapterViewHolder(View v) {
            super(v);
            mCbDocumentName = v.findViewById(R.id.mCbDocumentName);
            mImgDocument = v.findViewById(R.id.mImgDocument);
            mImgDelete = v.findViewById(R.id.mImgDelete);
            mFrameImage = v.findViewById(R.id.mFrameImage);
            mLytDocumentItem = v.findViewById(R.id.mLytDocumentItem);
            mImgDelete.setOnClickListener(this);
            mImgDocument.setOnClickListener(this);
            ClickGuard.guard(mImgDocument);
            ClickGuard.guard(mImgDelete);
            mCbDocumentName.setOnClickListener(this);
            ClickGuard.guard(mCbDocumentName);
        }


        @Override
        public void onClick(View v) {
            switch (v.getId()) {
                case R.id.mImgDelete:
                    if (mDocumentItemClickCallback != null)
                        mDocumentItemClickCallback.onDocumentDeleteClick(getAdapterPosition());
                    break;
                case R.id.mImgDocument:
                    if (mDocumentItemClickCallback != null)
                        mDocumentItemClickCallback.onDocumentImageClick(getAdapterPosition());
                    break;
                case R.id.mCbDocumentName:
                    if (mDocumentItemClickCallback != null)
                        mDocumentItemClickCallback.onDocumentClick(getAdapterPosition());
                    break;
                default:
                    break;
            }
        }
    }

    public interface IDocumentItemClickCallback {
        void onDocumentClick(int position);

        void onDocumentDeleteClick(int position);

        void onDocumentImageClick(int position);
    }

}