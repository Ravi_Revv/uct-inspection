package com.uct.inspection.view.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.squareup.picasso.Picasso;
import com.uct.inspection.R;
import com.uct.inspection.interfaces.IChecklistClickCallback;
import com.uct.inspection.interfaces.IChecklistE4ClickCallback;
import com.uct.inspection.interfaces.IRefurbDocumentClickCallback;
import com.uct.inspection.utils.ClickGuard;

import java.io.File;
import java.util.ArrayList;

public class ChecklistE4ImageAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private Context mContext = null;
    private ArrayList<String> mImageList;
    private int mParentPosition = -1;
    private IChecklistE4ClickCallback mIChecklistE4ClickCallback;
    private IRefurbDocumentClickCallback mIRefurbDocumentClickCallback;

    public void setRefurbDocumentClickCallback(IRefurbDocumentClickCallback mIRefurbDocumentClickCallback) {
        this.mIRefurbDocumentClickCallback = mIRefurbDocumentClickCallback;
    }


    public ChecklistE4ImageAdapter(Context mContext, ArrayList<String> imageList) {
        this.mContext = mContext;
        this.mImageList = imageList;
    }

    public void setIChecklistClickCallback(IChecklistE4ClickCallback mIChecklistClickCallback) {
        this.mIChecklistE4ClickCallback = mIChecklistClickCallback;
    }

    public void setParentPosition(int mParentPosition) {
        this.mParentPosition = mParentPosition;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup mParent, int viewType) {
        return new ChecklistImageViewHolder(LayoutInflater.from(mParent.getContext()).inflate(R.layout.item_checklist_image, mParent, false));

    }


    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        ChecklistImageViewHolder viewHolder = (ChecklistImageViewHolder) holder;
        //"https
        if (mImageList.get(holder.getAdapterPosition()).contains("https")) {
            Picasso.get().load(mImageList.get(holder.getAdapterPosition())).fit().centerCrop().
                    placeholder(R.drawable.ic_car_img_placeholder).error(R.drawable.ic_car_img_placeholder).into(viewHolder.mImgChecklist);
        } else {
            Picasso.get().load(new File(mImageList.get(holder.getAdapterPosition()))).fit().centerCrop().
                    placeholder(R.drawable.ic_car_img_placeholder).error(R.drawable.ic_car_img_placeholder).into(viewHolder.mImgChecklist);
        }


    }


    @Override
    public int getItemCount() {
        return this.mImageList.size();
    }


    public class ChecklistImageViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        ImageView mImgChecklist, mImgDelete;

        public ChecklistImageViewHolder(View v) {
            super(v);
            mImgChecklist = v.findViewById(R.id.mImgChecklist);
            mImgDelete = v.findViewById(R.id.mImgDelete);
            mImgDelete.setOnClickListener(this);
            mImgChecklist.setOnClickListener(this);
            ClickGuard.guard(mImgChecklist);
            ClickGuard.guard(mImgDelete);
        }


        @Override
        public void onClick(View v) {
            switch (v.getId()) {
                case R.id.mImgDelete:
                    if (mIChecklistE4ClickCallback != null)
                        mIChecklistE4ClickCallback.onChecklistDeleteImageClick(mParentPosition, getAdapterPosition());
                    break;

                case R.id.mImgChecklist:
                    if (mIChecklistE4ClickCallback != null)
                        mIChecklistE4ClickCallback.onChecklistImageClick(mParentPosition, getAdapterPosition());

                    break;
                default:
                    break;
            }
        }
    }
}