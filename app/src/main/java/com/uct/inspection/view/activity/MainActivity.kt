package com.uct.inspection.view.activity

import android.content.Intent
import android.view.View
import android.widget.Toast
import com.uct.inspection.R
import com.uct.inspection.camera.CameraActivity
import com.uct.inspection.core.base.BaseActivity
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : BaseActivity() {
    override fun setLayoutResource(): Int {
        return R.layout.activity_main
    }

    override fun setValues() {

    }

    override fun setToolbarId(): View? {
        return null
    }


    override fun registerClickListener(): Array<View>? {
        return arrayOf(mTxt)
    }

    override fun onClick(v: View?) {
        super.onClick(v)
        when (v?.id) {
            R.id.mTxt -> {
//                LoadingDialog().showLoadingDialog(mContext, "")
                Toast.makeText(mContext, "clicked me", Toast.LENGTH_LONG).show()
//                AwsLoginTask(mContext).execute()
                startActivity(Intent(mContext, CameraActivity::class.java))
            }
        }

    }
}
