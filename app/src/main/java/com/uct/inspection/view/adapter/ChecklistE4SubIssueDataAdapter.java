package com.uct.inspection.view.adapter;

import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.uct.inspection.R;
import com.uct.inspection.interfaces.IChecklistClickCallback;
import com.uct.inspection.interfaces.IChecklistE4ClickCallback;
import com.uct.inspection.model.checklist.ChecklistSubIssue;
import com.uct.inspection.model.checklistE4.ChecklistE4SubIssue;

import java.util.ArrayList;

public class ChecklistE4SubIssueDataAdapter extends RecyclerView.Adapter<ChecklistE4SubIssueDataAdapter.ViewHolder> {


    private int mParentPosition = -1;
    private IChecklistE4ClickCallback mIChecklistE4ClickCallback;

    public ChecklistE4SubIssueDataAdapter(ArrayList<ChecklistSubIssue> mCheckSubIssues) {
        this.mCheckE4SubIssues = mCheckSubIssues;
    }

    public void setIChecklistClickCallback(IChecklistE4ClickCallback mIChecklistE4ClickCallback) {
        this.mIChecklistE4ClickCallback = mIChecklistE4ClickCallback;
    }

    private ArrayList<ChecklistSubIssue> mCheckE4SubIssues;


    public void setParentPosition(int mParentPosition) {
        this.mParentPosition = mParentPosition;
    }


    @NonNull
    @Override
    public ChecklistE4SubIssueDataAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup mParent, int viewType) {

        return new ViewHolder(LayoutInflater.from(mParent.getContext()).inflate(R.layout.item_checklist_e4_sub_issue, mParent, false));
    }


    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.mTxtChecklistIssue.setText(mCheckE4SubIssues.get(holder.getAdapterPosition()).getJobName());
        holder.mEdtCost.setText(String.valueOf(mCheckE4SubIssues.get(holder.getAdapterPosition()).getCost()));
        if (mCheckE4SubIssues.get(holder.getAdapterPosition()).getMinCost() != 0 && mCheckE4SubIssues.get(holder.getAdapterPosition()).getMaxCost() != 0) {
            holder.mLytCost.setVisibility(View.VISIBLE);
        } else {
            holder.mLytCost.setVisibility(View.GONE);
        }

        if (mCheckE4SubIssues.get(holder.getAdapterPosition()).getStatus() == 1) {
            holder.mRgJobStatus.check(R.id.mRbJobStatus1);
        } else if (mCheckE4SubIssues.get(holder.getAdapterPosition()).getStatus() == 2) {
            holder.mRgJobStatus.check(R.id.mRbJobStatus2);
        } else if (mCheckE4SubIssues.get(holder.getAdapterPosition()).getStatus() == 3) {
            holder.mRgJobStatus.check(R.id.mRbJobStatus3);
        }

    }


    @Override
    public int getItemCount() {
        return this.mCheckE4SubIssues.size();
    }


    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        LinearLayout mLytCost;
        TextView mTxtChecklistIssue;
        EditText mEdtCost;
        TextView mTxtCostDec, mTxtCostInc;
        RadioGroup mRgJobStatus;
        RadioButton mRbJobStatus1, mRbJobStatus2, mRbJobStatus3;

        public ViewHolder(View v) {
            super(v);
            mLytCost = v.findViewById(R.id.mLytCost);
            mTxtChecklistIssue = v.findViewById(R.id.mTxtChecklistIssue);
            mEdtCost = v.findViewById(R.id.mEdtCost);
            mTxtCostInc = v.findViewById(R.id.mTxtCostInc);
            mTxtCostDec = v.findViewById(R.id.mTxtCostDec);
            mRgJobStatus = v.findViewById(R.id.mRgJobStatus);
            mRbJobStatus1 = v.findViewById(R.id.mRbJobStatus1);
            mRbJobStatus2 = v.findViewById(R.id.mRbJobStatus2);
            mRbJobStatus3 = v.findViewById(R.id.mRbJobStatus3);
            mTxtCostDec.setOnClickListener(this);
            mTxtCostInc.setOnClickListener(this);
//            mRgJobStatus.setOnCheckedChangeListener(this);

            mRbJobStatus1.setOnClickListener(this);
            mRbJobStatus2.setOnClickListener(this);
            mRbJobStatus3.setOnClickListener(this);

            mEdtCost.addTextChangedListener(new TextWatcher() {

                @Override
                public void afterTextChanged(Editable s) {
                }

                @Override
                public void beforeTextChanged(CharSequence s, int start,
                                              int count, int after) {
                }

                @Override
                public void onTextChanged(CharSequence s, int start,
                                          int before, int count) {
//                    if (mCheckSubIssues.get(getAdapterPosition()).getCost())
                    if (mEdtCost.getText() != null && !mEdtCost.getText().toString().trim().isEmpty() && mCheckE4SubIssues.get(getAdapterPosition()).getCost() != Integer.parseInt(mEdtCost.getText().toString().trim())) {
                        mCheckE4SubIssues.get(getAdapterPosition()).setCost(Integer.parseInt(mEdtCost.getText().toString().trim()));
                        Log.d("onTextChanged", "cost");
                        if (mIChecklistE4ClickCallback != null)
                            mIChecklistE4ClickCallback.onChecklistCostChange(mParentPosition);
                    } else if (mEdtCost.getText() != null && mEdtCost.getText().toString().trim().isEmpty()) {
                        mCheckE4SubIssues.get(getAdapterPosition()).setCost(0);
                        Log.d("onTextChanged", "comment");
                        if (mIChecklistE4ClickCallback != null)
                            mIChecklistE4ClickCallback.onChecklistCostChange(mParentPosition);
                    }

                }
            });
        }

        @Override
        public void onClick(View v) {
            switch (v.getId()) {
                case R.id.mTxtCostDec:
                    if (mCheckE4SubIssues.get(getAdapterPosition()).getCost() > mCheckE4SubIssues.get(getAdapterPosition()).getMinCost()) {
                        mCheckE4SubIssues.get(getAdapterPosition()).setCost(mCheckE4SubIssues.get(getAdapterPosition()).getCost() - 1);
                    }
                    notifyItemChanged(getAdapterPosition());
                    break;

                case R.id.mTxtCostInc:
                    if (mCheckE4SubIssues.get(getAdapterPosition()).getCost() < mCheckE4SubIssues.get(getAdapterPosition()).getMaxCost()) {
                        mCheckE4SubIssues.get(getAdapterPosition()).setCost(mCheckE4SubIssues.get(getAdapterPosition()).getCost() + 1);
                    }
                    notifyItemChanged(getAdapterPosition());
                    break;

                case R.id.mRbJobStatus1:
                    mCheckE4SubIssues.get(getAdapterPosition()).setStatus(1);
                    Log.d("onCheckedChanged", "1");
                    if (mIChecklistE4ClickCallback != null)
                        mIChecklistE4ClickCallback.onChecklistJobStatusChange(mParentPosition);
                    break;
                case R.id.mRbJobStatus2:
                    mCheckE4SubIssues.get(getAdapterPosition()).setStatus(2);
                    Log.d("onCheckedChanged", "2");
                    if (mIChecklistE4ClickCallback != null)
                        mIChecklistE4ClickCallback.onChecklistJobStatusChange(mParentPosition);
                    break;
                case R.id.mRbJobStatus3:
                    mCheckE4SubIssues.get(getAdapterPosition()).setStatus(3);
                    Log.d("onCheckedChanged", "3");
                    if (mIChecklistE4ClickCallback != null)
                        mIChecklistE4ClickCallback.onChecklistJobStatusChange(mParentPosition);
                    break;
            }

        }

        /*@Override
        public void onCheckedChanged(RadioGroup group, int checkedId) {
            switch (checkedId) {
                case R.id.mRbJobStatus1:
                    mCheckE4SubIssues.get(getAdapterPosition()).setStatus(1);
                    Log.d("onCheckedChanged","3");
                    if (mIChecklistE4ClickCallback != null)
                        mIChecklistE4ClickCallback.onChecklistJobStatusChange(mParentPosition);
                    break;
                case R.id.mRbJobStatus2:
                    mCheckE4SubIssues.get(getAdapterPosition()).setStatus(2);
                    Log.d("onCheckedChanged","3");
                    if (mIChecklistE4ClickCallback != null)
                        mIChecklistE4ClickCallback.onChecklistJobStatusChange(mParentPosition);
                    break;
                case R.id.mRbJobStatus3:
                    mCheckE4SubIssues.get(getAdapterPosition()).setStatus(3);
                    Log.d("onCheckedChanged","3");
                    if (mIChecklistE4ClickCallback != null)
                        mIChecklistE4ClickCallback.onChecklistJobStatusChange(mParentPosition);
                    break;
            }
        }*/
    }

} 