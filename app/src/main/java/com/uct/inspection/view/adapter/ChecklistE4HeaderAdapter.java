package com.uct.inspection.view.adapter;

import android.content.Context;
import android.os.Handler;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.flexbox.FlexDirection;
import com.google.android.flexbox.FlexboxLayoutManager;
import com.google.android.flexbox.JustifyContent;
import com.uct.inspection.R;
import com.uct.inspection.interfaces.IChecklistClickCallback;
import com.uct.inspection.interfaces.IChecklistE4ClickCallback;
import com.uct.inspection.model.checklist.ChecklistHeader;
import com.uct.inspection.model.checklist.ChecklistIssue;
import com.uct.inspection.model.checklist.ChecklistSubIssue;
import com.uct.inspection.model.checklistE4.ChecklistE4Header;
import com.uct.inspection.model.checklistE4.ChecklistE4Issue;
import com.uct.inspection.model.checklistE4.ChecklistE4SubIssue;
import com.uct.inspection.utils.ClickGuard;

import java.util.ArrayList;

public class ChecklistE4HeaderAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private Context mContext = null;
    private ArrayList<ChecklistHeader> mChecklistE4Header;
    private IChecklistE4ClickCallback mIChecklistE4ClickCallback;
    private int mErrorPosition = -1;
    private boolean isError;

    public void setIChecklistClickCallback(IChecklistE4ClickCallback mIChecklistE4ClickCallback) {
        this.mIChecklistE4ClickCallback = mIChecklistE4ClickCallback;
    }

    public ChecklistE4HeaderAdapter(Context mContext, ArrayList<ChecklistHeader> checklistE4Header) {
        this.mContext = mContext;
        this.mChecklistE4Header = checklistE4Header;
    }


    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup mParent, int viewType) {
        return new IssueViewHolder(LayoutInflater.from(mParent.getContext()).inflate(R.layout.item_checklist_header, mParent, false));

    }


    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        final IssueViewHolder viewHolder = (IssueViewHolder) holder;

        if (isError && position == mErrorPosition) {
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    isError = false;
                    mErrorPosition = -1;
                    viewHolder.mLytChecklistItem.setBackgroundColor(mContext.getResources().getColor(R.color.colorTransparent));

                }
            }, 1000);

            viewHolder.mLytChecklistItem.setBackgroundColor(mContext.getResources().getColor(R.color.colorError));
        }


        viewHolder.mTxtChecklistHeader.setText(mChecklistE4Header.get(viewHolder.getAdapterPosition()).getSubPartName());
        prepareIssueList(viewHolder.mRcIssue, viewHolder.getAdapterPosition(), new ArrayList<ChecklistIssue>(mChecklistE4Header.get(viewHolder.getAdapterPosition()).getChecklistIssue()));
        prepareCheckListImage(viewHolder.mRcImage, viewHolder.getAdapterPosition(), new ArrayList<String>(mChecklistE4Header.get(viewHolder.getAdapterPosition()).getImages()));
//        prepareE4SubIssueList(viewHolder.mRcSubIssue, viewHolder.getAdapterPosition(), new ArrayList<ChecklistSubIssue>(mChecklistE4Header.get(viewHolder.getAdapterPosition()).getAllSubIssues()));
        prepareE4SubIssueList(viewHolder.mRcSubIssue, viewHolder.getAdapterPosition(), new ArrayList<ChecklistSubIssue>(mChecklistE4Header.get(viewHolder.getAdapterPosition()).getAllE4SubIssues()));

        viewHolder.mEdtComment.setText(mChecklistE4Header.get(holder.getAdapterPosition()).getComments());
        if (mChecklistE4Header.get(holder.getAdapterPosition()).isFileRequired())
            viewHolder.mBtnAddImage.setVisibility(View.VISIBLE);
        else viewHolder.mBtnAddImage.setVisibility(View.GONE);

        if (mChecklistE4Header.get(holder.getAdapterPosition()).getLastNFIStatus() != null && !mChecklistE4Header.get(holder.getAdapterPosition()).getLastNFIStatus().isEmpty()) {
            viewHolder.mTxtLastNFIStatus.setVisibility(View.VISIBLE);
            viewHolder.mTxtLastNFIStatus.setText(mChecklistE4Header.get(holder.getAdapterPosition()).getLastNFIStatus());
        } else {
            viewHolder.mTxtLastNFIStatus.setVisibility(View.GONE);
        }

    }


    @Override
    public int getItemCount() {
        return mChecklistE4Header.size();

    }

    public void setError(boolean isError, int position) {
        this.isError = isError;
        this.mErrorPosition = position;
        notifyDataSetChanged();
    }

    public class IssueViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        TextView mTxtChecklistHeader, mTxtLastNFIStatus;
        RecyclerView mRcIssue, mRcSubIssue, mRcImage;
        Button mBtnAddImage;
        EditText mEdtComment;
        LinearLayout mLytChecklistItem;

        public IssueViewHolder(View v) {
            super(v);
            mTxtChecklistHeader = v.findViewById(R.id.mTxtChecklistHeader);
            mRcIssue = v.findViewById(R.id.mRcIssue);
            mRcSubIssue = v.findViewById(R.id.mRcSubIssue);
            mRcImage = v.findViewById(R.id.mRcImage);
            mBtnAddImage = v.findViewById(R.id.mBtnAddImage);
            mEdtComment = v.findViewById(R.id.mEdtComment);
            mLytChecklistItem = v.findViewById(R.id.mLytChecklistItem);
            mTxtLastNFIStatus = v.findViewById(R.id.mTxtLastNFIStatus);
            mBtnAddImage.setOnClickListener(this);
            ClickGuard.guard(mBtnAddImage);


            mEdtComment.addTextChangedListener(new TextWatcher() {

                @Override
                public void afterTextChanged(Editable s) {
                }

                @Override
                public void beforeTextChanged(CharSequence s, int start,
                                              int count, int after) {
                }

                @Override
                public void onTextChanged(CharSequence s, int start,
                                          int before, int count) {

                    if (mEdtComment.getText() != null && !mEdtComment.getText().toString().trim().isEmpty() && !mChecklistE4Header.get(getAdapterPosition()).getComments().equalsIgnoreCase(mEdtComment.getText().toString().trim())) {
                        mChecklistE4Header.get(getAdapterPosition()).setComments(mEdtComment.getText().toString().trim());
                        if (mIChecklistE4ClickCallback != null)
                            mIChecklistE4ClickCallback.onChecklistCommentChange(getAdapterPosition());
                    } else if (mEdtComment.getText() != null && mEdtComment.getText().toString().trim().isEmpty() && mChecklistE4Header.get(getAdapterPosition()).getComments() != null && !mChecklistE4Header.get(getAdapterPosition()).getComments().equalsIgnoreCase(mEdtComment.getText().toString().trim())) {
                        mChecklistE4Header.get(getAdapterPosition()).setComments("");
                        if (mIChecklistE4ClickCallback != null)
                            mIChecklistE4ClickCallback.onChecklistCommentChange(getAdapterPosition());
                    }

                }
            });

        }


        @Override
        public void onClick(View v) {
            switch (v.getId()) {
                case R.id.mBtnAddImage:
                    if (mIChecklistE4ClickCallback != null)
                        mIChecklistE4ClickCallback.onChecklistAddImageClick(getAdapterPosition());
                    break;
                default:
                    break;
            }
        }
    }

    private void prepareIssueList(RecyclerView recyclerView, final int mParentPosition, final ArrayList<ChecklistIssue> mChecklistIssue) {
        final ChecklistE4IssueDataAdapter adapter = new ChecklistE4IssueDataAdapter(mChecklistIssue);
        adapter.setParentPosition(mParentPosition);
        // Create the FlexboxLayoutMananger, only flexbox library version 0.3.0 or higher support.
        FlexboxLayoutManager flexboxLayoutManager = new FlexboxLayoutManager(mContext);
        // Set flex direction.
        flexboxLayoutManager.setFlexDirection(FlexDirection.ROW);
        // Set JustifyContent.
        flexboxLayoutManager.setJustifyContent(JustifyContent.FLEX_START);
        recyclerView.setLayoutManager(flexboxLayoutManager);
        recyclerView.setHasFixedSize(true);
        recyclerView.setAdapter(adapter);

//        ViewCompat.setNestedScrollingEnabled(recyclerView, false);
    }

    private void prepareE4SubIssueList(RecyclerView recyclerView, int mParentPosition, ArrayList<ChecklistSubIssue> mCheckSubIssues) {
        ChecklistE4SubIssueDataAdapter adapter = new ChecklistE4SubIssueDataAdapter(mCheckSubIssues);
        adapter.setParentPosition(mParentPosition);
        adapter.setIChecklistClickCallback(mIChecklistE4ClickCallback);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(mContext, LinearLayoutManager.VERTICAL, false));
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(adapter);
//        ViewCompat.setNestedScrollingEnabled(recyclerView, false);
    }

    private void prepareCheckListImage(RecyclerView recyclerView, int mParentPosition, ArrayList<String> imageList) {
        ChecklistE4ImageAdapter adapter = new ChecklistE4ImageAdapter(mContext, imageList);
        adapter.setParentPosition(mParentPosition);
        adapter.setIChecklistClickCallback(mIChecklistE4ClickCallback);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new GridLayoutManager(mContext, 4));
        /*recyclerView.addItemDecoration(new ItemDecoratorImageGridView(
                mContext.getResources().getDimensionPixelSize(R.dimen._10sdp),
                4));*/
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(adapter);
//        ViewCompat.setNestedScrollingEnabled(recyclerView, false);
    }

}