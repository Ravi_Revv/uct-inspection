package com.uct.inspection.view.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.uct.inspection.R;
import com.uct.inspection.interfaces.IRefurbDocumentClickCallback;
import com.uct.inspection.model.inspectionlist.InspectionFieldData;
import com.uct.inspection.utils.CommonUtils;

import java.util.ArrayList;

public class InspectionFieldsAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private Context mContext = null;
    private ArrayList<InspectionFieldData> mInspectionFieldData;
    private IRefurbDocumentClickCallback mIRefurbDocumentClickCallback;


    public void setRefurbDocumentClickCallback(IRefurbDocumentClickCallback mIRefurbDocumentClickCallback) {
        this.mIRefurbDocumentClickCallback = mIRefurbDocumentClickCallback;
    }

    public InspectionFieldsAdapter(Context mContext, ArrayList<InspectionFieldData> inspectionFieldData) {
        this.mContext = mContext;
        this.mInspectionFieldData = inspectionFieldData;
    }


    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup mParent, int viewType) {
        return new InspectionFieldViewHolder(LayoutInflater.from(mParent.getContext()).inflate(R.layout.item_inspectiction_fields, mParent, false));

    }


    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        final InspectionFieldViewHolder viewHolder = (InspectionFieldViewHolder) holder;

        if ((viewHolder.getAdapterPosition() + 1) % 2 == 0) {
//            Integer.parseInt()
            CommonUtils.setMarginLeft(viewHolder.mLytInspectionField, Math.round(mContext.getResources().getDimension(R.dimen._10sdp)));
        } else {
            CommonUtils.setMarginLeft(viewHolder.mLytInspectionField, Math.round(0));

        }
        viewHolder.mTxtFieldName.setText(mInspectionFieldData.get(viewHolder.getAdapterPosition()).getName());
        viewHolder.mTxtFieldValue.setText(mInspectionFieldData.get(viewHolder.getAdapterPosition()).getValue());


    }


    @Override
    public int getItemCount() {
        return mInspectionFieldData.size();

    }


    public class InspectionFieldViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        TextView mTxtFieldName;
        TextView mTxtFieldValue;
        LinearLayout mLytInspectionField;

        public InspectionFieldViewHolder(View v) {
            super(v);
            mTxtFieldName = v.findViewById(R.id.mTxtFieldName);
            mTxtFieldValue = v.findViewById(R.id.mTxtFieldValue);
            mLytInspectionField = v.findViewById(R.id.mLytInspectionField);
//            mBtnAddImage.setOnClickListener(this);
//            ClickGuard.guard(mBtnAddImage);

        }


        @Override
        public void onClick(View v) {
            switch (v.getId()) {
                case R.id.mBtnAddImage:
                    if (mIRefurbDocumentClickCallback != null)
                        mIRefurbDocumentClickCallback.onRefurbDocumentAddImageClick(getAdapterPosition());
                    break;
                default:
                    break;
            }
        }
    }


}