package com.uct.inspection.view.activity

import android.app.AlertDialog
import android.app.DatePickerDialog
import android.content.Intent
import android.text.InputFilter
import android.text.method.DigitsKeyListener
import android.view.View
import android.widget.AdapterView
import android.widget.ArrayAdapter
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.uct.inspection.R
import com.uct.inspection.core.base.BaseActivity
import com.uct.inspection.model.inspectionlist.InspectionListData
import com.uct.inspection.model.lead.LeadCarModel
import com.uct.inspection.model.lead.LeadCity
import com.uct.inspection.model.lead.LeadInfoData
import com.uct.inspection.model.lead.LeadNameValue
import com.uct.inspection.model.seller.SellerInfo
import com.uct.inspection.model.seller.SellerInfoData
import com.uct.inspection.utils.CommonData
import com.uct.inspection.utils.CommonDialog
import com.uct.inspection.utils.DateOperation
import com.uct.inspection.utils.enums.LeadCalenderType
import com.uct.inspection.view.adapter.EditInfoAdapter
import com.uct.inspection.viewmodel.EditLeadInfoViewModel
import kotlinx.android.synthetic.main.activity_edit_lead_info.*
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList


class EditLeadInfoActivity : BaseActivity() {


    private var mEditInfoAdapter: EditInfoAdapter? = null
    private var mInspectionListData: InspectionListData? = null//
    private var mLeadInfoData: LeadInfoData? = null
    private var mSellerInfo: SellerInfo? = null
    private var mEditLeadInfoViewModel: EditLeadInfoViewModel? = null

    override fun getIntentValues() {
        super.getIntentValues()
        mInspectionListData = intent.getParcelableExtra("inspectionData");
    }

    override fun setLayoutResource(): Int {
        return R.layout.activity_edit_lead_info
    }

    override fun setValues() {
//        mInspectionListData = CommonData.mInspectionData
        setUpObserver()


    }

    private fun setUpObserver() {
        setDataToView()
        mEditLeadInfoViewModel = EditLeadInfoViewModel(mContext)
        mEditLeadInfoViewModel?.getLeadInfoDataData()?.observe(
            this,
            androidx.lifecycle.Observer {
                this.mLeadInfoData = it
                prepareCarModelList()
                prepareSellerTypeList()
                prepareCityList()
                prepareInsuranceTypeList()
                prepareRegistrationTypeList()
//                prepareEditInfoList()
                if (mAutoTxtSellerType.text != null && mLeadInfoData?.sellerTypeList!!.contains(
                        mAutoTxtSellerType.text.toString().trim()
                    ) && mAutoTxtCity.text != null && mLeadInfoData?.leadCityList!!.contains(
                        mAutoTxtCity.text.toString().trim()
                    )
                ) {
                    mAutoTxtSeller.setText("")
                    prepareSellerList(ArrayList())
                    mEditLeadInfoViewModel?.getSellerInfo(
                        mLeadInfoData?.getmLeadCityList()!![mLeadInfoData?.leadCityList!!.indexOf(
                            mAutoTxtCity.text.toString().trim()
                        )].id,
                        mLeadInfoData?.getmSellerTypeList()!![mLeadInfoData?.sellerTypeList!!.indexOf(
                            mAutoTxtSellerType.text.toString().trim()
                        )].name
                    )
                }

            })


        mEditLeadInfoViewModel?.getLeadInfo()

        mEditLeadInfoViewModel?.sellerInfoData?.observe(
            this,
            androidx.lifecycle.Observer {
                mSellerInfo = it
                if (it != null && it.getSellerInfoData() != null && it.getSellerInfoData().size > 0) {
                    prepareSellerList(it.sellerList)
                } else {
                    prepareSellerList(ArrayList())
                }


            })

        mEditLeadInfoViewModel?.isLeadStatusUpdateLiveData?.observe(
            this,
            androidx.lifecycle.Observer {

                if (it) {
                    CommonDialog.With(mActivity)
                        .showDataValidationFailureDialog("Thank You",
                            "You have successfully updated lead.",
                            object : CommonDialog.IDataValidationFailureDialogClickCallback {

                                override fun onOkClick() {
                                    startInspectionListActivity()
                                }

                            })
                }

            })

    }

    private fun setDataToView() {
        mEdtRegistrationNo.setFilters(arrayOf<InputFilter>(InputFilter.AllCaps()))
//        mEdtRegistrationNo.setKeyListener(DigitsKeyListener.getInstance("ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789"));
        mEdtRegistrationNo.setText(mInspectionListData?.carRegistrationNumber)
        mEdtRegistrationDate.setText(DateOperation.convertServerToLocal(mInspectionListData?.registrationTime))
        mEdtManufactureDate.setText(DateOperation.convertServerToLocal(mInspectionListData?.manufacturingTimestamp))
        mEdtInsuranceValidity.setText(DateOperation.convertServerToLocal(mInspectionListData?.insuranceValidity))
        mEdtRto.setText(mInspectionListData?.rto)
        mEdtOwnershipSerialNo.setText(mInspectionListData?.ownershipSerial)
        mEdtKmsDriven.setText(mInspectionListData?.kmDriven)
        mEdtColor.setText(mInspectionListData?.carColor)
        mEdtComment.setText(mInspectionListData?.comments)

    }

    private fun prepareCarModelList() {

        /*val arrayAdapter = ArrayAdapter(
            mActivity,
            R.layout.item_lead_dropdown,
            R.id.mTxtItemDropdown,
            mLeadInfoData?.leadCarModelList!!
        )*/

        val arrayAdapter = ArrayAdapter(
            mActivity,
            android.R.layout.simple_dropdown_item_1line,
            mLeadInfoData?.leadCarModelList!!
        )

        if (mLeadInfoData?.leadCarModelList!!.contains(
                String.format(
                    "%s %s %s %s %s %s %s %s %s",
                    mInspectionListData?.carMake,
                    "|",
                    mInspectionListData?.carModel,
                    "|",
                    mInspectionListData?.carVariant,
                    "|",
                    mInspectionListData?.carFuel,
                    "|",
                    mInspectionListData?.carTransmission
                )
            )
        ) {
            mAutoTxtCarModel.setText(
                mLeadInfoData?.leadCarModelList!![mLeadInfoData?.leadCarModelList!!.indexOf(
                    String.format(
                        "%s %s %s %s %s %s %s %s %s",
                        mInspectionListData?.carMake,
                        "|",
                        mInspectionListData?.carModel,
                        "|",
                        mInspectionListData?.carVariant,
                        "|",
                        mInspectionListData?.carFuel,
                        "|",
                        mInspectionListData?.carTransmission
                    )
                )]
            )
        } else {
            mAutoTxtCarModel.setText("")
        }


        mAutoTxtCarModel.threshold = 1
        mAutoTxtCarModel.setAdapter(arrayAdapter)
        mAutoTxtCarModel.setOnClickListener { mAutoTxtCarModel.showDropDown() }

    }


    private fun prepareSellerTypeList() {
        val arrayAdapter = ArrayAdapter(
            mActivity,
            android.R.layout.simple_dropdown_item_1line,
            mLeadInfoData?.sellerTypeList!!
        )

        if (mLeadInfoData?.sellerTypeList!!.contains(mInspectionListData?.sellerType)) {
            mAutoTxtSellerType.setText(
                mLeadInfoData?.sellerTypeList!![mLeadInfoData?.sellerTypeList!!.indexOf(
                    mInspectionListData?.sellerType
                )]
            )
        } else {
            mAutoTxtSellerType.setText("")
        }

        mAutoTxtSellerType.threshold = 1
        mAutoTxtSellerType.setAdapter(arrayAdapter)
        mAutoTxtSellerType.setOnClickListener {
            mAutoTxtSellerType.showDropDown()
        }

        mAutoTxtSellerType.onItemClickListener = object : AdapterView.OnItemClickListener {
            override fun onItemClick(
                parent: AdapterView<*>?,
                view: View?,
                position: Int,
                id: Long
            ) {

                if (mAutoTxtCity.text != null && mLeadInfoData?.leadCityList!!.contains(
                        mAutoTxtCity.text.toString().trim()
                    )
                ) {
                    mAutoTxtSeller.setText("")
                    prepareSellerList(ArrayList())
                    mEditLeadInfoViewModel?.getSellerInfo(
                        mLeadInfoData?.getmLeadCityList()!![mLeadInfoData?.leadCityList!!.indexOf(
                            mAutoTxtCity.text.toString().trim()
                        )].id, mLeadInfoData?.getmSellerTypeList()!![position].name
                    )
                }
            }


        }
    }

    private fun prepareCityList() {
        val arrayAdapter = ArrayAdapter(
            mActivity,
            android.R.layout.simple_dropdown_item_1line,
            mLeadInfoData?.leadCityList!!
        )

        val bhn = LeadCity()
        bhn.id = mInspectionListData?.serviceCityID!!
        if (mLeadInfoData?.getmLeadCityList()!!.contains(bhn)) {
            mAutoTxtCity.setText(
                mLeadInfoData?.leadCityList!![mLeadInfoData?.getmLeadCityList()!!.indexOf(bhn)]
            )
        } else {
            mAutoTxtCity.setText("")
        }


        mAutoTxtCity.threshold = 1
        mAutoTxtCity.setAdapter(arrayAdapter)
        mAutoTxtCity.setOnClickListener { mAutoTxtCity.showDropDown() }

        mAutoTxtCity.onItemClickListener = object : AdapterView.OnItemClickListener {
            override fun onItemClick(
                parent: AdapterView<*>?,
                view: View?,
                position: Int,
                id: Long
            ) {

                if (mAutoTxtSellerType.text != null && mLeadInfoData?.sellerTypeList!!.contains(
                        mAutoTxtSellerType.text.toString().trim()
                    )
                ) {
                    mAutoTxtSeller.setText("")
                    prepareSellerList(ArrayList())
                    mEditLeadInfoViewModel?.getSellerInfo(
                        mLeadInfoData?.getmLeadCityList()!![position].id,
                        mLeadInfoData?.getmSellerTypeList()!![mLeadInfoData?.sellerTypeList!!.indexOf(
                            mAutoTxtSellerType.text.toString().trim()
                        )].name
                    )
                }

            }


        }
    }

    private fun prepareSellerList(sellerList: ArrayList<String>) {
        val arrayAdapter = ArrayAdapter(
            mActivity,
            android.R.layout.simple_dropdown_item_1line,
            sellerList
        )

        if (sellerList.contains(mInspectionListData?.sellerName)) {
            mAutoTxtSeller.setText(sellerList[sellerList.indexOf(mInspectionListData?.sellerName)])
        } else mAutoTxtSeller.setText("")
        mAutoTxtSeller.threshold = 1
        mAutoTxtSeller.setAdapter(arrayAdapter)
        mAutoTxtSeller.setOnClickListener { mAutoTxtSeller.showDropDown() }

    }


    private fun prepareInsuranceTypeList() {
        val arrayAdapter = ArrayAdapter(
            mActivity,
            android.R.layout.simple_dropdown_item_1line,
            mLeadInfoData?.insuranceTypeList!!
        )


        if (mLeadInfoData?.insuranceTypeList!!.contains(mInspectionListData?.insuranceType)) {
            mAutoTxtInsuranceType.setText(
                mLeadInfoData?.insuranceTypeList!![mLeadInfoData?.insuranceTypeList!!.indexOf(
                    mInspectionListData?.insuranceType
                )]
            )
        } else mAutoTxtInsuranceType.setText("")
        mAutoTxtInsuranceType.threshold = 1
        mAutoTxtInsuranceType.setAdapter(arrayAdapter)
        mAutoTxtInsuranceType.setOnClickListener { mAutoTxtInsuranceType.showDropDown() }

    }//


    private fun prepareRegistrationTypeList() {
        val arrayAdapter = ArrayAdapter(
            mActivity,
            android.R.layout.simple_dropdown_item_1line,
            mLeadInfoData?.registrationTypeList!!
        )

        if (mLeadInfoData?.registrationTypeList!!.contains(mInspectionListData?.registrationType)) {
            mAutoTxtRegistrationType.setText(
                mLeadInfoData?.registrationTypeList!![mLeadInfoData?.registrationTypeList!!.indexOf(
                    mInspectionListData?.registrationType
                )]
            )
        } else mAutoTxtRegistrationType.setText("")
        mAutoTxtRegistrationType.threshold = 1
        mAutoTxtRegistrationType.setAdapter(arrayAdapter)
        mAutoTxtRegistrationType.setOnClickListener { mAutoTxtRegistrationType.showDropDown() }

    }


    private fun showPopUpForDate(type: String) {
        val dateFormatter = SimpleDateFormat("yyyy-MM-dd")
        dateFormatter.timeZone = TimeZone.getTimeZone("Asia/Calcutta")
        if (type == LeadCalenderType.REGISTRATION.name) {

        }
        val newCalendar = Calendar.getInstance()
        newCalendar.timeZone = TimeZone.getTimeZone("Asia/Calcutta")
        newCalendar.add(Calendar.YEAR, 0)

        var style = R.style.MyTimePickerDialogTheme

        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP) {

            style = R.style.MyTimePickerDialogTheme
        } else {
            style = AlertDialog.THEME_HOLO_LIGHT
        }

        val dialog = DatePickerDialog(
            this,
            style,
            DatePickerDialog.OnDateSetListener { view, year, monthOfYear, dayOfMonth ->
                var ageBelow24 = false

                val newDate = Calendar.getInstance()
                newDate.set(year, monthOfYear, dayOfMonth)
                if (type == LeadCalenderType.REGISTRATION.name) {
                    mEdtRegistrationDate.setText(dateFormatter.format(newDate.time))
                } else if (type == LeadCalenderType.MANUFACTURE.name) {
                    mEdtManufactureDate.setText(dateFormatter.format(newDate.time))
                } else if (type == LeadCalenderType.INSURANCE_VALIDITY.name) {
                    mEdtInsuranceValidity.setText(dateFormatter.format(newDate.time))
                }


            },
            newCalendar.get(Calendar.YEAR),
            newCalendar.get(Calendar.MONTH),
            newCalendar.get(Calendar.DAY_OF_MONTH)
        )
        dialog.datePicker.maxDate = newCalendar.timeInMillis
        dialog.show()

    }


    private fun startInspectionListActivity() {
        val bdIntent = Intent(mContext, InspectionListActivity::class.java)
        bdIntent.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
        startActivity(bdIntent)
    }


    override fun setToolbarId(): View? {
        return null
    }


    override fun registerClickListener(): Array<View>? {
        return arrayOf(
            mImgViewBack,
            mBtnConfirm,
            mEdtRegistrationDate,
            mEdtInsuranceValidity,
            mEdtManufactureDate
        )
    }

    override fun onClick(v: View?) {
        super.onClick(v)
        when (v?.id) {
            R.id.mImgViewBack -> {
                finish()
            }

            R.id.mEdtRegistrationDate -> {
                showPopUpForDate(LeadCalenderType.REGISTRATION.name)
            }

            R.id.mEdtManufactureDate -> {
                showPopUpForDate(LeadCalenderType.MANUFACTURE.name)
            }

            R.id.mEdtInsuranceValidity -> {
                showPopUpForDate(LeadCalenderType.INSURANCE_VALIDITY.name)
            }

            R.id.mBtnConfirm -> {
                checkAllFieldValidation()
//
            }
        }

    }

    private fun checkAllFieldValidation() {
        var carModel: LeadCarModel? = null
        var sellerType: LeadNameValue? = null
        var leadCity: LeadCity? = null
        var sellerInfoData: SellerInfoData? = null
        var registrationDate: String? = null
        var registrationNumber: String? = null
        var manufactureDate: String? = null
        var insuranceValidity: String? = null
        var registrationType: LeadNameValue? = null
        var insuranceType: LeadNameValue? = null
        var ownershipSerialNo: String? = null
        var kmsDriven: String? = null
        if (mAutoTxtCarModel != null && mAutoTxtCarModel.text.toString().trim().isNotEmpty() && mLeadInfoData?.leadCarModelList!!.contains(
                mAutoTxtCarModel.text.toString().trim()
            )
        ) {
            mAutoTxtCarModel.setBackgroundResource(R.drawable.round_corner_light_grey_background)
            carModel =
                mLeadInfoData?.getmLeadCarModelList()!![mLeadInfoData?.leadCarModelList!!.indexOf(
                    mAutoTxtCarModel.text.toString().trim()
                )]
        } else {
            carModel = null
            mAutoTxtCarModel.getParent().requestChildFocus(mAutoTxtCarModel, mAutoTxtCarModel)
            mAutoTxtCarModel.setBackgroundResource(R.drawable.round_corner_red_background)
        }

        if (mAutoTxtSellerType != null && mAutoTxtSellerType.text.toString().trim().isNotEmpty() && mLeadInfoData?.sellerTypeList!!.contains(
                mAutoTxtSellerType.text.toString().trim()
            )
        ) {
            mAutoTxtSellerType.setBackgroundResource(R.drawable.round_corner_light_grey_background)
            sellerType =
                mLeadInfoData?.getmSellerTypeList()!![mLeadInfoData?.sellerTypeList!!.indexOf(
                    mAutoTxtSellerType.text.toString().trim()
                )]
        } else {
            mAutoTxtSellerType.getParent().requestChildFocus(mAutoTxtSellerType, mAutoTxtSellerType)
            sellerType = null
            mAutoTxtSellerType.setBackgroundResource(R.drawable.round_corner_red_background)
        }

        if (mAutoTxtCity != null && mAutoTxtCity.text.toString().trim().isNotEmpty() && mLeadInfoData?.leadCityList!!.contains(
                mAutoTxtCity.text.toString().trim()
            )
        ) {
            mAutoTxtCity.setBackgroundResource(R.drawable.round_corner_light_grey_background)
            leadCity = mLeadInfoData?.getmLeadCityList()!![mLeadInfoData?.leadCityList!!.indexOf(
                mAutoTxtCity.text.toString().trim()
            )]
        } else {
            leadCity = null
            mAutoTxtCity.getParent().requestChildFocus(mAutoTxtCity, mAutoTxtCity)
            mAutoTxtCity.setBackgroundResource(R.drawable.round_corner_red_background)
        }

        if (mAutoTxtSeller != null && mAutoTxtSeller.text.toString().trim().isNotEmpty() && mSellerInfo != null && mSellerInfo?.sellerList != null && mSellerInfo?.sellerList!!.size > 0 && mSellerInfo?.sellerList!!.contains(
                mAutoTxtSeller.text.toString().trim()
            )
        ) {
            mAutoTxtSeller.setBackgroundResource(R.drawable.round_corner_light_grey_background)
            sellerInfoData =
                mSellerInfo?.sellerInfoData!![mSellerInfo?.sellerList!!.indexOf(mAutoTxtSeller.text.toString().trim())]
        } else {
            sellerInfoData = null
            mAutoTxtSeller.getParent().requestChildFocus(mAutoTxtSeller, mAutoTxtSeller)
            mAutoTxtSeller.setBackgroundResource(R.drawable.round_corner_red_background)

        }

        if (mEdtRegistrationNo != null && mEdtRegistrationNo.text.toString().trim().isNotEmpty()) {
            registrationNumber = mEdtRegistrationDate.text.toString().trim()
            mEdtRegistrationNo.setBackgroundResource(R.drawable.round_corner_light_grey_background)
        } else {
            registrationNumber = null
            mEdtRegistrationNo.getParent()
                .requestChildFocus(mEdtRegistrationDate, mEdtRegistrationDate)
            mEdtRegistrationNo.setBackgroundResource(R.drawable.round_corner_red_background)
        }

        if (mEdtRegistrationDate != null && mEdtRegistrationDate.text.toString().trim().isNotEmpty()) {
            registrationDate = mEdtRegistrationDate.text.toString().trim()
            mEdtRegistrationDate.setBackgroundResource(R.drawable.round_corner_light_grey_background)
        } else {
            registrationDate = null
            mEdtRegistrationDate.getParent()
                .requestChildFocus(mEdtRegistrationDate, mEdtRegistrationDate)
            mEdtRegistrationDate.setBackgroundResource(R.drawable.round_corner_red_background)
        }

        if (mEdtManufactureDate != null && mEdtManufactureDate.text.toString().trim().isNotEmpty()) {
            manufactureDate = mEdtManufactureDate.text.toString().trim()
            mEdtManufactureDate.setBackgroundResource(R.drawable.round_corner_light_grey_background)
        } else {
            manufactureDate = null
            mEdtManufactureDate.getParent()
                .requestChildFocus(mEdtManufactureDate, mEdtManufactureDate)
            mEdtManufactureDate.setBackgroundResource(R.drawable.round_corner_red_background)
        }

        if (mEdtInsuranceValidity != null && mEdtInsuranceValidity.text.toString().trim().isNotEmpty()) {
            insuranceValidity = mEdtInsuranceValidity.text.toString().trim()
            mEdtInsuranceValidity.setBackgroundResource(R.drawable.round_corner_light_grey_background)
        } else {
            insuranceValidity = null
            mEdtInsuranceValidity.getParent()
                .requestChildFocus(mEdtInsuranceValidity, mEdtInsuranceValidity)
            mEdtInsuranceValidity.setBackgroundResource(R.drawable.round_corner_red_background)
        }

        if (mAutoTxtInsuranceType != null && mAutoTxtInsuranceType.text.toString().trim().isNotEmpty() && mLeadInfoData?.insuranceTypeList!!.contains(
                mAutoTxtInsuranceType.text.toString().trim()
            )
        ) {
            insuranceType =
                mLeadInfoData?.getmInsuranceTypeList()!![mLeadInfoData?.insuranceTypeList!!.indexOf(
                    mAutoTxtInsuranceType.text.toString().trim()
                )]
            mAutoTxtInsuranceType.setBackgroundResource(R.drawable.round_corner_light_grey_background)
        } else if (mAutoTxtInsuranceType != null && mAutoTxtInsuranceType.text.toString().trim().isEmpty()) {
            insuranceType = LeadNameValue("", "")
            mAutoTxtInsuranceType.setBackgroundResource(R.drawable.round_corner_light_grey_background)
        } else {
            insuranceType = null
            mAutoTxtInsuranceType.getParent()
                .requestChildFocus(mAutoTxtInsuranceType, mAutoTxtInsuranceType)
            mAutoTxtInsuranceType.setBackgroundResource(R.drawable.round_corner_red_background)
        }

        if (mAutoTxtRegistrationType != null && mAutoTxtRegistrationType.text.toString().trim().isNotEmpty() && mLeadInfoData?.registrationTypeList!!.contains(
                mAutoTxtRegistrationType.text.toString().trim()
            )
        ) {
            registrationType =
                mLeadInfoData?.getmRegistrationTypeList()!![mLeadInfoData?.registrationTypeList!!.indexOf(
                    mAutoTxtRegistrationType.text.toString().trim()
                )]
            mAutoTxtRegistrationType.setBackgroundResource(R.drawable.round_corner_light_grey_background)
        } else {
            registrationType = null
            mAutoTxtRegistrationType.getParent()
                .requestChildFocus(mAutoTxtRegistrationType, mAutoTxtRegistrationType)
            mAutoTxtRegistrationType.setBackgroundResource(R.drawable.round_corner_red_background)
        }

        if (mEdtOwnershipSerialNo != null && mEdtOwnershipSerialNo.text.toString().trim().isNotEmpty()) {
            ownershipSerialNo = mEdtOwnershipSerialNo.text.toString().trim()
            mEdtOwnershipSerialNo.setBackgroundResource(R.drawable.round_corner_light_grey_background)
        } else {
            ownershipSerialNo = null
            mEdtOwnershipSerialNo.getParent()
                .requestChildFocus(mEdtOwnershipSerialNo, mEdtOwnershipSerialNo)
            mEdtOwnershipSerialNo.setBackgroundResource(R.drawable.round_corner_red_background)
        }

        if (mEdtKmsDriven != null && mEdtKmsDriven.text.toString().trim().isNotEmpty()) {
            kmsDriven = mEdtKmsDriven.text.toString().trim()
            mEdtKmsDriven.setBackgroundResource(R.drawable.round_corner_light_grey_background)
        } else {
            kmsDriven = null
            mEdtKmsDriven.getParent().requestChildFocus(mEdtKmsDriven, mEdtKmsDriven)
            mEdtKmsDriven.setBackgroundResource(R.drawable.round_corner_red_background)
        }

        if (carModel != null && sellerType != null && leadCity != null && sellerInfoData != null && registrationNumber != null && registrationDate != null && manufactureDate != null && insuranceValidity != null && insuranceType != null && registrationType != null && ownershipSerialNo != null && kmsDriven != null) {
            mEditLeadInfoViewModel?.updateLeadInfo(
                mInspectionListData?.leadID,
                carModel.id,
                sellerInfoData.sellerID,
                mEdtRegistrationNo?.text.toString().trim(),
                mEdtColor.text.toString().trim(),
                DateOperation.convertLocalToServer(registrationDate),
                DateOperation.convertLocalToServer(manufactureDate),
                DateOperation.convertLocalToServer(insuranceValidity),
                insuranceType.name, mEdtRto.text.toString().trim(),
                registrationType.name,
                ownershipSerialNo,
                leadCity.id,
                mEdtComment.text.toString().trim(),
                kmsDriven
            )
        }
    }


}

