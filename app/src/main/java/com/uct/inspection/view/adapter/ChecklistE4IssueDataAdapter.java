package com.uct.inspection.view.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.uct.inspection.R;
import com.uct.inspection.model.checklist.ChecklistIssue;
import com.uct.inspection.model.checklistE4.ChecklistE4Issue;

import java.util.ArrayList;

public class ChecklistE4IssueDataAdapter extends RecyclerView.Adapter<ChecklistE4IssueDataAdapter.ViewHolder> {


    private int mParentPosition = -1;
    private ArrayList<ChecklistIssue> mChecklistIssue;

    public ChecklistE4IssueDataAdapter(ArrayList<ChecklistIssue> mChecklistIssue) {
        this.mChecklistIssue = mChecklistIssue;
    }


    public void setParentPosition(int mParentPosition) {
        this.mParentPosition = mParentPosition;
    }


    @NonNull
    @Override
    public ChecklistE4IssueDataAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup mParent, int viewType) {

        return new ViewHolder(LayoutInflater.from(mParent.getContext()).inflate(R.layout.item_checklist_issue, mParent, false));
    }


    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.mTxtChecklistIssue.setText(mChecklistIssue.get(holder.getAdapterPosition()).getIssueName());
        holder.mLytChecklistIssue.setSelected(mChecklistIssue.get(holder.getAdapterPosition()).getSelected());

    }


    @Override
    public int getItemCount() {
        return this.mChecklistIssue.size();
    }


    public class ViewHolder extends RecyclerView.ViewHolder {
        LinearLayout mLytChecklistIssue;
        TextView mTxtChecklistIssue;

        public ViewHolder(View v) {
            super(v);
            mLytChecklistIssue = v.findViewById(R.id.mLytChecklistIssue);
            mTxtChecklistIssue = v.findViewById(R.id.mTxtChecklistIssue);

        }


    }


} 