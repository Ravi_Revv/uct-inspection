package com.uct.inspection.camera

import android.content.Intent
import android.graphics.*
import android.media.ExifInterface
import android.util.DisplayMetrics
import android.util.Log
import android.view.Display
import android.view.View
import android.widget.Toast
import com.flurgle.camerakit.CameraKit
import com.flurgle.camerakit.CameraListener
import com.uct.inspection.R
import com.uct.inspection.core.base.BaseActivity
import com.uct.inspection.utils.Constants
import com.uct.inspection.utils.ImageCompress
import kotlinx.android.synthetic.main.activity_camera.*
import java.io.File
import java.io.FileNotFoundException
import java.io.FileOutputStream
import java.io.IOException
import java.lang.reflect.InvocationTargetException
import java.lang.reflect.Method
import java.util.HashMap

class CameraActivity : BaseActivity() {
    private var mInspectionId: Int = 0
    private var mFileName: String? = null
    private var mHeaderPosition: Int? = -1
    private var flashEnabled = false

    override fun getIntentValues() {
        super.getIntentValues()
        mInspectionId = intent.getIntExtra("mInspectionId", 0)
        mFileName = intent.getStringExtra("mFileName")
        mHeaderPosition = intent.getIntExtra("mHeaderPosition", -1)
    }

    override fun setLayoutResource(): Int {
        return R.layout.activity_camera
    }

    override fun setValues() {
        cameraSetUp()
    }

    override fun registerClickListener(): Array<View>? {
        return arrayOf(mBtnClickImg, mBtnFlash)
    }

    override fun setToolbarId(): View? {
        return null
    }

    private fun cameraSetUp() {
        camera.setZoom(CameraKit.Constants.ZOOM_PINCH)
        camera.setFocus(CameraKit.Constants.FOCUS_CONTINUOUS)
        camera.setMethod(CameraKit.Constants.METHOD_STANDARD)
        camera.setCameraListener(object : CameraListener() {
            override fun onPictureTaken(picture: ByteArray) {
                super.onPictureTaken(picture)

                try {
                    var file: File? = null
                    //create a file to write bitmap data
                    //                    file = new File(DEConstants.directory, "DE/Images"+"/"+bookingID+"/"+fileName+"_"+System.currentTimeMillis()+".jpeg");
                    file =
                        File(
                            Constants.DIRECTORY,
                            "UCT-Inspection/Images/$mInspectionId/$mFileName.jpeg"
                        )

                    if (!file!!.exists())
                        file.createNewFile()

                    //write the bytes in file
                    val fos = FileOutputStream(file)
                    fos.write(picture)
                    fos.flush()
                    fos.close()

                    sendImageData(ImageCompress.compressImage(file, mActivity))
//                    sendImageData(file.absolutePath)
                } catch (e: Exception) {
                    e.printStackTrace()
                }


            }

            override fun onCameraClosed() {
                super.onCameraClosed()

            }

        })
    }

    override fun onClick(v: View?) {
        super.onClick(v)
        when (v?.id) {
            R.id.mBtnClickImg -> {
                if (camera != null)
                    camera.captureImage()
            }

            R.id.mBtnFlash -> {
                if (camera != null) {
                    flashEnabled = !flashEnabled
                    if (flashEnabled) {
                        mBtnFlash.setBackgroundResource(R.drawable.camera_flash_on)
                        camera.setFlash(CameraKit.Constants.FLASH_ON)
                    } else {
                        mBtnFlash.setBackgroundResource(R.drawable.camera_flash)
                        camera.setFlash(CameraKit.Constants.FLASH_OFF)
                    }
                }
            }
        }

    }

    override fun onResume() {
        if (camera != null)
            camera.start()
        super.onResume()
    }

    override fun onPause() {
        if (camera != null)
            camera.stop()
        super.onPause()
    }


    private fun sendImageData(path: String) {
        Log.d("FilePath", path)
        val intent = Intent()
        intent.putExtra("mImagePath", path)
        intent.putExtra(
            "mHeaderPosition", mHeaderPosition
        )
        setResult(RESULT_OK, intent)
        finish()
    }
}
