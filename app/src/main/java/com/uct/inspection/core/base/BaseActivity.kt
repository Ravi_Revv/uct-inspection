package com.uct.inspection.core.base

import android.os.Bundle
import androidx.core.app.ActivityCompat
import com.google.firebase.analytics.FirebaseAnalytics
import com.google.firebase.crashlytics.FirebaseCrashlytics
import com.uct.inspection.interfaces.IPermissionResultCallback
import com.uct.inspection.preference.PreferenceManager
import com.uct.inspection.utils.CommonData
import com.uct.inspection.utils.PermissionUtils
import java.util.ArrayList


abstract class BaseActivity : BaseCoreActivity(), ActivityCompat.OnRequestPermissionsResultCallback,
    IPermissionResultCallback {
    internal var mPermissions = ArrayList<String>()

    internal var mPermissionUtils: PermissionUtils? = null

    private var mPreferenceManager: PreferenceManager? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        mPreferenceManager = PreferenceManager(mContext)
        super.onCreate(savedInstanceState)
//        initializeFilter()
        setCrashlyticsData()
    }

    override fun onResume() {
        super.onResume()

    }

    override fun onPause() {
        super.onPause()

    }

    override fun onDestroy() {
        super.onDestroy()
    }

    override fun onPermissionGranted(request_code: Int) {

    }

    override fun onPermissionDenied(request_code: Int) {
    }

    override fun onPartialPermissionGranted(
        request_code: Int,
        granted_permissions: ArrayList<String>?
    ) {

    }

    override fun onNeverAskAgain(request_code: Int) {

    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        mPermissionUtils?.onRequestPermissionsResult(requestCode, permissions, grantResults)
    }

    /*private fun enableLocation() {
        googleApiClient = GoogleApiClient.Builder(this)
            .addApi(LocationServices.API)
            .addConnectionCallbacks(this)
            .addOnConnectionFailedListener(this).build()
        googleApiClient.connect()
    }*/


    public fun getPreferenceManagerInstance(): PreferenceManager? {
        return mPreferenceManager
    }

    open fun onNewFragmentCreate() {

    }

    private fun setCrashlyticsData() {
        try {
            if (CommonData.getInspectorData(mContext) != null) {
                val crashlytics = FirebaseCrashlytics.getInstance()
                crashlytics.setUserId(CommonData.getInspectorData(this).userEmail)
                crashlytics.setCustomKey("Id", CommonData.getInspectorData(this).id)
                crashlytics.setCustomKey("userName", CommonData.getInspectorData(this).userName)
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }

    }
}