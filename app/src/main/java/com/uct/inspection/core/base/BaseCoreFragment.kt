package com.uct.inspection.core.base

import android.content.Context
import android.content.pm.PackageManager
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity
import com.uct.inspection.core.interfaces.CoreFragmentInterface
import com.uct.inspection.utils.ClickUtils


abstract class BaseCoreFragment : Fragment(), View.OnClickListener, CoreFragmentInterface {
    override val mActivity: FragmentActivity? get() = activity
    override val mContext: Context?
        get() = context
    override val baseActivityInstance: BaseActivity
        get() = activity as BaseActivity
    override val mPackageManager: PackageManager
        get() = mContext?.packageManager!!


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)


        getIntentValues()
    }


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        (mActivity as BaseActivity).onNewFragmentCreate()
        var mLayoutId: Int = setLayoutResource()
        if (mLayoutId != -1) {
            return inflater?.inflate(mLayoutId, container, false)
        }

        return null
    }

    override fun onResume() {
        super.onResume()
    }

    private var savedInstanceState: Bundle? = null
    fun getSavedInstanceState(): Bundle? {
        return savedInstanceState
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        if (view != null) {
//            initializeLocalViews(view)
//            initializeViews(view)
            this.savedInstanceState = savedInstanceState
            setValues()
            val viewList = registerClickListener();
            if (viewList != null && viewList.isNotEmpty()) {
                ClickUtils.setClickListener(viewList, this)
            }
        }

    }

    override fun getIntentValues() {

    }

    override fun onDetach() {
        super.onDetach()
        (mActivity as BaseActivity).onNewFragmentCreate()
    }

}
