package com.uct.inspection.core.base


import android.app.Activity
import android.content.Context
import android.os.Bundle
import android.view.View
import androidx.appcompat.app.ActionBar
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import com.uct.inspection.MyApplication
import com.uct.inspection.core.base.interfaces.CoreActivityInterface
import com.uct.inspection.utils.ClickUtils


abstract class BaseCoreActivity : AppCompatActivity(), View.OnClickListener, CoreActivityInterface {

    private var mToolbar: Toolbar? = null
    private var mActionbar: ActionBar? = null

    override val mActivity: Activity = this
    override val mContext: Context = this
    override val mApplicationInstance: MyApplication
        get() = application as MyApplication

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val mLayoutId = setLayoutResource()
        if (mLayoutId != -1) {
            setContentView(mLayoutId)
            getIntentValues()
            val toolbar: View? = setToolbarId()
            if (toolbar != null){
                supportActionBar?.show()
                setToolbar(toolbar)
            } else {
                supportActionBar?.hide()
            }

            setValues()
            val viewList = registerClickListener()
            if (viewList != null && viewList.size > 0) {
                ClickUtils.setClickListener(viewList, mContext)
            }
        }

    }


    private fun setToolbar(toolbarId: Int) {
        mToolbar = findViewById<Toolbar>(toolbarId) as Toolbar
        setSupportActionBar(mToolbar)
        mActionbar = supportActionBar
        mActionbar?.setDisplayShowTitleEnabled(false);
    }


    private fun setToolbar(toolbar: View) {
        mToolbar = toolbar as Toolbar;
        setSupportActionBar(mToolbar)
        mActionbar = supportActionBar
        mActionbar?.setDisplayShowTitleEnabled(false);
    }

    fun getToolbarInstance(): Toolbar? {
        return mToolbar;
    }

    fun getActionbarInstance(): ActionBar? {
        return mActionbar;
    }

    override fun onClick(v: View?) {

    }

    override fun getIntentValues() {

    }

}
