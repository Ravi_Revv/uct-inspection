package com.uct.inspection.core.base.interfaces

import android.app.Activity
import android.content.Context
import android.view.View
import com.uct.inspection.MyApplication


interface CoreActivityInterface {
    val mActivity: Activity
    val mContext: Context
    val mApplicationInstance: MyApplication
    fun setLayoutResource(): Int
    fun setValues()
    fun registerClickListener(): Array<View>?
    fun setToolbarId(): View?
    fun getIntentValues()
}
