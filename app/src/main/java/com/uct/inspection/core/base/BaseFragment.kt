package com.uct.inspection.core.base

import android.os.Bundle


abstract class BaseFragment : BaseCoreFragment() {


    override fun onCreate(savedInstanceState: Bundle?) {
        setHasOptionsMenu(true)
        super.onCreate(savedInstanceState)

    }

    override fun onDestroy() {
        super.onDestroy()

    }


    override fun onDestroyView() {
        super.onDestroyView()
    }
}