package com.uct.inspection.core.interfaces

import android.content.Context
import android.content.pm.PackageManager
import android.view.View
import androidx.fragment.app.FragmentActivity
import com.uct.inspection.core.base.BaseActivity


interface CoreFragmentInterface {
    val mActivity: FragmentActivity?
    val mContext: Context?
    val baseActivityInstance: BaseActivity
    val mPackageManager:PackageManager
//    val mApplicationInstance: MyApplication

    //    fun initializeViews(view: View)
//    fun initializeLocalViews(view: View)
    fun setLayoutResource(): Int
    fun setValues()
    fun registerClickListener():Array<View>?
    fun getIntentValues()
}