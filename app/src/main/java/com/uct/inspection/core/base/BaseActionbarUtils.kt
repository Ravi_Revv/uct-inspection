package com.uct.inspection.core.base

import androidx.appcompat.app.ActionBar


class BaseActionbarUtils {
    companion object {
         fun setActionbarTitleVisibility(mActionbar: ActionBar?, isVisible: Boolean) {
            mActionbar?.setDisplayShowTitleEnabled(isVisible);
        }

         fun setActionbarHomeEnable(mActionbar: ActionBar?, isEnable: Boolean) {
            mActionbar?.setDisplayHomeAsUpEnabled(isEnable)
            mActionbar?.setDisplayShowHomeEnabled(isEnable)
        }

         fun setTitleOfActionBar(mActionbar: ActionBar?, titleStr: String) {
            mActionbar?.setTitle(titleStr);
            setActionbarTitleVisibility(mActionbar, true)
        }

         fun changeHomeIndicatorIcon(mActionbar: ActionBar?, resourceId: Int) {
            mActionbar?.setHomeAsUpIndicator(resourceId)
        }



    }
}