package com.uct.inspection.model.checklistE4;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.Ignore;
import androidx.room.PrimaryKey;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.uct.inspection.model.checklist.InspectionChecklistData;

import java.util.List;

@Entity(tableName = "InspectionE4Checklist")
public class InspectionE4Checklist {
    public InspectionE4Checklist() {
    }

    @Ignore
    public InspectionE4Checklist(int statusCode, int inspectionId, String statusMessage, List<InspectionChecklistE4Data> inspectionChecklistE4Data, long insertTS) {
        this.statusCode = statusCode;
        this.inspectionId = inspectionId;
        this.statusMessage = statusMessage;
        this.inspectionChecklistE4Data = inspectionChecklistE4Data;
        this.insertTS = insertTS;
    }

    @SerializedName("status_code")
    @Expose
    private int statusCode;
    @PrimaryKey
    @ColumnInfo(name = "inspectionId")
    @SerializedName("inspectionId")
    @Expose
    private int inspectionId;

    @ColumnInfo(name = "insertTS")
    @SerializedName("insertTS")
    @Expose
    private long insertTS;

    @SerializedName("status_message")
    @Expose
    private String statusMessage;

    public List<InspectionChecklistE4Data> getInspectionChecklistE4Data() {
        return inspectionChecklistE4Data;
    }

    public void setInspectionChecklistE4Data(List<InspectionChecklistE4Data> inspectionChecklistE4Data) {
        this.inspectionChecklistE4Data = inspectionChecklistE4Data;
    }

    //    @ColumnInfo(name = "data")
    @SerializedName("data")
    @Expose
    private List<InspectionChecklistE4Data> inspectionChecklistE4Data = null;


    public int getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(int statusCode) {
        this.statusCode = statusCode;
    }

    public String getStatusMessage() {
        return statusMessage;
    }

    public void setStatusMessage(String statusMessage) {
        this.statusMessage = statusMessage;
    }



    public int getInspectionId() {
        return inspectionId;
    }

    public void setInspectionId(int inspectionId) {
        this.inspectionId = inspectionId;
    }


    public long getInsertTS() {
        return insertTS;
    }

    public void setInsertTS(long insertTS) {
        this.insertTS = insertTS;
    }

}
