package com.uct.inspection.model.inspectordetail;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class InspectorCreatedAt {
    @SerializedName("year")
    @Expose
    private int year;
    @SerializedName("monthValue")
    @Expose
    private int monthValue;
    @SerializedName("hour")
    @Expose
    private int hour;
    @SerializedName("minute")
    @Expose
    private int minute;
    @SerializedName("second")
    @Expose
    private int second;
    @SerializedName("dayOfYear")
    @Expose
    private int dayOfYear;
    @SerializedName("dayOfWeek")
    @Expose
    private String dayOfWeek;
    @SerializedName("month")
    @Expose
    private String month;
    @SerializedName("dayOfMonth")
    @Expose
    private int dayOfMonth;
    @SerializedName("nano")
    @Expose
    private int nano;

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public int getMonthValue() {
        return monthValue;
    }

    public void setMonthValue(int monthValue) {
        this.monthValue = monthValue;
    }

    public int getHour() {
        return hour;
    }

    public void setHour(int hour) {
        this.hour = hour;
    }

    public int getMinute() {
        return minute;
    }

    public void setMinute(int minute) {
        this.minute = minute;
    }

    public int getSecond() {
        return second;
    }

    public void setSecond(int second) {
        this.second = second;
    }

    public int getDayOfYear() {
        return dayOfYear;
    }

    public void setDayOfYear(int dayOfYear) {
        this.dayOfYear = dayOfYear;
    }

    public String getDayOfWeek() {
        return dayOfWeek;
    }

    public void setDayOfWeek(String dayOfWeek) {
        this.dayOfWeek = dayOfWeek;
    }

    public String getMonth() {
        return month;
    }

    public void setMonth(String month) {
        this.month = month;
    }

    public int getDayOfMonth() {
        return dayOfMonth;
    }

    public void setDayOfMonth(int dayOfMonth) {
        this.dayOfMonth = dayOfMonth;
    }

    public int getNano() {
        return nano;
    }

    public void setNano(int nano) {
        this.nano = nano;
    }

}
