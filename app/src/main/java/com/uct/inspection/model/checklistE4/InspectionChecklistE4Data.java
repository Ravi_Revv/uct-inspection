package com.uct.inspection.model.checklistE4;

import androidx.room.Ignore;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.uct.inspection.model.checklist.ChecklistHeader;

import java.util.List;

public class InspectionChecklistE4Data {

    public InspectionChecklistE4Data() {
    }
    @Ignore
    public InspectionChecklistE4Data(String headerName, String headerID, List<ChecklistE4Header> mChecklistE4Header) {
        this.headerName = headerName;
        this.headerID = headerID;
        this.mChecklistE4Header = mChecklistE4Header;
    }

    @SerializedName("headerName")
    @Expose
    private String headerName;
    @SerializedName("headerID")
    @Expose
    private String headerID;
    @SerializedName("subPartList")
    @Expose
    private List<ChecklistE4Header> mChecklistE4Header = null;
    public String getHeaderName() {
        return headerName;
    }

    public void setName(String headerName) {
        this.headerName = headerName;
    }

    public List<ChecklistE4Header> getChecklistHeader() {
        return mChecklistE4Header;
    }

    public void setChecklistHeader(List<ChecklistE4Header> headerList) {
        this.mChecklistE4Header = headerList;
    }


    public String getHeaderID() {
        return headerID;
    }

    public void setHeaderID(String headerID) {
        this.headerID = headerID;
    }

}
