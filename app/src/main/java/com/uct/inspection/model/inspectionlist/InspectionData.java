package com.uct.inspection.model.inspectionlist;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class InspectionData {
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("inspectionsList")
    @Expose
    private List<InspectionList> inspectionsList = null;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<InspectionList> getInspectionsList() {
        return inspectionsList;
    }

    public void setInspectionsList(List<InspectionList> inspectionsList) {
        this.inspectionsList = inspectionsList;
    }
}

