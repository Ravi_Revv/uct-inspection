package com.uct.inspection.model.checklistE4;

import androidx.room.Ignore;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.uct.inspection.model.checklist.ChecklistSubIssue;

import java.util.List;

public class ChecklistE4Issue {
    public ChecklistE4Issue() {
    }
    @Ignore
    public ChecklistE4Issue(String issueName, String issueID, int issueType, boolean selected) {
        this.issueName = issueName;
        this.issueID = issueID;
        this.issueType = issueType;
        this.selected = selected;
    }

    @SerializedName("issueName")
    @Expose
    private String issueName;
    @SerializedName("issueID")
    @Expose
    private String issueID;
    @SerializedName("issueType")
    @Expose
    private int issueType;
    @SerializedName("selected")
    @Expose
    private boolean selected;

    public String getIssueName() {
        return issueName;
    }

    public void setIssueName(String issueName) {
        this.issueName = issueName;
    }

    public boolean getSelected() {
        return selected;
    }

    public void setSelected(boolean selected) {
        this.selected = selected;
    }

    public int getIssueType() {
        return issueType;
    }

    public void setIssueType(int issueType) {
        this.issueType = issueType;
    }


    public String getIssueID() {
        return issueID;
    }

    public void setIssueID(String issueID) {
        this.issueID = issueID;
    }


}


