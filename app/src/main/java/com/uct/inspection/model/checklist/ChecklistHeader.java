package com.uct.inspection.model.checklist;

import androidx.room.Ignore;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

public class ChecklistHeader {
    public ChecklistHeader() {
    }

    @Ignore
    public ChecklistHeader(String subPartName, String subPartID, String allowedSelection, boolean fileRequired, boolean isMandatory, String comments, List<ChecklistIssue> mChecklistIssue, List<ChecklistSubIssue> mDefaultSubIssues, List<ChecklistSubIssue> mAllSubIssues, List<String> images, boolean isUnAssured, String lastNFIStatus) {
        this.subPartName = subPartName;
        this.subPartID = subPartID;
        this.allowedSelection = allowedSelection;
        this.fileRequired = fileRequired;
        this.isMandatory = isMandatory;
        this.comments = comments;
        this.mChecklistIssue = mChecklistIssue;
        this.mDefaultSubIssues = mDefaultSubIssues;
        this.allSubIssues = mAllSubIssues;
        this.images = images;
        this.isUnAssured = isUnAssured;
        this.lastNFIStatus = lastNFIStatus;
    }

    @SerializedName("subPartName")
    @Expose
    private String subPartName;

    @SerializedName("subPartID")
    @Expose
    private String subPartID;

    @SerializedName("allowedSelection")
    @Expose
    private String allowedSelection;

    @SerializedName("fileRequired")
    @Expose
    private boolean fileRequired;
    @SerializedName("isMandatory")
    @Expose
    private boolean isMandatory;
    @SerializedName("comments")
    @Expose
    private String comments;


    @SerializedName("issues")
    @Expose
    private List<ChecklistIssue> mChecklistIssue = null;
    @SerializedName("defaultSubIssues")
    @Expose
    private List<ChecklistSubIssue> mDefaultSubIssues = null;


    @SerializedName("allSubIssues")
    @Expose
    private List<ChecklistSubIssue> allSubIssues = null;

    @SerializedName("images")
    @Expose
    private List<String> images = null;

    @SerializedName("isUnAssured")
    @Expose
    private boolean isUnAssured = false;


    @SerializedName("lastNFIStatus")
    @Expose
    private String lastNFIStatus;

    public String getSubPartName() {
        return subPartName;
    }

    public void setName(String subPartName) {
        this.subPartName = subPartName;
    }

    public List<ChecklistIssue> getChecklistIssue() {

        return mChecklistIssue;
    }

    public void setChecklistIssue(List<ChecklistIssue> issues) {
        this.mChecklistIssue = issues;
    }

    public String getAllowedSelection() {
        return allowedSelection;
    }

    public void setAllowedSelection(String allowedSelection) {
        this.allowedSelection = allowedSelection;
    }

    public List<ChecklistSubIssue> getAllSubIssues() {
        if (this.allSubIssues == null) {
            this.allSubIssues = new ArrayList<>();
        } else this.allSubIssues.clear();

        this.allSubIssues.addAll(this.mDefaultSubIssues);
        for (int i = 0; i < mChecklistIssue.size(); i++) {
            if (mChecklistIssue.get(i).getIssueType() == 1 && mChecklistIssue.get(i).getSelected()) {
                for (int j = 0; j < mChecklistIssue.get(i).getChecklistSubIssue().size(); j++) {
                    if (!this.allSubIssues.contains(mChecklistIssue.get(i).getChecklistSubIssue().get(j))) {
                        this.allSubIssues.add(mChecklistIssue.get(i).getChecklistSubIssue().get(j));
                    }
                }
//                mDefaultSubIssues.addAll(mChecklistIssue.get(i).getChecklistSubIssue());
            }
        }

        return allSubIssues;
    }

    public List<ChecklistSubIssue> getAllE4SubIssues() {
        if (this.allSubIssues == null) {
            this.allSubIssues = new ArrayList<>();
        } else this.allSubIssues.clear();

        this.allSubIssues.addAll(this.mDefaultSubIssues);
        for (int i = 0; i < mChecklistIssue.size(); i++) {
            if (mChecklistIssue.get(i).getIssueType() == 1 && mChecklistIssue.get(i).getSelected()) {
                for (int j = 0; j < mChecklistIssue.get(i).getChecklistSubIssue().size(); j++) {
                    if (!this.allSubIssues.contains(mChecklistIssue.get(i).getChecklistSubIssue().get(j))&& mChecklistIssue.get(i).getChecklistSubIssue().get(j).getSelected()) {
                        this.allSubIssues.add(mChecklistIssue.get(i).getChecklistSubIssue().get(j));
                    }
                }
//                mDefaultSubIssues.addAll(mChecklistIssue.get(i).getChecklistSubIssue());
            }
        }

        return allSubIssues;
    }

    public void setAllSubIssues(List<ChecklistSubIssue> allSubIssues) {
        this.allSubIssues = allSubIssues;
    }

    public List<ChecklistSubIssue> getDefaultSubIssues() {
        /*for (int i = 0; i < mChecklistIssue.size(); i++) {
            if (mChecklistIssue.get(i).getIssueType() == 1 && mChecklistIssue.get(i).getSelected()) {
                for (int j = 0; j < mChecklistIssue.get(i).getChecklistSubIssue().size(); j++) {
                    if (!mDefaultSubIssues.contains(mChecklistIssue.get(i).getChecklistSubIssue().get(j))) {
                        mDefaultSubIssues.add(mChecklistIssue.get(i).getChecklistSubIssue().get(j));
                    }
                }
//                mDefaultSubIssues.addAll(mChecklistIssue.get(i).getChecklistSubIssue());
            } else if (mChecklistIssue.get(i).getIssueType() == 1 && !mChecklistIssue.get(i).getSelected() && mDefaultSubIssues.containsAll(mChecklistIssue.get(i).getChecklistSubIssue())) {
                mDefaultSubIssues.removeAll(mChecklistIssue.get(i).getChecklistSubIssue());
                if (mChecklistIssue.get(i).getSelected()) {
                    for (int j = 0; j < mChecklistIssue.get(i).getChecklistSubIssue().size(); j++) {
                        if (!mDefaultSubIssues.contains(mChecklistIssue.get(i).getChecklistSubIssue().get(j))) {
                            mDefaultSubIssues.add(mChecklistIssue.get(i).getChecklistSubIssue().get(j));
                        }
                    }
//                mDefaultSubIssues.addAll(mChecklistIssue.get(i).getChecklistSubIssue());
                }
            }
        }*/


        /*for (int i = 0; i < mChecklistIssue.size(); i++) {
            if (mChecklistIssue.get(i).getIssueType() == 1 && mChecklistIssue.get(i).getSelected() && !mDefaultSubIssues.containsAll(mChecklistIssue.get(i).getChecklistSubIssue())) {
                mDefaultSubIssues.addAll(mChecklistIssue.get(i).getChecklistSubIssue());
            } else if (mChecklistIssue.get(i).getIssueType() == 1 && !mChecklistIssue.get(i).getSelected() && mDefaultSubIssues.containsAll(mChecklistIssue.get(i).getChecklistSubIssue())) {
                mDefaultSubIssues.removeAll(mChecklistIssue.get(i).getChecklistSubIssue());
            }
        }*/
        return mDefaultSubIssues;
    }

    public void setDefaultSubIssues(List<ChecklistSubIssue> mDefaultSubIssues) {
        this.mDefaultSubIssues = mDefaultSubIssues;
    }

    public List<String> getImages() {
        if (images == null)
            return new ArrayList<>();
        return images;
    }

    public void setImages(List<String> images) {
        this.images = images;
    }


    public String getSubPartID() {
        return subPartID;
    }

    public void setSubPartID(String subPartID) {
        this.subPartID = subPartID;
    }

    public boolean isFileRequired() {
        return fileRequired;
    }

    public void setFileRequired(boolean fileRequired) {
        this.fileRequired = fileRequired;
    }

    public boolean isMandatory() {
        return isMandatory;
    }

    public void setMandatory(boolean mandatory) {
        isMandatory = mandatory;
    }

    public String getComments() {
        return comments;
    }

    public void setComments(String comments) {
        this.comments = comments;
    }


    public boolean isUnAssured() {
        return isUnAssured;
    }

    public void setUnAssured(boolean unAssured) {
        isUnAssured = unAssured;
    }

    public String getLastNFIStatus() {
        return lastNFIStatus;
    }

    public void setLastNFIStatus(String lastNFIStatus) {
        this.lastNFIStatus = lastNFIStatus;
    }
}
