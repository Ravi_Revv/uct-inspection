package com.uct.inspection.model.checklistE4;

import androidx.room.Ignore;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.uct.inspection.model.checklist.ChecklistIssue;
import com.uct.inspection.model.checklist.ChecklistSubIssue;

import java.util.ArrayList;
import java.util.List;

public class ChecklistE4Header {
    public ChecklistE4Header() {
    }

    @Ignore
    public ChecklistE4Header(String subPartName, String subPartID, String allowedSelection, boolean fileRequired, boolean isMandatory, String comments, List<ChecklistE4Issue> mChecklistE4Issue, List<ChecklistE4SubIssue> mChecklistE4SubIssues, List<String> images, boolean isUnAssured, String lastNFIStatus) {
        this.subPartName = subPartName;
        this.subPartID = subPartID;
        this.allowedSelection = allowedSelection;
        this.fileRequired = fileRequired;
        this.isMandatory = isMandatory;
        this.comments = comments;
        this.mChecklistE4Issue = mChecklistE4Issue;
        this.mChecklistE4SubIssues = mChecklistE4SubIssues;
        this.images = images;
        this.isUnAssured = isUnAssured;
        this.lastNFIStatus = lastNFIStatus;
    }

    @SerializedName("subPartName")
    @Expose
    private String subPartName;

    @SerializedName("subPartID")
    @Expose
    private String subPartID;

    @SerializedName("allowedSelection")
    @Expose
    private String allowedSelection;

    @SerializedName("fileRequired")
    @Expose
    private boolean fileRequired;
    @SerializedName("isMandatory")
    @Expose
    private boolean isMandatory;
    @SerializedName("comments")
    @Expose
    private String comments;


    @SerializedName("issues")
    @Expose
    private List<ChecklistE4Issue> mChecklistE4Issue = null;

    @SerializedName("subIssues")
    @Expose
    private List<ChecklistE4SubIssue> mChecklistE4SubIssues = null;

    @SerializedName("images")
    @Expose
    private List<String> images = null;

    @SerializedName("isUnAssured")
    @Expose
    private boolean isUnAssured = false;


    @SerializedName("lastNFIStatus")
    @Expose
    private String lastNFIStatus;

    public String getSubPartName() {
        return subPartName;
    }

    public void setName(String subPartName) {
        this.subPartName = subPartName;
    }

    public List<ChecklistE4Issue> getChecklistIssue() {

        return mChecklistE4Issue;
    }

    public void setChecklistIssue(List<ChecklistE4Issue> issues) {
        this.mChecklistE4Issue = issues;
    }

    public String getAllowedSelection() {
        return allowedSelection;
    }

    public void setAllowedSelection(String allowedSelection) {
        this.allowedSelection = allowedSelection;
    }

    public List<ChecklistE4SubIssue> getAllSubIssues() {
        return mChecklistE4SubIssues;
    }

    public void setAllSubIssues(List<ChecklistE4SubIssue> allSubIssues) {
        this.mChecklistE4SubIssues = allSubIssues;
    }

    public List<String> getImages() {
        if (images == null)
            return new ArrayList<>();
        return images;
    }

    public void setImages(List<String> images) {
        this.images = images;
    }


    public String getSubPartID() {
        return subPartID;
    }

    public void setSubPartID(String subPartID) {
        this.subPartID = subPartID;
    }

    public boolean isFileRequired() {
        return fileRequired;
    }

    public void setFileRequired(boolean fileRequired) {
        this.fileRequired = fileRequired;
    }

    public boolean isMandatory() {
        return isMandatory;
    }

    public void setMandatory(boolean mandatory) {
        isMandatory = mandatory;
    }

    public String getComments() {
        return comments;
    }

    public void setComments(String comments) {
        this.comments = comments;
    }


    public boolean isUnAssured() {
        return isUnAssured;
    }

    public void setUnAssured(boolean unAssured) {
        isUnAssured = unAssured;
    }

    public String getLastNFIStatus() {
        return lastNFIStatus;
    }

    public void setLastNFIStatus(String lastNFIStatus) {
        this.lastNFIStatus = lastNFIStatus;
    }
}
