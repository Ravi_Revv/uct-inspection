package com.uct.inspection.model.refurb;

public class RefurbDocumentListValidation {
    public RefurbDocumentListValidation(boolean isDataValid, int nonValidPosition, RefurbDocument data) {
        this.isDataValid = isDataValid;
        this.nonValidPosition = nonValidPosition;
        this.data = data;
    }

    public boolean isDataValid() {
        return isDataValid;
    }

    public void setDataValid(boolean dataValid) {
        isDataValid = dataValid;
    }

    public int getNonValidPosition() {
        return nonValidPosition;
    }

    public void setNonValidPosition(int nonValidPosition) {
        this.nonValidPosition = nonValidPosition;
    }

    public RefurbDocument getData() {
        return data;
    }

    public void setData(RefurbDocument data) {
        this.data = data;
    }

    private boolean isDataValid;
    private int nonValidPosition;
    private RefurbDocument data;
}
