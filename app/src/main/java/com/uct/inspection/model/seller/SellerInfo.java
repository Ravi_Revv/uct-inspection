package com.uct.inspection.model.seller;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.uct.inspection.model.lead.LeadNameValue;

import java.util.ArrayList;
import java.util.List;

public class SellerInfo {
    @SerializedName("status_code")
    @Expose
    private Integer statusCode;
    @SerializedName("status_message")
    @Expose
    private String statusMessage;



    @SerializedName("data")
    @Expose
    private List<SellerInfoData> mSellerInfoData = null;

    private ArrayList<String> sellerInfoData = null;

    public Integer getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(Integer statusCode) {
        this.statusCode = statusCode;
    }

    public String getStatusMessage() {
        return statusMessage;
    }

    public void setStatusMessage(String statusMessage) {
        this.statusMessage = statusMessage;
    }

    public ArrayList<String> getSellerList() {
        if (sellerInfoData == null) {
            sellerInfoData = new ArrayList<>();
            for (SellerInfoData sellerInfo : mSellerInfoData) {
                sellerInfoData.add(sellerInfo.getSellerName());
            }
        }
        return sellerInfoData;
    }

    public List<SellerInfoData> getSellerInfoData() {
        return mSellerInfoData;
    }

    public void setSellerInfoData(List<SellerInfoData> mSellerInfoData) {
        this.mSellerInfoData = mSellerInfoData;
    }
}
