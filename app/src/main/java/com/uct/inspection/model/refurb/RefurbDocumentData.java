package com.uct.inspection.model.refurb;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class RefurbDocumentData {
    public RefurbDocumentData(long insertTS, List<RefurbDocument> documentList) {
        this.insertTS = insertTS;
        this.documentList = documentList;
    }

    @SerializedName("insertTS")
    @Expose
    private long insertTS;

    @SerializedName("documentList")
    @Expose
    private List<RefurbDocument> documentList = null;

    public List<RefurbDocument> getDocumentList() {
        return documentList;
    }

    public void setDocumentList(List<RefurbDocument> documentList) {
        this.documentList = documentList;
    }

    public long getInsertTS() {
        return insertTS;
    }

    public void setInsertTS(long insertTS) {
        this.insertTS = insertTS;
    }
}
