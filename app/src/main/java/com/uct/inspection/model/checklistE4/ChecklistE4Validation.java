package com.uct.inspection.model.checklistE4;

public class ChecklistE4Validation {
    private boolean isDataValid;
    private int nonValidPosition;
    private int nonValidTabPosition;

    public ChecklistE4Validation(boolean isDataValid, int nonValidPosition, int nonValidTabPosition) {
        this.isDataValid = isDataValid;
        this.nonValidPosition = nonValidPosition;
        this.nonValidTabPosition = nonValidTabPosition;
    }

    public ChecklistE4Validation() {
    }

    public boolean isDataValid() {
        return isDataValid;
    }

    public void setDataValid(boolean dataValid) {
        isDataValid = dataValid;
    }

    public int getNonValidPosition() {
        return nonValidPosition;
    }

    public void setNonValidPosition(int nonValidPosition) {
        this.nonValidPosition = nonValidPosition;
    }



    public int getNonValidTabPosition() {
        return nonValidTabPosition;
    }

    public void setNonValidTabPosition(int nonValidTabPosition) {
        this.nonValidTabPosition = nonValidTabPosition;
    }



    public ChecklistE4Validation(boolean isDataValid, int nonValidPosition) {
        this.isDataValid = isDataValid;
        this.nonValidPosition = nonValidPosition;
    }
}
