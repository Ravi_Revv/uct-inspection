package com.uct.inspection.model.lead;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

public class LeadInfoData {
    @SerializedName("city")
    @Expose
    private List<LeadCity> leadCityList = null;
    private ArrayList<String> mLeadCityList = null;

    @SerializedName("carModel")
    @Expose
    private List<LeadCarModel> leadCarModelList = null;
    private ArrayList<String> mLeadCarModelList = null;

    @SerializedName("registrationType")
    @Expose
    private List<LeadNameValue> registrationTypeList = null;
    private ArrayList<String> mRegistrationTypeList = null;

    @SerializedName("sellerType")
    @Expose
    private List<LeadNameValue> sellerTypeList = null;
    private ArrayList<String> mSellerTypeList = null;

    @SerializedName("insuranceType")
    @Expose
    private List<LeadNameValue> insuranceTypeList = null;
    private ArrayList<String> mInsuranceTypeList = null;

    public ArrayList<String> getLeadCityList() {
        if (mLeadCityList == null) {
            mLeadCityList = new ArrayList<>();
            for (LeadCity leadCity : leadCityList) {
                mLeadCityList.add(leadCity.getName());
            }
        }

        return mLeadCityList;
    }

    public List<LeadCity> getmLeadCityList() {
        return leadCityList;
    }


    public ArrayList<String> getLeadCarModelList() {
        if (mLeadCarModelList == null) {
            mLeadCarModelList = new ArrayList<>();
            for (LeadCarModel leadCarModel : leadCarModelList) {
                mLeadCarModelList.add(String.format("%s %s %s %s %s %s %s %s %s", leadCarModel.getMake(), "|", leadCarModel.getModel(), "|", leadCarModel.getVariant(), "|", leadCarModel.getFuel(), "|", leadCarModel.getTransmission()));
            }
        }
        return mLeadCarModelList;
    }

    public List<LeadCarModel> getmLeadCarModelList() {
        return leadCarModelList;
    }

    public ArrayList<String> getRegistrationTypeList() {
        if (mRegistrationTypeList == null) {
            mRegistrationTypeList = new ArrayList<>();
            for (LeadNameValue leadNameValue : registrationTypeList) {
                mRegistrationTypeList.add(leadNameValue.getName());
            }
        }
        return mRegistrationTypeList;
    }

    public List<LeadNameValue> getmRegistrationTypeList() {
        return registrationTypeList;
    }



    public ArrayList<String> getSellerTypeList() {
        if (mSellerTypeList == null) {
            mSellerTypeList = new ArrayList<>();
            for (LeadNameValue leadNameValue : sellerTypeList) {
                mSellerTypeList.add(leadNameValue.getName());
            }
        }
        return mSellerTypeList;
    }

    public List<LeadNameValue> getmSellerTypeList() {
        return sellerTypeList;
    }


    public List<String> getInsuranceTypeList() {
        if (mInsuranceTypeList == null) {
            mInsuranceTypeList = new ArrayList<>();
            for (LeadNameValue leadNameValue : insuranceTypeList) {
                mInsuranceTypeList.add(leadNameValue.getName());
            }
        }

        return mInsuranceTypeList;
    }

    public List<LeadNameValue> getmInsuranceTypeList() {
        return insuranceTypeList;
    }

}
