package com.uct.inspection.model.verdict;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class InspectionVerdict {
    @SerializedName("status_code")
    @Expose
    private int statusCode;
    @SerializedName("status_message")
    @Expose
    private String statusMessage;
    @SerializedName("data")
    @Expose
    private List<InspectionVerdictData> mInspectionVerdictData = null;

    public int getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(int statusCode) {
        this.statusCode = statusCode;
    }

    public String getStatusMessage() {
        return statusMessage;
    }

    public void setStatusMessage(String statusMessage) {
        this.statusMessage = statusMessage;
    }

    public List<InspectionVerdictData> getInspectionVerdictListData() {
        return mInspectionVerdictData;
    }

    public InspectionVerdictData getInspectionVerdictData() {
        if (mInspectionVerdictData != null && mInspectionVerdictData.size() > 0) {
            return mInspectionVerdictData.get(0);
        }

        return null;
    }

    public void setInspectionVerdictData(List<InspectionVerdictData> data) {
        this.mInspectionVerdictData = data;
    }
}
