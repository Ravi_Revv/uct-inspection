package com.uct.inspection.model.inspectionlist;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class WorkshopData implements Parcelable {

    @SerializedName("workshopID")
    @Expose
    private String workshopID;
    @SerializedName("workshopName")
    @Expose
    private String workshopName;

    protected WorkshopData(Parcel in) {
        workshopID = in.readString();
        workshopName = in.readString();
    }

    public static final Creator<WorkshopData> CREATOR = new Creator<WorkshopData>() {
        @Override
        public WorkshopData createFromParcel(Parcel in) {
            return new WorkshopData(in);
        }

        @Override
        public WorkshopData[] newArray(int size) {
            return new WorkshopData[size];
        }
    };

    public String getWorkshopID() {
        return workshopID;
    }

    public void setWorkshopID(String workshopID) {
        this.workshopID = workshopID;
    }

    public String getWorkshopName() {
        return workshopName;
    }

    public void setWorkshopName(String workshopName) {
        this.workshopName = workshopName;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(workshopID);
        dest.writeString(workshopName);
    }
}
