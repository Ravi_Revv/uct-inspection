package com.uct.inspection.model.inspectordetail;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class InspectorData {
    public InspectorData(int id, String userName, String userEmail, int userCity, String password, String adminType) {
        this.id = id;
        this.userName = userName;
        this.userEmail = userEmail;
        this.userCity = userCity;
        this.password = password;
        this.adminType = adminType;
    }

    @SerializedName("id")
    @Expose
    private int id;
    @SerializedName("userName")
    @Expose
    private String userName;
    @SerializedName("userEmail")
    @Expose
    private String userEmail;
    @SerializedName("userCity")
    @Expose
    private int userCity;
    @SerializedName("password")
    @Expose
    private String password;
    @SerializedName("adminType")
    @Expose
    private String adminType;


    @SerializedName("createdAt")
    @Expose
    private InspectorCreatedAt createdAt;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getUserEmail() {
        return userEmail;
    }

    public void setUserEmail(String userEmail) {
        this.userEmail = userEmail;
    }

    public int getUserCity() {
        return userCity;
    }

    public void setUserCity(int userCity) {
        this.userCity = userCity;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getAdminType() {
        return adminType;
    }

    public void setAdminType(String adminType) {
        this.adminType = adminType;
    }

    public InspectorCreatedAt getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(InspectorCreatedAt createdAt) {
        this.createdAt = createdAt;
    }
}
