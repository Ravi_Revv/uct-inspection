package com.uct.inspection.model.files;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class FileRequest {
    @Expose
    @SerializedName("subPartID")
    private String subPartID;
    @Expose
    @SerializedName("fileRequired")
    private boolean fileRequired;
    @Expose
    @SerializedName("images")
    private List<String> images;

    public FileRequest(String subPartID, boolean fileRequired, List<String> images) {
        this.subPartID = subPartID;
        this.fileRequired = fileRequired;
        this.images = images;
    }
}
