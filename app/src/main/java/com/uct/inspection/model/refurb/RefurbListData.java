package com.uct.inspection.model.refurb;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class RefurbListData {


    @SerializedName("inspectionID")
    @Expose
    private int inspectionID;
    @SerializedName("inspectionType")
    @Expose
    private String inspectionType;

    @SerializedName("insertTS")
    @Expose
    private long insertTS;
    @SerializedName("carModel")
    @Expose
    private String carModel;
    @SerializedName("jobsCount")
    @Expose
    private String jobsCount;
    @SerializedName("regNo")
    @Expose
    private String regNo;
    @SerializedName("jobList")
    @Expose
    private List<JobListData> jobList = null;
    @SerializedName("documentList")
    @Expose
    private List<RefurbDocument> documentList = null;

    public String getCarModel() {
        return carModel;
    }

    public void setCarModel(String carModel) {
        this.carModel = carModel;
    }

    public String getJobsCount() {
        return jobsCount;
    }

    public void setJobsCount(String jobsCount) {
        this.jobsCount = jobsCount;
    }

    public String getRegNo() {
        return regNo;
    }

    public void setCarRegistrationNumber(String regNo) {
        this.regNo = regNo;
    }

    public List<JobListData> getJobListData() {
        return jobList;
    }

    public void setJobListData(List<JobListData> jobList) {
        this.jobList = jobList;
    }


    public List<RefurbDocument> getDocumentList() {
        return documentList;
    }

    public void setDocumentList(List<RefurbDocument> documentList) {
        this.documentList = documentList;
    }

    public int getInspectionID() {
        return inspectionID;
    }

    public void setInspectionID(int inspectionID) {
        this.inspectionID = inspectionID;
    }

    public String getInspectionType() {
        return inspectionType;
    }

    public void setInspectionType(String inspectionType) {
        this.inspectionType = inspectionType;
    }

    public long getInsertTS() {
        return insertTS;
    }

    public void setInsertTS(long insertTS) {
        this.insertTS = insertTS;
    }
}
