package com.uct.inspection.model.inspectionlist;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class InspectionListData implements Parcelable {
    @SerializedName("id")
    @Expose
    private int id;
    @SerializedName("inspectionType")
    @Expose
    private String inspectionType;
    @SerializedName("inspectionTime")
    @Expose
    private String inspectionTime;

    @SerializedName("inspectionTimeDisplay")
    @Expose
    private String inspectionTimeDisplay;

    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("contactNumber")
    @Expose
    private String contactNumber;
    @SerializedName("sellerName")
    @Expose
    private String sellerName;
    @SerializedName("sellerAddress")
    @Expose
    private String sellerAddress;
    @SerializedName("carModel")
    @Expose
    private String carModel;


    @SerializedName("carMake")
    @Expose
    private String carMake;
    @SerializedName("carVariant")
    @Expose
    private String carVariant;
    @SerializedName("carYear")
    @Expose
    private String carYear;
    @SerializedName("carRegistrationNumber")
    @Expose
    private String carRegistrationNumber;

    @SerializedName("carFuel")
    @Expose
    private String carFuel;

    @SerializedName("carTransmission")
    @Expose
    private String carTransmission;

    @SerializedName("sellerLat")
    @Expose
    private double sellerLat;

    @SerializedName("sellerLng")
    @Expose
    private double sellerLng;

    @SerializedName("leadID")
    @Expose
    private String leadID;

    @SerializedName("rto")
    @Expose
    private String rto;

    @SerializedName("registrationTime")
    @Expose
    private String registrationTime;

    @SerializedName("insuranceValidity")
    @Expose
    private String insuranceValidity;

    @SerializedName("insuranceType")
    @Expose
    private String insuranceType;

    @SerializedName("assignedBDE")
    @Expose
    private String assignedBDE;

    @SerializedName("kmDriven")
    @Expose
    private String kmDriven;
    @SerializedName("inspectionIDDisplay")
    @Expose
    private String inspectionIDDisplay;


    @SerializedName("insertTS")
    @Expose
    private long insertTS;

    @SerializedName("showPickupChecklist")
    @Expose
    private boolean showPickupChecklist;


    @SerializedName("cardType")
    @Expose
    private String cardType;

    @SerializedName("sellerID")
    @Expose
    private int sellerID;
    @SerializedName("sellerType")
    @Expose
    private String sellerType;
    @SerializedName("serviceCityID")
    @Expose
    private int serviceCityID;

    @SerializedName("registrationType")
    @Expose
    private String registrationType;

    @SerializedName("ownershipSerial")
    @Expose
    private String ownershipSerial;

    @SerializedName("carColor")
    @Expose
    private String carColor;

    @SerializedName("comments")
    @Expose
    private String comments;

    @SerializedName("kmDrivenDisplay")
    @Expose
    private String kmDrivenDisplay;

    @SerializedName("editLead")
    @Expose
    private boolean editLead;
    @SerializedName("manufacturingTimestamp")
    @Expose
    private String manufacturingTimestamp;

    @SerializedName("registrationTimeDisplay")
    @Expose
    private String registrationTimeDisplay;

    @SerializedName("insuranceValidityDisplay")
    @Expose
    private String insuranceValidityDisplay;

    @SerializedName("workshopName")
    @Expose
    private String workshopName;

    @SerializedName("workshopAddress")
    @Expose
    private String workshopAddress;

    @SerializedName("carID")
    @Expose
    private int carId;

    @SerializedName("noOfJobs")
    @Expose
    private String noOfJobs;

    @SerializedName("e3Inspector")
    @Expose
    private String e3Inspector;

    @SerializedName("refurbSpoc")
    @Expose
    private String refurbSpoc;

    @SerializedName("workshops")
    @Expose
    private List<WorkshopData> workshops;

    @SerializedName("refurbSpocs")
    @Expose
    private List<RefurbSpocsData> refurbSpocs;


    protected InspectionListData(Parcel in) {
        id = in.readInt();
        inspectionType = in.readString();
        inspectionTime = in.readString();
        inspectionTimeDisplay = in.readString();
        status = in.readString();
        contactNumber = in.readString();
        sellerName = in.readString();
        sellerAddress = in.readString();
        carModel = in.readString();
        carMake = in.readString();
        carVariant = in.readString();
        carYear = in.readString();
        carRegistrationNumber = in.readString();
        carFuel = in.readString();
        carTransmission = in.readString();
        sellerLat = in.readDouble();
        sellerLng = in.readDouble();
        leadID = in.readString();
        rto = in.readString();
        registrationTime = in.readString();
        insuranceValidity = in.readString();
        insuranceType = in.readString();
        assignedBDE = in.readString();
        kmDriven = in.readString();
        inspectionIDDisplay = in.readString();
        insertTS = in.readLong();
        showPickupChecklist = in.readByte() != 0;
        cardType = in.readString();
        editLead = in.readByte() != 0;
        sellerID = in.readInt();
        sellerType = in.readString();
        serviceCityID = in.readInt();
        registrationType = in.readString();
        ownershipSerial = in.readString();
        kmDrivenDisplay = in.readString();
        manufacturingTimestamp = in.readString();
        comments = in.readString();
        carColor = in.readString();
        registrationTimeDisplay = in.readString();
        insuranceValidityDisplay = in.readString();
        workshopName = in.readString();
        workshopAddress = in.readString();
        carId = in.readInt();
        noOfJobs = in.readString();
        e3Inspector = in.readString();
        refurbSpoc = in.readString();
//        in.readTypedList(workshops, WorkshopData.CREATOR);
//        in.readTypedList(refurbSpocs, RefurbSpocsData.CREATOR);
        workshops = in.createTypedArrayList(WorkshopData.CREATOR);
        refurbSpocs = in.createTypedArrayList(RefurbSpocsData.CREATOR);

    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(id);
        dest.writeString(inspectionType);
        dest.writeString(inspectionTime);
        dest.writeString(inspectionTimeDisplay);
        dest.writeString(status);
        dest.writeString(contactNumber);
        dest.writeString(sellerName);
        dest.writeString(sellerAddress);
        dest.writeString(carModel);
        dest.writeString(carMake);
        dest.writeString(carVariant);
        dest.writeString(carYear);
        dest.writeString(carRegistrationNumber);
        dest.writeString(carFuel);
        dest.writeString(carTransmission);
        dest.writeDouble(sellerLat);
        dest.writeDouble(sellerLng);
        dest.writeString(leadID);
        dest.writeString(rto);
        dest.writeString(registrationTime);
        dest.writeString(insuranceValidity);
        dest.writeString(insuranceType);
        dest.writeString(assignedBDE);
        dest.writeString(kmDriven);
        dest.writeString(inspectionIDDisplay);
        dest.writeLong(insertTS);
        dest.writeByte((byte) (showPickupChecklist ? 1 : 0));
        dest.writeString(cardType);
        dest.writeByte((byte) (editLead ? 1 : 0));
        dest.writeInt(sellerID);
        dest.writeString(sellerType);
        dest.writeInt(serviceCityID);
        dest.writeString(registrationType);
        dest.writeString(ownershipSerial);
        dest.writeString(kmDrivenDisplay);
        dest.writeString(manufacturingTimestamp);
        dest.writeString(comments);
        dest.writeString(carColor);
        dest.writeString(registrationTimeDisplay);
        dest.writeString(insuranceValidityDisplay);
        dest.writeString(workshopName);
        dest.writeString(workshopAddress);
        dest.writeInt(carId);
        dest.writeString(noOfJobs);
        dest.writeString(e3Inspector);
        dest.writeString(refurbSpoc);
        dest.writeTypedList(workshops);
        dest.writeTypedList(refurbSpocs);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<InspectionListData> CREATOR = new Creator<InspectionListData>() {
        @Override
        public InspectionListData createFromParcel(Parcel in) {
            return new InspectionListData(in);
        }

        @Override
        public InspectionListData[] newArray(int size) {
            return new InspectionListData[size];
        }
    };

    public String getCarFuel() {
        return carFuel;
    }

    public void setCarFuel(String carFuel) {
        this.carFuel = carFuel;
    }

    public String getCarTransmission() {
        return carTransmission;
    }

    public void setCarTransmission(String carTransmission) {
        this.carTransmission = carTransmission;
    }

    public double getSellerLat() {
        return sellerLat;
    }

    public void setSellerLat(double sellerLat) {
        this.sellerLat = sellerLat;
    }

    public double getSellerLng() {
        return sellerLng;
    }

    public void setSellerLng(double sellerLng) {
        this.sellerLng = sellerLng;
    }

    public String getLeadID() {
        return leadID;
    }

    public void setLeadID(String leadID) {
        this.leadID = leadID;
    }

    public String getRto() {
        return rto;
    }

    public void setRto(String rto) {
        this.rto = rto;
    }

    public String getRegistrationTime() {
        return registrationTime;
    }

    public void setRegistrationTime(String registrationTime) {
        this.registrationTime = registrationTime;
    }

    public String getInsuranceValidity() {
        return insuranceValidity;
    }

    public void setInsuranceValidity(String insuranceValidity) {
        this.insuranceValidity = insuranceValidity;
    }

    public String getInsuranceType() {
        return insuranceType;
    }

    public void setInsuranceType(String insuranceType) {
        this.insuranceType = insuranceType;
    }

    public String getAssignedBDE() {
        return assignedBDE;
    }

    public void setAssignedBDE(String assignedBDE) {
        this.assignedBDE = assignedBDE;
    }

    public String getKmDriven() {
        return kmDriven;
    }

    public void setKmDriven(String kmDriven) {
        this.kmDriven = kmDriven;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getInspectionType() {
        return inspectionType;
    }

    public void setInspectionType(String inspectionType) {
        this.inspectionType = inspectionType;
    }

    public String getInspectionTime() {
        return inspectionTime;
    }

    public void setInspectionTime(String inspectionTime) {
        this.inspectionTime = inspectionTime;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getContactNumber() {
        return contactNumber;
    }

    public void setContactNumber(String contactNumber) {
        this.contactNumber = contactNumber;
    }

    public String getSellerName() {
        return sellerName;
    }

    public void setSellerName(String sellerName) {
        this.sellerName = sellerName;
    }

    public String getSellerAddress() {
        return sellerAddress;
    }

    public void setSellerAddress(String sellerAddress) {
        this.sellerAddress = sellerAddress;
    }

    public String getCarModel() {
        return carModel;
    }

    public void setCarModel(String carModel) {
        this.carModel = carModel;
    }

    public String getCarVariant() {
        return carVariant;
    }

    public void setCarVariant(String carVariant) {
        this.carVariant = carVariant;
    }

    public String getCarYear() {
        return carYear;
    }

    public void setCarYear(String carYear) {
        this.carYear = carYear;
    }

    public String getCarRegistrationNumber() {
        return carRegistrationNumber;
    }

    public void setCarRegistrationNumber(String carRegistrationNumber) {
        this.carRegistrationNumber = carRegistrationNumber;
    }


    public String getInspectionTimeDisplay() {
        return inspectionTimeDisplay;
    }

    public void setInspectionTimeDisplay(String inspectionTimeDisplay) {
        this.inspectionTimeDisplay = inspectionTimeDisplay;
    }


    public String getInspectionIDDisplay() {
        return inspectionIDDisplay;
    }

    public void setInspectionIDDisplay(String inspectionIDDisplay) {
        this.inspectionIDDisplay = inspectionIDDisplay;
    }


    public String getCarMake() {
        return carMake;
    }

    public void setCarMake(String carMake) {
        this.carMake = carMake;
    }

    public long getInsertTS() {
        return insertTS;
    }

    public void setInsertTS(long insertTS) {
        this.insertTS = insertTS;
    }


    public boolean isShowPickupChecklist() {
        return showPickupChecklist;
    }

    public void setShowPickupChecklist(boolean showPickupChecklist) {
        this.showPickupChecklist = showPickupChecklist;
    }

    public String getCardType() {
        return cardType;
    }

    public void setCardType(String cardType) {
        this.cardType = cardType;
    }


    public int getSellerID() {
        return sellerID;
    }

    public void setSellerID(int sellerID) {
        this.sellerID = sellerID;
    }

    public String getSellerType() {
        return sellerType;
    }

    public void setSellerType(String sellerType) {
        this.sellerType = sellerType;
    }

    public int getServiceCityID() {
        return serviceCityID;
    }

    public void setServiceCityID(int serviceCityID) {
        this.serviceCityID = serviceCityID;
    }

    public String getRegistrationType() {
        return registrationType;
    }

    public void setRegistrationType(String registrationType) {
        this.registrationType = registrationType;
    }

    public String getOwnershipSerial() {
        return ownershipSerial;
    }

    public void setOwnershipSerial(String ownershipSerial) {
        this.ownershipSerial = ownershipSerial;
    }

    public String getCarColor() {
        return carColor;
    }

    public void setCarColor(String carColor) {
        this.carColor = carColor;
    }

    public String getComments() {
        return comments;
    }

    public void setComments(String comments) {
        this.comments = comments;
    }

    public String getKmDrivenDisplay() {
        return kmDrivenDisplay;
    }

    public void setKmDrivenDisplay(String kmDrivenDisplay) {
        this.kmDrivenDisplay = kmDrivenDisplay;
    }

    public boolean isEditLead() {
        return editLead;
    }

    public void setEditLead(boolean editLead) {
        this.editLead = editLead;
    }

    public String getManufacturingTimestamp() {
        return manufacturingTimestamp;
    }

    public void setManufacturingTimestamp(String manufacturingTimestamp) {
        this.manufacturingTimestamp = manufacturingTimestamp;
    }


    public String getRegistrationTimeDisplay() {
        return registrationTimeDisplay;
    }

    public void setRegistrationTimeDisplay(String registrationTimeDisplay) {
        this.registrationTimeDisplay = registrationTimeDisplay;
    }

    public String getInsuranceValidityDisplay() {
        return insuranceValidityDisplay;
    }

    public void setInsuranceValidityDisplay(String insuranceValidityDisplay) {
        this.insuranceValidityDisplay = insuranceValidityDisplay;
    }

    public String getWorkshopName() {
        return workshopName;
    }

    public void setWorkshopName(String workshopName) {
        this.workshopName = workshopName;
    }

    public String getWorkshopAddress() {
        return workshopAddress;
    }

    public void setWorkshopAddress(String workshopAddress) {
        this.workshopAddress = workshopAddress;
    }

    public int getCarId() {
        return carId;
    }

    public void setCarId(int carId) {
        this.carId = carId;
    }

    public String getNoOfJobs() {
        return noOfJobs;
    }

    public void setNoOfJobs(String noOfJobs) {
        this.noOfJobs = noOfJobs;
    }

    public String getE3Inspector() {
        return e3Inspector;
    }

    public void setE3Inspector(String e3Inspector) {
        this.e3Inspector = e3Inspector;
    }

    public String getRefurbSpoc() {
        return refurbSpoc;
    }

    public void setRefurbSpoc(String refurbSpoc) {
        this.refurbSpoc = refurbSpoc;
    }

    public List<WorkshopData> getWorkshops() {
        return workshops;
    }

    public void setWorkshops(List<WorkshopData> workshops) {
        this.workshops = workshops;
    }

    public List<RefurbSpocsData> getRefurbSpocs() {
        return refurbSpocs;
    }

    public void setRefurbSpocs(List<RefurbSpocsData> refurbSpocs) {
        this.refurbSpocs = refurbSpocs;
    }
}
