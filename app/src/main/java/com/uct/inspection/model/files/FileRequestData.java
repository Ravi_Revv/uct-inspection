package com.uct.inspection.model.files;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class FileRequestData {
    @SerializedName("insertTS")
    @Expose
    private long insertTS;
    @SerializedName("data")
    @Expose
    private List<FileRequest> mInspectionChecklistFile = null;

    public FileRequestData(long insertTS, List<FileRequest> mInspectionChecklistFile) {
        this.insertTS = insertTS;
        this.mInspectionChecklistFile = mInspectionChecklistFile;
    }
}
