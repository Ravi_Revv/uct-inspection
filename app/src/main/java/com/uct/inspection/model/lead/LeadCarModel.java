package com.uct.inspection.model.lead;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class LeadCarModel {
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getMake() {
        return make;
    }

    public void setMake(String make) {
        this.make = make;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public String getVariant() {
        return variant;
    }

    public void setVariant(String variant) {
        this.variant = variant;
    }

    public String getFuel() {
        return fuel;
    }

    public void setFuel(String fuel) {
        this.fuel = fuel;
    }

    public String getTransmission() {
        return transmission;
    }

    public void setTransmission(String transmission) {
        this.transmission = transmission;
    }

    public String getBodyType() {
        return bodyType;
    }

    public void setBodyType(String bodyType) {
        this.bodyType = bodyType;
    }

    public int getSeats() {
        return seats;
    }

    public void setSeats(int seats) {
        this.seats = seats;
    }

    public int getStartYear() {
        return startYear;
    }

    public void setStartYear(int startYear) {
        this.startYear = startYear;
    }

    public int getEndYear() {
        return endYear;
    }

    public void setEndYear(int endYear) {
        this.endYear = endYear;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public int getVersionNumber() {
        return versionNumber;
    }

    public void setVersionNumber(int versionNumber) {
        this.versionNumber = versionNumber;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    @SerializedName("id")
    @Expose
    private int id;
    @SerializedName("make")
    @Expose
    private String make;
    @SerializedName("model")
    @Expose
    private String model;
    @SerializedName("variant")
    @Expose
    private String variant;
    @SerializedName("fuel")
    @Expose
    private String fuel;
    @SerializedName("transmission")
    @Expose
    private String transmission;
    @SerializedName("bodyType")
    @Expose
    private String bodyType;
    @SerializedName("seats")
    @Expose
    private int seats;
    @SerializedName("startYear")
    @Expose
    private int startYear;
    @SerializedName("endYear")
    @Expose
    private int endYear;
    @SerializedName("price")
    @Expose
    private double price;
    @SerializedName("versionNumber")
    @Expose
    private int versionNumber;
    @SerializedName("active")
    @Expose
    private boolean active;


}
