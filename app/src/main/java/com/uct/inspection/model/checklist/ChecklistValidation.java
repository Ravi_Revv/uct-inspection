package com.uct.inspection.model.checklist;

public class ChecklistValidation {
    private boolean isDataValid;
    private int nonValidPosition;
    private int nonValidTabPosition;

    public ChecklistValidation(boolean isDataValid, int nonValidPosition, int nonValidTabPosition) {
        this.isDataValid = isDataValid;
        this.nonValidPosition = nonValidPosition;
        this.nonValidTabPosition = nonValidTabPosition;
    }

    public ChecklistValidation() {
    }

    public boolean isDataValid() {
        return isDataValid;
    }

    public void setDataValid(boolean dataValid) {
        isDataValid = dataValid;
    }

    public int getNonValidPosition() {
        return nonValidPosition;
    }

    public void setNonValidPosition(int nonValidPosition) {
        this.nonValidPosition = nonValidPosition;
    }



    public int getNonValidTabPosition() {
        return nonValidTabPosition;
    }

    public void setNonValidTabPosition(int nonValidTabPosition) {
        this.nonValidTabPosition = nonValidTabPosition;
    }



    public ChecklistValidation(boolean isDataValid, int nonValidPosition) {
        this.isDataValid = isDataValid;
        this.nonValidPosition = nonValidPosition;
    }
}
