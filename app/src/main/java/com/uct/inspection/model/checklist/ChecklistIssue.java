package com.uct.inspection.model.checklist;

import androidx.room.Embedded;
import androidx.room.Ignore;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class ChecklistIssue {
    public ChecklistIssue() {
    }
    @Ignore
    public ChecklistIssue(String issueName, String issueID, int issueType, boolean selected, List<ChecklistSubIssue> mChecklistSubIssue) {
        this.issueName = issueName;
        this.issueID = issueID;
        this.issueType = issueType;
        this.selected = selected;
        this.mChecklistSubIssue = mChecklistSubIssue;
    }

    @SerializedName("issueName")
    @Expose
    private String issueName;
    @SerializedName("issueID")
    @Expose
    private String issueID;
    @SerializedName("issueType")
    @Expose
    private int issueType;
    @SerializedName("selected")
    @Expose
    private boolean selected;
    @SerializedName("subIssues")
    @Expose
    private List<ChecklistSubIssue> mChecklistSubIssue = null;



    public String getIssueName() {
        return issueName;
    }

    public void setIssueName(String issueName) {
        this.issueName = issueName;
    }

    public boolean getSelected() {
        return selected;
    }

    public void setSelected(boolean selected) {
        this.selected = selected;
    }

    public List<ChecklistSubIssue> getChecklistSubIssue() {
        return mChecklistSubIssue;
    }

    public void setChecklistSubIssue(List<ChecklistSubIssue> subIssues) {
        this.mChecklistSubIssue = subIssues;
    }


    public int getIssueType() {
        return issueType;
    }

    public void setIssueType(int issueType) {
        this.issueType = issueType;
    }


    public String getIssueID() {
        return issueID;
    }

    public void setIssueID(String issueID) {
        this.issueID = issueID;
    }


}


