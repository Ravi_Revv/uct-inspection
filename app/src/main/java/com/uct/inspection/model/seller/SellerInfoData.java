package com.uct.inspection.model.seller;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class SellerInfoData {

    @SerializedName("sellerName")
    @Expose
    private String sellerName;
    @SerializedName("sellerCity")
    @Expose
    private String sellerCity;
    @SerializedName("sellerID")
    @Expose
    private int sellerID;

    public String getSellerName() {
        return sellerName;
    }

    public void setSellerName(String sellerName) {
        this.sellerName = sellerName;
    }

    public String getSellerCity() {
        return sellerCity;
    }

    public void setSellerCity(String sellerCity) {
        this.sellerCity = sellerCity;
    }

    public int getSellerID() {
        return sellerID;
    }

    public void setSellerID(int sellerID) {
        this.sellerID = sellerID;
    }
}
