package com.uct.inspection.model.document;

public class DocumentListValidation {
    public DocumentListValidation(boolean isDataValid, int nonValidPosition, InspectionDocumentData data) {
        this.isDataValid = isDataValid;
        this.nonValidPosition = nonValidPosition;
        this.data = data;
    }

    public boolean isDataValid() {
        return isDataValid;
    }

    public void setDataValid(boolean dataValid) {
        isDataValid = dataValid;
    }

    public int getNonValidPosition() {
        return nonValidPosition;
    }

    public void setNonValidPosition(int nonValidPosition) {
        this.nonValidPosition = nonValidPosition;
    }

    public InspectionDocumentData getData() {
        return data;
    }

    public void setData(InspectionDocumentData data) {
        this.data = data;
    }

    private boolean isDataValid;
    private int nonValidPosition;
    private InspectionDocumentData data;
}
