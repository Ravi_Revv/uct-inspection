package com.uct.inspection.model.checklistE4;

import androidx.annotation.Nullable;
import androidx.room.Ignore;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ChecklistE4SubIssue {
    public ChecklistE4SubIssue() {
    }

    @Ignore
    public ChecklistE4SubIssue(String jobName, String jobID, int cost, int maxCost, int minCost, boolean selected, int subIssueType) {
        this.jobName = jobName;
        this.jobID = jobID;
        this.cost = cost;
        this.maxCost = maxCost;
        this.minCost = minCost;
        this.selected = selected;
        this.subIssueType = subIssueType;
    }

    @SerializedName("jobName")
    @Expose
    private String jobName;

    @SerializedName("jobID")
    @Expose
    private String jobID;
    @SerializedName("cost")
    @Expose
    private int cost;
    @SerializedName("status")
    @Expose
    private int status;
    @SerializedName("maxCost")
    @Expose
    private int maxCost;
    @SerializedName("minCost")
    @Expose
    private int minCost;
    @SerializedName("selected")
    @Expose
    private boolean selected;
    @SerializedName("subIssueType")
    @Expose
    private int subIssueType;


    public String getJobName() {
        return jobName;
    }

    public void setJobName(String name) {
        this.jobName = jobName;
    }

    public int getCost() {
        return cost;
    }

    public void setCost(int cost) {
        this.cost = cost;
    }

    public int getMaxCost() {
        return maxCost;
    }

    public void setMaxCost(int max) {
        this.maxCost = max;
    }

    public int getMinCost() {
        return minCost;
    }

    public void setMinCost(int minCost) {
        this.minCost = minCost;
    }

    public boolean getSelected() {
        return selected;
    }

    public void setSelected(boolean selected) {
        this.selected = selected;
    }

    @Override
    public int hashCode() {
        return super.hashCode();
    }


    public String getJobID() {
        return jobID;
    }

    public void setJobID(String jobID) {
        this.jobID = jobID;
    }

    @Override
    public boolean equals(@Nullable Object obj) {
        if (!(obj instanceof ChecklistE4SubIssue))
            return false;
        return this.jobID.equals(((ChecklistE4SubIssue) obj).jobID);
//        return super.equals(obj);
    }

    public int getSubIssueType() {
        return subIssueType;
    }

    public void setSubIssueType(int subIssueType) {
        this.subIssueType = subIssueType;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }
}
