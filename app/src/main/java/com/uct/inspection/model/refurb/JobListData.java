package com.uct.inspection.model.refurb;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class JobListData {
    @SerializedName("subPartName")
    @Expose
    private String subPartName;
    @SerializedName("issues")
    @Expose
    private String issues;
    @SerializedName("jobs")
    @Expose
    private String jobs;

    public String getSubPartName() {
        return subPartName;
    }

    public void setSubPartName(String subPartName) {
        this.subPartName = subPartName;
    }

    public String getIssues() {
        return issues;
    }

    public void setIssues(String issues) {
        this.issues = issues;
    }

    public String getJobs() {
        return jobs;
    }

    public void setJobs(String jobs) {
        this.jobs = jobs;
    }
}
