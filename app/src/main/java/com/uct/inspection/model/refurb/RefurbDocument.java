package com.uct.inspection.model.refurb;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class RefurbDocument {
    @SerializedName("isMandatory")
    @Expose
    private boolean isMandatory;
    @SerializedName("documentName")
    @Expose
    private String documentName;
    @SerializedName("documentList")
    @Expose
    private List<String> documentList;
    @SerializedName("documentID")
    @Expose
    private int documentID;

    public boolean isMandatory() {
        return isMandatory;
    }

    public void setMandatory(boolean mandatory) {
        isMandatory = mandatory;
    }

    public String getDocumentName() {
        return documentName;
    }

    public void setDocumentName(String documentName) {
        this.documentName = documentName;
    }

    public List<String> getDocumentList() {
        return documentList;
    }

    public void setDocumentList(List<String> documentList) {
        this.documentList = documentList;
    }

    public int getDocumentID() {
        return documentID;
    }

    public void setDocumentID(int documentID) {
        this.documentID = documentID;
    }
}
