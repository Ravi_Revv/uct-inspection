package com.uct.inspection.model.checklist;

import androidx.room.Embedded;
import androidx.room.Ignore;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class InspectionChecklistData {

    public InspectionChecklistData() {
    }
    @Ignore
    public InspectionChecklistData(String headerName, String headerID, List<ChecklistHeader> mChecklistHeader) {
        this.headerName = headerName;
        this.headerID = headerID;
        this.mChecklistHeader = mChecklistHeader;
    }

    @SerializedName("headerName")
    @Expose
    private String headerName;
    @SerializedName("headerID")
    @Expose
    private String headerID;
    @SerializedName("subPartList")
    @Expose
    private List<ChecklistHeader> mChecklistHeader = null;
    public String getHeaderName() {
        return headerName;
    }

    public void setName(String headerName) {
        this.headerName = headerName;
    }

    public List<ChecklistHeader> getChecklistHeader() {
        return mChecklistHeader;
    }

    public void setChecklistHeader(List<ChecklistHeader> headerList) {
        this.mChecklistHeader = headerList;
    }


    public String getHeaderID() {
        return headerID;
    }

    public void setHeaderID(String headerID) {
        this.headerID = headerID;
    }

}
