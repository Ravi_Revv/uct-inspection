package com.uct.inspection.model.inspectionlist;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class RefurbSpocsData implements Parcelable {
    @SerializedName("refurbSpocID")
    @Expose
    private String refurbSpocID;
    @SerializedName("refurbSpocName")
    @Expose
    private String refurbSpocName;

    protected RefurbSpocsData(Parcel in) {
        refurbSpocID = in.readString();
        refurbSpocName = in.readString();
    }

    public static final Creator<RefurbSpocsData> CREATOR = new Creator<RefurbSpocsData>() {
        @Override
        public RefurbSpocsData createFromParcel(Parcel in) {
            return new RefurbSpocsData(in);
        }

        @Override
        public RefurbSpocsData[] newArray(int size) {
            return new RefurbSpocsData[size];
        }
    };

    public String getRefurbSpocID() {
        return refurbSpocID;
    }

    public void setRefurbSpocID(String refurbSpocID) {
        this.refurbSpocID = refurbSpocID;
    }

    public String getRefurbSpocName() {
        return refurbSpocName;
    }

    public void setRefurbSpocName(String refurbSpocName) {
        this.refurbSpocName = refurbSpocName;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(refurbSpocID);
        dest.writeString(refurbSpocName);
    }
}
