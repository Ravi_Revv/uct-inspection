package com.uct.inspection.model.verdict;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class InspectionVerdictData {
    @SerializedName("leadID")
    @Expose
    private int leadID;
    @SerializedName("inspectionType")
    @Expose
    private String inspectionType;
    @SerializedName("issuesCount")
    @Expose
    private int issuesCount;
    @SerializedName("jobsCount")
    @Expose
    private int jobsCount;
    @SerializedName("unAssuredPartCount")
    @Expose
    private int unAssuredPartCount;
    @SerializedName("refurbishedCost")
    @Expose
    private int refurbishedCost;
    @SerializedName("title")
    @Expose
    private String title;
    @SerializedName("warning")
    @Expose
    private boolean warning;
    @SerializedName("partsNotInspectedCount")
    @Expose
    private int partsNotInspectedCount;

    public int getLeadID() {
        return leadID;
    }

    public void setLeadID(int leadID) {
        this.leadID = leadID;
    }

    public String getInspectionType() {
        return inspectionType;
    }

    public void setInspectionType(String inspectionType) {
        this.inspectionType = inspectionType;
    }

    public int getIssuesCount() {
        return issuesCount;
    }

    public void setIssuesCount(int issuesCount) {
        this.issuesCount = issuesCount;
    }

    public int getJobsCount() {
        return jobsCount;
    }

    public void setJobsCount(int jobsCount) {
        this.jobsCount = jobsCount;
    }

    public int getUnAssuredPartCount() {
        return unAssuredPartCount;
    }

    public void setUnAssuredPartCount(int unAssuredPartCount) {
        this.unAssuredPartCount = unAssuredPartCount;
    }

    public int getRefurbishedCost() {
        return refurbishedCost;
    }

    public void setRefurbishedCost(int refurbishedCost) {
        this.refurbishedCost = refurbishedCost;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public boolean getWarning() {
        return warning;
    }

    public void setWarning(boolean warning) {
        this.warning = warning;
    }


    public int getPartsNotInspectedCount() {
        return partsNotInspectedCount;
    }

    public void setPartsNotInspectedCount(int partsNotInspectedCount) {
        this.partsNotInspectedCount = partsNotInspectedCount;
    }
}
