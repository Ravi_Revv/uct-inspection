package com.uct.inspection.model.checklist;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.Ignore;
import androidx.room.PrimaryKey;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

@Entity(tableName = "InspectionChecklist")
public class InspectionChecklist {
    public InspectionChecklist() {
    }

    @Ignore
    public InspectionChecklist(int statusCode, int inspectionId, String statusMessage, List<InspectionChecklistData> mInspectionChecklistData, long insertTS) {
        this.statusCode = statusCode;
        this.inspectionId = inspectionId;
        this.statusMessage = statusMessage;
        this.mInspectionChecklistData = mInspectionChecklistData;
        this.insertTS = insertTS;
    }

    @SerializedName("status_code")
    @Expose
    private int statusCode;
    @PrimaryKey
    @ColumnInfo(name = "inspectionId")
    @SerializedName("inspectionId")
    @Expose
    private int inspectionId;

    @ColumnInfo(name = "insertTS")
    @SerializedName("insertTS")
    @Expose
    private long insertTS;

    @SerializedName("status_message")
    @Expose
    private String statusMessage;

    //    @ColumnInfo(name = "data")
    @SerializedName("data")
    @Expose
    private List<InspectionChecklistData> mInspectionChecklistData = null;


    public int getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(int statusCode) {
        this.statusCode = statusCode;
    }

    public String getStatusMessage() {
        return statusMessage;
    }

    public void setStatusMessage(String statusMessage) {
        this.statusMessage = statusMessage;
    }

    public List<InspectionChecklistData> getInspectionChecklistData() {
        return mInspectionChecklistData;
    }

    public void setInspectionChecklistData(List<InspectionChecklistData> data) {
        this.mInspectionChecklistData = data;
    }


    public int getInspectionId() {
        return inspectionId;
    }

    public void setInspectionId(int inspectionId) {
        this.inspectionId = inspectionId;
    }


    public long getInsertTS() {
        return insertTS;
    }

    public void setInsertTS(long insertTS) {
        this.insertTS = insertTS;
    }

}
