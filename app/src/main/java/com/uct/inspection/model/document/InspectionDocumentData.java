package com.uct.inspection.model.document;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class InspectionDocumentData {
    @SerializedName("documentName")
    @Expose
    private String documentName;
    @SerializedName("documentPath")
    @Expose
    private String documentPath;

    @SerializedName("selected")
    @Expose
    private boolean selected;

    @SerializedName("documentID")
    @Expose
    private int documentID;

    @SerializedName("documentType")
    @Expose
    private String documentType;

    @SerializedName("isMandatory")
    @Expose
    private boolean isMandatory;

    public int getDocumentID() {
        return documentID;
    }

    public void setDocumentID(int documentID) {
        this.documentID = documentID;
    }

    public String getDocumentType() {
        return documentType;
    }

    public void setDocumentType(String documentType) {
        this.documentType = documentType;
    }

    public boolean isMandatory() {
        return isMandatory;
    }

    public void setMandatory(boolean mandatory) {
        isMandatory = mandatory;
    }

    public String getDocumentName() {
        return documentName;
    }

    public void setDocumentName(String documentName) {
        this.documentName = documentName;
    }

    public String getDocumentPath() {
        return documentPath;
    }

    public void setDocumentPath(String documentPath) {
        this.documentPath = documentPath;
    }

    public boolean isSelected() {
        return selected;
    }

    public void setSelected(boolean selected) {
        this.selected = selected;
    }
}
