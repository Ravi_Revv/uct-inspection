package com.uct.inspection.model.inspectionlist;

import android.os.Parcel;
import android.os.Parcelable;

public class InspectionFieldData implements Parcelable {
    private String name;
    private String value;
    private boolean isMandatory;
    private boolean isEditable;
    private boolean isVisible;
    private String type;

    protected InspectionFieldData(Parcel in) {
        name = in.readString();
        value = in.readString();
        isMandatory = in.readByte() != 0;
        isEditable = in.readByte() != 0;
        isVisible = in.readByte() != 0;
        type = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(name);
        dest.writeString(value);
        dest.writeByte((byte) (isMandatory ? 1 : 0));
        dest.writeByte((byte) (isEditable ? 1 : 0));
        dest.writeByte((byte) (isVisible ? 1 : 0));
        dest.writeString(type);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<InspectionFieldData> CREATOR = new Creator<InspectionFieldData>() {
        @Override
        public InspectionFieldData createFromParcel(Parcel in) {
            return new InspectionFieldData(in);
        }

        @Override
        public InspectionFieldData[] newArray(int size) {
            return new InspectionFieldData[size];
        }
    };

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public boolean isMandatory() {
        return isMandatory;
    }

    public void setMandatory(boolean mandatory) {
        isMandatory = mandatory;
    }

    public boolean isEditable() {
        return isEditable;
    }

    public void setEditable(boolean editable) {
        isEditable = editable;
    }

    public boolean isVisible() {
        return isVisible;
    }

    public void setVisible(boolean visible) {
        isVisible = visible;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
}
