package com.uct.inspection.aws

import android.content.Context
import android.os.AsyncTask
import android.util.Log
import android.widget.Toast
import com.amazonaws.services.s3.AmazonS3Client
import com.amazonaws.services.s3.model.*
import com.uct.inspection.interfaces.IAwsCallback
import com.uct.inspection.model.checklist.InspectionChecklist
import com.uct.inspection.utils.LoadingDialog
import java.io.File
import java.lang.Exception

class AwsUploadTask internal constructor(
    mContext: Context,
    mIAwsCallback: IAwsCallback,
    mAmazonS3Client: AmazonS3Client,
    mInspectionChecklist: InspectionChecklist
) :
    AsyncTask<String, String, Boolean>() {

    private val context by lazy { mContext }
    private val amazonS3Client by lazy { mAmazonS3Client }
    private val awsCallback by lazy { mIAwsCallback }
    private val inspectionChecklist by lazy { mInspectionChecklist }
    override fun onPreExecute() {
        super.onPreExecute()
        LoadingDialog().showLoadingDialog(context, "")
    }


    override fun doInBackground(vararg params: String?): Boolean? {
        val uploadedFileList = ArrayList<String>()

        try {
            for (i in 0 until inspectionChecklist.inspectionChecklistData.size) {
                for (j in 0 until inspectionChecklist.inspectionChecklistData[i].checklistHeader.size) {
                    uploadedFileList.clear()
                    for (k in 0 until inspectionChecklist.inspectionChecklistData[i].checklistHeader[j].images.size) {
                        val imagePath =
                            inspectionChecklist.inspectionChecklistData[i].checklistHeader[j].images[k]

                        if (imagePath.contains(AwsConstant.AWS_S3_BASE_URL)) {
                            uploadedFileList.add(imagePath)
                        } else {
                            val localFilePath = File(imagePath)
                            if (localFilePath.exists()) {
                                val putObjectRequest = PutObjectRequest(
                                    AwsConstant.BUCKET_NAME_STAGING,
                                    AwsConstant.UCT_DIRECTORY + inspectionChecklist.inspectionId.toString() + "/" + localFilePath.name,
                                    localFilePath.absoluteFile
                                )
                                amazonS3Client.putObject(
                                    putObjectRequest.withCannedAcl(
                                        CannedAccessControlList.PublicRead
                                    )
                                )
                                val uploadedFile =
                                    AwsConstant.AWS_S3_BASE_URL + AwsConstant.BUCKET_NAME_STAGING + "/" + AwsConstant.UCT_DIRECTORY + inspectionChecklist.inspectionId.toString() + "/" + localFilePath.name
//                                Log.d("uploadedFile", uploadedFile)
                                uploadedFileList.add(uploadedFile)

//                        uploadedFileList.add(AwsConstant.AWS_S3_BASE_URL + AwsConstant.BUCKET_NAME_STAGING + "/" + AwsConstant.UCT_DIRECTORY + inspectionChecklist.inspectionId.toString() + "/" + localFilePath.name)

                            }
                        }

                    }

                    inspectionChecklist.inspectionChecklistData[i].checklistHeader[j].images.clear()
                    inspectionChecklist.inspectionChecklistData[i].checklistHeader[j].images.addAll(
                        uploadedFileList
                    )

                }
            }
        } catch (e: AmazonS3Exception) {
            return false
        } catch (e: Exception) {
            return false
        }

        return true
    }

    override fun onPostExecute(result: Boolean) {
        super.onPostExecute(result)
        awsCallback.onAwsFileUploadSuccess(result)
        if (result) {

            /*for (i in result.indices) {
                Log.d("UploadedFile", result[i])
            }*/
//            Toast.makeText(context, "uploaded success", Toast.LENGTH_SHORT).show()
        } else Toast.makeText(context, "Image uploading failed", Toast.LENGTH_SHORT).show()

        LoadingDialog().dismissLoadingDialog()
    }
}