package com.uct.inspection.aws

import android.content.Context
import android.os.AsyncTask
import android.util.Log
import android.widget.Toast
import com.amazonaws.services.s3.AmazonS3Client
import com.amazonaws.services.s3.model.*
import com.uct.inspection.interfaces.IAwsCallback
import com.uct.inspection.model.checklist.InspectionChecklist
import com.uct.inspection.utils.LoadingDialog
import java.io.File
import java.lang.Exception

class AwsSingleFileUploadTask internal constructor(
    mContext: Context,
    mIAwsCallback: IAwsCallback,
    mAmazonS3Client: AmazonS3Client,
    mFilePath: String,
    mInspectionId: Int
) :
    AsyncTask<String, String, String>() {

    private val context by lazy { mContext }
    private val amazonS3Client by lazy { mAmazonS3Client }
    private val awsCallback by lazy { mIAwsCallback }
    private val filePath by lazy { mFilePath }
    private val inspectionId by lazy { mInspectionId }
    override fun onPreExecute() {
        super.onPreExecute()
        LoadingDialog().showLoadingDialog(context, "")
    }


    override fun doInBackground(vararg params: String?): String? {
        try {
            val localFilePath = File(filePath)
            if (localFilePath.exists()) {
                val putObjectRequest = PutObjectRequest(
                    AwsConstant.BUCKET_NAME_STAGING,
                    AwsConstant.UCT_DIRECTORY + inspectionId.toString() + "/" + localFilePath.name,
                    localFilePath.absoluteFile
                )
                amazonS3Client.putObject(
                    putObjectRequest.withCannedAcl(
                        CannedAccessControlList.PublicRead
                    )
                )
                val uploadedFile =
                    AwsConstant.AWS_S3_BASE_URL + AwsConstant.BUCKET_NAME_STAGING + "/" + AwsConstant.UCT_DIRECTORY + inspectionId.toString() + "/" + localFilePath.name
//                                Log.d("uploadedFile", uploadedFile)
//                uploadedFileList.add(uploadedFile)
                return uploadedFile

            }


        } catch (e: AmazonS3Exception) {
            return null
        } catch (e: Exception) {
            return null
        }

        return null
    }

    override fun onPostExecute(result: String?) {
        super.onPostExecute(result)

        if (result != null && result.isNotEmpty()) {
            awsCallback.onAwsFileUploadSuccess(result)
            /*for (i in result.indices) {
                Log.d("UploadedFile", result[i])
            }*/
//            Toast.makeText(context, "uploaded success", Toast.LENGTH_SHORT).show()
        } else Toast.makeText(context, "Image uploading failed", Toast.LENGTH_SHORT).show()

        LoadingDialog().dismissLoadingDialog()
    }
}