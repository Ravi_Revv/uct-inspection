package com.uct.inspection.aws

import android.content.Context
import android.os.AsyncTask
import android.util.Log
import android.widget.Toast
import com.amazonaws.AmazonClientException
import com.amazonaws.ClientConfiguration
import com.amazonaws.Protocol
import com.amazonaws.auth.BasicAWSCredentials
import com.amazonaws.services.s3.AmazonS3Client
import com.amazonaws.services.s3.model.CannedAccessControlList
import com.amazonaws.services.s3.model.ObjectMetadata
import com.amazonaws.services.s3.model.PutObjectRequest
import com.google.firebase.crashlytics.FirebaseCrashlytics
import com.uct.inspection.interfaces.IAwsCallback
import com.uct.inspection.utils.LoadingDialog
import java.io.ByteArrayInputStream

class AwsDirectoryTask internal constructor(
    mContext: Context,
    mIAwsCallback: IAwsCallback,
    mInspectionId: Int,
    mAmazonS3Client: AmazonS3Client
) :
    AsyncTask<String, Integer, Boolean>() {

    private val context by lazy { mContext }
    private val awsCallback by lazy { mIAwsCallback }
    private val inspectionId by lazy { mInspectionId }
    private val amazonS3Client by lazy { mAmazonS3Client }
    override fun onPreExecute() {
        super.onPreExecute()
        LoadingDialog().showLoadingDialog(context, "")
    }


    override fun doInBackground(vararg params: String?): Boolean {
        try {
//            if (bookingid != null && bookingid != "" && bucketName == BUCKET_NAME) {
            val metadata = ObjectMetadata()
            metadata.contentLength = 0

            // create empty content
            val emptyContent = ByteArrayInputStream(ByteArray(0))

            // create a PutObjectRequest passing the folder name suffixed by /
            //        PutObjectRequest putObjectRequest = new PutObjectRequest(bucketName,
            //                bookingid+"/photos/", emptyContent, metadata);
            val putObjectRequest = arrayOfNulls<PutObjectRequest>(1)
            putObjectRequest[0] = PutObjectRequest(
                AwsConstant.BUCKET_NAME_STAGING,
                AwsConstant.UCT_DIRECTORY + inspectionId.toString(),
                emptyContent,
                metadata
            ).withCannedAcl(
                CannedAccessControlList.PublicRead
            )

            // send request to S3 to create folder and sub folders
            for (i in putObjectRequest.indices) {
                amazonS3Client.putObject(putObjectRequest[i])
            }
            return true
            /* } else {
                 Log.e("booking id empty", "or bucket name umatched")
                 return false
             }*/
        } catch (e: AmazonClientException) {
            FirebaseCrashlytics.getInstance().recordException(e)
            if (e.message != null)
                FirebaseCrashlytics.getInstance().log(e.message!!)
            e.printStackTrace()
            //            EventBus.getDefault().post(new FolderCreated(false));
            return false
        }

    }

    override fun onPostExecute(result: Boolean?) {
        super.onPostExecute(result)
        LoadingDialog().dismissLoadingDialog()
        if (result!!) {
            awsCallback.onAwsDirectoryCreationSuccess(amazonS3Client)
        } else Toast.makeText(context, "directory null", Toast.LENGTH_SHORT).show()
    }
}