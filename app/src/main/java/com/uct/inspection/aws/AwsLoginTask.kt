package com.uct.inspection.aws

import android.content.Context
import android.os.AsyncTask
import android.widget.Toast
import com.amazonaws.ClientConfiguration
import com.amazonaws.Protocol
import com.amazonaws.auth.BasicAWSCredentials
import com.amazonaws.services.s3.AmazonS3Client
import com.uct.inspection.interfaces.IAwsCallback
import com.uct.inspection.utils.LoadingDialog

class AwsLoginTask internal constructor(mContext: Context, mIAwsCallback: IAwsCallback) :
    AsyncTask<String, String, AmazonS3Client>() {

    private val context by lazy { mContext }
    private val awsCallback by lazy { mIAwsCallback }
    override fun onPreExecute() {
        super.onPreExecute()
        LoadingDialog().showLoadingDialog(context, "")
    }


    override fun doInBackground(vararg params: String?): AmazonS3Client {
        val credentials = BasicAWSCredentials(AwsConstant.ACCESS_KEY, AwsConstant.SECRET_KEY)
        val clientConfig = ClientConfiguration()
        clientConfig.protocol = Protocol.HTTP
        clientConfig.connectionTimeout = 30 * 1000
        clientConfig.socketTimeout = 30 * 1000
        clientConfig.isEnableGzip = true
        clientConfig.maxErrorRetry = 5
        val retryPolicy = ClientConfiguration.DEFAULT_RETRY_POLICY
        clientConfig.retryPolicy = retryPolicy
        return AmazonS3Client(credentials, clientConfig)
    }

    override fun onPostExecute(result: AmazonS3Client?) {
        super.onPostExecute(result)
        LoadingDialog().dismissLoadingDialog()
        if (result != null) {
            awsCallback.onAwsLoginSuccess(result)
        } else Toast.makeText(context, "aws null", Toast.LENGTH_SHORT).show()
    }
}