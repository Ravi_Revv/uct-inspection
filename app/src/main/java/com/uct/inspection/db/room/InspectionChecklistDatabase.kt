package com.uct.inspection.db.room

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import com.uct.inspection.db.Converters
import com.uct.inspection.model.checklist.InspectionChecklist
import com.uct.inspection.model.checklistE4.InspectionE4Checklist
import com.uct.inspection.utils.Constants

@Database(
    entities = arrayOf(InspectionChecklist::class, InspectionE4Checklist::class),
    version = Constants.DB_VERSION,
    exportSchema = false
)
@TypeConverters(Converters::class)
abstract class InspectionChecklistDatabase : RoomDatabase() {

    abstract fun inspectionChecklistDao(): DAOAccess

    companion object {

        @Volatile
        private var INSTANCE: InspectionChecklistDatabase? = null

        fun getDatabaseClient(context: Context): InspectionChecklistDatabase {

            if (INSTANCE != null) return INSTANCE!!

            synchronized(this) {

                INSTANCE = Room
                    .databaseBuilder(
                        context,
                        InspectionChecklistDatabase::class.java,
                        "uct_inspection_db"
                    )
                    .fallbackToDestructiveMigration()
                    .build()

                return INSTANCE!!

            }
        }

    }

}