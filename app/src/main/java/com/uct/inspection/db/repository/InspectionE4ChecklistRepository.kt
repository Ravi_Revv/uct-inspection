package com.uct.inspection.db.repository

import android.content.Context
import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.google.gson.Gson
import com.uct.inspection.db.room.InspectionChecklistDatabase
import com.uct.inspection.model.checklist.InspectionChecklist
import com.uct.inspection.model.checklistE4.InspectionE4Checklist
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers.IO
import kotlinx.coroutines.launch

class InspectionE4ChecklistRepository {

    companion object {

        var inspectionChecklistDatabase: InspectionChecklistDatabase? = null

        var inspectionE4Checklist: LiveData<InspectionE4Checklist> = MutableLiveData()
        var allInspectionE4Checklist: LiveData<List<InspectionE4Checklist>> = MutableLiveData()

        fun initializeDB(context: Context): InspectionChecklistDatabase {
            return InspectionChecklistDatabase.getDatabaseClient(context)
        }

        fun insertInspectionChecklistData(
            context: Context,
            inspectionChecklist: InspectionE4Checklist,
            position: Int
        ) {
            if (inspectionChecklistDatabase == null)
                inspectionChecklistDatabase = initializeDB(context)

            CoroutineScope(IO).launch {

                inspectionChecklistDatabase!!.inspectionChecklistDao()
                    .insertData(inspectionChecklist)
                Log.d("checklistJson", Gson().toJson(inspectionChecklist))

            }

        }


        fun insertAllInspectionChecklistData(
            context: Context,
            inspectionChecklist: InspectionChecklist
        ) {
            if (inspectionChecklistDatabase == null)
                inspectionChecklistDatabase = initializeDB(context)

            CoroutineScope(IO).launch {
                for (i in 0 until inspectionChecklist.inspectionChecklistData.size) {
                    for (j in 0 until inspectionChecklist.inspectionChecklistData[i].checklistHeader.size) {
                        for (k in 0 until inspectionChecklist.inspectionChecklistData[i].checklistHeader[j].checklistIssue.size) {
                            for (l in 0 until inspectionChecklist.inspectionChecklistData[i].checklistHeader[j].checklistIssue[k].checklistSubIssue.size) {
                                if (inspectionChecklist.inspectionChecklistData[i].checklistHeader[j].checklistIssue[k].selected && inspectionChecklist.inspectionChecklistData[i].checklistHeader[j].allSubIssues.contains(
                                        inspectionChecklist.inspectionChecklistData[i].checklistHeader[j].checklistIssue[k].checklistSubIssue[l]
                                    )
                                ) {

                                    val pos =
                                        inspectionChecklist.inspectionChecklistData[i].checklistHeader[j].allSubIssues.indexOf(
                                            inspectionChecklist.inspectionChecklistData[i].checklistHeader[j].checklistIssue[k].checklistSubIssue[l]
                                        )
                                    inspectionChecklist.inspectionChecklistData[i].checklistHeader[j].checklistIssue[k].checklistSubIssue[l] =
                                        inspectionChecklist.inspectionChecklistData[i].checklistHeader[j].allSubIssues[pos]
                                }

                            }
                        }
                        for (m in 0 until inspectionChecklist.inspectionChecklistData[i].checklistHeader[j].defaultSubIssues.size) {
                            if (inspectionChecklist.inspectionChecklistData[i].checklistHeader[j].allSubIssues.contains(
                                    inspectionChecklist.inspectionChecklistData[i].checklistHeader[j].defaultSubIssues[m]
                                )
                            ) {
                                val pos =
                                    inspectionChecklist.inspectionChecklistData[i].checklistHeader[j].allSubIssues.indexOf(
                                        inspectionChecklist.inspectionChecklistData[i].checklistHeader[j].defaultSubIssues[m]
                                    )
                                inspectionChecklist.inspectionChecklistData[i].checklistHeader[j].defaultSubIssues[m] =
                                    inspectionChecklist.inspectionChecklistData[i].checklistHeader[j].allSubIssues[pos]
                            }

                        }
                    }
                }

                inspectionChecklistDatabase!!.inspectionChecklistDao()
                    .insertData(inspectionChecklist)
                Log.d("checklistJson", Gson().toJson(inspectionChecklist))

            }

        }


        fun getInspectionE4ChecklistData(
            context: Context,
            inspectionId: Int
        ): LiveData<InspectionE4Checklist>? {
            if (inspectionChecklistDatabase == null)
                inspectionChecklistDatabase = initializeDB(context)

            inspectionE4Checklist = inspectionChecklistDatabase!!.inspectionChecklistDao()
                .getInspectionE4Checklist(inspectionId)
//            Log.d("checklistJson", "Get From DB :-  "+Gson().toJson(inspectionChecklist))
            return inspectionE4Checklist
        }

        fun getAllInspectionE4ChecklistData(context: Context): LiveData<List<InspectionE4Checklist>>? {
            if (inspectionChecklistDatabase == null)
                inspectionChecklistDatabase = initializeDB(context)

            allInspectionE4Checklist =
                inspectionChecklistDatabase!!.inspectionChecklistDao().getAllInspectionE4Checklist()
//            Log.d("checklistJson", "Get From DB :-  "+Gson().toJson(inspectionChecklist))
            return allInspectionE4Checklist
        }


        fun deleteInspectionChecklistById(
            context: Context,
            inspectionId: Int
        ) {
            if (inspectionChecklistDatabase == null)
                inspectionChecklistDatabase = initializeDB(context)

            CoroutineScope(IO).launch {

                inspectionChecklistDatabase!!.inspectionChecklistDao()
                    .deleteInspectionChecklistById(inspectionId)
//                Log.d("checklistJson", "delete :- " + inspectionId.toString())

            }

        }


    }


}