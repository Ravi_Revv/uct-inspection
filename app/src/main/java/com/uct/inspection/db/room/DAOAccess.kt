package com.uct.inspection.db.room

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.uct.inspection.model.LoginTableModel
import com.uct.inspection.model.checklist.InspectionChecklist
import com.uct.inspection.model.checklistE4.InspectionE4Checklist

@Dao
interface DAOAccess {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertData(inspectionChecklist: InspectionChecklist)

    @Query("SELECT * FROM InspectionChecklist WHERE inspectionId =:inspectionId")
    fun getInspectionChecklist(inspectionId: Int?): LiveData<InspectionChecklist>

    @Query("SELECT * FROM InspectionChecklist")
    fun getAllInspectionChecklist(): LiveData<List<InspectionChecklist>>

    @Query("DELETE FROM InspectionChecklist WHERE inspectionId =:inspectionId")
    suspend fun deleteInspectionChecklistById(inspectionId: Int?)


    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertData(inspectionE4Checklist: InspectionE4Checklist)

    @Query("SELECT * FROM InspectionE4Checklist WHERE inspectionId =:inspectionId")
    fun getInspectionE4Checklist(inspectionId: Int?): LiveData<InspectionE4Checklist>

    @Query("SELECT * FROM InspectionE4Checklist")
    fun getAllInspectionE4Checklist(): LiveData<List<InspectionE4Checklist>>

    @Query("DELETE FROM InspectionE4Checklist WHERE inspectionId =:inspectionId")
    suspend fun deleteInspectionE4ChecklistById(inspectionId: Int?)

}