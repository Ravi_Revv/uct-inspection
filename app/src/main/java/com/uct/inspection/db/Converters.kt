package com.uct.inspection.db

import androidx.room.TypeConverter
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import com.uct.inspection.model.checklist.ChecklistHeader
import com.uct.inspection.model.checklist.ChecklistIssue
import com.uct.inspection.model.checklist.ChecklistSubIssue
import com.uct.inspection.model.checklist.InspectionChecklistData
import com.uct.inspection.model.checklistE4.InspectionChecklistE4Data


class Converters {
    @TypeConverter
    fun fromString(value: String): List<InspectionChecklistData> {
        val listType = object : TypeToken<List<InspectionChecklistData>>() {

        }.type
        return Gson().fromJson(value, listType)
    }

    @TypeConverter
    fun fromArrayList(list: List<InspectionChecklistData>): String {
        val gson = Gson()
        return gson.toJson(list)
    }

    @TypeConverter
    fun fromStringE4(value: String): List<InspectionChecklistE4Data> {
        val listType = object : TypeToken<List<InspectionChecklistE4Data>>() {

        }.type
        return Gson().fromJson(value, listType)
    }

    @TypeConverter
    fun fromArrayListE4(list: List<InspectionChecklistE4Data>): String {
        val gson = Gson()
        return gson.toJson(list)
    }



    @TypeConverter
    fun fromStringChecklistHeader(value: String): List<ChecklistHeader> {
        val listType = object : TypeToken<List<ChecklistHeader>>() {

        }.type
        return Gson().fromJson(value, listType)
    }

    @TypeConverter
    fun fromArrayListChecklistHeader(list: List<ChecklistHeader>): String {
        val gson = Gson()
        return gson.toJson(list)
    }

    @TypeConverter
    fun fromStringChecklistIssue(value: String): List<ChecklistIssue> {
        val listType = object : TypeToken<List<ChecklistIssue>>() {

        }.type
        return Gson().fromJson(value, listType)
    }

    @TypeConverter
    fun fromArrayListChecklistIssue(list: List<ChecklistIssue>): String {
        val gson = Gson()
        return gson.toJson(list)
    }

    @TypeConverter
    fun fromStringChecklistSubIssue(value: String):List<ChecklistSubIssue> {
        val listType = object : TypeToken<List<ChecklistSubIssue>>() {

        }.type
        return Gson().fromJson(value, listType)
    }

    @TypeConverter
    fun fromArrayListChecklistSubIssue(list: List<ChecklistSubIssue>): String {
        val gson = Gson()
        return gson.toJson(list)
    }

    @TypeConverter
    fun fromStringChecklistImages(value: String): List<String> {
        val listType = object : TypeToken<List<String>>() {

        }.type
        return Gson().fromJson(value, listType)
    }

    @TypeConverter
    fun fromArrayListChecklistImages(list: List<String>): String {
        val gson = Gson()
        return gson.toJson(list)
    }
}