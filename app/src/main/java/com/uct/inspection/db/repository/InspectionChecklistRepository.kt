package com.uct.inspection.db.repository

import android.content.Context
import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.google.gson.Gson
import com.uct.inspection.db.room.InspectionChecklistDatabase
import com.uct.inspection.model.checklist.InspectionChecklist
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers.IO
import kotlinx.coroutines.launch

class InspectionChecklistRepository {

    companion object {

        var inspectionChecklistDatabase: InspectionChecklistDatabase? = null

        var inspectionChecklist: LiveData<InspectionChecklist> = MutableLiveData()
        var allInspectionChecklist: LiveData<List<InspectionChecklist>> = MutableLiveData()

        fun initializeDB(context: Context): InspectionChecklistDatabase {
            return InspectionChecklistDatabase.getDatabaseClient(context)
        }

        fun insertInspectionChecklistData(
            context: Context,
            inspectionChecklist: InspectionChecklist,
            position: Int
        ) {
            if (inspectionChecklistDatabase == null)
                inspectionChecklistDatabase = initializeDB(context)

            CoroutineScope(IO).launch {
                //                for (i in 0 until inspectionChecklist.inspectionChecklistData.size) {


                for (j in 0 until inspectionChecklist.inspectionChecklistData[position].checklistHeader.size) {
                    for (k in 0 until inspectionChecklist.inspectionChecklistData[position].checklistHeader[j].checklistIssue.size) {
                        for (l in 0 until inspectionChecklist.inspectionChecklistData[position].checklistHeader[j].checklistIssue[k].checklistSubIssue.size) {
                            if (inspectionChecklist.inspectionChecklistData[position].checklistHeader[j].checklistIssue[k].selected && inspectionChecklist.inspectionChecklistData[position].checklistHeader[j].allSubIssues.contains(
                                    inspectionChecklist.inspectionChecklistData[position].checklistHeader[j].checklistIssue[k].checklistSubIssue[l]
                                )
                            ) {

                                val pos =
                                    inspectionChecklist.inspectionChecklistData[position].checklistHeader[j].allSubIssues.indexOf(
                                        inspectionChecklist.inspectionChecklistData[position].checklistHeader[j].checklistIssue[k].checklistSubIssue[l]
                                    )
                                inspectionChecklist.inspectionChecklistData[position].checklistHeader[j].checklistIssue[k].checklistSubIssue[l] =
                                    inspectionChecklist.inspectionChecklistData[position].checklistHeader[j].allSubIssues[pos]
                            }

                        }
                    }
                    for (m in 0 until inspectionChecklist.inspectionChecklistData[position].checklistHeader[j].defaultSubIssues.size) {
                        if (inspectionChecklist.inspectionChecklistData[position].checklistHeader[j].allSubIssues.contains(
                                inspectionChecklist.inspectionChecklistData[position].checklistHeader[j].defaultSubIssues[m]
                            )
                        ) {
                            val pos =
                                inspectionChecklist.inspectionChecklistData[position].checklistHeader[j].allSubIssues.indexOf(
                                    inspectionChecklist.inspectionChecklistData[position].checklistHeader[j].defaultSubIssues[m]
                                )
                            inspectionChecklist.inspectionChecklistData[position].checklistHeader[j].defaultSubIssues[m] =
                                inspectionChecklist.inspectionChecklistData[position].checklistHeader[j].allSubIssues[pos]
                        }

                    }
                }
//                }

                inspectionChecklistDatabase!!.inspectionChecklistDao()
                    .insertData(inspectionChecklist)
                Log.d("checklistJson", Gson().toJson(inspectionChecklist))

            }

        }


        fun insertAllInspectionChecklistData(
            context: Context,
            inspectionChecklist: InspectionChecklist
        ) {
            if (inspectionChecklistDatabase == null)
                inspectionChecklistDatabase = initializeDB(context)

            CoroutineScope(IO).launch {
                for (i in 0 until inspectionChecklist.inspectionChecklistData.size) {
                    for (j in 0 until inspectionChecklist.inspectionChecklistData[i].checklistHeader.size) {
                        for (k in 0 until inspectionChecklist.inspectionChecklistData[i].checklistHeader[j].checklistIssue.size) {
                            for (l in 0 until inspectionChecklist.inspectionChecklistData[i].checklistHeader[j].checklistIssue[k].checklistSubIssue.size) {
                                if (inspectionChecklist.inspectionChecklistData[i].checklistHeader[j].checklistIssue[k].selected && inspectionChecklist.inspectionChecklistData[i].checklistHeader[j].allSubIssues.contains(
                                        inspectionChecklist.inspectionChecklistData[i].checklistHeader[j].checklistIssue[k].checklistSubIssue[l]
                                    )
                                ) {

                                    val pos =
                                        inspectionChecklist.inspectionChecklistData[i].checklistHeader[j].allSubIssues.indexOf(
                                            inspectionChecklist.inspectionChecklistData[i].checklistHeader[j].checklistIssue[k].checklistSubIssue[l]
                                        )
                                    inspectionChecklist.inspectionChecklistData[i].checklistHeader[j].checklistIssue[k].checklistSubIssue[l] =
                                        inspectionChecklist.inspectionChecklistData[i].checklistHeader[j].allSubIssues[pos]
                                }

                            }
                        }
                        for (m in 0 until inspectionChecklist.inspectionChecklistData[i].checklistHeader[j].defaultSubIssues.size) {
                            if (inspectionChecklist.inspectionChecklistData[i].checklistHeader[j].allSubIssues.contains(
                                    inspectionChecklist.inspectionChecklistData[i].checklistHeader[j].defaultSubIssues[m]
                                )
                            ) {
                                val pos =
                                    inspectionChecklist.inspectionChecklistData[i].checklistHeader[j].allSubIssues.indexOf(
                                        inspectionChecklist.inspectionChecklistData[i].checklistHeader[j].defaultSubIssues[m]
                                    )
                                inspectionChecklist.inspectionChecklistData[i].checklistHeader[j].defaultSubIssues[m] =
                                    inspectionChecklist.inspectionChecklistData[i].checklistHeader[j].allSubIssues[pos]
                            }

                        }
                    }
                }

                inspectionChecklistDatabase!!.inspectionChecklistDao()
                    .insertData(inspectionChecklist)
                Log.d("checklistJson", Gson().toJson(inspectionChecklist))

            }

        }


        fun getInspectionChecklistData(
            context: Context,
            inspectionId: Int
        ): LiveData<InspectionChecklist>? {
            if (inspectionChecklistDatabase == null)
                inspectionChecklistDatabase = initializeDB(context)

            inspectionChecklist = inspectionChecklistDatabase!!.inspectionChecklistDao()
                .getInspectionChecklist(inspectionId)
//            Log.d("checklistJson", "Get From DB :-  "+Gson().toJson(inspectionChecklist))
            return inspectionChecklist
        }

        fun getAllInspectionChecklistData(context: Context): LiveData<List<InspectionChecklist>>? {
            if (inspectionChecklistDatabase == null)
                inspectionChecklistDatabase = initializeDB(context)

            allInspectionChecklist =
                inspectionChecklistDatabase!!.inspectionChecklistDao().getAllInspectionChecklist()
//            Log.d("checklistJson", "Get From DB :-  "+Gson().toJson(inspectionChecklist))
            return allInspectionChecklist
        }


        fun deleteInspectionChecklistById(
            context: Context,
            inspectionId: Int
        ) {
            if (inspectionChecklistDatabase == null)
                inspectionChecklistDatabase = initializeDB(context)

            CoroutineScope(IO).launch {

                inspectionChecklistDatabase!!.inspectionChecklistDao()
                    .deleteInspectionChecklistById(inspectionId)
//                Log.d("checklistJson", "delete :- " + inspectionId.toString())

            }

        }


    }


}