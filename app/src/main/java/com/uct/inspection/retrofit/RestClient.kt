package com.uct.inspection.retrofit

import com.jakewharton.retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import com.uct.inspection.utils.Config
import io.reactivex.Scheduler
import io.reactivex.schedulers.Schedulers
import okhttp3.*
import okhttp3.MediaType.Companion.toMediaType
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.io.IOException
import java.util.concurrent.TimeUnit


object RestClient {

    var retrofit: Retrofit? = null
    var authRetrofit: Retrofit? = null
    var gmsRetrofit: Retrofit? = null
    var okHttpClient: OkHttpClient? = null
    var authOkHttpClient: OkHttpClient? = null
    val REQUEST_TIMEOUT: Int = 60
    var scheduler: Scheduler? = null

    @JvmStatic
    fun getAuthApiService(): ApiService {

        if (authOkHttpClient == null) {
            initAuthOkHttp()
        }

        if (authRetrofit == null) {
            authRetrofit = Retrofit.Builder()
                .baseUrl(Config.getAuthBaseURL())
                .client(authOkHttpClient)
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .addConverterFactory(GsonConverterFactory.create())
                .build()
        }

        return authRetrofit!!.create(ApiService::class.java)
    }

    @JvmStatic
    fun getApiService(): ApiService {

        if (okHttpClient == null) {
            initOkHttp()
        }

        if (retrofit == null) {
            retrofit = Retrofit.Builder()
                .baseUrl(Config.getBaseURL())
                .client(okHttpClient)
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .addConverterFactory(GsonConverterFactory.create())
                .build()
        }

        return retrofit!!.create(ApiService::class.java)
    }

    private fun initOkHttp() {
        val httpClient = OkHttpClient().newBuilder()
            .connectTimeout(REQUEST_TIMEOUT.toLong(), TimeUnit.SECONDS)
            .readTimeout(REQUEST_TIMEOUT.toLong(), TimeUnit.SECONDS)
            .writeTimeout(REQUEST_TIMEOUT.toLong(), TimeUnit.SECONDS)

        val interceptor = HttpLoggingInterceptor()

        if (Config.getAppMode().equals(Config.AppMode.LIVE))
            interceptor.setLevel(HttpLoggingInterceptor.Level.NONE)
        else
            interceptor.setLevel(HttpLoggingInterceptor.Level.BODY)

        httpClient.addInterceptor(interceptor)

        httpClient.addInterceptor(object : Interceptor {
            @Throws(IOException::class)
            override fun intercept(chain: Interceptor.Chain): Response {
                val original: Request = chain.request()
                val requestBuilder: Request.Builder = original.newBuilder()
                    .addHeader("Accept", "application/json")
                    .addHeader("Content-Type", "application/json")
                // Adding Authorization token (API Key)
                // Requests will be denied without API key

                /*
                    if (!TextUtils.isEmpty(PrefUtils.getApiKey(context))) {
                           requestBuilder.addHeader("Authorization", PrefUtils.getApiKey(context))
                    }
                */


                /*  To Avoid Blank Response */
                val response = chain.proceed(requestBuilder.build())
                if (!response.isSuccessful) {
                    return response
                }

                if (response.code != 204 && response.code != 205) {
                    return response
                }

                if ((response.body?.contentLength() ?: -1) >= 0) {
                    return response
                }

                val emptyBody = ResponseBody.create(
                    "text/plain".toMediaType(),
                    "{\"message\":\"Done\",\"data\":{}}"
                )

                return response
                    .newBuilder()
                    .code(200)
                    .body(emptyBody)
                    .build()

                /*  To Avoid Blank Response */

//                val request: Request = requestBuilder.build()
//                return chain.proceed(request)
            }
        })
        okHttpClient = httpClient.build()
    }

    private fun initAuthOkHttp() {
        val httpClient = OkHttpClient().newBuilder()
            .connectTimeout(REQUEST_TIMEOUT.toLong(), TimeUnit.SECONDS)
            .readTimeout(REQUEST_TIMEOUT.toLong(), TimeUnit.SECONDS)
            .writeTimeout(REQUEST_TIMEOUT.toLong(), TimeUnit.SECONDS)

        val interceptor = HttpLoggingInterceptor()

        if (Config.getAppMode().equals(Config.AppMode.LIVE))
            interceptor.setLevel(HttpLoggingInterceptor.Level.NONE)
        else
            interceptor.setLevel(HttpLoggingInterceptor.Level.BODY)

        httpClient.addInterceptor(interceptor)

        httpClient.addInterceptor(object : Interceptor {
            @Throws(IOException::class)
            override fun intercept(chain: Interceptor.Chain): Response {
                val original: Request = chain.request()
                val requestBuilder: Request.Builder = original.newBuilder()
                    .addHeader("Accept", "application/json")
                    .addHeader("Content-Type", "application/json")
                // Adding Authorization token (API Key)
                // Requests will be denied without API key

                /*
                    if (!TextUtils.isEmpty(PrefUtils.getApiKey(context))) {
                           requestBuilder.addHeader("Authorization", PrefUtils.getApiKey(context))
                    }
                */


                /*  To Avoid Blank Response */
                val response = chain.proceed(requestBuilder.build())
                if (!response.isSuccessful) {
                    return response
                }

                if (response.code != 204 && response.code != 205) {
                    return response
                }

                if ((response.body?.contentLength() ?: -1) >= 0) {
                    return response
                }

                val emptyBody = ResponseBody.create(
                    "text/plain".toMediaType(),
                    "{\"message\":\"Done\",\"data\":{}}"
                )

                return response
                    .newBuilder()
                    .code(200)
                    .body(emptyBody)
                    .build()

                /*  To Avoid Blank Response */

//                val request: Request = requestBuilder.build()
//                return chain.proceed(request)
            }
        })
        authOkHttpClient = httpClient.build()
    }

    @JvmStatic
    fun subscribeScheduler(): Scheduler {
        if (scheduler == null) {
            scheduler = Schedulers.io()
        }

        return scheduler!!
    }
}