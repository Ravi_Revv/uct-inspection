package com.uct.inspection.retrofit;

import java.util.HashMap;

import io.reactivex.Observable;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Response;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.POST;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface ApiService {

    @POST("/user/login")
    Observable<Response<ResponseBody>> login(@Body HashMap<String, String> body);

    @GET("supply/api/v1/inspection/list")
    Observable<Response<ResponseBody>> getInspectionList(@Header("Token") String Token);

    @GET("supply/api/v1/inspection/checklist/{inspectionId}")
    Observable<Response<ResponseBody>> getInspectionChecklist(@Header("Token") String Token, @Path("inspectionId") int inspectionId);

    @POST("supply/api/v1/inspection/checklist/{inspectionId}/saveData")
    Observable<Response<ResponseBody>> saveInspectionChecklist(@Header("Token") String Token, @Path("inspectionId") int inspectionId, @Body RequestBody inspectionChecklist);

    @POST("supply/api/v1/inspection/checklist/{inspectionId}/saveImage")
    Observable<Response<ResponseBody>> saveInspectionChecklistImage(@Header("Token") String Token, @Path("inspectionId") int inspectionId, @Body RequestBody inspectionChecklist);

    @GET("supply/api/v1/inspection/verdict/{inspectionId}")
    Observable<Response<ResponseBody>> getInspectionVerdict(@Header("Token") String Token, @Path("inspectionId") int inspectionId);

    @POST("supply/api/v1/inspection/verdict/{inspectionId}/save")
    Observable<Response<ResponseBody>> saveInspectionVerdict(@Header("Token") String Token, @Path("inspectionId") int inspectionId, @Body HashMap<String, String> body);

    @GET("supply/api/v1/inspection/pickupChecklist/{inspectionId}")
    Observable<Response<ResponseBody>> getPickupChecklist(@Header("Token") String Token, @Path("inspectionId") int inspectionId);

    @POST("supply/api/v1/inspection/pickupChecklist/{inspectionId}/save")
    Observable<Response<ResponseBody>> savePickupChecklist(@Header("Token") String Token, @Path("inspectionId") int inspectionId, @Body RequestBody inspectionChecklist);

    @GET("/supply/api/v1/refurb/jobList/{inspectionId}")
    Observable<Response<ResponseBody>> getRefurbJobList(@Header("Token") String Token, @Path("inspectionId") int inspectionId);

    @POST("supply/api/v1/refurb/{inspectionId}/documents/save")
    Observable<Response<ResponseBody>> saveRefurbJobList(@Header("Token") String Token, @Path("inspectionId") int inspectionId, @Body RequestBody inspectionChecklist);

    @POST("supply/api/v1/refurb/{inspectionId}/update")
    Observable<Response<ResponseBody>> updateRefurbStatus(@Header("Token") String Token, @Path("inspectionId") int inspectionId, @Body RequestBody inspectionChecklist);

    @GET("supply/api/lead/dataForLeadCreation")
    Observable<Response<ResponseBody>> getLeadInfo(@Header("Token") String Token);

    @GET("supply/api/seller/sellersForLeadCreation")
    Observable<Response<ResponseBody>> getSeller(@Header("Token") String Token, @Query("serviceCityID") int serviceCityID, @Query("sellerType") String sellerType);

    @POST("supply/api/lead/update")
    Observable<Response<ResponseBody>> updateLeadInfo(@Header("Token") String Token, @Body RequestBody inspectionChecklist);


}
