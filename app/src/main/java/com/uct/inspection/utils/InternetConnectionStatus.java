package com.uct.inspection.utils;

/**
 * This file is used by all the other classes to check Internet connection
 * Developed by ClickLabs. Developer: Raman goyal
 * Link: http://www.click-labs.com/
 */

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

public class InternetConnectionStatus {

    private static Context context;
    private static InternetConnectionStatus instance = new InternetConnectionStatus();
    private ConnectivityManager connectManager;

    private boolean connected = false;

    public static InternetConnectionStatus with(Context ctx) {
        context = ctx.getApplicationContext();
        return instance;
    }

    public boolean isOnline() {
        try {
            connectManager = (ConnectivityManager) context
                    .getSystemService(Context.CONNECTIVITY_SERVICE);

            NetworkInfo networkInfo = connectManager.getActiveNetworkInfo();
            connected = networkInfo != null && networkInfo.isAvailable()
                    && networkInfo.isConnected();
            return connected;

        } catch (Exception e) {
            e.printStackTrace();
        }
        return connected;
    }
}
