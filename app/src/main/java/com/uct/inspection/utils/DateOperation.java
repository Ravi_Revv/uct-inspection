package com.uct.inspection.utils;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;

public class DateOperation {

    public static String convertServerToLocal(String date) {
        String dateInLocal = "";
        try {

            SimpleDateFormat serverFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss", Locale.getDefault());
            Date d = serverFormat.parse(date);
//            SimpleDateFormat dateFormatter = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm");
            SimpleDateFormat dateFormatter = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault());
            dateFormatter.setTimeZone(TimeZone.getDefault());
            if (d != null)
                dateInLocal = dateFormatter.format(d);
            else dateInLocal = "";
        } catch (Exception e) {
            e.printStackTrace();
            dateInLocal = "";
        }
        return dateInLocal;
    }

    public static String convertLocalToServer(String date) {
        String dateInLocal = "";
        try {

            SimpleDateFormat serverFormat = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault());
            Date d = serverFormat.parse(date);
//            SimpleDateFormat dateFormatter = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm");
            SimpleDateFormat dateFormatter = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS", Locale.getDefault());
            dateFormatter.setTimeZone(TimeZone.getDefault());
            if (d != null)
                dateInLocal = dateFormatter.format(d);
            else dateInLocal = "";
        } catch (Exception e) {
            e.printStackTrace();
            dateInLocal = "";
        }
        return dateInLocal;
    }
}
