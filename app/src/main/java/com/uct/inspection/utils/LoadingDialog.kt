package com.uct.inspection.utils

import android.app.ProgressDialog
import android.content.Context
import android.util.Log
import android.view.View
import android.view.WindowManager
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import com.uct.inspection.R


class LoadingDialog {
    //    private var progressDialog: ProgressDialog? = null
    companion object {
        var progressDialog: ProgressDialog? = null
    }

    public fun showLoadingDialog(context: Context, message: String) {
        var message = message

        if (message.isEmpty())
            message = "Loading..."

        if (isDialogShowing()) {
            dismissLoadingDialog()
        }
        if ((context as AppCompatActivity).isFinishing) {
            return
        }

        progressDialog = ProgressDialog(context, R.style.Theme_AppCompat_Translucent)
        progressDialog?.show()
        val layoutParams = progressDialog?.getWindow()!!.getAttributes()
        layoutParams.dimAmount = 0.7f
        progressDialog?.getWindow()!!.addFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND)
        progressDialog?.setCancelable(false)
        progressDialog?.setContentView(R.layout.dialog_loading_box)


        val messageText = progressDialog?.findViewById(R.id.tvProgress) as TextView
        messageText.text = message
        if (message.contains("Loading")) {
            messageText.visibility = View.GONE
        } else {
            messageText.visibility = View.VISIBLE
        }


    }

    fun isDialogShowing(): Boolean {
        try {
            return if (progressDialog == null) {
                false
            } else {
                progressDialog?.isShowing()!!
            }
        } catch (e: Exception) {
            return false
        }

    }

    fun dismissLoadingDialog() {
        try {
            if (progressDialog != null) {
                if (progressDialog?.isShowing()!!)
                    progressDialog?.dismiss()
                progressDialog = null
            }
        } catch (e: Exception) {
            Log.e("e", "=$e")
        }

    }
}