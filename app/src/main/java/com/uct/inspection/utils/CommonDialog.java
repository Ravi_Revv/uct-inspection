package com.uct.inspection.utils;

import android.app.Activity;
import android.app.Dialog;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.uct.inspection.R;


public class CommonDialog {

    private Activity mActivity;

    private CommonDialog(Activity activity) {
        this.mActivity = activity;
    }

    public static CommonDialog With(Activity activity) {
        return new CommonDialog(activity);
    }


    public void showUnAssuredDialog(final IUnassuredClickCallback mIUnassuredClickCallback) {
        try {
            final Dialog unAssuredDialog = new Dialog(mActivity, R.style.full_screen_dialog);
            View view = mActivity.getLayoutInflater().inflate(R.layout.dialog_unassured_inspection, null);
            if (unAssuredDialog.getWindow() != null)
                unAssuredDialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
            unAssuredDialog.setCancelable(false);
            unAssuredDialog.setCanceledOnTouchOutside(false);
            unAssuredDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            unAssuredDialog.setContentView(view);
            TextView mTxtContinueInspection = unAssuredDialog.findViewById(R.id.mTxtContinueInspection);
            final TextView mTxtVerdict = unAssuredDialog.findViewById(R.id.mTxtVerdict);

            mTxtContinueInspection.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (mIUnassuredClickCallback != null) {
                        unAssuredDialog.dismiss();
                        mIUnassuredClickCallback.onContinueInspection();
                    }

                }
            });

            mTxtVerdict.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (mIUnassuredClickCallback != null) {
                        unAssuredDialog.dismiss();
                        mIUnassuredClickCallback.onGoVerdict();
                    }

                }
            });

            unAssuredDialog.show();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public interface IUnassuredClickCallback {
        void onGoVerdict();

        void onContinueInspection();
    }


    public void showDataValidationFailureDialog(String title, String message, final IDataValidationFailureDialogClickCallback dataValidationFailureDialogClickCallback) {

        final Dialog failureDialog = new Dialog(mActivity, R.style.full_screen_dialog);
        View view = mActivity.getLayoutInflater().inflate(R.layout.dialog_request_failure_msg, null);
        if (failureDialog.getWindow() != null)
            failureDialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        failureDialog.setCancelable(false);
        failureDialog.setCanceledOnTouchOutside(false);
        failureDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        failureDialog.setContentView(view);
        TextView mTxtMsg = view.findViewById(R.id.mTxtMsg);
        TextView mTxtHeader = view.findViewById(R.id.mTxtHeader);
        mTxtMsg.setText(message);
        if (title != null && !title.isEmpty()) {
            mTxtHeader.setText(title);
        }
        Button mBtnOk = view.findViewById(R.id.mBtnOk);

        /*mBtnOk.setOnClickListener(view1 -> {
            failureDialog.dismiss();
            if (requestFailureDialogClickCallback != null)
                requestFailureDialogClickCallback.onOkClick();
        });*/

        mBtnOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                failureDialog.dismiss();
                if (dataValidationFailureDialogClickCallback != null)
                    dataValidationFailureDialogClickCallback.onOkClick();
            }
        });

        failureDialog.show();
    }

    public interface IDataValidationFailureDialogClickCallback {
        void onOkClick();
    }

}
