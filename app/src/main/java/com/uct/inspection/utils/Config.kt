package com.uct.inspection.utils

object Config {
    internal var GCM_PROJECT_NUMBER = ""
    internal var BASE_URL = ""
    internal var AUTH_BASE_URL = ""
    internal var BROWSER_KEY = ""
    internal var appMode = AppMode.LIVE // ALSO CHANGE IN string.xml for production MAP api key

    fun getAppMode(): AppMode {
        return appMode
    }

    fun setAppMode(appMode: AppMode) {
        Config.appMode = appMode
    }


    /**
     * Base URL
     *
     * @return
     */
    fun getBaseURL(): String {
        init(appMode)
        return BASE_URL
    }

    fun setBaseURL(url: String) {
        BASE_URL = url
        init(appMode)
    }

    /**
     * Base URL
     *
     * @return
     */
    fun getAuthBaseURL(): String {
        init(appMode)
        return AUTH_BASE_URL
    }


    /**
     * GCM project number
     *
     * @return
     */
    fun getGCMProjectNumber(): String {
        init(appMode)
        return GCM_PROJECT_NUMBER
    }


    /**
     * Browser Key
     *
     * @return
     */
    fun getBrowserKey(): String {
        init(appMode)
        return BROWSER_KEY
    }


    /**
     * Initialize all the variable in this method
     *
     * @param appMode
     */
    fun init(appMode: AppMode) {

        when (appMode) {
            AppMode.DEV -> {
                BASE_URL = "http://13.232.197.101:3333"
//                GCM_PROJECT_NUMBER = "179663074908"
//                BROWSER_KEY = "AIzaSyAgk_8Dmrl0J9n4lqHsO3HQOqbC8wG4_D0"
            }

            AppMode.TEST -> {
                BASE_URL = "http://13.233.159.54:3000"
//                GCM_PROJECT_NUMBER = "179663074908"
//                BROWSER_KEY = "AIzaSyAgk_8Dmrl0J9n4lqHsO3HQOqbC8wG4_D0"
            }
            AppMode.LIVE -> {
//                BASE_URL = "https://admin.revv.co.in"
                BASE_URL = "http://uct-dashboard.revv.co.in"
                AUTH_BASE_URL = "http://uct-dashboard.revv.co.in"
//                GCM_PROJECT_NUMBER = "179663074908"
//                BROWSER_KEY = "AIzaSyCoeNlxRtI2keSjIG2kT-cc5hU2ng7Tx-8"
            }

            AppMode.DEPLOYEMENT -> {
                BASE_URL = "http://35.154.20.45:3333"
//                GCM_PROJECT_NUMBER = "179663074908"
//                BROWSER_KEY = "AIzaSyCoeNlxRtI2keSjIG2kT-cc5hU2ng7Tx-8"
            }

            AppMode.STAGING -> {
//                BASE_URL = "http://ec2-52-66-240-185.ap-south-1.compute.amazonaws.com:8080"
//                AUTH_BASE_URL = "http://ec2-52-66-240-185.ap-south-1.compute.amazonaws.com:8080"

                BASE_URL = "http://test2-uct-dashboard.revv.co.in"
                AUTH_BASE_URL = "http://test2-uct-dashboard.revv.co.in"

//                BASE_URL = "https://test1-uct-dashboard.revv.co.in"
//                AUTH_BASE_URL = "https://test1-uct-dashboard.revv.co.in"


//                GCM_PROJECT_NUMBER = "179663074908"
//                BROWSER_KEY = "AIzaSyCoeNlxRtI2keSjIG2kT-cc5hU2ng7Tx-8"
            }
        }


    }

    enum class AppMode {
        DEV, TEST, LIVE, STAGING, DEPLOYEMENT
    }

}