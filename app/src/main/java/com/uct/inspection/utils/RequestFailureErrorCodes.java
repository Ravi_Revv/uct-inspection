package com.uct.inspection.utils;

import android.app.Activity;
import android.content.Intent;

import com.jakewharton.retrofit2.adapter.rxjava2.HttpException;
import com.uct.inspection.R;
import com.uct.inspection.utils.enums.ApiResponseFlags;
import com.uct.inspection.view.activity.LoginActivity;

import org.json.JSONObject;

import java.io.IOException;
import java.net.SocketTimeoutException;

public class RequestFailureErrorCodes {

    public static void checkCode(Activity activity, Throwable throwable) {
        if (throwable instanceof HttpException) {
//            ResponseBody responseBody = ((HttpException) throwable).response().errorBody();
            int code = ((HttpException) throwable).response().code();
            if (code == ApiResponseFlags.FORBIDDEN.getOrdinal()) {
                showErrorDialog(activity, "Invalid user email or password.");
            } else {
                showErrorDialog(activity, activity.getString(R.string.something_went_wrong));
            }
//            showErrorDialog(activity, getErrorMessageFromCode(activity, code, responseBody), code);
//            Log.d("ErrorBody", "code :-" + code);
//            Toast.makeText(activity, getErrorMessageFromCode(activity, code, responseBody), Toast.LENGTH_SHORT).show();
        } else if (throwable instanceof SocketTimeoutException) {
            showErrorDialog(activity, "Our server is not responding your request.\nPlease try again later.");

        } else if (throwable instanceof IOException) {
            showErrorDialog(activity, "Check your internet connection and try again.");

        } else {
            showErrorDialog(activity, activity.getString(R.string.something_went_wrong));
        }

    }


    public static void showErrorMessageFromResponse(final Activity activity, JSONObject responseObject) {
        String errorMessage = activity.getString(R.string.no_response_from_server);
        try {
            if (responseObject != null) {
                if (responseObject.getInt("status_code") == ApiResponseFlags.BAD_REQUEST.getOrdinal()) {
                    errorMessage = responseObject.getString("status_message");
                    showErrorDialog(activity, errorMessage);

                } else if (responseObject.getInt("status_code") == ApiResponseFlags.UNAUTHORIZED.getOrdinal()) {
                    errorMessage = responseObject.getString("status_message");
                    CommonDialog.With(activity).showDataValidationFailureDialog("", errorMessage, new CommonDialog.IDataValidationFailureDialogClickCallback() {
                        @Override
                        public void onOkClick() {
                            startLoginActivity(activity);
                        }
                    });
                } else {
                    errorMessage = responseObject.getString("status_message");
                    showErrorDialog(activity, errorMessage);
                }
            } else {
                errorMessage = activity.getString(R.string.something_went_wrong);
                showErrorDialog(activity, errorMessage);
            }

        } catch (Exception e) {
            e.printStackTrace();
            errorMessage = activity.getString(R.string.something_went_wrong);
            showErrorDialog(activity, errorMessage);
        }

    }

    public static void showErrorMessageFromResponse(final Activity activity, int responseCode, String responseMessage) {
        String errorMessage = activity.getString(R.string.no_response_from_server);
        try {

            if (responseCode == ApiResponseFlags.BAD_REQUEST.getOrdinal()) {
                errorMessage = responseMessage;
                showErrorDialog(activity, errorMessage);

            } else if (responseCode == ApiResponseFlags.UNAUTHORIZED.getOrdinal()) {
                errorMessage = responseMessage;
                CommonDialog.With(activity).showDataValidationFailureDialog("", errorMessage, new CommonDialog.IDataValidationFailureDialogClickCallback() {
                    @Override
                    public void onOkClick() {
                        startLoginActivity(activity);
                    }
                });
            } else if (responseCode == ApiResponseFlags.FORBIDDEN.getOrdinal()) {
                errorMessage = responseMessage;
                showErrorDialog(activity, errorMessage);
            } else {
                errorMessage = responseMessage;
                showErrorDialog(activity, errorMessage);
            }


        } catch (Exception e) {
            e.printStackTrace();
            errorMessage = activity.getString(R.string.something_went_wrong);
            showErrorDialog(activity, errorMessage);
        }

    }


    public static void showErrorDialog(Activity activity, String errorMessage) {
        CommonDialog.With(activity).showDataValidationFailureDialog("", errorMessage, new CommonDialog.IDataValidationFailureDialogClickCallback() {
            @Override
            public void onOkClick() {

            }
        });
    }

    private static void startLoginActivity(Activity activity) {
        CommonData.removeUserPreference(activity);
        Intent bdIntent = new Intent(activity, LoginActivity.class);
        bdIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        activity.startActivity(bdIntent);
    }
}
