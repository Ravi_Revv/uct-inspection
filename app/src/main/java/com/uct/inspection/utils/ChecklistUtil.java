package com.uct.inspection.utils;

import com.uct.inspection.model.checklist.ChecklistValidation;
import com.uct.inspection.model.checklist.InspectionChecklist;
import com.uct.inspection.model.checklist.InspectionChecklistData;
import com.uct.inspection.model.checklistE4.ChecklistE4Validation;
import com.uct.inspection.model.checklistE4.InspectionChecklistE4Data;
import com.uct.inspection.model.checklistE4.InspectionE4Checklist;
import com.uct.inspection.model.document.DocumentListValidation;
import com.uct.inspection.model.document.InspectionDocumentData;
import com.uct.inspection.model.refurb.RefurbDocument;
import com.uct.inspection.model.refurb.RefurbDocumentListValidation;

import java.util.ArrayList;

import kotlin.jvm.JvmStatic;

public class ChecklistUtil {

    @JvmStatic
    public static ChecklistValidation validateChecklistData(InspectionChecklistData inspectionChecklistData) {
        ChecklistValidation chk = new ChecklistValidation(false, -1);
        for (int j = 0; j < inspectionChecklistData.getChecklistHeader().size(); j++) {
            chk = new ChecklistValidation(false, j);
            if (inspectionChecklistData.getChecklistHeader().get(j).isMandatory()) {
                for (int k = 0; k < inspectionChecklistData.getChecklistHeader().get(j).getChecklistIssue().size(); k++) {
                    if (inspectionChecklistData.getChecklistHeader().get(j).getChecklistIssue().get(k).getSelected()) {
                        chk.setDataValid(true);
                        break;
                    }
                }
                if (chk.isDataValid()) {
                    chk.setDataValid(false);

                    for (int m = 0; m < inspectionChecklistData.getChecklistHeader().get(j).getAllSubIssues().size(); m++) {
                        if (inspectionChecklistData.getChecklistHeader().get(j).getAllSubIssues().get(m).getSelected()) {
                            chk.setDataValid(true);
                            if (inspectionChecklistData.getChecklistHeader().get(j).getAllSubIssues().get(m).getCost() > inspectionChecklistData.getChecklistHeader().get(j).getAllSubIssues().get(m).getMaxCost() || inspectionChecklistData.getChecklistHeader().get(j).getAllSubIssues().get(m).getCost() < inspectionChecklistData.getChecklistHeader().get(j).getAllSubIssues().get(m).getMinCost())
                                return new ChecklistValidation(false, j);
                        }
                    }
                    if (!chk.isDataValid()) {
                        return chk;
                    }

                } else
                    return chk;
            } else {
                chk.setDataValid(true);
                chk.setNonValidPosition(j);
            }


        }

        return chk;
    }

    @JvmStatic
    public static ChecklistValidation validateChecklistData(InspectionChecklist inspectionChecklist, int tabPosition) {
        ChecklistValidation chk = new ChecklistValidation(false, -1, -1);

//        for (int i = 0; i < inspectionChecklist.getInspectionChecklistData().size(); i++) {
        for (int i = 0; i < tabPosition; i++) {
            InspectionChecklistData inspectionChecklistData = inspectionChecklist.getInspectionChecklistData().get(i);
            for (int j = 0; j < inspectionChecklistData.getChecklistHeader().size(); j++) {
                chk = new ChecklistValidation(false, j, i);
                if (inspectionChecklistData.getChecklistHeader().get(j).isMandatory()) {
                    for (int k = 0; k < inspectionChecklistData.getChecklistHeader().get(j).getChecklistIssue().size(); k++) {
                        if (inspectionChecklistData.getChecklistHeader().get(j).getChecklistIssue().get(k).getSelected()) {
                            chk.setDataValid(true);
                            break;
                        }
                    }
                    if (chk.isDataValid()) {
                        chk.setDataValid(false);

                        for (int m = 0; m < inspectionChecklistData.getChecklistHeader().get(j).getAllSubIssues().size(); m++) {
                            if (inspectionChecklistData.getChecklistHeader().get(j).getAllSubIssues().get(m).getSelected()) {
                                chk.setDataValid(true);
                                if (inspectionChecklistData.getChecklistHeader().get(j).getAllSubIssues().get(m).getCost() > inspectionChecklistData.getChecklistHeader().get(j).getAllSubIssues().get(m).getMaxCost() || inspectionChecklistData.getChecklistHeader().get(j).getAllSubIssues().get(m).getCost() < inspectionChecklistData.getChecklistHeader().get(j).getAllSubIssues().get(m).getMinCost())
                                    return new ChecklistValidation(false, j);
                            }
                        }
                        if (!chk.isDataValid()) {
                            return chk;
                        }

                    } else
                        return chk;
                } else {
                    chk.setDataValid(true);
                    chk.setNonValidPosition(j);
                }


            }
        }


        return chk;
    }

    public static DocumentListValidation isValidDocumentList(ArrayList<InspectionDocumentData> documentList) {
        for (int j = 0; j < documentList.size(); j++) {
            if (documentList.get(j).isMandatory() && documentList.get(j).getDocumentPath() == null) {
                return new DocumentListValidation(false, j, documentList.get(j));
            }
        }
        return new DocumentListValidation(true, 0, null);
    }

    public static RefurbDocumentListValidation isValidRefurbDocumentList(ArrayList<RefurbDocument> refurbDocument) {
        for (int j = 0; j < refurbDocument.size(); j++) {
            if (refurbDocument.get(j).isMandatory() && refurbDocument.get(j).getDocumentList() == null) {
                return new RefurbDocumentListValidation(false, j, refurbDocument.get(j));
            } else if (refurbDocument.get(j).isMandatory() && refurbDocument.get(j).getDocumentList() != null && refurbDocument.get(j).getDocumentList().size() == 0) {
                return new RefurbDocumentListValidation(false, j, refurbDocument.get(j));
            }
        }
        return new RefurbDocumentListValidation(true, 0, null);
    }


    @JvmStatic
    public static ChecklistE4Validation validateChecklistDataStatus(InspectionChecklistData inspectionChecklistData) {
        ChecklistE4Validation chk = new ChecklistE4Validation(false, -1);
        for (int j = 0; j < inspectionChecklistData.getChecklistHeader().size(); j++) {
            chk = new ChecklistE4Validation(false, j);
            if (inspectionChecklistData.getChecklistHeader().get(j).isMandatory()) {
                for (int m = 0; m < inspectionChecklistData.getChecklistHeader().get(j).getAllE4SubIssues().size(); m++) {
                    chk.setDataValid(true);
                    if (inspectionChecklistData.getChecklistHeader().get(j).getAllE4SubIssues().get(m).getCost() > inspectionChecklistData.getChecklistHeader().get(j).getAllE4SubIssues().get(m).getMaxCost() || inspectionChecklistData.getChecklistHeader().get(j).getAllE4SubIssues().get(m).getCost() < inspectionChecklistData.getChecklistHeader().get(j).getAllE4SubIssues().get(m).getMinCost()) {
                        return new ChecklistE4Validation(false, j);
                    } else {
                        if (inspectionChecklistData.getChecklistHeader().get(j).getAllE4SubIssues().get(m).getStatus() == 1 || inspectionChecklistData.getChecklistHeader().get(j).getAllE4SubIssues().get(m).getStatus() == 2 || inspectionChecklistData.getChecklistHeader().get(j).getAllE4SubIssues().get(m).getStatus() == 3) {
                            chk.setDataValid(true);
                        } else {
                            return new ChecklistE4Validation(false, j);
                        }
                    }

                }
                if (!chk.isDataValid()) {
                    return chk;
                }


            } else {
                chk.setDataValid(true);
                chk.setNonValidPosition(j);
            }


        }

        return chk;
    }

    @JvmStatic
    public static ChecklistE4Validation validateChecklistDataStatus(InspectionChecklist inspectionChecklist, int tabPosition) {
        ChecklistE4Validation chk = new ChecklistE4Validation(false, -1, -1);

//        for (int i = 0; i < inspectionChecklist.getInspectionChecklistData().size(); i++) {
        for (int i = 0; i < tabPosition; i++) {
            InspectionChecklistData inspectionChecklistData = inspectionChecklist.getInspectionChecklistData().get(i);
            for (int j = 0; j < inspectionChecklistData.getChecklistHeader().size(); j++) {
                chk = new ChecklistE4Validation(false, j, i);
                if (inspectionChecklistData.getChecklistHeader().get(j).isMandatory()) {
                    for (int m = 0; m < inspectionChecklistData.getChecklistHeader().get(j).getAllE4SubIssues().size(); m++) {
                        chk.setDataValid(true);

                        if (inspectionChecklistData.getChecklistHeader().get(j).getAllE4SubIssues().get(m).getCost() > inspectionChecklistData.getChecklistHeader().get(j).getAllE4SubIssues().get(m).getMaxCost() || inspectionChecklistData.getChecklistHeader().get(j).getAllE4SubIssues().get(m).getCost() < inspectionChecklistData.getChecklistHeader().get(j).getAllE4SubIssues().get(m).getMinCost()) {
                            return new ChecklistE4Validation(false, j);
                        } else {
                            if (inspectionChecklistData.getChecklistHeader().get(j).getAllE4SubIssues().get(m).getStatus() == 1 || inspectionChecklistData.getChecklistHeader().get(j).getAllE4SubIssues().get(m).getStatus() == 2 || inspectionChecklistData.getChecklistHeader().get(j).getAllE4SubIssues().get(m).getStatus() == 3) {
                                chk.setDataValid(true);
                            } else {
                                return new ChecklistE4Validation(false, j);
                            }
                        }

                    }
                    if (!chk.isDataValid()) {
                        return chk;
                    }


                } else {
                    chk.setDataValid(true);
                    chk.setNonValidPosition(j);
                }


            }
        }


        return chk;
    }
}
