package com.uct.inspection.utils.enums;

public enum InspectionCardType {

    REFURBISHMENT("REFURBISHMENT"),
    POST_REFURBISHMENT("POST_REFURBISHMENT"),
    INSPECTION("INSPECTION"),
    PICKUP_CHECKLIST("PICKUP_CHECKLIST");

    private String ordinal;

    InspectionCardType(String ordinal) {
        this.ordinal = ordinal;
    }

    public String getInspectionCardType() {
        return ordinal;
    }
}