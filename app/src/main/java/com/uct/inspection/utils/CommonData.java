package com.uct.inspection.utils;

import android.content.Context;

import com.amazonaws.services.s3.AmazonS3Client;
import com.uct.inspection.model.inspectionlist.InspectionListData;
import com.uct.inspection.model.inspectordetail.InspectorData;
import com.uct.inspection.preference.PreferenceManager;

public class CommonData {
    public static AmazonS3Client amazonS3Client = null;
    public static InspectionListData mInspectionData = null;

    /**
     * @param context
     * @param userData
     */
    public static void saveInspectorData(Context context, InspectorData userData) {
        PreferenceManager.PreferenceInstance.getInstance(context).saveInspectorData(userData);
    }

    /**
     * @param context
     * @return
     */
    public static InspectorData getInspectorData(Context context) {
        return PreferenceManager.PreferenceInstance.getInstance(context).getInspectorData();
    }

    public static void saveAuthToken(Context context, String authToken) {
        PreferenceManager.PreferenceInstance.getInstance(context).saveAuthToken(authToken);
    }

    /**
     * @param context
     * @return
     */
    public static String getAuthToken(Context context) {
        return PreferenceManager.PreferenceInstance.getInstance(context).getAuthToken();
    }

    public static void removeUserPreference(Context context) {
        PreferenceManager.PreferenceInstance.getInstance(context).clearInspectorPreference();
    }
}
