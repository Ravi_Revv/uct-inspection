package com.uct.inspection.utils.enums;

public enum ApiResponseFlags {

    OK(200),
    BAD_REQUEST(400),
    UNAUTHORIZED(401),
    NOT_FOUND(404),
    NOT_ALLOWED(405),
    USER_ALREADY_CHECKED_IN(409),
    LOCATION_NOT_SERVED(512),
    CARS_NOTAVAIABLE(425),
    NO_BOOKINGS_FOUND(421),
    CustomerNotFound(411),
    SIGNUP_WITH_GOOGLE(532),
    INTERNAL_SERVER_ERROR(500),
    FORBIDDEN(403);

    private int ordinal;

    ApiResponseFlags(int ordinal) {
        this.ordinal = ordinal;
    }

    public int getOrdinal() {
        return ordinal;
    }
}