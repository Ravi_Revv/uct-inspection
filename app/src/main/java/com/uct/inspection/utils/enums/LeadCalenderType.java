package com.uct.inspection.utils.enums;

public enum LeadCalenderType {

    REGISTRATION("REGISTRATION"),
    MANUFACTURE("MANUFACTURE"),
    INSURANCE_VALIDITY("INSURANCE_VALIDITY");

    private String ordinal;

    LeadCalenderType(String ordinal) {
        this.ordinal = ordinal;
    }

    public String getLeadCalenderType() {
        return ordinal;
    }
}