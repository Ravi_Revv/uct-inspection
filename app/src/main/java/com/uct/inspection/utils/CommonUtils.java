package com.uct.inspection.utils;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;

import java.io.IOException;
import java.io.InputStream;
import java.text.DecimalFormat;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class CommonUtils {

    public static DecimalFormat decimalFormat = new DecimalFormat("0.#");

    public static Bitmap base64ImageConverter(String encodedImage) {
        try {
            String cleanImage = encodedImage.replace("data:image/png;base64,", "").replace("data:image/jpeg;base64,", "");
            byte[] decodedString = Base64.decode(cleanImage, Base64.DEFAULT);
            return BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);
        } catch (Exception e) {
            e.printStackTrace();
            Log.e("BitmapUpiImage", "Bad base 64 image");
        }
        return null;

    }


    /**
     * @param mContext current ui reference
     * @return current app version
     */
    public static String getAppVersion(Context mContext) {
        int versionCode = 2207;
        try {
            versionCode = mContext.getPackageManager().getPackageInfo(mContext.getPackageName(), 0).versionCode;
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
        return versionCode + "";
    }

    public static int getAppVersionInt(Context mContext) {
        int versionCode = 125;
        try {
            versionCode = mContext.getPackageManager().getPackageInfo(mContext.getPackageName(), 0).versionCode;
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
        return versionCode;
    }


    public static HashMap<String, String> bundleToMap(Bundle extras) {
        HashMap<String, String> map = new HashMap<String, String>();

        Set<String> ks = extras.keySet();
        Iterator<String> iterator = ks.iterator();
        while (iterator.hasNext()) {
            String key = iterator.next();
            map.put(key, extras.getString(key));
        }
        return map;
    }

    public static String getFuelTypeHtmlText(boolean isWithFuel) {
        if (isWithFuel)
            return "<html><body><b>" + "With" + "</b>&nbsp;<br/>" + "Fuel" + "</body></html>";
        else
            return "<html><body><b>" + "Without" + "</b>&nbsp;<br/>" + "Fuel" + "</body></html>";
    }

    public static String getPricePlanTypeHtmlText(String plan) {
        return "<html><body><b>" + plan + "</b>&nbsp;<br/>" + "KM" + "</body></html>";

    }

    public static String splitKmPlanDisplay(String kmPlanDisplay) {
        if (kmPlanDisplay != null && !kmPlanDisplay.isEmpty()) {
            String[] kmDisplayArr = kmPlanDisplay.split("\\s+");
            return kmDisplayArr[0].trim();
        }
        return "";
    }

    public static String testHtmlText(String kmPlanDisplay) {

        if (kmPlanDisplay != null && !kmPlanDisplay.isEmpty()) {
            String[] kmDisplayArr = kmPlanDisplay.split("\\s+");
            String plan = kmDisplayArr[0].trim().trim();
//            return "<html><body><b>" + plan + "</b>&nbsp;<br/>" + "KM" + "</body></html>";
            return plan;
        }
        return null;
    }


    public static String getDeviceName() {
        String manufacturer = Build.MANUFACTURER;
        String model = Build.MODEL;
        if (model.startsWith(manufacturer)) {
            return (model.toUpperCase());
        } else {
            return (manufacturer + " " + model).toUpperCase();
        }
    }

    public static String getDeviceToken(Context context) {
        return "dggoGa-EBxw:APA91bEmpDnXzdEJiDYSVSryYHnKTUUfBC9ozQ0Z_y9d51HVzSY3OqeBN4h57IEz8LnV0borgq4MST06SiF6iHviYv9_AukcuahwK9AjmGSZzo7Ht6Q08BvpH1l-fEx0BjjYMPsGXKuS";
    }


    public static boolean isValidEmail(CharSequence target) {
        return !TextUtils.isEmpty(target) && android.util.Patterns.EMAIL_ADDRESS.matcher(target).matches();
    }

    public static boolean isValidMobile(String phone) {
//        return !TextUtils.isEmpty(phone) && android.util.Patterns.PHONE.matcher(phone).matches();
        return (phone != null && phone.length() == 10);
    }

    public static boolean isValidPhone(String phone) {
        if (TextUtils.isEmpty(phone))
            return false;
        if (!Pattern.matches("[a-zA-Z]+", phone)) {
            return /*phone.length() > 9 &&*/ phone.length() == 10;
        }
        return false;
    }

    public static String capitalizeFirstLetter(String original) {
        if (original == null || original.length() == 0) {
            return original;
        }
        return original.substring(0, 1).toUpperCase() + original.substring(1);
    }


    public static String getSecurityDepositeText(Number securityAmount) {
        char[] suffix = {' ', 'k', 'M', 'B', 'T', 'P', 'E'};
        long numValue = securityAmount.longValue();
        int value = (int) Math.floor(Math.log10(numValue));
        int base = value / 3;
        if (value >= 3 && base < suffix.length) {
            String currentString = numValue / Math.pow(10, base * 3) + "";
            String[] separated = currentString.split("\\.");
            if (separated[1].equals("0")) {
                return "Including " + new DecimalFormat("#0").format(numValue / Math.pow(10, base * 3)) + suffix[base] + "\nrefundable deposit";
            } else {
                return "Including " + new DecimalFormat("#0.0").format(numValue / Math.pow(10, base * 3)) + suffix[base] + "\nrefundable deposit";
            }
        } else {
            return "Including " + new DecimalFormat("#,##0").format(numValue) + "\nrefundable deposit";
        }
    }

    public static boolean isValidLicenseNo(String str) {
        // Regex to check valid
        // Indian driving license number
        if (str == null) {
            return false;
        }
        if (str.isEmpty())
            return false;

        String regex
                = "^(([A-Z]{2}[0-9]{2})"
                + "( )|([A-Z]{2}-[0-9]"
                + "{2}))((19|20)[0-9]"
                + "[0-9])[0-9]{7}$";

        // Compile the ReGex
        Pattern p = Pattern.compile(regex);

        // If the string is empty
        // return false


        // Find match between given string
        // and regular expression
        // uSing Pattern.matcher()

        Matcher m = p.matcher(str);

        // Return if the string
        // matched the ReGex
        return m.matches();
    }

    public static boolean isValidAadhaarNo(String str) {
        if (str == null) {
            return false;
        }
        if (str.isEmpty())
            return false;

        String regex = "[0-9]{12}";
        Pattern p = Pattern.compile(regex);
        Matcher m = p.matcher(str);
        return m.matches();
    }

    public static boolean isValidPan(String str) {
        if (str == null) {
            return false;
        }
        if (str.isEmpty())
            return false;

        String regex = "[A-Z]{5}[0-9]{4}[A-Z]{1}";
        Pattern p = Pattern.compile(regex);
        Matcher m = p.matcher(str);
        return m.matches();
    }

    public static boolean isValidPassport(String str) {
        if (str == null) {
            return false;
        }
        if (str.isEmpty())
            return false;

        String regex = ".*";
        Pattern p = Pattern.compile(regex);
        Matcher m = p.matcher(str);
        return m.matches();
    }

    public static long getCurrentTimeStamp() {
        return System.currentTimeMillis();
    }

    public static int getOSVersion() {
        return Build.VERSION.SDK_INT;
    }


    private static boolean appInstalledOrNot(Activity activity, String pack) {
        PackageManager pm = activity.getPackageManager();
        try {
            pm.getPackageInfo(pack, PackageManager.GET_ACTIVITIES);
            return true;
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }

        return false;
    }

    public static void openCallDialer(String number, Context context) {
        try {
            Intent intent = new Intent(
                    Intent.ACTION_DIAL,
                    Uri.fromParts("tel", number, null));
            context.startActivity(intent);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void openNavigationMap(double latitude, double longitude, Context context) {
        try {
            Intent i = new Intent(Intent.ACTION_VIEW, Uri.parse("google.navigation:q=" + latitude + "," + longitude));
            i.setClassName("com.google.android.apps.maps",
                    "com.google.android.maps.MapsActivity");
            context.startActivity(i);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static String loadJSONFromAsset(Context context,String filename) {
        String json = null;
        try {
//            InputStream is = context.getAssets().open("checklist.json");
            InputStream is = context.getAssets().open(filename);
            int size = is.available();
            byte[] buffer = new byte[size];
            is.read(buffer);
            is.close();
            json = new String(buffer, "UTF-8");
        } catch (IOException ex) {
            ex.printStackTrace();
            return null;
        }
        return json;
    }

    public static String removeSpace(String string) {
        if (string == null)
            return "";
        string= string.replaceAll("\\s+", "_");
        return string.replaceAll("/", "_");
    }

    public static void setMarginLeft(View v, int left) {
        ViewGroup.MarginLayoutParams params =
                (ViewGroup.MarginLayoutParams)v.getLayoutParams();
        params.setMargins(left, params.topMargin,
                params.rightMargin, params.bottomMargin);
    }
}
