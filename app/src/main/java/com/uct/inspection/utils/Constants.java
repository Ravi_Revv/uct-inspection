package com.uct.inspection.utils;

import android.os.Environment;

public class Constants {
    public static final String DIRECTORY = Environment.getExternalStorageDirectory().getPath();


    /**** Intent Request Code ******/
    public static final int INTENT_CAMERA_REQUEST_CODE = 101;
    public static final int INTENT_GALLERY_REQUEST_CODE = 102;
    public static final int INTENT_REFURB_EDIT_JOB = 103;

    /**** VERDICT CONSTANTS ******/
    public static final String VERDICT_PASS = "COMPLETED";
    public static final String VERDICT_FAIL = "REJECTED";


    /**** ROOM DB CONSTANTS ******/
    public static final int DB_VERSION = 1;


}
