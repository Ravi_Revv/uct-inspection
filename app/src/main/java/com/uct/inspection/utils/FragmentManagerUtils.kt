package com.uct.inspection.utils

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager


class FragmentManagerUtils {
    companion object {
        fun replaceFragmentAddToStack(
            fragmentManager: FragmentManager,
            containerId: Int,
            fragment: Fragment
        ) {
            val tag = fragment.javaClass.name;
            fragmentManager.beginTransaction()
//                    .setCustomAnimations(R.anim.enter_from_left, R.anim.exit_to_right, R.anim.enter_from_right, R.anim.exit_to_left)
                .replace(containerId, fragment, tag)
                .addToBackStack(tag)
                .commitAllowingStateLoss()
        }

        fun replaceFragment(
            fragmentManager: FragmentManager,
            containerId: Int,
            fragment: Fragment
        ) {
            fragmentManager.beginTransaction()
//                    .setCustomAnimations(R.anim.enter_from_left, R.anim.exit_to_right, R.anim.enter_from_right, R.anim.exit_to_left)
                .replace(containerId, fragment)

                .commitAllowingStateLoss()

        }

        fun hideAndAddFragment(
            fragmentManager: FragmentManager,
            containerId: Int,
            fragment: Fragment
        ) {
            val tag = fragment.javaClass.name;
            val transaction = fragmentManager.beginTransaction()
            val myHideFragment = fragmentManager.findFragmentById(containerId);
            transaction.hide(myHideFragment!!);
            transaction.add(containerId, fragment)
            transaction.addToBackStack(tag);
            transaction.commitAllowingStateLoss();


        }
    }
}