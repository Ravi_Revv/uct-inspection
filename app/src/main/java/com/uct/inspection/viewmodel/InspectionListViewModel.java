package com.uct.inspection.viewmodel;

import android.app.Activity;
import android.content.Context;

import androidx.annotation.NonNull;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.google.gson.Gson;
import com.uct.inspection.R;
import com.uct.inspection.model.inspectionlist.Inspection;
import com.uct.inspection.retrofit.ApiService;
import com.uct.inspection.retrofit.RestClient;
import com.uct.inspection.utils.CommonData;
import com.uct.inspection.utils.LoadingBox;
import com.uct.inspection.utils.RequestFailureErrorCodes;
import com.uct.inspection.utils.enums.ApiResponseFlags;

import org.json.JSONObject;

import java.util.HashMap;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Consumer;
import okhttp3.ResponseBody;
import retrofit2.Response;

public class InspectionListViewModel extends ViewModel {

    private Context mContext;
    private Activity mActivity;
    private CompositeDisposable compositeDisposable = new CompositeDisposable();
    private MutableLiveData<Inspection> mInspection = new MutableLiveData<Inspection>();


    public InspectionListViewModel(@NonNull Context context) {
        this.mContext = context;
        this.mActivity = (Activity) context;
    }

    public void getInspectionList(boolean isSwipe) {
        if (!isSwipe)
            LoadingBox.showLoadingDialog((Activity) mContext, "");
        ApiService apiService = RestClient.getApiService();
        HashMap<String, String> map = new HashMap<>();
        Disposable disposable = apiService.getInspectionList(CommonData.getAuthToken(mContext))
                .subscribeOn(RestClient.subscribeScheduler())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Consumer<Response<ResponseBody>>() {
                    @Override
                    public void accept(Response<ResponseBody> resObj) throws Exception {
                        try {
                            LoadingBox.dismissLoadingDialog();
//                            Log.d("LoginResponse", resObj.toString());
                            /*if (resObj.getStatusCode() == ApiResponseFlags.OK.getOrdinal()) {
                                setInspectionLiveData(resObj);
                            } else {
                                RequestFailureErrorCodes.showErrorMessageFromResponse(mActivity, resObj.getStatusCode(), resObj.getStatusMessage());
                            }*/


                            if (resObj.code() == 200) {
//                                Log.d("LoginResponse", "Token :- " + resObj.headers().get("Token"));
                                JSONObject jsonObject = new JSONObject(resObj.body().string());
                                if (jsonObject.getInt("status_code") == ApiResponseFlags.OK.getOrdinal()) {
                                    setInspectionLiveData(new Gson().fromJson(jsonObject.toString(), Inspection.class));
                                } else {
                                    RequestFailureErrorCodes.showErrorMessageFromResponse(mActivity, jsonObject);
                                }

                            } else {
                                RequestFailureErrorCodes.showErrorDialog(mActivity, mActivity.getString(R.string.something_went_wrong));
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                            LoadingBox.dismissLoadingDialog();
                        }


                    }
                }, new Consumer<Throwable>() {
                    @Override
                    public void accept(Throwable throwable) throws Exception {
                        LoadingBox.dismissLoadingDialog();
                        RequestFailureErrorCodes.checkCode((Activity) mContext, throwable);

                    }
                });

        compositeDisposable.add(disposable);
    }


    private void setInspectionLiveData(Inspection inspection) {
        mInspection.setValue(inspection);
    }

    public MutableLiveData<Inspection> getInspectionData() {
        return mInspection;
    }

    private void unSubscribeFromObservable() {
        if (compositeDisposable != null && !compositeDisposable.isDisposed()) {
            compositeDisposable.dispose();
        }
    }

    public void reset() {
        unSubscribeFromObservable();
        compositeDisposable = null;
        mContext = null;
    }
}
