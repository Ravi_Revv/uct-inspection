package com.uct.inspection.viewmodel;

import android.app.Activity;
import android.content.Context;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.google.gson.Gson;
import com.uct.inspection.R;
import com.uct.inspection.model.inspectionlist.Inspection;
import com.uct.inspection.model.verdict.InspectionVerdict;
import com.uct.inspection.retrofit.ApiService;
import com.uct.inspection.retrofit.RestClient;
import com.uct.inspection.utils.CommonData;
import com.uct.inspection.utils.LoadingBox;
import com.uct.inspection.utils.RequestFailureErrorCodes;
import com.uct.inspection.utils.enums.ApiResponseFlags;

import org.json.JSONObject;

import java.util.HashMap;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Consumer;
import okhttp3.ResponseBody;
import retrofit2.Response;

public class InspectionVerdictViewModel extends ViewModel {

    private Context mContext;
    private Activity mActivity;
    private CompositeDisposable compositeDisposable = new CompositeDisposable();
    private MutableLiveData<InspectionVerdict> mInspectionVerdict = new MutableLiveData<InspectionVerdict>();
    private MutableLiveData<Boolean> mIsInspectionSave = new MutableLiveData<Boolean>();


    public InspectionVerdictViewModel(@NonNull Context context) {
        this.mContext = context;
        this.mActivity = (Activity) context;
    }

    public void getInspectionVerdict(int inspectionId) {
        LoadingBox.showLoadingDialog(mActivity, "");
        ApiService apiService = RestClient.getApiService();
        HashMap<String, String> map = new HashMap<>();
        Disposable disposable = apiService.getInspectionVerdict(CommonData.getAuthToken(mContext), inspectionId)
                .subscribeOn(RestClient.subscribeScheduler())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Consumer<Response<ResponseBody>>() {
                    @Override
                    public void accept(Response<ResponseBody> resObj) throws Exception {
                        try {
                            LoadingBox.dismissLoadingDialog();
                            if (resObj.code() == 200) {
//                                Log.d("LoginResponse", "Token :- " + resObj.headers().get("Token"));
                                JSONObject jsonObject = new JSONObject(resObj.body().string());
                                if (jsonObject.getInt("status_code") == ApiResponseFlags.OK.getOrdinal()) {
                                    setInspectionVerdictLiveData(new Gson().fromJson(jsonObject.toString(), InspectionVerdict.class));
                                } else {
                                    RequestFailureErrorCodes.showErrorMessageFromResponse(mActivity, jsonObject);
                                }

                            } else {
                                RequestFailureErrorCodes.showErrorDialog(mActivity, mActivity.getString(R.string.something_went_wrong));
                            }

                        } catch (Exception e) {
                            e.printStackTrace();
                            LoadingBox.dismissLoadingDialog();
                            RequestFailureErrorCodes.showErrorDialog(mActivity, mActivity.getString(R.string.something_went_wrong));

                        }
                    }
                }, new Consumer<Throwable>() {
                    @Override
                    public void accept(Throwable throwable) throws Exception {
                        LoadingBox.dismissLoadingDialog();
                        RequestFailureErrorCodes.checkCode((Activity) mContext, throwable);
                    }
                });

        compositeDisposable.add(disposable);
    }

    public void saveInspectionVerdict(int inspectionId, String verdict, String remarks) {
        LoadingBox.showLoadingDialog(mActivity, "");
        ApiService apiService = RestClient.getApiService();
        HashMap<String, String> map = new HashMap<>();
        map.put("verdict", verdict);
        map.put("remarks", remarks);
        Disposable disposable = apiService.saveInspectionVerdict(CommonData.getAuthToken(mContext), inspectionId, map)
                .subscribeOn(RestClient.subscribeScheduler())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Consumer<Response<ResponseBody>>() {
                    @Override
                    public void accept(Response<ResponseBody> resObj) throws Exception {
                        try {
                            LoadingBox.dismissLoadingDialog();
                            if (resObj.code() == 200) {
                                JSONObject jsonObject = new JSONObject(resObj.body().string());
                                if (jsonObject.getInt("status_code") == ApiResponseFlags.OK.getOrdinal()) {
                                    setIsInspectionSaveLiveData(true);
                                } else {
                                    RequestFailureErrorCodes.showErrorMessageFromResponse(mActivity, jsonObject);
                                }

                            } else {
                                RequestFailureErrorCodes.showErrorDialog(mActivity, mActivity.getString(R.string.something_went_wrong));

                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                            LoadingBox.dismissLoadingDialog();
                            RequestFailureErrorCodes.showErrorDialog(mActivity, mActivity.getString(R.string.something_went_wrong));

                        }
                    }
                }, new Consumer<Throwable>() {
                    @Override
                    public void accept(Throwable throwable) throws Exception {
                        LoadingBox.dismissLoadingDialog();
                        RequestFailureErrorCodes.checkCode((Activity) mContext, throwable);
                    }
                });

        compositeDisposable.add(disposable);
    }


    private void setInspectionVerdictLiveData(InspectionVerdict inspectionVerdict) {
        mInspectionVerdict.setValue(inspectionVerdict);
    }

    public MutableLiveData<Boolean> getIsInspectionSaveLiveData() {
        return mIsInspectionSave;
    }

    private void setIsInspectionSaveLiveData(boolean isInspectionSave) {
        mIsInspectionSave.setValue(isInspectionSave);
    }

    public MutableLiveData<InspectionVerdict> getInspectionVerdictData() {
        return mInspectionVerdict;
    }

    private void unSubscribeFromObservable() {
        if (compositeDisposable != null && !compositeDisposable.isDisposed()) {
            compositeDisposable.dispose();
        }
    }

    public void reset() {
        unSubscribeFromObservable();
        compositeDisposable = null;
        mContext = null;
    }
}
