package com.uct.inspection.viewmodel;

import android.app.Activity;
import android.content.Context;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.uct.inspection.R;
import com.uct.inspection.model.inspectordetail.InspectorData;
import com.uct.inspection.retrofit.ApiService;
import com.uct.inspection.retrofit.RestClient;
import com.uct.inspection.utils.CommonData;
import com.uct.inspection.utils.LoadingBox;
import com.uct.inspection.utils.RequestFailureErrorCodes;
import com.uct.inspection.utils.enums.ApiResponseFlags;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Consumer;
import okhttp3.ResponseBody;
import retrofit2.Response;

public class LoginViewModel extends ViewModel {

    private Context mContext;
    private Activity mActivity;
    private CompositeDisposable compositeDisposable = new CompositeDisposable();
    private MutableLiveData<InspectorData> mInspectorData = new MutableLiveData<InspectorData>();


    public LoginViewModel(@NonNull Context context) {
        this.mContext = context;
        this.mActivity = (Activity) context;
    }

    public void login(String email, String password) {
        LoadingBox.showLoadingDialog(mActivity, "");
        ApiService apiService = RestClient.getAuthApiService();
        HashMap<String, String> map = new HashMap<>();
        map.put("userEmail", email);
        map.put("password", password);
        Disposable disposable = apiService.login(map)
                .subscribeOn(RestClient.subscribeScheduler())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Consumer<Response<ResponseBody>>() {
                    @Override
                    public void accept(Response<ResponseBody> resObj) throws Exception {
                        try {
                            LoadingBox.dismissLoadingDialog();
                            if (resObj.code() == 200) {
//                                Log.d("LoginResponse", "Token :- " + resObj.headers().get("Token"));
                                JSONObject jsonObject = new JSONObject(resObj.body().string());
                                if (jsonObject.getInt("status_code") == ApiResponseFlags.OK.getOrdinal()) {
                                    JSONArray dataArr = jsonObject.getJSONArray("data");
                                    if (dataArr.length() > 0) {
                                        JSONObject dataObj = dataArr.getJSONObject(0);
//                                    CommonData.saveInspectorData(mContext, new InspectorData(dataObj.getInt("id"), dataObj.getString("userName"), dataObj.getString("userEmail"), dataObj.getInt("userCity"), dataObj.getString("password"), dataObj.getString("adminType")));
                                        setInspectorDataLiveData(new InspectorData(dataObj.getInt("id"), dataObj.getString("userName"), dataObj.getString("userEmail"), dataObj.getInt("userCity"), dataObj.getString("password"), dataObj.getString("adminType")));
                                        CommonData.saveAuthToken(mContext, resObj.headers().get("Token"));
                                    }
                                } else {
                                    RequestFailureErrorCodes.showErrorMessageFromResponse(mActivity, jsonObject);
                                }

                            } else {
                                RequestFailureErrorCodes.showErrorDialog(mActivity, mActivity.getString(R.string.something_went_wrong));
                            }


                        } catch (JSONException e) {
                            e.printStackTrace();
                            LoadingBox.dismissLoadingDialog();
                            RequestFailureErrorCodes.showErrorDialog(mActivity, mActivity.getString(R.string.something_went_wrong));
                        }
                    }
                }, new Consumer<Throwable>() {
                    @Override
                    public void accept(Throwable throwable) throws Exception {
                        LoadingBox.dismissLoadingDialog();
                        RequestFailureErrorCodes.checkCode((Activity) mContext, throwable);

                    }
                });

        compositeDisposable.add(disposable);
    }


    private void setInspectorDataLiveData(InspectorData inspectorData) {
        mInspectorData.setValue(inspectorData);
    }

    public MutableLiveData<InspectorData> getInspectorData() {
        return mInspectorData;
    }

    private void unSubscribeFromObservable() {
        if (compositeDisposable != null && !compositeDisposable.isDisposed()) {
            compositeDisposable.dispose();
        }
    }

    public void reset() {
        unSubscribeFromObservable();
        compositeDisposable = null;
        mContext = null;
    }
}
