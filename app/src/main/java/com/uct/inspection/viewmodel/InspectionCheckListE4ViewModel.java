package com.uct.inspection.viewmodel;

import android.app.Activity;
import android.content.Context;
import android.util.Log;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.google.gson.Gson;
import com.uct.inspection.R;
import com.uct.inspection.db.repository.InspectionChecklistRepository;
import com.uct.inspection.db.repository.InspectionE4ChecklistRepository;
import com.uct.inspection.model.checklist.ChecklistHeader;
import com.uct.inspection.model.checklist.InspectionChecklist;
import com.uct.inspection.model.checklistE4.InspectionE4Checklist;
import com.uct.inspection.model.files.FileRequest;
import com.uct.inspection.model.files.FileRequestData;
import com.uct.inspection.retrofit.ApiService;
import com.uct.inspection.retrofit.RestClient;
import com.uct.inspection.utils.CommonData;
import com.uct.inspection.utils.LoadingBox;
import com.uct.inspection.utils.RequestFailureErrorCodes;
import com.uct.inspection.utils.enums.ApiResponseFlags;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Consumer;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Response;

public class InspectionCheckListE4ViewModel extends ViewModel {

    private Context mContext;
    private Activity mActivity;
    private CompositeDisposable compositeDisposable = new CompositeDisposable();
    private MutableLiveData<InspectionE4Checklist> mInspectionE4Checklist = new MutableLiveData<InspectionE4Checklist>();
    private LiveData<InspectionE4Checklist> mInspectionE4ChecklistDb = new MutableLiveData<InspectionE4Checklist>();
    private LiveData<List<InspectionE4Checklist>> mAllInspectionE4ChecklistDb = new MutableLiveData<List<InspectionE4Checklist>>();
    private MutableLiveData<Boolean> mIsFinalSubmit = new MutableLiveData<Boolean>();
    private MutableLiveData<Boolean> mIsFinalFileSubmit = new MutableLiveData<Boolean>();
    private MutableLiveData<Boolean> mIsUnAssuredSubmit = new MutableLiveData<Boolean>();

    public InspectionCheckListE4ViewModel(@NonNull Context context) {
        this.mContext = context;
        this.mActivity = (Activity) context;
    }

    public void getInspectionChecklist(final int inspectionId) {
        LoadingBox.showLoadingDialog(mActivity, "");
        ApiService apiService = RestClient.getApiService();
        Disposable disposable = apiService.getInspectionChecklist(CommonData.getAuthToken(mContext), inspectionId)
                .subscribeOn(RestClient.subscribeScheduler())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Consumer<Response<ResponseBody>>() {
                    @Override
                    public void accept(Response<ResponseBody> resObj) throws Exception {
                        try {
                            LoadingBox.dismissLoadingDialog();
                            if (resObj.code() == 200) {
//                                Log.d("LoginResponse", "Token :- " + resObj.headers().get("Token"));
                                JSONObject jsonObject = new JSONObject(resObj.body().string());
                                if (jsonObject.getInt("status_code") == ApiResponseFlags.OK.getOrdinal()) {
                                    setInspectionChecklistLiveData(new Gson().fromJson(jsonObject.toString(), InspectionE4Checklist.class));
                                } else {
                                    RequestFailureErrorCodes.showErrorMessageFromResponse(mActivity, jsonObject);
                                }

                            } else {
                                RequestFailureErrorCodes.showErrorDialog(mActivity, mActivity.getString(R.string.something_went_wrong));
                            }


                        } catch (Exception e) {
                            e.printStackTrace();
                            LoadingBox.dismissLoadingDialog();
                            RequestFailureErrorCodes.showErrorDialog(mActivity, mActivity.getString(R.string.something_went_wrong));

                        }
                    }
                }, new Consumer<Throwable>() {
                    @Override
                    public void accept(Throwable throwable) throws Exception {
                        LoadingBox.dismissLoadingDialog();
                        RequestFailureErrorCodes.checkCode((Activity) mContext, throwable);
                    }
                });

        compositeDisposable.add(disposable);
    }

    public void saveInspectionChecklist(final int inspectionId, InspectionE4Checklist inspectionChecklist, final boolean isFinalSubmit) {
        LoadingBox.showLoadingDialog(mActivity, "");
        Gson gson = new Gson();
//        inspectionChecklist.setInsertTS(System.currentTimeMillis());
        String jsonString = gson.toJson(inspectionChecklist);
//        Log.e("jsonString", jsonString);
        RequestBody body = RequestBody.create(okhttp3.MediaType.parse("application/json; charset=utf-8"), jsonString);
        ApiService apiService = RestClient.getApiService();
        Disposable disposable = apiService.saveInspectionChecklist(CommonData.getAuthToken(mContext), inspectionId, body)
                .subscribeOn(RestClient.subscribeScheduler())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Consumer<Response<ResponseBody>>() {
                    @Override
                    public void accept(Response<ResponseBody> resObj) throws Exception {
                        try {
                            LoadingBox.dismissLoadingDialog();

                            if (resObj.code() == 200) {
                                JSONObject jsonObject = new JSONObject(resObj.body().string());
                                if (jsonObject.getInt("status_code") == ApiResponseFlags.OK.getOrdinal()) {
                                    setIsFinalSubmitLiveData(isFinalSubmit);
                                } else {
                                    RequestFailureErrorCodes.showErrorMessageFromResponse(mActivity, jsonObject);
                                }

                            } else {
                                RequestFailureErrorCodes.showErrorDialog(mActivity, mActivity.getString(R.string.something_went_wrong));

                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                            LoadingBox.dismissLoadingDialog();
                            RequestFailureErrorCodes.showErrorDialog(mActivity, mActivity.getString(R.string.something_went_wrong));

                        }
                    }
                }, new Consumer<Throwable>() {
                    @Override
                    public void accept(Throwable throwable) throws Exception {
                        LoadingBox.dismissLoadingDialog();
                        RequestFailureErrorCodes.checkCode((Activity) mContext, throwable);

                    }
                });

        compositeDisposable.add(disposable);
    }


    public void saveInspectionChecklistFromUnAssured(final int inspectionId, InspectionChecklist inspectionChecklist, final boolean isFinalSubmit) {
        LoadingBox.showLoadingDialog(mActivity, "");
        Gson gson = new Gson();
//        inspectionChecklist.setInsertTS(System.currentTimeMillis());
        String jsonString = gson.toJson(inspectionChecklist);
//        Log.e("jsonString", jsonString);
        RequestBody body = RequestBody.create(okhttp3.MediaType.parse("application/json; charset=utf-8"), jsonString);
        ApiService apiService = RestClient.getApiService();
        Disposable disposable = apiService.saveInspectionChecklist(CommonData.getAuthToken(mContext), inspectionId, body)
                .subscribeOn(RestClient.subscribeScheduler())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Consumer<Response<ResponseBody>>() {
                    @Override
                    public void accept(Response<ResponseBody> resObj) throws Exception {
                        try {
                            LoadingBox.dismissLoadingDialog();

                            if (resObj.code() == 200) {
                                JSONObject jsonObject = new JSONObject(resObj.body().string());
                                if (jsonObject.getInt("status_code") == ApiResponseFlags.OK.getOrdinal()) {
                                    setIsUnAssuredSubmitLiveData(isFinalSubmit);
                                } else {
                                    RequestFailureErrorCodes.showErrorMessageFromResponse(mActivity, jsonObject);
                                }

                            } else {
                                RequestFailureErrorCodes.showErrorDialog(mActivity, mActivity.getString(R.string.something_went_wrong));

                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                            LoadingBox.dismissLoadingDialog();
                            RequestFailureErrorCodes.showErrorDialog(mActivity, mActivity.getString(R.string.something_went_wrong));

                        }
                    }
                }, new Consumer<Throwable>() {
                    @Override
                    public void accept(Throwable throwable) throws Exception {
                        LoadingBox.dismissLoadingDialog();
                        RequestFailureErrorCodes.checkCode((Activity) mContext, throwable);

                    }
                });

        compositeDisposable.add(disposable);
    }


    public void saveInspectionChecklistImage(final int inspectionId, InspectionChecklist inspectionChecklist, final boolean isFinalSubmit) {
        LoadingBox.showLoadingDialog(mActivity, "");
        Gson gson = new Gson();
        List<FileRequest> images = new ArrayList<>();
        for (int i = 0; i < inspectionChecklist.getInspectionChecklistData().size(); i++) {
            for (int j = 0; j < inspectionChecklist.getInspectionChecklistData().get(i).getChecklistHeader().size(); j++) {
//                for (int k = 0; k < inspectionChecklist.getInspectionChecklistData().get(i).getChecklistHeader().get(j).getImages().size(); k++) {
                ChecklistHeader header = inspectionChecklist.getInspectionChecklistData().get(i).getChecklistHeader().get(j);
                images.add(new FileRequest(header.getSubPartID(), header.isFileRequired(), header.getImages()));
//                }
            }
        }

        String jsonString = gson.toJson(new FileRequestData(inspectionChecklist.getInsertTS(), images));
//        Log.e("jsonString", jsonString);
        RequestBody body = RequestBody.create(okhttp3.MediaType.parse("application/json; charset=utf-8"), jsonString);
        ApiService apiService = RestClient.getApiService();
        Disposable disposable = apiService.saveInspectionChecklistImage(CommonData.getAuthToken(mContext), inspectionId, body)
                .subscribeOn(RestClient.subscribeScheduler())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Consumer<Response<ResponseBody>>() {
                    @Override
                    public void accept(Response<ResponseBody> resObj) throws Exception {
                        try {
                            LoadingBox.dismissLoadingDialog();
                            if (resObj.code() == 200) {
                                JSONObject jsonObject = new JSONObject(resObj.body().string());
                                if (jsonObject.getInt("status_code") == ApiResponseFlags.OK.getOrdinal()) {
                                    setIsFinalFileSubmitLiveData(isFinalSubmit);
                                } else {
                                    RequestFailureErrorCodes.showErrorMessageFromResponse(mActivity, jsonObject);
                                }
                            } else {
                                RequestFailureErrorCodes.showErrorDialog(mActivity, mActivity.getString(R.string.something_went_wrong));

                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                            LoadingBox.dismissLoadingDialog();
                            RequestFailureErrorCodes.showErrorDialog(mActivity, mActivity.getString(R.string.something_went_wrong));

                        }
                    }
                }, new Consumer<Throwable>() {
                    @Override
                    public void accept(Throwable throwable) throws Exception {
                        LoadingBox.dismissLoadingDialog();
                        RequestFailureErrorCodes.checkCode((Activity) mContext, throwable);

                    }
                });

        compositeDisposable.add(disposable);
    }


    public void insertInspectionE4ChecklistData(InspectionE4Checklist inspectionChecklist, int position) {
        /*for (int i = 0; i < inspectionChecklist.getInspectionChecklistData().size(); i++) {
            for (int j = 0; j < inspectionChecklist.getInspectionChecklistData().get(i).getChecklistHeader().size(); j++) {
                for (int k = 0; k < inspectionChecklist.getInspectionChecklistData().get(i).getChecklistHeader().get(j).getChecklistIssue().size(); k++) {
                    for (int l = 0; l < inspectionChecklist.getInspectionChecklistData().get(i).getChecklistHeader().get(j).getChecklistIssue().get(k).getChecklistSubIssue().size(); l++) {
                        if (inspectionChecklist.getInspectionChecklistData().get(i).getChecklistHeader().get(j).getChecklistIssue().get(k).getSelected() && inspectionChecklist.getInspectionChecklistData().get(i).getChecklistHeader().get(j).getAllSubIssues().contains(inspectionChecklist.getInspectionChecklistData().get(i).getChecklistHeader().get(j).getChecklistIssue().get(k).getChecklistSubIssue().get(l))) {
                            int pos = inspectionChecklist.getInspectionChecklistData().get(i).getChecklistHeader().get(j).getAllSubIssues().indexOf(inspectionChecklist.getInspectionChecklistData().get(i).getChecklistHeader().get(j).getChecklistIssue().get(k).getChecklistSubIssue().get(l));
                            inspectionChecklist.getInspectionChecklistData().get(i).getChecklistHeader().get(j).getChecklistIssue().get(k).getChecklistSubIssue().set(l, inspectionChecklist.getInspectionChecklistData().get(i).getChecklistHeader().get(j).getAllSubIssues().get(pos));
                        }

                    }
                }
                for (int m = 0; m < inspectionChecklist.getInspectionChecklistData().get(i).getChecklistHeader().get(j).getDefaultSubIssues().size(); m++) {
                    if (inspectionChecklist.getInspectionChecklistData().get(i).getChecklistHeader().get(j).getAllSubIssues().contains(inspectionChecklist.getInspectionChecklistData().get(i).getChecklistHeader().get(j).getDefaultSubIssues().get(m))) {
                        int pos = inspectionChecklist.getInspectionChecklistData().get(i).getChecklistHeader().get(j).getAllSubIssues().indexOf(inspectionChecklist.getInspectionChecklistData().get(i).getChecklistHeader().get(j).getDefaultSubIssues().get(m));
                        inspectionChecklist.getInspectionChecklistData().get(i).getChecklistHeader().get(j).getDefaultSubIssues().set(m, inspectionChecklist.getInspectionChecklistData().get(i).getChecklistHeader().get(j).getAllSubIssues().get(pos));
                    }

                }
            }
        }*/
        Toast.makeText(mContext, "insert", Toast.LENGTH_SHORT).show();
        inspectionChecklist.setInsertTS(System.currentTimeMillis());
        Log.d("setInsertTS", Long.toString(inspectionChecklist.getInsertTS()));
        InspectionE4ChecklistRepository.Companion.insertInspectionChecklistData(mContext, inspectionChecklist, position);
    }


    public MutableLiveData<Boolean> isFinalFileSubmit() {
        return mIsFinalFileSubmit;
    }

    private void setIsFinalFileSubmitLiveData(boolean isIsFinalFileSubmit) {
        mIsFinalFileSubmit.setValue(isIsFinalFileSubmit);
    }

    public MutableLiveData<Boolean> isFinalSubmit() {
        return mIsFinalSubmit;
    }

    private void setIsFinalSubmitLiveData(boolean isFinalSubmit) {
        mIsFinalSubmit.setValue(isFinalSubmit);
    }

    public MutableLiveData<Boolean> isUnAssuredSubmit() {
        return mIsUnAssuredSubmit;
    }

    private void setIsUnAssuredSubmitLiveData(boolean isUnAssuredSubmit) {
        mIsUnAssuredSubmit.setValue(isUnAssuredSubmit);
    }


    public LiveData<InspectionE4Checklist> getInspectionChecklistDataFromDb(int inspectionId) {
        mInspectionE4ChecklistDb = InspectionE4ChecklistRepository.Companion.getInspectionE4ChecklistData(mContext, inspectionId);
        return mInspectionE4ChecklistDb;
    }

    public LiveData<List<InspectionE4Checklist>> getAllInspectionChecklistDataFromDb() {
        mAllInspectionE4ChecklistDb = InspectionE4ChecklistRepository.Companion.getAllInspectionE4ChecklistData(mContext);
        return mAllInspectionE4ChecklistDb;
    }

    private void setInspectionChecklistLiveData(InspectionE4Checklist inspectionChecklist) {
        mInspectionE4Checklist.setValue(inspectionChecklist);
    }

    public MutableLiveData<InspectionE4Checklist> getInspectionChecklistData() {
        return mInspectionE4Checklist;
    }

    private void unSubscribeFromObservable() {
        if (compositeDisposable != null && !compositeDisposable.isDisposed()) {
            compositeDisposable.dispose();
        }
    }

    public void reset() {
        unSubscribeFromObservable();
        compositeDisposable = null;
        mContext = null;
    }
}
