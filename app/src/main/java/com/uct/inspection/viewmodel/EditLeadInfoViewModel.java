package com.uct.inspection.viewmodel;

import android.app.Activity;
import android.content.Context;

import androidx.annotation.NonNull;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.google.gson.Gson;
import com.uct.inspection.R;
import com.uct.inspection.model.lead.LeadInfo;
import com.uct.inspection.model.lead.LeadInfoData;
import com.uct.inspection.model.refurb.RefurbDocument;
import com.uct.inspection.model.refurb.RefurbDocumentData;
import com.uct.inspection.model.refurb.RefurbList;
import com.uct.inspection.model.seller.SellerInfo;
import com.uct.inspection.model.seller.SellerInfoData;
import com.uct.inspection.retrofit.ApiService;
import com.uct.inspection.retrofit.RestClient;
import com.uct.inspection.utils.CommonData;
import com.uct.inspection.utils.LoadingBox;
import com.uct.inspection.utils.RequestFailureErrorCodes;
import com.uct.inspection.utils.enums.ApiResponseFlags;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.List;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Consumer;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Response;

public class EditLeadInfoViewModel extends ViewModel {

    private Context mContext;
    private Activity mActivity;
    private CompositeDisposable compositeDisposable = new CompositeDisposable();
    private MutableLiveData<LeadInfoData> mLeadInfoData = new MutableLiveData<LeadInfoData>();
    private MutableLiveData<SellerInfo> mSellerInfo = new MutableLiveData<SellerInfo>();
    private MutableLiveData<Boolean> mIsRefurbDocumentSave = new MutableLiveData<Boolean>();
    private MutableLiveData<Boolean> mIsLeadStatusUpdate = new MutableLiveData<Boolean>();


    public EditLeadInfoViewModel(@NonNull Context context) {
        this.mContext = context;
        this.mActivity = (Activity) context;
    }

    public void getLeadInfo() {
        LoadingBox.showLoadingDialog(mActivity, "");
        ApiService apiService = RestClient.getApiService();
        HashMap<String, String> map = new HashMap<>();
        Disposable disposable = apiService.getLeadInfo(CommonData.getAuthToken(mContext))
                .subscribeOn(RestClient.subscribeScheduler())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Consumer<Response<ResponseBody>>() {
                    @Override
                    public void accept(Response<ResponseBody> resObj) throws Exception {
                        try {
                            LoadingBox.dismissLoadingDialog();
                            if (resObj.code() == 200) {
//                                Log.d("LoginResponse", "Token :- " + resObj.headers().get("Token"));
                                JSONObject jsonObject = new JSONObject(resObj.body().string());
                                if (jsonObject.getInt("status_code") == ApiResponseFlags.OK.getOrdinal()) {
                                    setLeadInfoDataLiveData(new Gson().fromJson(jsonObject.toString(), LeadInfo.class));
                                } else {
                                    RequestFailureErrorCodes.showErrorMessageFromResponse(mActivity, jsonObject);
                                }

                            } else {
                                RequestFailureErrorCodes.showErrorDialog(mActivity, mActivity.getString(R.string.something_went_wrong));
                            }

                        } catch (Exception e) {
                            e.printStackTrace();
                            LoadingBox.dismissLoadingDialog();
                            RequestFailureErrorCodes.showErrorDialog(mActivity, mActivity.getString(R.string.something_went_wrong));

                        }
                    }
                }, new Consumer<Throwable>() {
                    @Override
                    public void accept(Throwable throwable) throws Exception {
                        LoadingBox.dismissLoadingDialog();
                        RequestFailureErrorCodes.checkCode((Activity) mContext, throwable);
                    }
                });

        compositeDisposable.add(disposable);
    }


    public void getSellerInfo(int serviceCityId, String sellerType) {
        LoadingBox.showLoadingDialog(mActivity, "");
        ApiService apiService = RestClient.getApiService();
        HashMap<String, String> map = new HashMap<>();
        Disposable disposable = apiService.getSeller(CommonData.getAuthToken(mContext),serviceCityId,sellerType)
                .subscribeOn(RestClient.subscribeScheduler())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Consumer<Response<ResponseBody>>() {
                    @Override
                    public void accept(Response<ResponseBody> resObj) throws Exception {
                        try {
                            LoadingBox.dismissLoadingDialog();
                            if (resObj.code() == 200) {
//                                Log.d("LoginResponse", "Token :- " + resObj.headers().get("Token"));
                                JSONObject jsonObject = new JSONObject(resObj.body().string());
                                if (jsonObject.getInt("status_code") == ApiResponseFlags.OK.getOrdinal()) {
                                    setSellerInfoDataLiveData(new Gson().fromJson(jsonObject.toString(), SellerInfo.class));
                                } else {
                                    setSellerInfoDataLiveData(null);
                                    RequestFailureErrorCodes.showErrorMessageFromResponse(mActivity, jsonObject);
                                }

                            } else {
                                setSellerInfoDataLiveData(null);
                                RequestFailureErrorCodes.showErrorDialog(mActivity, mActivity.getString(R.string.something_went_wrong));
                            }

                        } catch (Exception e) {
                            e.printStackTrace();
                            LoadingBox.dismissLoadingDialog();
                            RequestFailureErrorCodes.showErrorDialog(mActivity, mActivity.getString(R.string.something_went_wrong));

                        }
                    }
                }, new Consumer<Throwable>() {
                    @Override
                    public void accept(Throwable throwable) throws Exception {
                        LoadingBox.dismissLoadingDialog();
                        RequestFailureErrorCodes.checkCode((Activity) mContext, throwable);
                    }
                });

        compositeDisposable.add(disposable);
    }



    public void updateLeadInfo(String leadID,int carModelID,int sellerID,String carRegistrationNumber,String carColour,String registrationTimestamp,
                                   String manufacturingTimestamp,String insuranceValidity,String insuranceType,String rto,String registrationType,String ownershipSerial,int serviceCityID,String comments,String kmDriven) {
        LoadingBox.showLoadingDialog(mActivity, "");
        ApiService apiService = RestClient.getApiService();
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("id", leadID);
            jsonObject.put("carModelID", carModelID);
            jsonObject.put("sellerID", sellerID);
            jsonObject.put("registrationNumber", carRegistrationNumber);
            jsonObject.put("carColour", carColour);
            jsonObject.put("registrationTimestamp", registrationTimestamp);
            jsonObject.put("manufacturingTimestamp", manufacturingTimestamp);
            jsonObject.put("insuranceValidity", insuranceValidity);
            jsonObject.put("insuranceType", insuranceType);
            jsonObject.put("rto", rto);
            jsonObject.put("registrationType", registrationType);
            jsonObject.put("ownershipSerial", ownershipSerial);
            jsonObject.put("serviceCityID", serviceCityID);
            jsonObject.put("comments", comments);
            jsonObject.put("kmsDriven", kmDriven);
        } catch (JSONException e) {
            e.printStackTrace();
        }
//        String jsonString = new Gson().toJson(jsonObject);
        RequestBody body = RequestBody.create(okhttp3.MediaType.parse("application/json; charset=utf-8"), jsonObject.toString());
        Disposable disposable = apiService.updateLeadInfo(CommonData.getAuthToken(mContext), body)
                .subscribeOn(RestClient.subscribeScheduler())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Consumer<Response<ResponseBody>>() {
                    @Override
                    public void accept(Response<ResponseBody> resObj) throws Exception {
                        try {
                            LoadingBox.dismissLoadingDialog();
                            if (resObj.code() == 200) {
                                JSONObject jsonObject = new JSONObject(resObj.body().string());
                                if (jsonObject.getInt("status_code") == ApiResponseFlags.OK.getOrdinal()) {
                                    setLeadStatusUpdateLiveData(true);
                                } else {
                                    RequestFailureErrorCodes.showErrorMessageFromResponse(mActivity, jsonObject);
                                }

                            } else {
                                RequestFailureErrorCodes.showErrorDialog(mActivity, mActivity.getString(R.string.something_went_wrong));

                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                            LoadingBox.dismissLoadingDialog();
                            RequestFailureErrorCodes.showErrorDialog(mActivity, mActivity.getString(R.string.something_went_wrong));

                        }
                    }
                }, new Consumer<Throwable>() {
                    @Override
                    public void accept(Throwable throwable) throws Exception {
                        LoadingBox.dismissLoadingDialog();
                        RequestFailureErrorCodes.checkCode((Activity) mContext, throwable);
                    }
                });

        compositeDisposable.add(disposable);
    }


    public MutableLiveData<LeadInfoData> getLeadInfoDataData() {
        return mLeadInfoData;
    }

    private void setLeadInfoDataLiveData(LeadInfo leadInfo) {
        if (leadInfo != null && leadInfo.getLeadInfoData() != null && leadInfo.getLeadInfoData().size() > 0) {
            mLeadInfoData.setValue(leadInfo.getLeadInfoData().get(0));
        }

    }

    public MutableLiveData<SellerInfo> getSellerInfoData() {
        return mSellerInfo;
    }

    private void setSellerInfoDataLiveData(SellerInfo sellerInfo) {
            mSellerInfo.setValue(sellerInfo);
    }

    public MutableLiveData<Boolean> isRefurbDocumentSaveLiveData() {
        return mIsRefurbDocumentSave;
    }

    private void setIsRefurbDocumentSaveLiveData(boolean isDocumentSave) {
        mIsRefurbDocumentSave.setValue(isDocumentSave);
    }

    public MutableLiveData<Boolean> isLeadStatusUpdateLiveData() {
        return mIsLeadStatusUpdate;
    }

    private void setLeadStatusUpdateLiveData(boolean isLeadStatusUpdate) {
        mIsLeadStatusUpdate.setValue(isLeadStatusUpdate);
    }


    private void unSubscribeFromObservable() {
        if (compositeDisposable != null && !compositeDisposable.isDisposed()) {
            compositeDisposable.dispose();
        }
    }

    public void reset() {
        unSubscribeFromObservable();
        compositeDisposable = null;
        mContext = null;
    }
}
