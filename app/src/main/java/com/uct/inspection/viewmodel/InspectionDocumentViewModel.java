package com.uct.inspection.viewmodel;

import android.app.Activity;
import android.content.Context;

import androidx.annotation.NonNull;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.google.gson.Gson;
import com.uct.inspection.R;
import com.uct.inspection.model.document.InspectionDocument;
import com.uct.inspection.model.document.InspectionDocumentData;
import com.uct.inspection.model.files.FileRequestData;
import com.uct.inspection.model.verdict.InspectionVerdict;
import com.uct.inspection.retrofit.ApiService;
import com.uct.inspection.retrofit.RestClient;
import com.uct.inspection.utils.CommonData;
import com.uct.inspection.utils.LoadingBox;
import com.uct.inspection.utils.RequestFailureErrorCodes;
import com.uct.inspection.utils.enums.ApiResponseFlags;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.List;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Consumer;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Response;

public class InspectionDocumentViewModel extends ViewModel {

    private Context mContext;
    private Activity mActivity;
    private CompositeDisposable compositeDisposable = new CompositeDisposable();
    private MutableLiveData<InspectionDocument> mInspectionDocument = new MutableLiveData<InspectionDocument>();
    private MutableLiveData<Boolean> mIsDocumentSave = new MutableLiveData<Boolean>();


    public InspectionDocumentViewModel(@NonNull Context context) {
        this.mContext = context;
        this.mActivity = (Activity) context;
    }

    public void getPickupChecklist(int inspectionId) {
        LoadingBox.showLoadingDialog(mActivity, "");
        ApiService apiService = RestClient.getApiService();
        HashMap<String, String> map = new HashMap<>();
        Disposable disposable = apiService.getPickupChecklist(CommonData.getAuthToken(mContext), inspectionId)
                .subscribeOn(RestClient.subscribeScheduler())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Consumer<Response<ResponseBody>>() {
                    @Override
                    public void accept(Response<ResponseBody> resObj) throws Exception {
                        try {
                            LoadingBox.dismissLoadingDialog();
                            if (resObj.code() == 200) {
//                                Log.d("LoginResponse", "Token :- " + resObj.headers().get("Token"));
                                JSONObject jsonObject = new JSONObject(resObj.body().string());
                                if (jsonObject.getInt("status_code") == ApiResponseFlags.OK.getOrdinal()) {
                                    setInspectionDocumentLiveData(new Gson().fromJson(jsonObject.toString(), InspectionDocument.class));
                                } else {
                                    RequestFailureErrorCodes.showErrorMessageFromResponse(mActivity, jsonObject);
                                }

                            } else {
                                RequestFailureErrorCodes.showErrorDialog(mActivity, mActivity.getString(R.string.something_went_wrong));
                            }

                        } catch (Exception e) {
                            e.printStackTrace();
                            LoadingBox.dismissLoadingDialog();
                            RequestFailureErrorCodes.showErrorDialog(mActivity, mActivity.getString(R.string.something_went_wrong));

                        }
                    }
                }, new Consumer<Throwable>() {
                    @Override
                    public void accept(Throwable throwable) throws Exception {
                        LoadingBox.dismissLoadingDialog();
                        RequestFailureErrorCodes.checkCode((Activity) mContext, throwable);
                    }
                });

        compositeDisposable.add(disposable);
    }

    public void savePickupChecklist(int inspectionId, List<InspectionDocumentData> documentData) {
        LoadingBox.showLoadingDialog(mActivity, "");
        ApiService apiService = RestClient.getApiService();
        String jsonString = new Gson().toJson(documentData);
//        Log.e("jsonString", jsonString);
        RequestBody body = RequestBody.create(okhttp3.MediaType.parse("application/json; charset=utf-8"), jsonString);
        Disposable disposable = apiService.savePickupChecklist(CommonData.getAuthToken(mContext), inspectionId, body)
                .subscribeOn(RestClient.subscribeScheduler())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Consumer<Response<ResponseBody>>() {
                    @Override
                    public void accept(Response<ResponseBody> resObj) throws Exception {
                        try {
                            LoadingBox.dismissLoadingDialog();
                            if (resObj.code() == 200) {
                                JSONObject jsonObject = new JSONObject(resObj.body().string());
                                if (jsonObject.getInt("status_code") == ApiResponseFlags.OK.getOrdinal()) {
                                    setIsDocumentSaveLiveData(true);
                                } else {
                                    RequestFailureErrorCodes.showErrorMessageFromResponse(mActivity, jsonObject);
                                }

                            } else {
                                RequestFailureErrorCodes.showErrorDialog(mActivity, mActivity.getString(R.string.something_went_wrong));

                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                            LoadingBox.dismissLoadingDialog();
                            RequestFailureErrorCodes.showErrorDialog(mActivity, mActivity.getString(R.string.something_went_wrong));

                        }
                    }
                }, new Consumer<Throwable>() {
                    @Override
                    public void accept(Throwable throwable) throws Exception {
                        LoadingBox.dismissLoadingDialog();
                        RequestFailureErrorCodes.checkCode((Activity) mContext, throwable);
                    }
                });

        compositeDisposable.add(disposable);
    }


    public MutableLiveData<InspectionDocument> getInspectionDocumentData() {
        return mInspectionDocument;
    }

    private void setInspectionDocumentLiveData(InspectionDocument inspectionDocument) {
        mInspectionDocument.setValue(inspectionDocument);
    }

    public MutableLiveData<Boolean> isInspectionDocumentSaveLiveData() {
        return mIsDocumentSave;
    }

    private void setIsDocumentSaveLiveData(boolean isDocumentSave) {
        mIsDocumentSave.setValue(isDocumentSave);
    }


    private void unSubscribeFromObservable() {
        if (compositeDisposable != null && !compositeDisposable.isDisposed()) {
            compositeDisposable.dispose();
        }
    }

    public void reset() {
        unSubscribeFromObservable();
        compositeDisposable = null;
        mContext = null;
    }
}
