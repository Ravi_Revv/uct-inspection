package com.uct.inspection.viewmodel;

import android.app.Activity;
import android.content.Context;

import androidx.annotation.NonNull;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.google.gson.Gson;
import com.uct.inspection.R;
import com.uct.inspection.model.document.InspectionDocument;
import com.uct.inspection.model.document.InspectionDocumentData;
import com.uct.inspection.model.refurb.RefurbDocument;
import com.uct.inspection.model.refurb.RefurbDocumentData;
import com.uct.inspection.model.refurb.RefurbList;
import com.uct.inspection.model.refurb.RefurbListData;
import com.uct.inspection.retrofit.ApiService;
import com.uct.inspection.retrofit.RestClient;
import com.uct.inspection.utils.CommonData;
import com.uct.inspection.utils.LoadingBox;
import com.uct.inspection.utils.RequestFailureErrorCodes;
import com.uct.inspection.utils.enums.ApiResponseFlags;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.List;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Consumer;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Response;

public class RefurbJobViewModel extends ViewModel {

    private Context mContext;
    private Activity mActivity;
    private CompositeDisposable compositeDisposable = new CompositeDisposable();
    private MutableLiveData<RefurbList> mRefurbList = new MutableLiveData<RefurbList>();
    private MutableLiveData<Boolean> mIsRefurbDocumentSave = new MutableLiveData<Boolean>();
    private MutableLiveData<Boolean> mIsRefurbStatusUpdate = new MutableLiveData<Boolean>();


    public RefurbJobViewModel(@NonNull Context context) {
        this.mContext = context;
        this.mActivity = (Activity) context;
    }

    public void getRefurbJobList(int inspectionId) {
        LoadingBox.showLoadingDialog(mActivity, "");
        ApiService apiService = RestClient.getApiService();
        HashMap<String, String> map = new HashMap<>();
        Disposable disposable = apiService.getRefurbJobList(CommonData.getAuthToken(mContext), inspectionId)
                .subscribeOn(RestClient.subscribeScheduler())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Consumer<Response<ResponseBody>>() {
                    @Override
                    public void accept(Response<ResponseBody> resObj) throws Exception {
                        try {
                            LoadingBox.dismissLoadingDialog();
                            if (resObj.code() == 200) {
//                                Log.d("LoginResponse", "Token :- " + resObj.headers().get("Token"));
                                JSONObject jsonObject = new JSONObject(resObj.body().string());
                                if (jsonObject.getInt("status_code") == ApiResponseFlags.OK.getOrdinal()) {
                                    setRefurbListLiveData(new Gson().fromJson(jsonObject.toString(), RefurbList.class));
                                } else {
                                    RequestFailureErrorCodes.showErrorMessageFromResponse(mActivity, jsonObject);
                                }

                            } else {
                                RequestFailureErrorCodes.showErrorDialog(mActivity, mActivity.getString(R.string.something_went_wrong));
                            }

                        } catch (Exception e) {
                            e.printStackTrace();
                            LoadingBox.dismissLoadingDialog();
                            RequestFailureErrorCodes.showErrorDialog(mActivity, mActivity.getString(R.string.something_went_wrong));

                        }
                    }
                }, new Consumer<Throwable>() {
                    @Override
                    public void accept(Throwable throwable) throws Exception {
                        LoadingBox.dismissLoadingDialog();
                        RequestFailureErrorCodes.checkCode((Activity) mContext, throwable);
                    }
                });

        compositeDisposable.add(disposable);
    }

    public void saveRefurbJobList(int inspectionId, List<RefurbDocument> refurbDocument) {
        LoadingBox.showLoadingDialog(mActivity, "");
        ApiService apiService = RestClient.getApiService();
        String jsonString = new Gson().toJson(new RefurbDocumentData(System.currentTimeMillis(), refurbDocument));

//        Log.e("jsonString", jsonString);
        RequestBody body = RequestBody.create(okhttp3.MediaType.parse("application/json; charset=utf-8"), jsonString);
        Disposable disposable = apiService.saveRefurbJobList(CommonData.getAuthToken(mContext), inspectionId, body)
                .subscribeOn(RestClient.subscribeScheduler())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Consumer<Response<ResponseBody>>() {
                    @Override
                    public void accept(Response<ResponseBody> resObj) throws Exception {
                        try {
                            LoadingBox.dismissLoadingDialog();
                            if (resObj.code() == 200) {
                                JSONObject jsonObject = new JSONObject(resObj.body().string());
                                if (jsonObject.getInt("status_code") == ApiResponseFlags.OK.getOrdinal()) {
                                    setIsRefurbDocumentSaveLiveData(true);
                                } else {
                                    RequestFailureErrorCodes.showErrorMessageFromResponse(mActivity, jsonObject);
                                }

                            } else {
                                RequestFailureErrorCodes.showErrorDialog(mActivity, mActivity.getString(R.string.something_went_wrong));

                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                            LoadingBox.dismissLoadingDialog();
                            RequestFailureErrorCodes.showErrorDialog(mActivity, mActivity.getString(R.string.something_went_wrong));

                        }
                    }
                }, new Consumer<Throwable>() {
                    @Override
                    public void accept(Throwable throwable) throws Exception {
                        LoadingBox.dismissLoadingDialog();
                        RequestFailureErrorCodes.checkCode((Activity) mContext, throwable);
                    }
                });

        compositeDisposable.add(disposable);
    }


    public void updateRefurbStatus(int inspectionId, String status) {
        LoadingBox.showLoadingDialog(mActivity, "");
        ApiService apiService = RestClient.getApiService();
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("status", status);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        String jsonString = new Gson().toJson(jsonObject);

//        Log.e("jsonString", jsonString);
        RequestBody body = RequestBody.create(okhttp3.MediaType.parse("application/json; charset=utf-8"), jsonObject.toString());
        Disposable disposable = apiService.updateRefurbStatus(CommonData.getAuthToken(mContext), inspectionId, body)
                .subscribeOn(RestClient.subscribeScheduler())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Consumer<Response<ResponseBody>>() {
                    @Override
                    public void accept(Response<ResponseBody> resObj) throws Exception {
                        try {
                            LoadingBox.dismissLoadingDialog();
                            if (resObj.code() == 200) {
                                JSONObject jsonObject = new JSONObject(resObj.body().string());
                                if (jsonObject.getInt("status_code") == ApiResponseFlags.OK.getOrdinal()) {
                                    setIsRefurbStatusUpdateLiveData(true);
                                } else {
                                    RequestFailureErrorCodes.showErrorMessageFromResponse(mActivity, jsonObject);
                                }

                            } else {
                                RequestFailureErrorCodes.showErrorDialog(mActivity, mActivity.getString(R.string.something_went_wrong));

                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                            LoadingBox.dismissLoadingDialog();
                            RequestFailureErrorCodes.showErrorDialog(mActivity, mActivity.getString(R.string.something_went_wrong));

                        }
                    }
                }, new Consumer<Throwable>() {
                    @Override
                    public void accept(Throwable throwable) throws Exception {
                        LoadingBox.dismissLoadingDialog();
                        RequestFailureErrorCodes.checkCode((Activity) mContext, throwable);
                    }
                });

        compositeDisposable.add(disposable);
    }


    public MutableLiveData<RefurbList> getRefurbListData() {
        return mRefurbList;
    }

    private void setRefurbListLiveData(RefurbList refurbList) {
        mRefurbList.setValue(refurbList);
    }

    public MutableLiveData<Boolean> isRefurbDocumentSaveLiveData() {
        return mIsRefurbDocumentSave;
    }

    private void setIsRefurbDocumentSaveLiveData(boolean isDocumentSave) {
        mIsRefurbDocumentSave.setValue(isDocumentSave);
    }

    public MutableLiveData<Boolean> isRefurbStatusUpdateLiveData() {
        return mIsRefurbStatusUpdate;
    }

    private void setIsRefurbStatusUpdateLiveData(boolean isRefurbStatusUpdate) {
        mIsRefurbStatusUpdate.setValue(isRefurbStatusUpdate);
    }


    private void unSubscribeFromObservable() {
        if (compositeDisposable != null && !compositeDisposable.isDisposed()) {
            compositeDisposable.dispose();
        }
    }

    public void reset() {
        unSubscribeFromObservable();
        compositeDisposable = null;
        mContext = null;
    }
}
