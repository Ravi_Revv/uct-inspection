package com.uct.inspection.interfaces;

import com.amazonaws.services.s3.AmazonS3Client;

public interface IAwsCallback {
    void onAwsLoginSuccess(AmazonS3Client amazonS3Client);

    void onAwsDirectoryCreationSuccess(AmazonS3Client amazonS3Client);

    void onAwsFileUploadSuccess(boolean isUploaded);
    void onAwsFileUploadSuccess(String uploadedFilePath);
}
