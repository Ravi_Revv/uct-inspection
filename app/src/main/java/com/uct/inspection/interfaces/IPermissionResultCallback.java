package com.uct.inspection.interfaces;

import java.util.ArrayList;


public interface IPermissionResultCallback {
    void onPermissionGranted(int request_code);
    void onPartialPermissionGranted(int request_code, ArrayList<String> granted_permissions);
    void onPermissionDenied(int request_code);
    void onNeverAskAgain(int request_code);
}
