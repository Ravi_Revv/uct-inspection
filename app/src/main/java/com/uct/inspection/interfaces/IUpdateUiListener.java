package com.uct.inspection.interfaces;

import androidx.lifecycle.LiveData;

import com.uct.inspection.model.checklist.InspectionChecklist;

public interface IUpdateUiListener {

    void onUpdateUi(LiveData<InspectionChecklist> inspectionChecklistLiveData);
}
