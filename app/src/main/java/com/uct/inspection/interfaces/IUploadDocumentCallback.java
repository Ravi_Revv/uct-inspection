package com.uct.inspection.interfaces;

public interface IUploadDocumentCallback {
    void onCameraClick(int position);
    void onImageGalleryClick(int position);
    void onCloseClick(int position);

}
