package com.uct.inspection.interfaces;

public interface IChecklistE4ClickCallback {

    void onChecklistAddImageClick(int position);

    void onChecklistDeleteImageClick(int mParentPosition, int position);

    void onChecklistImageClick(int mParentPosition, int position);

    void onChecklistCostChange(int mParentPosition);

    void onChecklistJobStatusChange(int mParentPosition);

    void onChecklistCommentChange(int mParentPosition);

}
