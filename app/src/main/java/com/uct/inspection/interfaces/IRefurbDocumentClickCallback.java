package com.uct.inspection.interfaces;

public interface IRefurbDocumentClickCallback {

    void onRefurbDocumentAddImageClick(int position);

    void onRefurbDocumentDeleteImageClick(int mParentPosition, int position);

    void onRefurbDocumentImageClick(int mParentPosition, int position);


}
