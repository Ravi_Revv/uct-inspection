package com.uct.inspection.interfaces;

public interface IChecklistClickCallback {
    void onChecklistIssueClick(int parentPosition);

    void onUnAssuredChecklistIssueClick();

    void onChecklistSubIssueClick(int parentPosition);

    void onChecklistAddImageClick(int position);

    void onChecklistDeleteImageClick(int mParentPosition, int position);

    void onChecklistImageClick(int mParentPosition, int position);

    void onChecklistCostChange(int mParentPosition);

    void onChecklistCommentChange(int mParentPosition);

}
