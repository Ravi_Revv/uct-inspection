package com.uct.inspection.preference;


public final class PreferenceConstant {
    private PreferenceConstant() {

    }

    public static final String APP_PREFERENCE = "uct_inspection_preference";
    public static final String APP_INSPECTOR_PREFERENCE = "uct_inspector_preference";

    /*-----------------------INSPECTOR DATA-----------------------------------*/

    public static final String INSPECTOR_DATA = "inspector_data";
    public static final String AUTH_TOKEN = "auth_token";

}
