package com.uct.inspection.preference

import android.content.Context
import android.content.SharedPreferences
import com.google.gson.Gson
import com.uct.inspection.model.inspectordetail.InspectorData


class PreferenceManager(mContext: Context) {
    private var mAppSharedPreferences: SharedPreferences? = null
    private var mUserSharedPreference: SharedPreferences? = null

    companion object PreferenceInstance {
        private var preferenceManagerInstance: PreferenceManager? = null;
        fun getInstance(mContext: Context): PreferenceManager {
            if (preferenceManagerInstance == null)
                preferenceManagerInstance = PreferenceManager(mContext)
            return preferenceManagerInstance as PreferenceManager
        }

    }

    init {
        mAppSharedPreferences =
            mContext.getSharedPreferences(PreferenceConstant.APP_PREFERENCE, Context.MODE_PRIVATE)
        mUserSharedPreference = mContext.getSharedPreferences(
            PreferenceConstant.APP_INSPECTOR_PREFERENCE,
            Context.MODE_PRIVATE
        )
    }

    /*------------------User Data------------------------------*/
    fun saveInspectorData(inspectorData: InspectorData) {
        val mEditor: SharedPreferences.Editor? = mUserSharedPreference?.edit()
        mEditor?.putString(PreferenceConstant.INSPECTOR_DATA, saveObject(inspectorData))
        mEditor?.apply()
    }

    fun getInspectorData(): InspectorData? {
        return getObject(
            mUserSharedPreference?.getString(PreferenceConstant.INSPECTOR_DATA, "")!!,
            InspectorData::class.java
        )
    }

    fun saveAuthToken(authToken: String) {
        val mEditor: SharedPreferences.Editor? = mUserSharedPreference?.edit()
        mEditor?.putString(PreferenceConstant.AUTH_TOKEN, authToken)
        mEditor?.apply()
    }

    fun getAuthToken(): String? {
        return mUserSharedPreference?.getString(PreferenceConstant.AUTH_TOKEN, "")
    }

    fun clearInspectorPreference() {
        mUserSharedPreference?.edit()?.clear()?.apply()
    }

    private fun saveObject(`object`: Any?): String {
        requireNotNull(`object`) { "object is null" }

        return Gson().toJson(`object`)
    }

    // To get object from prefrences

    fun <T> getObject(data: String, a: Class<T>): T? {
        return if (data == null) {
            null
        } else {
            try {
                Gson().fromJson(data, a)
            } catch (e: Exception) {
                throw IllegalArgumentException(
                    "Object storaged with key "
                            + data + " is instanceof other class"
                )
            }

        }
    }

}